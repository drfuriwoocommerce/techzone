<?php
/**
 * Header Layout 1
 */

$header_wrapper = [
	'header-main-wrapper'
];

$has_custom_header_mobile = teckzone_has_custom_header_mobile();
if ( intval(  $has_custom_header_mobile ) ) {
	$header_wrapper[] = 'header-mobile-enable';
}
?>

<div class="<?php echo esc_attr( implode( ' ', $header_wrapper ) ) ?>">
	<div class="container">
		<div class="header-top">
			<div class="header-menu-mobile tz-menu-mobile hidden-lg">
				<div class="menu-mobile-wrapper">
					<nav class="primary-menu-mobile">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary-menu',
								'container'      => false,
								'menu_class'     => 'menu',
							)
						);
						?>
					</nav>
				</div>
				<div class="teckzone-off-canvas-layer"></div>
				<div class="menu-icon menu-icon-js"><div class="teckzone-icon"><i class="icon-menu"></i></div></div>
			</div>
			<div class="header-logo">
				<?php get_template_part( 'template-parts/logo' ); ?>
			</div>
			<div class="header-search hidden-md hidden-sm hidden-xs">
				<?php teckzone_header_search(); ?>
			</div>
			<div class="header-elements">
				<div class="header-element header-element--search hidden-lg">
					<a href="#" class="open-header-search"><i class="icon-magnifier"></i></a>
					<div class="search-panel-content">
						<div class="top-content">
							<form method="get" class="form-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<div class="search-inner-content">
									<div class="search-wrapper">
										<span class="loading-icon"></span>
										<input type="text" name="s" class="search-field" autocomplete="off"
										       placeholder="<?php echo apply_filters( 'teckzone_header_search_placeholder', esc_html__( 'I\'m shopping for...','teckzone' ) ); ?>">
									</div>
								</div>
								<button class="search-submit" type="submit"><i class="icon-magnifier"></i></button>
							</form>
							<a href="#" class="close-search-panel"><?php echo apply_filters( 'teckzone_header_search_close_panel_text', esc_html__( 'Cancel','teckzone' ) ); ?></a>
						</div>
					</div>
					<div class="teckzone-off-canvas-layer"></div>
				</div>
				<?php teckzone_header_elements(); ?>
			</div>
		</div>
	</div>
	<div class="header-main hidden-md hidden-sm hidden-xs">
		<div class="container">
			<nav id="primary-menu" class="main-navigation primary-navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary-menu',
						'container'      => false,
						'menu_class'     => 'menu',
						'walker'         => new Teckzone_Mega_Menu_Walker(),
					)
				);
				?>
			</nav>
		</div>
	</div>
</div>
