<?php
/**
 * Header Account Template
 */
if ( ! class_exists( 'WooCommerce' ) ) {
	return;
}
$wishlist = '';
if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
	$wishlist = sprintf(
		'<li>
			<a href="%s">%s</a>
		</li>',
		esc_url( get_permalink( get_option( 'yith_wcwl_wishlist_page_id' ) ) ),
		esc_html__( 'Wishlist', 'teckzone' )
	);
}

$classes = [
	'header-element header-element--account',
	is_user_logged_in() ? 'logged' : 'login'
];

$login_id = $text = '';

if ( ! is_user_logged_in() ) {
	$login_id = 'tz-login';
	$text = sprintf(
		'<span class="login-text">
			<span class="login">%s</span>
			<span class="register">%s</span>
		</span>',
		esc_html__( 'Login', 'teckzone' ),
		esc_html__( 'Register', 'teckzone' )
	);
}

$icon_acc = '<span class="user-icon teckzone-icon"><i aria-hidden="true" class="icon-user"></i></span>';

$acc_html = sprintf(
	'<a id="%s" href="%s">%s%s</a>',
	esc_attr( $login_id ),
	esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ),
	$icon_acc,
	$text
);
?>
<div class="<?php echo esc_attr( implode( ' ', $classes ) ) ?>">
	<?php
	if ( is_user_logged_in() ) :
		$orders  = get_option( 'woocommerce_myaccount_orders_endpoint', 'orders' );
		$account = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );

		$user_id = get_current_user_id();
		$author  = get_user_by( 'id', $user_id );

		if ( $orders ) {
			$account .= $orders;
		}


        $user_menu = sprintf(
            '<ul>
                %s
                <li>
                    <a href="%s">%s</a>
                </li>
                <li>
                    <a href="%s">%s</a>
                </li>
            </ul>',
            $wishlist,
            esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ),
            esc_html__( 'Account Settings', 'teckzone' ),
            esc_url( $account ),
            esc_html__( 'Orders History', 'teckzone' )
        );

		echo sprintf(
			'%s
			<span class="dropdown"></span>
			<div class="dropdown-submenu">
				<div class="wrapper">
					<div class="preamble">%s, %s!</div>
					%s
					<a href="%s" class="logout">
						<i class="icon-enter-left"></i>
						%s
					</a>
				</div>
			</div>',
			$acc_html,
			esc_html__( 'Hi', 'teckzone' ),
			esc_html( $author->display_name ),
			$user_menu,
			esc_url( wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ) ),
			esc_html__( 'Logout', 'teckzone' )
		);
	else :
		echo sprintf(
			'%s',
			$acc_html
		);
	endif;
	?>
</div>
