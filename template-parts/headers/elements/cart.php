<?php
/**
 * Header Cart Template
 */
if ( ! function_exists( 'WC' ) ) {
	return;
}

if ( ! function_exists( 'woocommerce_mini_cart' ) ) {
	return '';
}

$icon_cart = '<span class="teckzone-icon"><i aria-hidden="true" class="icon-cart"></i></span>';

ob_start();
woocommerce_mini_cart();
$mini_cart    = ob_get_clean();
$mini_content = sprintf( 
	'<div class="mini-cart-content">
		<div class="mini-cart-content__wrapper">
			<div class="mini-cart-header hidden-lg">
				<span class="mini-cart-header__text">%s</span>
				<a href="#" class="close-cart-panel"><i class="icon-arrow-right"></i></a>
			</div>
			<div class="widget_shopping_cart_content">%s</div>
		</div>
	</div>',
	esc_html__( 'Shopping Cart', 'teckzone' ),
	$mini_cart 
);

$cart_count = WC()->cart->cart_contents_count;
$cart_price = WC()->cart->get_cart_subtotal();

?>
<div class="header-element header-element--cart">
	<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ) ?>">
		<span class="cart-content">
			<span class="cart-icon">
				<span class="mini-cart-counter teckzone-mini-cart-counter"><?php echo intval( $cart_count ) ?></span>
				<?php echo wp_kses_post( $icon_cart ) ?>
			</span>
			<span class="cart-info hidden-md hidden-sm hidden-xs">
				<span class="label"><?php esc_html_e( 'Shopping Cart', 'teckzone' ) ?></span>
				<span class="price"><?php echo wp_kses_post( $cart_price ) ?></span>
			</span>
		</span>
	</a>
	<span class="dropdown"></span>
	<?php echo wp_kses_post( $mini_content ) ?>
	<div class="teckzone-off-canvas-layer"></div>
</div>
