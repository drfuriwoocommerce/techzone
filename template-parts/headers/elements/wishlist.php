<?php
/**
 * Header Wishlish Template
 */

if ( ! function_exists( 'YITH_WCWL' ) ) {
	return;
}

$count = YITH_WCWL()->count_products();
$url = get_permalink( get_option( 'yith_wcwl_wishlist_page_id' ) );
?>
<div class="header-element header-element--wishlist">
	<a href="<?php echo esc_url( $url ) ?>">
		<span class="teckzone-icon"><i aria-hidden="true" class="icon-heart"></i></span>
		<span class="mini-item-counter"><?php echo intval( $count ) ?></span>
	</a>
</div>
