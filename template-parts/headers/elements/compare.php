<?php
/**
 * Header Compare Template
 */
if ( ! class_exists( 'YITH_Woocompare' ) ) {
	return;
}

global $yith_woocompare;

if ( is_admin() ) {
	$count = [];
} else {
	$count = $yith_woocompare->obj->products_list;
}

?>
<div class="header-element header-element--compare">
	<a class="yith-contents yith-woocompare-open" href="#">
		<span class="teckzone-icon"><i aria-hidden="true" class="icon-repeat"></i></span>
		<span class="mini-item-counter"><?php echo sizeof( $count ); ?></span>
	</a>
</div>
