<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Teckzone
 */

$layout = teckzone_get_option( 'single_post_layout' );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'single-post-wrapper' ); ?>>
	<header class="entry-header">
		<?php if ( $layout == 'full-content' ) : ?>
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-xs-12">
		<?php endif ?>

		<?php teckzone_breadcrumbs( array( 'display_last_item' => false ) ); ?>
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<?php teckzone_single_post_meta(); ?>

		<?php if ( $layout == 'full-content' ) : ?>
		</div><!-- .row -->
		</div><!-- .col -->
		<?php endif ?>

		<?php if ( $layout == 'full-content' ) :
			teckzone_single_post_entry_format();
		endif ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'teckzone' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php if ( $layout == 'full-content' ) : ?>
			<div class="row">
			<div class="col-md-10 col-md-offset-1 col-xs-12">
		<?php endif ?>

		<?php teckzone_entry_footer(); ?>

		<?php if ( $layout == 'full-content' ) : ?>
			</div><!-- .row -->
			</div><!-- .col -->
		<?php endif ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
