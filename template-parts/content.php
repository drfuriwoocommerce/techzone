<?php
/**
 * Template part for displaying posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Teckzone
 */
global $wp_query, $teckzone_post_carousel;
$current = $wp_query->current_post + 1;

$classes = 'blog-wrapper';

$layout      = teckzone_get_option( 'blog_layout' );
$view        = teckzone_get_option( 'blog_view' );
$post_format = get_post_format();

$size = 'teckzone-blog-default';

$read_more = '';

if ( $view == 'default' ) {
	if ( $current != 1 ) {
		$classes .= ' col-flex-md-6 col-flex-sm-6 col-flex-xs-6';
		$size = 'teckzone-blog-grid';

	} else {
		$classes .= ' col-flex-xs-12';

		$read_more = sprintf(
			'<a href="%s" class="read-more">%s<i class="icon-chevron-right"></i></a>',
			get_the_permalink(),
			apply_filters( 'teckzone_blog_read_more_text', esc_html__( 'Continue Reading', 'teckzone' ) )
		);
	}

} elseif ( $view == 'small-thumb' ) {
	$classes .= ' col-flex-xs-12';
	$size = 'teckzone-blog-small-thumb';

} elseif ( $view == 'grid' ) {
	$classes .= ' col-flex-md-4 col-flex-sm-6 col-flex-xs-6';
	$size = 'teckzone-blog-grid';

} elseif ( $view == 'list' ) {
	$classes .= ' col-flex-xs-12';
	$size = 'teckzone-blog-listing';
}

if ( isset($teckzone_post_carousel['image_size']) ) {
	$size = $teckzone_post_carousel['image_size'];
}

$excerpt_length = absint( teckzone_get_option( 'excerpt_length' ) );

$hidden = '';
$author = get_post_meta( get_the_ID(), 'quote_author', true );
$link   = get_post_meta( get_the_ID(), 'url', true );

if (
	( $post_format == 'quote' && $author ) ||
	( $post_format == 'link' && $link && ( $view == 'small-thumb' || $view == 'list' ) )
) {
	$hidden = 'hidden';
}

if ( ! has_post_thumbnail() ) {
	$classes .= ' no-thumb';
}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<div class="blog-inner">
		<?php teckzone_post_format( $view, $size ); ?>

		<div class="entry-summary <?php echo esc_attr( $hidden ); ?>">
			<div class="entry-content">
				<?php teckzone_blog_meta(); ?>
				<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' ); ?>
				<?php if ( $view != 'grid' ) : ?>
					<div class="entry-excerpt"><?php echo teckzone_content_limit( $excerpt_length, '' ); ?></div>
				<?php endif; ?>
			</div>

			<div class="entry-footer">
				<?php echo teckzone_meta_date(); ?>
				<?php echo wp_kses_post( $read_more ); ?>
			</div>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
