<?php
/**
 * Page Header Default Layout 1
 */

$css_class = array( 'page-header page-header-layout-1 text-center' );

$page_header = teckzone_get_page_header();

if ( ! in_array( 'title', $page_header ) ) {
	$css_class[] = 'hide-title';
}

$container = apply_filters( 'teckzone_page_header_container', teckzone_content_container_class() );

?>
<div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
	<div class="<?php echo esc_attr( $container ); ?>">
		<?php the_archive_title( '<h1>', '</h1>' ); ?>
		<?php teckzone_get_breadcrumbs(); ?>
	</div>
</div>