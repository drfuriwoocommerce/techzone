<?php
/**
 * Page Header Default Layout 2
 */
$css_class = array( 'page-header page-header-layout-2' );

$page_header = teckzone_get_page_header();

if ( ! in_array( 'title', $page_header ) || teckzone_is_catalog() ) {
	$css_class[] = 'hide-title';
}

$container = apply_filters( 'teckzone_page_header_container', teckzone_content_container_class() );

?>
<div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">

	<?php if ( in_array( 'breadcrumb', $page_header ) ) : ?>
		<div class="page-breadcrumbs">
			<div class="<?php echo esc_attr( $container ); ?>">
				<?php teckzone_get_breadcrumbs(); ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="page-title text-center">
		<div class="container">
			<?php the_archive_title( '<h1>', '</h1>' ); ?>
			<?php do_action( 'teckzone_page_header_after_title' ); ?>
		</div>
	</div>
</div>
