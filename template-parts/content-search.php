<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Teckzone
 */
$classes = 'blog-wrapper col-flex-xs-12';

$excerpt_length = absint( teckzone_get_option( 'excerpt_length' ) );

$view = 'small-thumb';
$size = 'teckzone-blog-small-thumb';

if ( ! has_post_thumbnail() ) {
	$classes .= ' no-thumb';
}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<div class="blog-inner">
		<?php teckzone_post_format( $view, $size ); ?>

		<div class="entry-summary">
			<div class="entry-content">
				<?php teckzone_blog_meta(); ?>
				<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' ); ?>
				<div class="entry-excerpt"><?php echo teckzone_content_limit( $excerpt_length, '' ); ?></div>
			</div>

			<div class="entry-footer">
				<?php echo teckzone_meta_date(); ?>
			</div>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
