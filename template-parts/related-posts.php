<?php
/**
 * The template part for displaying related posts
 *
 * @package Sober
 */

// Only support posts
if ( 'post' != get_post_type() ) {
	return;
}

if ( ! intval( teckzone_get_option( 'related_posts' ) ) ) {
	return;
}

global $post;

$numbers = intval( teckzone_get_option( 'related_posts_numbers' ) );

$related_posts = new WP_Query(
	array(
		'post_type'           => 'post',
		'posts_per_page'      => $numbers,
		'ignore_sticky_posts' => 1,
		'no_found_rows'       => 1,
		'order'               => 'rand',
		'post__not_in'        => array( $post->ID ),
		'tax_query'           => array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'category',
				'field'    => 'term_id',
				'terms'    => teckzone_get_related_terms( 'category', $post->ID ),
				'operator' => 'IN',
			),
			array(
				'taxonomy' => 'post_tag',
				'field'    => 'term_id',
				'terms'    => teckzone_get_related_terms( 'post_tag', $post->ID ),
				'operator' => 'IN',
			),
		),
	)
);


if ( ! $related_posts->have_posts() ) {
	return;
}

?>

	<div class="teckzone-related-posts teckzone-blog-grid">
		<div class="related-section-title">
			<?php
			if ( $titles = teckzone_get_option( 'related_posts_title' ) ) {

				echo sprintf( '<h2 class="related-title">%s</h2>', $titles );
			}
			?>
		</div>
		<div class="list-post row-flex">
			<?php
			while ( $related_posts->have_posts() ) : $related_posts->the_post();

				$size = 'teckzone-blog-grid';

				$classes = 'blog-wrapper col-flex-md-4 col-flex-sm-6 col-flex-xs-6';

				$post_format       = get_post_format();
				$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
				$thumb             = wp_get_attachment_image( $post_thumbnail_id, $size );
				$symbol            = $thumb_html = '';

				if ( ! has_post_thumbnail() ) {
					$classes .= ' no-thumb';
				}

				switch ( $post_format ) {
					case 'video':
						$symbol = get_template_directory_uri() . '/images/play-icon.png';
						$symbol = '<span class="post-format-icon"><img src="' . esc_url( $symbol ) . '" alt =""/></span>';
						break;
					case 'audio':
						$symbol = '<span class="post-format-icon"><i class="icon-music-note"></i></span>';
						break;
					case 'link':
						$symbol = '<span class="post-format-icon"><i class="icon-link2"></i></span>';
						break;
					case 'quote':
						$symbol = '<span class="post-format-icon"><i class="icon-quote-open"></i></span>';
						break;
					default:
						break;
				}

				?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
					<div class="blog-inner">
						<?php
						if ( $thumb ) {
							printf(
								'<div class="entry-format format-%s"><a class="entry-image" href="%s">%s%s</a></div>',
								esc_attr( $post_format ),
								esc_url( get_the_permalink() ),
								$symbol,
								$thumb
							);
						}
						?>

						<div class="entry-summary">
							<div class="entry-content">
								<?php teckzone_blog_meta(); ?>
								<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' ); ?>
							</div>

							<div class="entry-footer">
								<?php echo teckzone_meta_date(); ?>
							</div>
						</div>
					</div>
				</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile; ?>
		</div>
	</div>
<?php
wp_reset_postdata();