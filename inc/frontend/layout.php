<?php
/**
 * Custom layout functions  by hooking templates
 *
 * @package Teckzone
 */


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function teckzone_body_classes( $classes ) {
	$blog_view = teckzone_get_option( 'blog_view' );

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	$classes[] = teckzone_get_layout();

	if ( teckzone_is_blog() ) {
		$classes[] = 'teckzone-blog-page';
		$classes[] = 'teckzone-blog-' . $blog_view;
	}

	if ( teckzone_is_catalog() ) {
		$catalog_layout = teckzone_get_catalog_layout();
		$classes[]      = 'tz-catalog-layout-' . $catalog_layout;
	}

	if ( intval( teckzone_get_option( 'preloader' ) ) ) {
		$classes[] = 'tz-preloader';
	}

	if ( is_search() && ! teckzone_is_catalog() ) {
		$classes[] = 'teckzone-blog-small-thumb';
	}

	return $classes;
}

add_filter( 'body_class', 'teckzone_body_classes' );

/**
 * Print the open tags of site content container
 */

if ( ! function_exists( 'teckzone_open_site_content_container' ) ) :
	function teckzone_open_site_content_container() {

		printf( '<div class="%s"><div class="site-content-row row-flex">', esc_attr( apply_filters( 'teckzone_site_content_container_class', teckzone_content_container_class() ) ) );
	}
endif;

add_action( 'teckzone_after_site_content_open', 'teckzone_open_site_content_container', 10 );

/**
 * Print the close tags of site content container
 */

if ( ! function_exists( 'teckzone_close_site_content_container' ) ) :
	function teckzone_close_site_content_container() {
		print( '</div></div>' );
	}

endif;

add_action( 'teckzone_before_site_content_close', 'teckzone_close_site_content_container' );

