<?php
/**
 * Hooks for template footer
 *
 * @package Teckzone
 */


/**
 * Show footer
 */

if ( ! function_exists( 'teckzone_show_footer' ) ) :
	function teckzone_show_footer() {
		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			return;
		}
		$custom_layout      = get_post_meta( get_the_ID(), 'footer_layout', true );
		$elementor_instance = Elementor\Plugin::instance();

		if ( $custom_layout ) {
			echo ! empty( $custom_layout ) ? $elementor_instance->frontend->get_builder_content_for_display( $custom_layout ) : '';

		} else {
			$footer_layout = teckzone_get_option( 'footer_layout' );
			echo ! empty( $footer_layout ) ? $elementor_instance->frontend->get_builder_content_for_display( $footer_layout ) : '';
		}
	}

endif;

add_action( 'teckzone_footer', 'teckzone_show_footer' );

/**
 * Adds photoSwipe dialog element
 */
function teckzone_gallery_images_lightbox() {

	if ( ! is_singular() ) {
		return;
	}

	?>
    <div id="pswp" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="pswp__bg"></div>

        <div class="pswp__scroll-wrap">

            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">


                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close"
                            title="<?php esc_attr_e( 'Close (Esc)', 'teckzone' ) ?>"></button>

                    <button class="pswp__button pswp__button--share"
                            title="<?php esc_attr_e( 'Share', 'teckzone' ) ?>"></button>

                    <button class="pswp__button pswp__button--fs"
                            title="<?php esc_attr_e( 'Toggle fullscreen', 'teckzone' ) ?>"></button>

                    <button class="pswp__button pswp__button--zoom"
                            title="<?php esc_attr_e( 'Zoom in/out', 'teckzone' ) ?>"></button>

                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left"
                        title="<?php esc_attr_e( 'Previous (arrow left)', 'teckzone' ) ?>">
                </button>

                <button class="pswp__button pswp__button--arrow--right"
                        title="<?php esc_attr_e( 'Next (arrow right)', 'teckzone' ) ?>">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
	<?php
}

add_action( 'wp_footer', 'teckzone_gallery_images_lightbox' );

/**
 * Adds photoSwipe dialog element
 */
function teckzone_product_images_degree_lightbox() {
	if ( ! is_singular( 'product' ) ) {
		return;
	}

	$images_dg = get_post_meta( get_the_ID(), 'product_360_view', true );
	if ( empty( $images_dg ) ) {
		return;
	}

	?>
    <div id="product-degree-pswp" class="product-degree-pswp">
        <div class="degree-pswp-bg"></div>
        <div class="degree-pswp-close"><i class="icon-cross2"></i></div>
        <div class="tz-product-gallery-degree">

            <div class="tz-gallery-degree-spinner"></div>
            <ul class="product-degree-images"></ul>
        </div>
    </div>
	<?php
}

add_action( 'wp_footer', 'teckzone_product_images_degree_lightbox' );

/**
 * Adds quick view modal to footer
 */
if ( ! function_exists( 'teckzone_quick_view_modal' ) ) :
	function teckzone_quick_view_modal() {
		if ( is_404() ) {
			return;
		}
		?>

        <div id="tz-quick-view-modal" class="tz-quick-view-modal teckzone-modal woocommerce" tabindex="-1">
            <div class="tz-modal-overlay"></div>
            <div class="modal-content container">
                <a href="#" class="close-modal">
                    <i class="icon-cross"></i>
                </a>

                <div class="product-modal-content"></div>
            </div>
            <div class="tz-loading"></div>
        </div>

		<?php
	}

endif;

add_action( 'wp_footer', 'teckzone_quick_view_modal' );

/**
 * Add newsletter popup on the footer
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'teckzone_newsletter_popup' ) ) :
	function teckzone_newsletter_popup() {
		if ( ! teckzone_get_option( 'newsletter_popup' ) ) {
			return;
		}

		if ( ! intval( teckzone_get_option( 'newsletter_home_popup' ) ) ) {
			if ( teckzone_is_homepage() || is_front_page() ) {
				return;
			}
		}

		$tz_newletter = '';
		if ( isset( $_COOKIE['tz_newletter'] ) ) {
			$tz_newletter = $_COOKIE['tz_newletter'];
		}

		if ( ! empty( $tz_newletter ) ) {
			return;
		}

		$output = array();

		if ( $desc = teckzone_get_option( 'newsletter_content' ) ) {
			$output[] = sprintf( '<div class="n-desc">%s</div>', do_shortcode( wp_kses( $desc, wp_kses_allowed_html( 'post' ) ) ) );
		}

		if ( $form = teckzone_get_option( 'newsletter_form' ) ) {
			$output[] = sprintf( '<div class="n-form">%s</div>', do_shortcode( wp_kses( $form, wp_kses_allowed_html( 'post' ) ) ) );
		}

		$output[] = sprintf( '<a href="#" class="n-close">%s</a>', apply_filters( 'teckzone_newsletter_notices', esc_html__( 'Don\'t show this popup again', 'teckzone' ) ) );

		?>
        <div id="tz-newsletter-popup" class="teckzone-modal tz-newsletter-popup" tabindex="-1" aria-hidden="true">
            <div class="tz-modal-overlay"></div>
            <div class="modal-content">
                <a href="#" class="close-modal">
                    <i class="icon-cross"></i>
                </a>

                <div class="newletter-content">
					<?php $image = teckzone_get_option( 'newsletter_bg_image' );
					if ( $image ) {
						echo sprintf( '<div class="n-image" style="background-image:url(%s)"></div>', esc_url( $image ) );
					} ?>
                    <div class="nl-inner">
						<?php echo implode( '', $output ) ?>
                    </div>
                </div>
            </div>
        </div>
		<?php
	}
endif;

add_action( 'wp_footer', 'teckzone_newsletter_popup' );

/**
 * Add page loader
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'teckzone_loader' ) ) :
	function teckzone_loader() {
		if ( ! intval( teckzone_get_option( 'preloader' ) ) ) {
			return;
		}
		
		echo '<div id="teckzone-preloader" class="teckzone-preloader"><div class="teckzone-loading"></div></div>';
	}
endif;

add_action( 'wp_footer', 'teckzone_loader' );

/**
 * Woocommerce Catalog Ordering Popup
 */
function tz_mobile_catalog_sorting_popup() {
	if ( ! function_exists( 'woocommerce_catalog_ordering' ) ) {
		return;
	}

	if ( teckzone_is_vendor_page() ) {
		return;
	}

	$els = (array) teckzone_get_option( 'toolbar_elements' );

	if ( empty( $els ) ) {
		return;
	}

	if ( ! in_array( 'sort_by', $els ) ) {
		return;
	}

	echo '<div class="tz-catalog-sorting-mobile" id="tz-catalog-sorting-mobile">';
	woocommerce_catalog_ordering();
	echo '</div>';
}

add_action( 'wp_footer', 'tz_mobile_catalog_sorting_popup' );

/**
 * Canvas Off Layer
 */
function tz_off_canvas_layer() {
	echo '<div class="teckzone-off-canvas-layer teckzone-off-canvas-layer--footer"></div>';
}

add_action( 'wp_footer', 'tz_off_canvas_layer' );

/**
 * Adds popup add to cart to footer
 */
if ( ! function_exists( 'teckzone_popup_add_to_cart' ) ) :
	function teckzone_popup_add_to_cart() {
		if ( is_404() ) {
			return;
		}
		?>

        <div id="tz-popup-add-to-cart-modal" class="tz-popup-add-to-cart-modal teckzone-modal woocommerce" tabindex="-1">
            <div class="tz-modal-overlay"></div>
            <div class="modal-content container">
                <a href="#" class="close-modal">
                    <i class="icon-cross"></i>
                </a>

                <div class="product-modal-content"></div>
            </div>
            <div class="tz-loading"></div>
        </div>

		<?php
	}

endif;

add_action( 'wp_footer', 'teckzone_popup_add_to_cart' );
