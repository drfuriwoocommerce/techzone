<?php
/**
 * Add Post Element
 *
 * @package Teckzone
 */

/**
 * Get post format image
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_single_post_entry_format' ) ) :
	function teckzone_single_post_entry_format() {
		if ( ! is_singular( 'post' ) ) {
			return;
		}

		if ( ! intval( teckzone_get_option( 'show_post_format' ) ) ) {
			return;
		}

		$html = $css = '';
		switch ( get_post_format() ) {
			case 'gallery':
				$css  = 'gallery';
				$html = teckzone_post_format_gallery( 'full' );

				break;
			case 'audio':
				$css  = 'audio';
				$html = teckzone_post_format_audio();
				break;

			case 'video':
				$css  = 'video';
				$html = teckzone_post_format_video();
				break;

			case 'link':
				$css  = 'link';
				$html = teckzone_post_format_image( '', 'full' );
				break;

			default:
				$css  = 'image';
				$html = teckzone_post_format_image( '', 'full' );
				break;
		}

		if ( empty( $html ) ) {
			return;
		}

		echo apply_filters(
			'teckzone_single_post_entry_format_html',
			sprintf( '<div class="single-post-entry-format col-xs-12"><div class="entry-format format-%s">%s</div></div>', esc_attr( $css ), $html )
		);
	}
endif;

if ( teckzone_get_option( 'single_post_layout' ) != 'full-content' ) {
	add_action( 'teckzone_after_site_content_open', 'teckzone_single_post_entry_format', 50 );
}

/**
 * Get post format image
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_blog_cats_filter' ) ) :
	function teckzone_blog_cats_filter() {
		if ( ! teckzone_is_blog() ) {
			return;
		}

		if ( ! intval( teckzone_get_option( 'show_blog_cats' ) ) ) {
			return;
		}

		$custom_slug = intval( teckzone_get_option( 'custom_blog_cats' ) );
		$cats_slug   = (array) teckzone_get_option( 'blog_cats_slug' );

		$args = [
			'custom'        => $custom_slug,
			'custom_values' => $cats_slug,
		];

		$args = apply_filters( 'teckzone_blog_cats_filter_args', $args );

		teckzone_taxs_list( $args );
	}
endif;

add_action( 'teckzone_before_post_loop', 'teckzone_blog_cats_filter' );