<?php
/**
 * Hooks for template header
 *
 * @package Teckzone
 */

/**
 * Enqueue scripts and styles.
 */
function teckzone_scripts() {
	/**
	 * Register and enqueue styles
	 */
	wp_register_style( 'teckzone-fonts', teckzone_fonts_url(), array(), '20190510' );
	wp_register_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.3.7' );
	wp_register_style( 'linearicons', get_template_directory_uri() . '/css/linearicons.min.css', array(), '1.0.0' );
	wp_register_style( 'eleganticons', get_template_directory_uri() . '/css/eleganticons.min.css', array(), '1.0.0' );
	wp_register_style( 'ionicons', get_template_directory_uri() . '/css/ionicons.min.css', array(), '2.0.0' );
	wp_register_style( 'photoswipe', get_template_directory_uri() . '/css/photoswipe.min.css', array(), '4.7.0' );
	wp_register_style( 'magnific', get_template_directory_uri() . '/css/magnific-popup.css', array(), '2.0' );

	wp_enqueue_style(
		'teckzone', get_template_directory_uri() . '/style.css', array(
		'teckzone-fonts',
		'bootstrap',
		'linearicons',
		'eleganticons',
		'ionicons',
		'photoswipe',
		'magnific',
	), '20190510'
	);

	wp_add_inline_style( 'teckzone', teckzone_get_inline_style() );

	/**
	 * Register and enqueue scripts
	 */

	wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/js/plugins/html5shiv.min.js', array(), '3.7.2' );
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'respond', get_template_directory_uri() . '/js/plugins/respond.min.js', array(), '1.4.2' );
	wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

	wp_register_script( 'photoswipe', get_template_directory_uri() . '/js/plugins/photoswipe.min.js', array(), '4.1.1', true );
	wp_register_script( 'photoswipe-ui', get_template_directory_uri() . '/js/plugins/photoswipe-ui.min.js', array( 'photoswipe' ), '4.1.1', true );
	wp_register_script( 'slick', get_template_directory_uri() . '/js/plugins/slick.min.js', array(), '1.0', true );
	wp_register_script( 'fitvids', get_template_directory_uri() . '/js/plugins/jquery.fitvids.js', array(), '1.1.0', true );
	wp_register_script( 'countdown', get_template_directory_uri() . '/js/plugins/jquery.coundown.js', array(), '1.0.0', true );
	wp_register_script( 'counterup', get_template_directory_uri() . '/js/plugins/jquery.counterup.min.js', array(), '1.0.0', true );
	wp_register_script( 'waypoints', get_template_directory_uri() . '/js/plugins/waypoints.min.js', array(), '2.0.2', true );
	wp_register_script( 'isinviewport', get_template_directory_uri() . '/js/plugins/isInViewport.min.js', array(), '1.0.0', true );
	wp_register_script( 'magnific', get_template_directory_uri() . '/js/plugins/jquery.magnific-popup.js', array(), '1.0', true );
	wp_register_script( 'slimscroll', get_template_directory_uri() . '/js/plugins/jquery.slimscroll.js', array(), '1.0', true );
	wp_register_script( 'threesixty', get_template_directory_uri() . '/js/plugins/threesixty.min.js', array(), '2.0.5', true );
	wp_register_script( 'notify', get_template_directory_uri() . '/js/plugins/notify.min.js', array(), '1.0.0', true );
	wp_register_script( 'sticky-kit', get_template_directory_uri() . '/js/plugins/sticky-kit.min.js', array( 'jquery' ), '1.1.3', true );
	wp_register_script( 'sticky', get_template_directory_uri() . '/js/plugins/jquery.sticky.js', array( 'jquery' ), '1.0.4', true );

	wp_enqueue_style( 'photoswipe' );
	wp_enqueue_script( 'photoswipe-ui' );

	$photoswipe_skin = 'photoswipe-default-skin';
	if ( wp_style_is( $photoswipe_skin, 'registered' ) && ! wp_style_is( $photoswipe_skin, 'enqueued' ) ) {
		wp_enqueue_style( $photoswipe_skin );
	}

	wp_enqueue_script(
		'teckzone', get_template_directory_uri() . "/js/scripts.js", array(
		'jquery',
		'slick',
		'imagesloaded',
		'photoswipe',
		'fitvids',
		'countdown',
		'counterup',
		'waypoints',
		'isinviewport',
		'magnific',
		'slimscroll',
		'threesixty',
		'notify',
		'sticky-kit',
		'sticky',
	), '20190510', true
	);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
	 * Localize a script.
	 */

	$product_images_dg = '';
	if ( is_singular( 'product' ) ) {
		$images_dg = get_post_meta( get_the_ID(), 'product_360_view', false );
		if ( $images_dg ) {
			foreach ( $images_dg as $image ) {
				$image_dg = wp_get_attachment_image_src( $image, 'full' );
				$product_images_dg .= $product_images_dg ? ',' : '';
				$product_images_dg .= $image_dg ? $image_dg[0] : '';
			}
		}
	}

	$teckzone_data = array(
		'ajax_url'            	=> class_exists( 'WC_AJAX' ) ? WC_AJAX::get_endpoint( '%%endpoint%%' ) : '',
		'nonce'               	=> wp_create_nonce( '_teckzone_nonce' ),
		'isRTL'               	=> is_rtl(),
		'days'                	=> esc_html__( 'days', 'teckzone' ),
		'hours'               	=> esc_html__( 'hours', 'teckzone' ),
		'minutes'             	=> esc_html__( 'minutes', 'teckzone' ),
		'seconds'             	=> esc_html__( 'seconds', 'teckzone' ),
		'collapse_the_widget' 	=> array(
			'collapse' => intval( teckzone_get_option( 'collapse_the_widget' ) ),
			'status'   => teckzone_get_option( 'collapse_the_widget_status' ),
		),
		'blog'                => array(
			'nav_type' => teckzone_get_option( 'navigation_type' )
		),
		'nl_days'             	=> intval( teckzone_get_option( 'newsletter_reappear' ) ),
		'nl_seconds'          	=> intval( teckzone_get_option( 'newsletter_visible' ) ) == 2 ? intval( teckzone_get_option( 'newsletter_seconds' ) ) : 0,
		'product_degree'      	=> $product_images_dg,
		'product_gallery'     	=> intval( teckzone_get_option( 'product_images_lightbox' ) ),
		'add_to_cart_ajax'    	=> intval( teckzone_get_option( 'product_add_to_cart_ajax' ) ),
		'quantity_ajax'      	=> intval( teckzone_get_option( 'quantity_ajax' ) ),
	);

	if ( teckzone_is_catalog() ) {
		$teckzone_data['catalog_sticky_sidebar'] = array(
			'sticky_sidebar' 	=> intval( teckzone_get_option( 'catalog_sticky_sidebar' ) ),
			'offset' 			=> intval( teckzone_get_option( 'catalog_sticky_sidebar_offset' ) ),
		);
	}

	if ( is_singular( 'product' ) ) {
		$teckzone_data['currency_param'] = array(
			'currency_pos'    => get_option( 'woocommerce_currency_pos' ),
			'currency_symbol' => function_exists( 'get_woocommerce_currency_symbol' ) ? get_woocommerce_currency_symbol() : '',
			'thousand_sep'    => function_exists( 'wc_get_price_thousand_separator' ) ? wc_get_price_thousand_separator() : '',
			'decimal_sep'     => function_exists( 'wc_get_price_decimal_separator' ) ? wc_get_price_decimal_separator() : '',
			'price_decimals'  => function_exists( 'wc_get_price_decimals' ) ? wc_get_price_decimals() : '',
		);
	}

	if ( intval( teckzone_get_option( 'added_to_cart_notice' ) ) ) {
		$teckzone_data['added_to_cart_notice'] = array(
			'added_to_cart_text'    => esc_html__( 'has been added to your cart.', 'teckzone' ),
			'added_to_cart_texts'   => esc_html__( 'have been added to your cart.', 'teckzone' ),
			'cart_view_text'        => esc_html__( 'View Cart', 'teckzone' ),
			'cart_view_link'        => function_exists( 'wc_get_cart_url' ) ? esc_url( wc_get_cart_url() ) : '',
			'cart_notice_auto_hide' => intval( teckzone_get_option( 'cart_notice_auto_hide' ) ) > 0 ? intval( teckzone_get_option( 'cart_notice_auto_hide' ) ) * 1000 : 0,
		);
	}

	if ( intval( teckzone_get_option( 'added_to_wishlist_notice' ) ) && defined( 'YITH_WCWL' ) ) {
		$teckzone_data['added_to_wishlist_notice'] = array(
			'added_to_wishlist_text'    => esc_html__( 'has been added to your wishlist.', 'teckzone' ),
			'added_to_wishlist_texts'   => esc_html__( 'have been added to your wishlist.', 'teckzone' ),
			'wishlist_view_text'        => esc_html__( 'View Wishlist', 'teckzone' ),
			'wishlist_view_link'        => esc_url( get_permalink( get_option( 'yith_wcwl_wishlist_page_id' ) ) ),
			'wishlist_notice_auto_hide' => intval( teckzone_get_option( 'wishlist_notice_auto_hide' ) ) > 0 ? intval( teckzone_get_option( 'wishlist_notice_auto_hide' ) ) * 1000 : 0,
		);
	}

	if ( teckzone_is_mobile() && is_singular( 'product' ) ) {
		if ( intval( teckzone_get_option( 'product_collapse_tab' ) ) ) {
			$teckzone_data['product_collapse_tab'] = array(
				'status' => teckzone_get_option( 'product_collapse_tab_status' )
			);
		}
	}

	wp_localize_script(
		'teckzone', 'teckzoneData', $teckzone_data
	);
}

add_action( 'wp_enqueue_scripts', 'teckzone_scripts', 25 );

/**
 * Display the site header
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'teckzone_show_header' ) ) :
	function teckzone_show_header() {
		$custom_layout = get_post_meta( get_the_ID(), 'header_layout', true );

		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			$header_layout = 1;
			get_template_part( 'template-parts/headers/layout', $header_layout );

		} else {
			$elementor_instance = Elementor\Plugin::instance();

			if ( $custom_layout ) {
				echo ! empty( $custom_layout ) ? '<div class="header-wrapper-elementor">' . $elementor_instance->frontend->get_builder_content_for_display( $custom_layout ) . '</div>' : '';
			} else {
				$header_layout = teckzone_get_option( 'header_layout' );
				if ( $header_layout ) {
					echo ! empty( $header_layout ) ? '<div class="header-wrapper-elementor">' . $elementor_instance->frontend->get_builder_content_for_display( $header_layout ) . '</div>' : '';
				} else {
					$header_layout = 1;
					get_template_part( 'template-parts/headers/layout', $header_layout );
				}
			}
		}
	}
endif;
add_action( 'teckzone_header', 'teckzone_show_header' );

/**
 * Display the site header mobile
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'teckzone_show_header_mobile' ) ) :
	function teckzone_show_header_mobile() {
		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			return;
		}

		$elementor_instance = Elementor\Plugin::instance();
		$custom_layout      = get_post_meta( get_the_ID(), 'header_mobile', true );

		if ( ! $custom_layout || empty( $custom_layout ) ) {
			$custom_layout = teckzone_get_option( 'header_mobile_layout' );
		}

		if ( ! $custom_layout || empty( $custom_layout ) ) {
			return;
		}

		echo '<div class="header-mobile-wrapper-elementor">'. $elementor_instance->frontend->get_builder_content_for_display( $custom_layout ) .'</div>';
	}
endif;
add_action( 'teckzone_header', 'teckzone_show_header_mobile' );

/**
 * Show page header
 *
 * @since 1.0.0
 */
function teckzone_show_page_header() {
	if ( ! teckzone_get_page_header() ) {
		return;
	}

	$layout = teckzone_get_option( 'page_header_page_layout' );

	if ( teckzone_is_blog() ) {
		$layout = teckzone_get_option( 'page_header_blog_layout' );
	}

	if ( teckzone_is_catalog() || teckzone_is_vendor_page() ) {
		$layout = 2;
	}

	if ( is_page() ) {
		$layout = teckzone_get_option( 'page_header_page_layout' );

		if ( get_post_meta( get_the_ID(), 'page_header_layout', true ) != '') {
			$layout = get_post_meta( get_the_ID(), 'page_header_layout', true );
		}
	}

	get_template_part( 'template-parts/page-headers/layout', $layout );
}

add_action( 'teckzone_after_header', 'teckzone_show_page_header', 20 );