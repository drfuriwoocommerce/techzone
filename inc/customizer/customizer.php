<?php
/**
 * Theme customizer
 *
 * @package Teckzone
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Teckzone_Customize {
	/**
	 * Customize settings
	 *
	 * @var array
	 */
	protected $config = array();

	/**
	 * The class constructor
	 *
	 * @param array $config
	 */
	public function __construct( $config ) {
		$this->config = $config;

		if ( ! class_exists( 'Kirki' ) ) {
			return;
		}

		$this->register();
	}

	/**
	 * Register settings
	 */
	public function register() {

		/**
		 * Add the theme configuration
		 */
		if ( ! empty( $this->config['theme'] ) ) {
			Kirki::add_config(
				$this->config['theme'], array(
					'capability'  => 'edit_theme_options',
					'option_type' => 'theme_mod',
				)
			);
		}

		/**
		 * Add panels
		 */
		if ( ! empty( $this->config['panels'] ) ) {
			foreach ( $this->config['panels'] as $panel => $settings ) {
				Kirki::add_panel( $panel, $settings );
			}
		}

		/**
		 * Add sections
		 */
		if ( ! empty( $this->config['sections'] ) ) {
			foreach ( $this->config['sections'] as $section => $settings ) {
				Kirki::add_section( $section, $settings );
			}
		}

		/**
		 * Add fields
		 */
		if ( ! empty( $this->config['theme'] ) && ! empty( $this->config['fields'] ) ) {
			foreach ( $this->config['fields'] as $name => $settings ) {
				if ( ! isset( $settings['settings'] ) ) {
					$settings['settings'] = $name;
				}

				Kirki::add_field( $this->config['theme'], $settings );
			}
		}
	}

	/**
	 * Get config ID
	 *
	 * @return string
	 */
	public function get_theme() {
		return $this->config['theme'];
	}

	/**
	 * Get customize setting value
	 *
	 * @param string $name
	 *
	 * @return bool|string
	 */
	public function get_option( $name ) {

		$default = $this->get_option_default( $name );

		return get_theme_mod( $name, $default );
	}

	/**
	 * Get default option values
	 *
	 * @param $name
	 *
	 * @return mixed
	 */
	public function get_option_default( $name ) {
		if ( ! isset( $this->config['fields'][$name] ) ) {
			return false;
		}

		return isset( $this->config['fields'][$name]['default'] ) ? $this->config['fields'][$name]['default'] : false;
	}
}

/**
 * This is a short hand function for getting setting value from customizer
 *
 * @param string $name
 *
 * @return bool|string
 */
function teckzone_get_option( $name ) {
	global $teckzone_customize;

	$value = false;

	if ( class_exists( 'Kirki' ) ) {
		$value = Kirki::get_option( 'teckzone', $name );
	} elseif ( ! empty( $teckzone_customize ) ) {
		$value = $teckzone_customize->get_option( $name );
	}

	return apply_filters( 'teckzone_get_option', $value, $name );
}

/**
 * Get nav menus
 *
 * @return array
 */
function teckzone_customizer_get_categories( $taxonomies, $default = false ) {
	if ( ! taxonomy_exists( $taxonomies ) ) {
		return;
	}

	if ( ! is_admin() ) {
		return;
	}

	$output = array();

	if ( $default ) {
		$output[0] = esc_html__( 'Select Category', 'teckzone' );
	}

	global $wpdb;
	$post_meta_infos = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT a.term_id AS id, b.name as name, b.slug AS slug
						FROM {$wpdb->term_taxonomy} AS a
						INNER JOIN {$wpdb->terms} AS b ON b.term_id = a.term_id
						WHERE a.taxonomy = '%s'", $taxonomies
		), ARRAY_A
	);

	if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
		foreach ( $post_meta_infos as $value ) {
			$output[$value['slug']] = $value['name'];
		}
	}


	return $output;
}

/**
 * Get an array of posts.
 *
 * @static
 * @access public
 *
 * @param array $args Define arguments for the get_posts function.
 *
 * @return array
 */
function teckzone_customizer_get_posts( $args ) {

	if ( ! is_admin() ) {
		return;
	}

	if ( is_string( $args ) ) {
		$args = add_query_arg(
			array(
				'suppress_filters' => false,
			)
		);
	} elseif ( is_array( $args ) && ! isset( $args['suppress_filters'] ) ) {
		$args['suppress_filters'] = false;
	}

	$args['posts_per_page'] = - 1;

	$posts = get_posts( $args );

	// Properly format the array.
	$items    = array();
	$items[0] = esc_html__( 'Default', 'teckzone' );
	foreach ( $posts as $post ) {
		$items[$post->ID] = $post->post_title;
	}
	wp_reset_postdata();

	return $items;

}

/**
 * Get default option values
 *
 * @param $name
 *
 * @return mixed
 */
function teckzone_get_option_default( $name ) {
	global $teckzone_customize;

	if ( empty( $teckzone_customize ) ) {
		return false;
	}

	return $teckzone_customize->get_option_default( $name );
}

/**
 * Move some default sections to `general` panel that registered by theme
 *
 * @param object $wp_customize
 */
function teckzone_customize_modify( $wp_customize ) {
	$wp_customize->get_section( 'title_tagline' )->panel     = 'general';
	$wp_customize->get_section( 'static_front_page' )->panel = 'general';
}

add_action( 'customize_register', 'teckzone_customize_modify' );


/**
 * Get customize settings
 *
 * @return array
 */
function teckzone_customize_settings() {
	/**
	 * Customizer configuration
	 */

	$settings = array(
		'theme' => 'teckzone',
	);

	$panels = array(
		'general' => array(
			'priority' => 5,
			'title'    => esc_html__( 'General', 'teckzone' ),
		),
		// Styling
		'styling' => array(
			'title'    => esc_html__( 'Styling', 'teckzone' ),
			'priority' => 10,
		),
		// Header
		'header'  => array(
			'title'      => esc_html__( 'Header', 'teckzone' ),
			'priority'   => 10,
			'capability' => 'edit_theme_options',
		),
		// Page
		'pages'   => array(
			'title'      => esc_html__( 'Page', 'teckzone' ),
			'priority'   => 10,
			'capability' => 'edit_theme_options',
		),
		// Blog
		'blog'    => array(
			'title'      => esc_html__( 'Blog', 'teckzone' ),
			'priority'   => 10,
			'capability' => 'edit_theme_options',
		),
		// Footer
		'footer'  => array(
			'title'      => esc_html__( 'Footer', 'teckzone' ),
			'priority'   => 10,
			'capability' => 'edit_theme_options',
		),
	);

	$sections = array(
		// Maintenance
		'maintenance'      => array(
			'title'      => esc_html__( 'Maintenance', 'teckzone' ),
			'priority'   => 5,
			'capability' => 'edit_theme_options',
		),
		// Styling
		'styling_general'  => array(
			'title'       => esc_html__( 'General', 'teckzone' ),
			'description' => '',
			'priority'    => 210,
			'capability'  => 'edit_theme_options',
			'panel'       => 'styling',
		),
		'color_scheme'           => array(
			'title'       => esc_html__( 'Color Scheme', 'teckzone' ),
			'description' => '',
			'priority'    => 210,
			'capability'  => 'edit_theme_options',
			'panel'       => 'styling',
		),
		// Header
		'header_logo'      => array(
			'title'       => esc_html__( 'Header Logo', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'header',
		),
		'header_layout'    => array(
			'title'       => esc_html__( 'Header Layout', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'header',
		),
		'header_elements'  => array(
			'title'       => esc_html__( 'Header Elements', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'header',
		),
		// Pages
		'page_header_page' => array(
			'title'       => esc_html__( 'Page Header', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'pages',
		),
		'single_page'      => array(
			'title'       => esc_html__( 'Page Layout', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'pages',
		),

		// Blog Sections
		'page_header_blog' => array(
			'title'       => esc_html__( 'Blog Page Header', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'blog',
		),
		'blog_page'        => array(
			'title'       => esc_html__( 'Blog Page', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'blog',
		),
		'single_post'      => array(
			'title'       => esc_html__( 'Single Post', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'blog',
		),
		// Footer
		'footer_layout'    => array(
			'title'       => esc_html__( 'Footer Layout', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'footer',
		),
		'newsletter'       => array(
			'title'       => esc_html__( 'NewsLetter Popup', 'teckzone' ),
			'description' => '',
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'footer',
		),
	);

	$fields = array(
		// Maintenance
		'maintenance_enable'      => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Enable Maintenance Mode', 'teckzone' ),
			'description' => esc_html__( 'Put your site into maintenance mode', 'teckzone' ),
			'default'     => false,
			'section'     => 'maintenance',
		),
		'maintenance_mode'        => array(
			'type'        => 'radio',
			'label'       => esc_html__( 'Mode', 'teckzone' ),
			'description' => esc_html__( 'Select the correct mode for your site', 'teckzone' ),
			'tooltip'     => wp_kses_post( sprintf( esc_html__( 'If you are putting your site into maintenance mode for a longer perior of time, you should set this to "Coming Soon". Maintenance will return HTTP 503, Comming Soon will set HTTP to 200. <a href="%s" target="_blank">Learn more</a>', 'teckzone' ), 'https://yoast.com/http-503-site-maintenance-seo/' ) ),
			'default'     => 'maintenance',
			'section'     => 'maintenance',
			'choices'     => array(
				'maintenance' => esc_attr__( 'Maintenance', 'teckzone' ),
				'coming_soon' => esc_attr__( 'Coming Soon', 'teckzone' ),
			),
		),
		'maintenance_page'        => array(
			'type'    => 'dropdown-pages',
			'label'   => esc_html__( 'Maintenance Page', 'teckzone' ),
			'default' => 0,
			'section' => 'maintenance',
		),
		'maintenance_textcolor'   => array(
			'type'    => 'radio',
			'label'   => esc_html__( 'Text Color', 'teckzone' ),
			'default' => 'dark',
			'section' => 'maintenance',
			'choices' => array(
				'dark'  => esc_attr__( 'Dark', 'teckzone' ),
				'light' => esc_attr__( 'Light', 'teckzone' ),
			),
		),
		// Styling
		'preloader'               => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Preloader', 'teckzone' ),
			'default'     => 0,
			'section'     => 'styling_general',
			'priority'    => 10,
			'description' => esc_html__( 'Display a preloader when page is loading.', 'teckzone' ),
		),
		'color_scheme'                                     => array(
			'type'     => 'palette',
			'label'    => esc_html__( 'Base Color Scheme', 'teckzone' ),
			'default'  => '',
			'section'  => 'color_scheme',
			'priority' => 10,
			'choices'  => array(
				''        => array( '#226ce0' ),
				'#fec500' => array( '#fec500' ),
				'#fa9d25' => array( '#fa9d25' ),
			),
		),
		'custom_color_scheme'                              => array(
			'type'     => 'toggle',
			'label'    => esc_html__( 'Custom Color Scheme', 'teckzone' ),
			'default'  => 0,
			'section'  => 'color_scheme',
			'priority' => 10,
		),
		'custom_color'                                     => array(
			'type'            => 'color',
			'label'           => esc_html__( 'Color', 'teckzone' ),
			'default'         => '',
			'section'         => 'color_scheme',
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'custom_color_scheme',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		//Header
		'logo_type'               => array(
			'type'    => 'radio',
			'label'   => esc_html__( 'Logo Type', 'teckzone' ),
			'default' => 'image',
			'section' => 'header_logo',
			'choices' => array(
				'image' => esc_html__( 'Image', 'teckzone' ),
				'svg'   => esc_html__( 'SVG', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'header_layout',
					'operator' => '==',
					'value'    => 0,
				),
			),
		),
		'logo_svg'                => array(
			'type'            => 'textarea',
			'label'           => esc_html__( 'Logo SVG', 'teckzone' ),
			'section'         => 'header_logo',
			'description'     => esc_html__( 'Paste SVG code of your logo here', 'teckzone' ),
			'output'          => array(
				array(
					'element' => '.site-branding .logo',
				),
			),
			'active_callback' => array(
				array(
					'setting'  => 'header_layout',
					'operator' => '==',
					'value'    => 0,
				),
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'svg',
				),

			),
		),
		'logo'                    => array(
			'type'            => 'image',
			'label'           => esc_html__( 'Logo', 'teckzone' ),
			'default'         => '',
			'section'         => 'header_logo',
			'active_callback' => array(
				array(
					'setting'  => 'header_layout',
					'operator' => '==',
					'value'    => 0,
				),
				array(
					'setting'  => 'logo_type',
					'operator' => '==',
					'value'    => 'image',
				),
			),
		),
		'logo_dimension'          => array(
			'type'            => 'dimensions',
			'label'           => esc_html__( 'Logo Dimension', 'teckzone' ),
			'default'         => array(
				'width'  => '150',
				'height' => '',
			),
			'section'         => 'header_logo',
			'active_callback' => array(
				array(
					'setting'  => 'header_layout',
					'operator' => '==',
					'value'    => 0,
				),
				array(
					'setting'  => 'logo_type',
					'operator' => '!=',
					'value'    => 'text',
				),
			),
		),
		'header_layout'           => array(
			'type'     => 'select',
			'label'    => esc_html__( 'Header Layout', 'teckzone' ),
			'section'  => 'header_layout',
			'default'  => '',
			'priority' => 10,
			'choices'  => teckzone_customizer_get_posts( array( 'post_type' => 'tz_header' ) ),
		),
		'header_layout_custom_field_1'     => array(
			'type'    => 'custom',
			'section' => 'header_layout',
			'default' => '<hr/>',
		),
		'header_mobile_layout'           => array(
			'type'     => 'select',
			'label'    => esc_html__( 'Header Mobile Layout', 'teckzone' ),
			'section'  => 'header_layout',
			'default'  => '',
			'priority' => 10,
			'choices'  => teckzone_customizer_get_posts( array( 'post_type' => 'tz_header' ) ),
		),
		'header_elements'         => array(
			'type'     => 'sortable',
			'label'    => esc_html__( 'Elements', 'teckzone' ),
			'section'  => 'header_elements',
			'default'  => array( 'compare', 'wishlist', 'cart' ),
			'priority' => 10,
			'choices'  => array(
				'compare'  => esc_html__( 'Compare', 'teckzone' ),
				'wishlist' => esc_html__( 'Wishlist', 'teckzone' ),
				'cart'     => esc_html__( 'Cart', 'teckzone' ),
				'account'  => esc_html__( 'Account', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'header_layout',
					'operator' => '==',
					'value'    => 0,
				),
			),
		),
		// Page
		'page_header_page'        => array(
			'type'        => 'toggle',
			'default'     => 1,
			'label'       => esc_html__( 'Enable Page Header', 'teckzone' ),
			'section'     => 'page_header_page',
			'description' => esc_html__( 'Enable to show a page header for the page below the site header', 'teckzone' ),
			'priority'    => 10,
		),
		'page_header_page_layout' => array(
			'type'            => 'select',
			'label'           => esc_html__( 'Page Header Layout', 'teckzone' ),
			'section'         => 'page_header_page',
			'default'         => '1',
			'priority'        => 10,
			'description'     => esc_html__( 'Select default layout for page header.', 'teckzone' ),
			'choices'         => array(
				'1' => esc_html__( 'Layout 1', 'teckzone' ),
				'2' => esc_html__( 'Layout 2', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'page_header_page',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		'page_header_pages_els'   => array(
			'type'            => 'multicheck',
			'label'           => esc_html__( 'Page Header Elements', 'teckzone' ),
			'section'         => 'page_header_page',
			'default'         => array( 'breadcrumb', 'title' ),
			'priority'        => 10,
			'choices'         => array(
				'breadcrumb' => esc_html__( 'BreadCrumb', 'teckzone' ),
				'title'      => esc_html__( 'Title', 'teckzone' ),
			),
			'description'     => esc_html__( 'Select which elements you want to show.', 'teckzone' ),
			'active_callback' => array(
				array(
					'setting'  => 'page_header_page',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		'page_layout'             => array(
			'type'        => 'select',
			'label'       => esc_html__( 'Page Layout', 'teckzone' ),
			'default'     => 'full-content',
			'section'     => 'single_page',
			'priority'    => 10,
			'description' => esc_html__( 'Select default layout for page.', 'teckzone' ),
			'choices'     => array(
				'full-content'    => esc_html__( 'Full Content', 'teckzone' ),
				'sidebar-content' => esc_html__( 'Sidebar Content', 'teckzone' ),
				'content-sidebar' => esc_html__( 'Content Sidebar', 'teckzone' ),
			),
		),

		// Blog Page Header
		'page_header_blog'        => array(
			'type'        => 'toggle',
			'default'     => 1,
			'label'       => esc_html__( 'Enable Page Header', 'teckzone' ),
			'section'     => 'page_header_blog',
			'description' => esc_html__( 'Enable to show a page header for the blog page below the site header', 'teckzone' ),
			'priority'    => 10,
		),
		'page_header_blog_layout' => array(
			'type'            => 'select',
			'label'           => esc_html__( 'Blog Layout', 'teckzone' ),
			'section'         => 'page_header_blog',
			'default'         => '1',
			'priority'        => 10,
			'description'     => esc_html__( 'Select default layout for page header.', 'teckzone' ),
			'choices'         => array(
				'1' => esc_html__( 'Layout 1', 'teckzone' ),
				'2' => esc_html__( 'Layout 2', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'page_header_blog',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		'page_header_blog_els'    => array(
			'type'            => 'multicheck',
			'label'           => esc_html__( 'Page Header Elements', 'teckzone' ),
			'section'         => 'page_header_blog',
			'default'         => array( 'breadcrumb', 'title' ),
			'priority'        => 10,
			'choices'         => array(
				'breadcrumb' => esc_html__( 'BreadCrumb', 'teckzone' ),
				'title'      => esc_html__( 'Title', 'teckzone' ),
			),
			'description'     => esc_html__( 'Select which elements you want to show.', 'teckzone' ),
			'active_callback' => array(
				array(
					'setting'  => 'page_header_blog',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),

		// Blog
		'blog_view'               => array(
			'type'     => 'select',
			'label'    => esc_html__( 'Blog View', 'teckzone' ),
			'section'  => 'blog_page',
			'default'  => 'list',
			'priority' => 10,
			'choices'  => array(
				'default'     => esc_html__( 'Default', 'teckzone' ),
				'small-thumb' => esc_html__( 'Small Thumbnail', 'teckzone' ),
				'grid'        => esc_html__( 'Grid', 'teckzone' ),
				'list'        => esc_html__( 'Listing', 'teckzone' ),
			),
		),
		'blog_layout'             => array(
			'type'            => 'select',
			'label'           => esc_html__( 'Blog Layout', 'teckzone' ),
			'section'         => 'blog_page',
			'default'         => 'content-sidebar',
			'priority'        => 10,
			'description'     => esc_html__( 'Select default sidebar for the blog page.', 'teckzone' ),
			'choices'         => array(
				'content-sidebar' => esc_html__( 'Right Sidebar', 'teckzone' ),
				'sidebar-content' => esc_html__( 'Left Sidebar', 'teckzone' ),
				'full-content'    => esc_html__( 'Full Content', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'blog_view',
					'operator' => 'in',
					'value'    => array( 'default', 'small-thumb', 'list' ),
				),
			),
		),
		'blog_entry_meta'         => array(
			'type'     => 'multicheck',
			'label'    => esc_html__( 'Entry Meta', 'teckzone' ),
			'section'  => 'blog_page',
			'default'  => array( 'cat', 'comment' ),
			'choices'  => array(
				'cat'     => esc_html__( 'Cat', 'teckzone' ),
				'comment' => esc_html__( 'Comment', 'teckzone' ),
			),
			'priority' => 10,
		),
		'excerpt_length'          => array(
			'type'     => 'number',
			'label'    => esc_html__( 'Excerpt Length', 'teckzone' ),
			'section'  => 'blog_page',
			'default'  => 17,
			'priority' => 10,
		),
		'blog_custom_field_1'     => array(
			'type'    => 'custom',
			'section' => 'blog_page',
			'default' => '<hr/>',
		),
		'show_blog_cats'          => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Show Categories List', 'teckzone' ),
			'section'     => 'blog_page',
			'default'     => '',
			'description' => esc_html__( 'Display categories list above posts list on blog, archive page', 'teckzone' ),
			'priority'    => 10,
		),
		'custom_blog_cats'        => array(
			'type'     => 'toggle',
			'label'    => esc_html__( 'Custom Categories List', 'teckzone' ),
			'section'  => 'blog_page',
			'default'  => 0,
			'priority' => 10,
			'active_callback' => array(
				array(
					'setting'  => 'show_blog_cats',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		'blog_cats_slug'          => array(
			'type'            => 'select',
			'section'         => 'blog_page',
			'label'           => esc_html__( 'Custom Categories', 'teckzone' ),
			'description'     => esc_html__( 'Select product categories you want to show.', 'teckzone' ),
			'default'         => '',
			'multiple'        => 999,
			'priority'        => 10,
			'choices'         => teckzone_customizer_get_categories( 'category' ),
			'active_callback' => array(
				array(
					'setting'  => 'custom_blog_cats',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		'blog_custom_field_2'     => array(
			'type'    => 'custom',
			'section' => 'blog_page',
			'default' => '<hr/>',
		),
		'navigation_type'         => array(
			'type'     => 'select',
			'label'    => esc_html__( 'Navigation Type', 'teckzone' ),
			'section'  => 'blog_page',
			'default'  => 'numeric',
			'choices'  => array(
				'numeric'  => esc_html__( 'Numeric', 'teckzone' ),
				'infinite' => esc_html__( 'Infinite Scroll', 'teckzone' ),
			),
			'priority' => 10,
		),
		// Single Post
		'single_post_layout'      => array(
			'type'        => 'select',
			'label'       => esc_html__( 'Single Post Layout', 'teckzone' ),
			'section'     => 'single_post',
			'default'     => 'content-sidebar',
			'priority'    => 10,
			'description' => esc_html__( 'Select default sidebar for the single post page.', 'teckzone' ),
			'choices'     => array(
				'content-sidebar' => esc_html__( 'Right Sidebar', 'teckzone' ),
				'sidebar-content' => esc_html__( 'Left Sidebar', 'teckzone' ),
				'full-content'    => esc_html__( 'Full Content', 'teckzone' ),
			),
		),
		'post_entry_meta'         => array(
			'type'     => 'multicheck',
			'label'    => esc_html__( 'Entry Meta', 'teckzone' ),
			'section'  => 'single_post',
			'default'  => array( 'cat', 'comment', 'date' ),
			'choices'  => array(
				'cat'     => esc_html__( 'Cat', 'teckzone' ),
				'comment' => esc_html__( 'Comment', 'teckzone' ),
				'date'    => esc_html__( 'Date', 'teckzone' ),
			),
			'priority' => 10,
		),
		'show_post_format'        => array(
			'type'     => 'toggle',
			'label'    => esc_html__( 'Show Post Format', 'teckzone' ),
			'section'  => 'single_post',
			'default'  => 1,
			'priority' => 10,
		),
		'show_author_box'         => array(
			'type'     => 'toggle',
			'label'    => esc_html__( 'Show Author Box', 'teckzone' ),
			'section'  => 'single_post',
			'default'  => 0,
			'priority' => 10,
		),
		'show_post_navigation'    => array(
			'type'     => 'toggle',
			'label'    => esc_html__( 'Show Post Navigation', 'teckzone' ),
			'section'  => 'single_post',
			'default'  => 0,
			'priority' => 10,
		),
		'post_custom_field_1'     => array(
			'type'    => 'custom',
			'section' => 'single_post',
			'default' => '<hr/>',
		),

		'show_post_social_share'  => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Show Socials Share', 'teckzone' ),
			'description' => esc_html__( 'Check this option to show socials share in the single post page.', 'teckzone' ),
			'section'     => 'single_post',
			'default'     => 0,
			'priority'    => 10,
		),

		'post_socials_share'      => array(
			'type'            => 'multicheck',
			'label'           => esc_html__( 'Socials Share', 'teckzone' ),
			'section'         => 'single_post',
			'default'         => array( 'facebook', 'twitter', 'google', 'tumblr' ),
			'choices'         => array(
				'facebook'  => esc_html__( 'Facebook', 'teckzone' ),
				'twitter'   => esc_html__( 'Twitter', 'teckzone' ),
				'google'    => esc_html__( 'Google Plus', 'teckzone' ),
				'tumblr'    => esc_html__( 'Tumblr', 'teckzone' ),
				'pinterest' => esc_html__( 'Pinterest', 'teckzone' ),
				'linkedin'  => esc_html__( 'Linkedin', 'teckzone' ),
			),
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'show_post_social_share',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),

		'post_custom_field_2'     => array(
			'type'    => 'custom',
			'section' => 'single_post',
			'default' => '<hr/>',
		),

		'related_posts'           => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Related Posts', 'teckzone' ),
			'description' => esc_html__( 'Check this option to show related posts in the single post page.', 'teckzone' ),
			'section'     => 'single_post',
			'default'     => 0,
			'priority'    => 10,
		),
		'related_posts_numbers'   => array(
			'type'            => 'number',
			'label'           => esc_html__( 'Number of Related Posts', 'teckzone' ),
			'description'     => esc_html__( 'How many related post would you like to show', 'teckzone' ),
			'section'         => 'single_post',
			'default'         => 3,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'related_post',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		'related_posts_title'     => array(
			'type'            => 'text',
			'label'           => esc_html__( 'Related Posts Title', 'teckzone' ),
			'section'         => 'single_post',
			'default'         => esc_html__( 'Related Posts', 'teckzone' ),
			'priority'        => 20,
			'active_callback' => array(
				array(
					'setting'  => 'related_post',
					'operator' => '==',
					'value'    => 1,
				),
			),
		),
		// Footer
		'footer_layout'           => array(
			'type'     => 'select',
			'label'    => esc_html__( 'Footer Layout', 'teckzone' ),
			'section'  => 'footer_layout',
			'default'  => '',
			'priority' => 10,
			'choices'  => teckzone_customizer_get_posts( array( 'post_type' => 'tz_footer' ) ),
		),
		// NewsLetter
		'newsletter_popup'        => array(
			'type'        => 'toggle',
			'label'       => esc_html__( 'Enable NewsLetter Popup', 'teckzone' ),
			'default'     => 0,
			'section'     => 'newsletter',
			'priority'    => 10,
			'description' => esc_html__( 'Check this option to show newsletter popup.', 'teckzone' ),
		),
		'newsletter_home_popup'   => array(
			'type'        => 'toggle',
			'label'       => esc_html__( "Show in Homepage", 'teckzone' ),
			'default'     => 1,
			'section'     => 'newsletter',
			'priority'    => 10,
			'description' => esc_html__( 'Check this option to enable newsletter popup in the homepage.', 'teckzone' ),
		),
		'newsletter_bg_image'     => array(
			'type'     => 'image',
			'label'    => esc_html__( 'Background Image', 'teckzone' ),
			'default'  => '',
			'section'  => 'newsletter',
			'priority' => 20,
		),
		'newsletter_content'      => array(
			'type'     => 'textarea',
			'label'    => esc_html__( 'Content', 'teckzone' ),
			'default'  => '',
			'section'  => 'newsletter',
			'priority' => 20,
		),
		'newsletter_form'         => array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'NewsLetter Form', 'teckzone' ),
			'default'     => '',
			'description' => sprintf( wp_kses_post( 'Enter the shortcode of MailChimp form . You can edit your sign - up form in the <a href= "%s" > MailChimp for WordPress form settings </a>.', 'teckzone' ), admin_url( 'admin.php?page=mailchimp-for-wp-forms' ) ),
			'section'     => 'newsletter',
			'priority'    => 20,
		),
		'newsletter_reappear'     => array(
			'type'        => 'number',
			'label'       => esc_html__( 'Reappear', 'teckzone' ),
			'default'     => '1',
			'section'     => 'newsletter',
			'priority'    => 20,
			'description' => esc_html__( 'Reappear after how many day(s) using Cookie', 'teckzone' ),
		),
		'newsletter_visible'      => array(
			'type'     => 'select',
			'label'    => esc_html__( 'Visible', 'teckzone' ),
			'default'  => '1',
			'section'  => 'newsletter',
			'priority' => 20,
			'choices'  => array(
				'1' => esc_html__( 'After page loaded', 'teckzone' ),
				'2' => esc_html__( 'After how many seconds', 'teckzone' ),
			),
		),
		'newsletter_seconds'      => array(
			'type'            => 'number',
			'label'           => esc_html__( 'Seconds', 'teckzone' ),
			'default'         => '10',
			'section'         => 'newsletter',
			'priority'        => 20,
			'active_callback' => array(
				array(
					'setting'  => 'newsletter_visible',
					'operator' => '==',
					'value'    => '2',
				),
			),
		),
	);

	$settings['panels']   = apply_filters( 'teckzone_customize_panels', $panels );
	$settings['sections'] = apply_filters( 'teckzone_customize_sections', $sections );
	$settings['fields']   = apply_filters( 'teckzone_customize_fields', $fields );

	return $settings;
}

$teckzone_customize = new Teckzone_Customize( teckzone_customize_settings() );