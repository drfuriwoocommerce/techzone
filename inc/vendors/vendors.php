<?php
/**
 * Vendors Compatibility File
 *
 * @package Teckzone
 */

/**
 * Vendors setup function.
 *
 * @return void
 */
function teckzone_vendors_setup() {
	global $teckzone_dokan;
	$teckzone_dokan = new Teckzone_Dokan;

	global $teckzone_wcfmvendors;
	$teckzone_wcfmvendors = new Teckzone_WCFMVendors;
}

add_action( 'after_setup_theme', 'teckzone_vendors_setup' );

require get_theme_file_path( '/inc/vendors/theme-options.php' );
require get_theme_file_path( '/inc/vendors/dokan.php' );
require get_theme_file_path( '/inc/vendors/wcfm_vendors.php' );