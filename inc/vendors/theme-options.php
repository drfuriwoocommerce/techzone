<?php
/**
 * Theme options of Vendors.
 *
 * @package Teckzone
 */


/**
 * Adds theme options panels of WooCommerce.
 *
 * @param array $panels Theme options panels.
 *
 * @return array
 */
function teckzone_vendors_customize_panels( $panels ) {
	$panels['vendors'] = array(
		'priority' => 200,
		'title'    => esc_html__( 'Vendors', 'teckzone' ),
	);

	return $panels;
}

add_filter( 'teckzone_customize_panels', 'teckzone_vendors_customize_panels' );

/**
 * Adds theme options sections of Teckzone.
 *
 * @param array $sections Theme options sections.
 *
 * @return array
 */
function teckzone_vendors_customize_sections( $sections ) {
	$sections = array_merge(
		$sections, array(
			'vendor_catalog_page' => array(
				'title'       => esc_html__( 'Catalog Page', 'teckzone' ),
				'description' => '',
				'priority'    => 45,
				'panel'       => 'vendors',
				'capability'  => 'edit_theme_options',
			),

			'vendor_single_product' => array(
				'title'       => esc_html__( 'Single Product', 'teckzone' ),
				'description' => '',
				'priority'    => 45,
				'panel'       => 'vendors',
				'capability'  => 'edit_theme_options',
			),

			'vendor_page' => array(
				'title'       => esc_html__( 'Vendor Page', 'teckzone' ),
				'description' => '',
				'priority'    => 45,
				'panel'       => 'vendors',
				'capability'  => 'edit_theme_options',
			),
		)
	);

	return $sections;
}

add_filter( 'teckzone_customize_sections', 'teckzone_vendors_customize_sections' );

/**
 * Adds theme options of vendors.
 *
 * @param array $settings Theme options.
 *
 * @return array
 */
function teckzone_vendors_customize_settings( $fields ) {

	if ( ! class_exists( 'WeDevs_Dokan' ) && ! class_exists( 'WCFMmp' )) {
		return $fields;
	}

	$fields = array_merge(
		$fields, array(
			// Catalog Vendor
			'catalog_sold_by' => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Vendor Sold By', 'teckzone' ),
				'section'     => 'vendor_catalog_page',
				'default'     => 1,
				'priority'    => 10,
				'description' => esc_html__( 'Check this option to show sold by vendor.', 'teckzone' ),
			),

			// Single Vendor
			'single_product_sold_by' => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Vendor Sold By', 'teckzone' ),
				'section'     => 'vendor_single_product',
				'default'     => 1,
				'priority'    => 10,
				'description' => esc_html__( 'Check this option to show sold by vendor.', 'teckzone' ),
			),

			// Vendor Page
			'catalog_vendor_container_width'                 => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Container Width', 'teckzone' ),
				'default'  => 'normal',
				'section'  => 'vendor_page',
				'priority' => 10,
				'choices'  => array(
					'normal'     => esc_html__( 'Normal', 'teckzone' ),
					'full-width'     => esc_html__( 'Full Width', 'teckzone' ),
					'wide' => esc_html__( 'Wide', 'teckzone' ),
				),
			),
			'catalog_vendor_layout_container_width'      => array(
				'type'     => 'custom',
				'section'  => 'vendor_page',
				'default'  => '<hr>',
				'priority' => 10,
			),
			'page_header_vendor_els' => array(
				'type'     => 'multicheck',
				'label'    => esc_html__( 'Page Header Element', 'teckzone' ),
				'section'  => 'vendor_page',
				'default'  => array( 'breadcrumb' ),
				'priority' => 10,
				'choices'  => array(
					'breadcrumb' => esc_html__( 'Breadcrumb', 'teckzone' ),
				),
			),
			'catalog_vendor_toolbar_els' => array(
				'type'        => 'multicheck',
				'label'       => esc_html__( 'ToolBar Elements', 'teckzone' ),
				'section'     => 'vendor_page',
				'default'     => array( 'total_product', 'view' ),
				'priority'    => 10,
				'choices'     => array(
					'total_product' => esc_html__( 'Products Found', 'teckzone' ),
					'view'  => esc_html__( 'View', 'teckzone' ),
				),
				'description' => esc_html__( 'Select which elements you want to show.', 'teckzone' ),
			),
			'catalog_vendor_view'        => array(
				'type'     => 'select',
				'label'    => esc_html__( 'View', 'teckzone' ),
				'section'  => 'vendor_page',
				'default'  => 'grid',
				'priority' => 10,
				'choices'  => array(
					'grid' => esc_html__( 'Grid', 'teckzone' ),
					'list' => esc_html__( 'List', 'teckzone' ),
				),
			),
			'catalog_vendor_columns'    => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Product Columns', 'teckzone' ),
				'section'     => 'vendor_page',
				'default'     => '4',
				'priority'    => 10,
				'choices'     => array(
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'3' => esc_html__( '3 Columns', 'teckzone' ),
				),
				'description' => esc_html__( 'Specify how many product columns you want to show.', 'teckzone' ),
			),
			'catalog_vendor_sidebar_title'         => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Sidebar Title', 'teckzone' ),
				'default'         => esc_html__( 'Filter Products', 'teckzone' ),
				'section'         => 'vendor_page',
				'priority'        => 10,
			),
		)
	);

	if ( class_exists( 'WCFMmp' ) ) {
		$fields = array_merge(
			$fields, array(
				'wcfm_vendor_store_header'        => array(
					'type'     => 'select',
					'label'    => esc_html__( 'Store Header Template', 'teckzone' ),
					'section'  => 'vendor_page',
					'default'  => 'themes',
					'priority' => 10,
					'choices'  => array(
						'plugin' => esc_html__( 'Plugin', 'teckzone' ),
						'themes' => esc_html__( 'Themes', 'teckzone' ),
					),
				),
			)
		);
	}

	return $fields;
}

add_filter( 'teckzone_customize_fields', 'teckzone_vendors_customize_settings', 30 );