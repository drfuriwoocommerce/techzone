<?php
/**
 * Functions of stylesheets and CSS
 *
 * @package Helendo
 */

if ( ! function_exists( 'teckzone_get_inline_style' ) ) :
	/**
	 * Get inline style data
	 */
	function teckzone_get_inline_style() {
		$css = '';

		$get_id = get_the_ID();
		if ( function_exists( 'is_shop' ) && is_shop() ) {
			$get_id = intval( get_option( 'woocommerce_shop_page_id' ) );
		} elseif ( teckzone_is_blog() ) {
			$get_id = intval( get_option( 'page_for_posts' ) );
		}

		/* Page Header */
		$page_header_custom_spacing = get_post_meta( $get_id, 'page_header_custom_spacing', true );
		$page_header_top_padding    = get_post_meta( $get_id, 'page_header_top_padding', true );
		$page_header_bottom_padding = get_post_meta( $get_id, 'page_header_bottom_padding', true );

		if ( $page_header_custom_spacing == 'custom' ) {
			$css .= '.page-header.page-header-layout-1 { padding-top: ' . intval( $page_header_top_padding ) . 'px; }';
			$css .= '.page-header-layout-2 .page-title { padding-top: ' . intval( $page_header_top_padding ) . 'px; }';
			$css .= '.page-header { padding-bottom: ' . intval( $page_header_bottom_padding ) . 'px; }';
		}

		/* Content Spacing */
		$page_content_top_spacing    = get_post_meta( $get_id, 'page_content_top_spacing', true );
		$page_content_bottom_spacing = get_post_meta( $get_id, 'page_content_bottom_spacing', true );

		if ( $page_content_top_spacing == 'custom' ) {
			$page_content_top_padding = get_post_meta( $get_id, 'page_content_top_padding', true );
			$css                      .= '.site-content { padding-top: ' . intval( $page_content_top_padding ) . 'px !important; }';
		} elseif ( $page_content_top_spacing == 'no-spacing' ) {
			$css .= '.site-content { padding-top: 0 !important; }';
		}

		if ( $page_content_bottom_spacing == 'custom' ) {
			$page_content_bottom_padding = get_post_meta( $get_id, 'page_content_bottom_padding', true );
			$css                         .= '.site-content { padding-bottom: ' . intval( $page_content_bottom_padding ) . 'px !important; }';
		} elseif ( $page_content_bottom_spacing == 'no-spacing' ) {
			$css .= '.site-content { padding-bottom: 0 !important; }';
		}

		/* Color Scheme */
		$color_scheme_option = teckzone_get_option( 'color_scheme' );

		if ( intval( teckzone_get_option( 'custom_color_scheme' ) ) ) {
			$color_scheme_option = teckzone_get_option( 'custom_color' );
		}

		// Don't do anything if the default color scheme is selected.
		if ( $color_scheme_option ) {
			$css .= teckzone_get_color_scheme_css( $color_scheme_option );
		}

		/* Typography */
		$css .= teckzone_typography_css();

		return apply_filters( 'teckzone_inline_style', $css );
	}
endif;

/**
 * Returns CSS for the color schemes.
 *
 *
 * @param array $colors Color scheme colors.
 *
 * @return string Color scheme CSS.
 */
function teckzone_get_color_scheme_css( $colors ) {
	return <<<CSS

	/* Background Color */

	.slick-dots li button:hover:before,
	.slick-dots li.slick-active button:before,
	.btn-primary,
	.btn-primary:hover, 
	.btn-primary:focus, 
	.btn-primary:active,
	.tz-search-form .search-submit,
	.tz-search-form--mobile .top-content,
	.teckzone-cart .woocommerce-mini-cart__buttons a.checkout,
	.teckzone-cart-mobile .mini-cart-header,
	.teckzone-menu-department .department-menu ul.teckzone-department-menu > li:hover,
	.tz-menu-mobile--elementor .top-content,
	.teckzone-timeline ul.timeline__date a.active:before,
	.teckzone-tab-list .box-nav.active .teckzone-icon,
	.teckzone-tab-list .number:after,
	.teckzone-tab-list .box-nav.active .field-icon,
	.teckzone-slide-button,
	.teckzone-slide-button:hover,
	.teckzone-banner-medium .btn-button,
	.teckzone-banner .banner-layer-wrapper .banner-layer--button a.link,
	.teckzone-banner .banner-layer-wrapper .banner-layer--button span.link,
	.tz-products-grid ul.product-filter li.active,
	.tz-product-deals-grid ul.product-filter li.active,
	.tz-product-tab-carousel-2 .tabs-header .tabs-nav a.active,
	button,input[type="button"],input[type="reset"],input[type="submit"],
	button.alt,input[type="button"].alt,input[type="reset"].alt,input[type="submit"].alt,
	button.alt:hover,input[type="button"].alt:hover,input[type="reset"].alt:hover,input[type="submit"].alt:hover,
	button:hover, button:focus,input[type="button"]:hover,input[type="button"]:focus,input[type="reset"]:hover,input[type="reset"]:focus,input[type="submit"]:hover,input[type="submit"]:focus,
	.wpcf7-form input[type="radio"]:checked:before,
	.main-navigation li.tz-menu-item__magic-line,
	.site-header .header-main,
	.site-header .header-element--cart .mini-cart-header,
	.header-element--cart .woocommerce-mini-cart__buttons a.checkout,
	.teckzone-cart-mobile .woocommerce-mini-cart__buttons a.checkout,
	.teckzone-cart-mobile .woocommerce-mini-cart__buttons a:hover,
	.header-element--search .top-content,
	.teckzone-taxs-list ul li a:hover, .teckzone-taxs-list ul li a.selected,
	.primary-sidebar.catalog-sidebar .catalog-filter-mobile-header,
	.woocommerce table.wishlist_table td.product-add-to-cart a,
	.woocommerce .wishlist_table.mobile li .additional-info-wrapper .product-add-to-cart a,
	ul.products li.product .product-button .button,
	.woocommerce div.product .tawc-deal .deal-progress .progress-value,
	.sticky-product-info-wrapper .sc-product-info .sc-tabs li a:after,
	.sticky-product-info-wrapper .sc-product-cart .button,
	.woocommerce-account .tabs-nav li a.active span:after,
	.woocommerce-cart .cart_totals a.button,
	.woocommerce-tabs ul.tabs .tz-wc-tab__magic-line,
	.wcfm-template-themes .woocommerce-tabs .wcfmmp_product_mulvendor_container .wcfmmp_product_mulvendor_rowbody .button,
	.wcfm-template-themes .woocommerce-tabs .wcfmmp_product_mulvendor_container .wcfmmp_product_mulvendor_rowbody .button:hover, 
	.wcfm-template-themes .woocommerce-tabs .wcfmmp_product_mulvendor_container .wcfmmp_product_mulvendor_rowbody .button:focus, 
	.wcfm-template-themes .woocommerce-tabs .wcfmmp_product_mulvendor_container .wcfmmp_product_mulvendor_rowbody .button:active,
	.tz-catalog-filter-mobile .catalog-filter-mobile-header,
	.dokan-widget-area .seller-form .dokan-btn,
	.dokan-widget-area .seller-form .dokan-btn:hover, 
	.dokan-widget-area .seller-form .dokan-btn:focus, 
	.dokan-widget-area .seller-form .dokan-btn:active,
	.dokan-widget-area .seller-form .dokan-btn:hover,
	.dokan-widget-area .dokan-store-contact .dokan-btn,
	.dokan-widget-area .dokan-store-contact .dokan-btn:hover, 
	.dokan-widget-area .dokan-store-contact .dokan-btn:focus, 
	.dokan-widget-area .dokan-store-contact .dokan-btn:active,
	.dokan-widget-area .dokan-store-contact .dokan-btn:hover,
	input[type="submit"].dokan-btn-theme,a.dokan-btn-theme,.dokan-btn-theme,
	input[type="submit"].dokan-btn-theme:hover, 
	input[type="submit"].dokan-btn-theme:focus, 
	input[type="submit"].dokan-btn-theme:active,
	a.dokan-btn-theme:hover,a.dokan-btn-theme:focus,
	a.dokan-btn-theme:active,
	.dokan-btn-theme:hover,
	.dokan-btn-theme:focus,
	.dokan-btn-theme:active,
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry,
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store,
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry:hover, 
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry:focus, 
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry:active,
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store:hover,
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store:focus,
	.wcfm-template-themes #wcfmmp-store #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store:active,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry:hover,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry:focus,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfm_catalog_enquiry:active,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store:hover,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store:focus,
	.wcfm-template-themes #wcfmmp-stores-lists #wcfmmp-stores-wrap ul.wcfmmp-store-wrap li .wcfmmp-visit-store:active{background-color: $colors}
	
	/* Border Color */

	.teckzone-cart .woocommerce-mini-cart__buttons a.checkout,
	.teckzone-cart-mobile .woocommerce-mini-cart__buttons a.checkout,
	.teckzone-cart-mobile .woocommerce-mini-cart__buttons a:hover,
	.teckzone-menu-department .department-menu ul.teckzone-department-menu > li:hover > a,
	.teckzone-image-box .image-content ul li a:hover,
	.teckzone-popular-tags .inner-content a:hover,
	.tz-products-grid ul.product-filter li.active,
	.tz-product-deals-grid ul.product-filter li.active,
	.tz-product-tab-carousel-2 .tabs-header .tabs-nav a.active,
	.wpcf7-form input[type="checkbox"]:checked,.wpcf7-form input[type="radio"]:checked,
	.widget .tagcloud a:hover,
	.post-tags a:hover,
	.teckzone-taxs-list ul li a:hover, .teckzone-taxs-list ul li a.selected,
	.tz-product-fbt ul.products-list li a:before,
	.tz-attr-swatches .tz-swatch-item:hover,
	.woocommerce div.product .woocommerce-product-gallery .product-degree-images,
	.woocommerce div.product .woocommerce-product-gallery .flex-control-nav li:hover img,
	.woocommerce div.product .woocommerce-product-gallery .flex-control-nav img.flex-active,
	.woocommerce div.product.product-type-variable .tawcvs-swatches .swatch.selected,
	.tz-product-fbt ul.products-list li:before,
	.woocommerce-tabs #review_form #respond .comment-form-cookies-consent input:checked + label:before,
	.woocommerce .woocommerce-form__label-for-checkbox input:checked + span:before,
	.woocommerce ul.payment_methods > li > input:checked + label:before,.woocommerce ul.woocommerce-shipping-methods > li > input:checked + label:before,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen a:before,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen.show-swatch .swatch-color,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen.show-swatch .swatch-image,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen.show-swatch .swatch-label,
	.catalog-sidebar .widget_rating_filter ul .wc-layered-nav-rating.chosen a:before,
	.catalog-sidebar .widget_rating_filter ul .wc-layered-nav-rating.chosen.show-swatch .swatch-label,
	.mobile-version.woocommerce div.product .woocommerce-product-gallery .flex-control-nav img.flex-active{border-color: $colors}

	/* Color */
	blockquote:before,
	.tz-search-form .hot-words li a:hover,
	.tz-search-form .search-results ul li .title-item:hover,
	.teckzone-login-regis .header-account--content a:hover,
	.teckzone-currencies .currency-content ul li a:hover,
	.teckzone-currencies .currency-content ul li.actived a,
	.teckzone-content-recently-viewed .recently-button-products .header-link:hover span,
	.tz-product-recently-viewed-carousel .recently-button-products .footer-link:hover span,
	.tz-product-recently-viewed-carousel--other .recently-button-products .header-link:hover span,
	.tz-quick-links ul a:hover span,
	.teckzone-counter .teckzone-icon,
	.teckzone-image-grid .teckzone-image-grid__header .header-link:hover span,
	.teckzone-member h4.name a:hover,
	.teckzone-timeline ul.timeline__date a.active,
	.teckzone-timeline .timeline__event-title,
	.teckzone-faqs ul.tabs-nav a.active, .teckzone-faqs ul.tabs-nav a:hover,
	.teckzone-testimonials-2 .teckzone-icon,
	.teckzone-tab-list .number,
	.teckzone-tab-list .box-nav.active .nav-title,
	.teckzone-icon-list .teckzone-icon,
	.teckzone-icon-list-2 .teckzone-icon,
	.teckzone-banner-small .header-link:hover span,
	.teckzone-trending-search-carousel ul.collection-list a:hover .collection-item__name,
	.teckzone-trending-search-carousel-2 ul.collection-list a:hover .collection-item__name,
	.teckzone-trending-search-carousel-3 ul.collection-list .collection-item__info .name:hover,
	.teckzone-image-box .box-title a:hover,
	.teckzone-image-box .image-content ul li a:hover,
	.teckzone-popular-tags .inner-content a:hover,
	.teckzone-products-carousel-with-category .cat-header .header-link:hover span,
	.teckzone-products-carousel-with-category .categories-box .product-cats a:hover .cat-name,
	.teckzone-products-carousel-with-category .quick-link-box ul a:hover,
	.teckzone-products-carousel-with-banner .cat-header .header-links ul a:hover,
	.teckzone-products-carousel-with-banner .quick-link-box ul a:hover,
	.teckzone-products-carousel-with-banner-2 .cat-header .header-link:hover span,
	.tz-products-grid .product-grid__header .header-link:hover span,
	.teckzone-product-categories .cat-header .cats-link:hover span,
	.teckzone-product-categories .group-item a:hover .cat-name,
	.teckzone-product-categories-2 .group-item a:hover .cat-name,
	.teckzone-product-categories-2 .cat-footer .cats-link:hover span,
	.teckzone-product-categories-3 .cat-header .header-link:hover span,
	.teckzone-product-categories-3 .cat-item__list-cats ul a:hover,
	.teckzone-product-categories-4 .cat-header .cats-link:hover span,
	.teckzone-product-categories-4 .group-item a:hover .cat-name,
	.teckzone-product-categories-carousel .cat-header .header-link:hover span,
	.teckzone-product-categories-carousel ul.product-cats li.cat-item a:hover .cat-name,
	.tz-product-deals-day .cat-header .header-link:hover span,
	.tz-product-deals-grid .cat-header .header-link:hover span,
	.tz-product-with-category .header-cat .extra-links a.extra-link:hover,
	.tz-product-with-category .header-cat .extra-links .header-link:hover span,
	.tz-product-with-category-2 .header-cat .header-link:hover span,
	.tz-product-with-category-2 .categories-box ul.extra-links a:hover,
	.tz-product-tab-carousel .tabs-header .tabs-nav a.active, 
	.tz-product-tab-carousel .tabs-header .tabs-nav a:hover,
	.tz-product-tab-carousel-3 .tabs-header .tabs-nav a.active,
	.tz-products-of-brands .brand-item__header .brand-info a:hover,
	.teckzone-list-links .name:hover,
	.wpcf7-form input[type="checkbox"]:checked:before,
	.wpcf7-form input[type="radio"]:checked:before,
	.main-navigation li:hover > a, 
	.main-navigation li.focus > a,
	.main-navigation .current_page_item > a,
	.main-navigation .current-menu-item > a,
	.main-navigation .current_page_ancestor > a,
	.main-navigation .current-menu-ancestor > a,
	.tz-menu-mobile ul.menu > li:not(.tz-menu-item__magic-line).active > a,
	.widget ul li a:hover,
	.widget .tagcloud a:hover,
	.teckzone-recent-posts-widget .recent-post .entry-title a:hover,
	.widget_recent_comments ul li a:hover,
	.widget_recent_comments ul li .comment-author-link a:hover,
	.widget-area ul li.current-cat > a,
	.widget-area ul li.current-cat > .count, 
	.widget-area ul li.chosen > a,
	.widget-area ul li.chosen > .count,
	.widget-area ul li .children li.current-cat > a,
	.tz_widget_product_categories ul li.current-cat a,
	.widget_product_categories ul li.current-cat a,
	.tz_widget_product_categories ul li .children li a:hover,
	.widget_product_categories ul li .children li a:hover,
	.tz_widget_product_categories ul li .children li.current-cat a,
	.widget_product_categories ul li .children li.current-cat a,
	.header-element--account .dropdown-submenu ul a:hover,
	.tag-sticky-2 .entry-title:before,
	.entry-meta .meta a:hover,
	.entry-meta .meta-cat a:hover,
	.post-tags a:hover,
	.blog-wrapper .entry-title a:hover,
	.blog-wrapper .entry-footer .meta .meta-author,
	.blog-wrapper .entry-footer .meta a:hover,
	.comment-respond .logged-in-as a:hover,
	.error404 .page-content a,
	ul.site-breadcrumb a,
	.catalog-toolbar .shop-view .shop-view__icon a.current,
	.tz-attr-swatches .tz-swatch-item:hover,
	ul.products li.product .tz-cat,
	ul.products li.product .woocommerce-loop-category__title a:hover,
	ul.products li.product .woocommerce-loop-product__title a:hover,
	.tz-catalog-sorting-mobile .woocommerce-ordering ul li a.active,
	.tz-catalog-categories .cat-item .cat-item__wrapper:hover .box-title,
	.tz-catalog-categories-2 .cat-item:hover .box-title,
	.woocommerce div.product .tz-summary-meta .sold-by-meta a,
	.woocommerce div.product .tawc-deal .deal-expire-text i,
	.woocommerce div.product .product_meta .posted_in a,
	.woocommerce div.product.product-type-variable form.cart .variations .reset_variations,
	.woocommerce div.product.product-type-variable .tawcvs-swatches .swatch.selected,
	.tz-product-fbt ul.products-list li a:after,
	.tz-product-gallery-degree .nav_bar a:hover,
	.woocommerce-checkout .form-login-section .woocommerce-info a,
	.woocommerce-checkout .coupon-section .woocommerce-info a,
	.woocommerce-checkout form.checkout .place-order .woocommerce-terms-and-conditions-link,
	.woocommerce-cart table.shop_table tbody .actions button.empty-cart-button,
	.woocommerce-tabs table.shop_attributes td a,
	.woocommerce-tabs #review_form #respond .comment-form-cookies-consent label:after,
	.wcfm-template-themes .woocommerce-tabs .wcfmmp_product_mulvendor_container .wcfmmp_product_mulvendor_rowbody .vendor_name a:hover,
	.yith-wcwl-add-to-wishlist .yith-wcwl-add-button > a,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse > a,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse > a,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse > a,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse > a,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse > a i,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse > a i,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse > a:hover,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse > a:hover,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse > a:hover i,
	.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse > a:hover i,
	.woocommerce .product-feature-items i,
	.woocommerce .woocommerce-form__label-for-checkbox span:not(.required):after,
	.woocommerce ul.payment_methods > li > label:after,.woocommerce ul.woocommerce-shipping-methods > li > label:after,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen a,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen a:after,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen .count,
	.catalog-sidebar .woocommerce-widget-layered-nav .woocommerce-widget-layered-nav-list .woocommerce-widget-layered-nav-list__item.chosen.show-swatch .swatch-label,
	.catalog-sidebar .widget_rating_filter ul .wc-layered-nav-rating.chosen a:after,
	.catalog-sidebar .widget_rating_filter ul .wc-layered-nav-rating.chosen.show-swatch .swatch-label,
	.catalog-sidebar .widget_layered_nav_filters ul li.clear a,
	.tz_widget_product_info_extended .tz-summary-meta .sold-by-meta a,
	.dokan-widget-area .dokan-category-menu #cat-drop-stack > ul li.parent-cat-wrap a:hover,
	.dokan-widget-area .dokan-store-menu #cat-drop-stack > ul li.parent-cat-wrap a:hover,
	.dokan-widget-area .widget-collapse #cat-drop-stack > ul li.parent-cat-wrap a:hover,
	.dokan-store-sidebar ul li.current-cat > a,
	.dokan-store-sidebar ul li.current-cat > .count, 
	.dokan-store-sidebar ul li.chosen > a,.dokan-store-sidebar ul li.chosen > .count,
	.dokan-store-sidebar ul li .children li.current-cat > a,
	.wcfm-template-themes #wcfmmp-store .widget-area.sidebar .categories_list ul li.parent_cat a,
	.wcfm-template-themes #wcfmmp-stores-lists .widget-area.sidebar .categories_list ul li.parent_cat a,
	.wcfm-template-themes #wcfmmp-store .widget-area.sidebar .categories_list ul li .children li a:hover,
	.wcfm-template-themes #wcfmmp-stores-lists .widget-area.sidebar .categories_list ul li .children li a:hover,
	.wcfm-template-themes #wcfmmp-store .widget-area.sidebar .categories_list ul li .children li.current-cat a,
	.wcfm-template-themes #wcfmmp-stores-lists .widget-area.sidebar .categories_list ul li .children li.current-cat a{color: $colors}

	.main-color {color: $colors !important;}

	/* Other */
	.teckzone-counter .teckzone-icon svg{fill:$colors}

	.teckzone-cart .blockUI.blockOverlay:after{border-color: $colors $colors $colors transparent;}

	.widget_price_filter .ui-slider .ui-slider-handle,
	.widget_price_filter .ui-slider .ui-slider-range,
	.tz-product-deals-day ul.products li.product .tawc-deal .progress-value,
	.tz-product-deals-grid ul.products li.product .tawc-deal .progress-value{background:$colors}

	.tz-elementor-ajax-wrapper .teckzone-loading:after,
	.tz-products-tabs .teckzone-loading:after,
	.teckzone-footer-recently-viewed .teckzone-loading:after,
	.teckzone-content-recently-viewed .teckzone-loading:after,
	.teckzone-header-recently-viewed .teckzone-loading:after,
	.teckzone-slides-wrapper .teckzone-loading:after,
	.tz-products-grid .product-grid__content .products-wrapper .tz-loader .teckzone-loading:after,
	.tz-products-grid a.ajax-load-products .teckzone-loading:after,
	.tz-product-deals-grid .products-wrapper .tz-loader .teckzone-loading:after,
	.tz-product-deals-grid a.ajax-load-products .teckzone-loading:after,
	.tz-products-of-brands .load-more a .teckzone-loading:after,
	.tz-preloader .teckzone-preloader .teckzone-loading:after,
	.tz-catalog-ajax-loader .teckzone-loading:after,
	.woocommerce .blockUI.blockOverlay:after{border-color: $colors transparent $colors transparent;}

	.widget ul li a:hover,
	.widget_recent_comments ul li a:hover,
	.widget_recent_comments ul li .comment-author-link a:hover,
	.tz_widget_product_categories ul li .children li a:hover,
	.widget_product_categories ul li .children li a:hover,
	.tz_widget_product_categories ul li .children li.current-cat a,
	.widget_product_categories ul li .children li.current-cat a,
	.header-element--account .dropdown-submenu ul a:hover,
	.entry-meta .meta a:hover,
	.entry-meta .meta-cat a:hover,
	.blog-wrapper .entry-footer .meta a:hover,
	.error404 .page-content a,
	.teckzone-products-search .hot-words li a:hover,
	.teckzone-content-recently-viewed .recently-button-products .header-link:hover .link-text,
	.tz-product-recently-viewed-carousel .recently-button-products .footer-link:hover .link-text,
	.tz-product-recently-viewed-carousel--other .recently-button-products .header-link:hover .link-text,
	.tz-quick-links ul a:hover .link-text,
	.teckzone-image-grid .teckzone-image-grid__header .header-link:hover .link-text,
	.teckzone-faqs ul.tabs-nav a.active, .teckzone-faqs ul.tabs-nav a:hover,
	.teckzone-banner-small .header-link:hover .link-text,
	.teckzone-trending-search-carousel ul.collection-list a:hover .collection-item__name,
	.teckzone-trending-search-carousel-2 ul.collection-list a:hover .collection-item__name,
	.teckzone-trending-search-carousel-3 ul.collection-list .collection-item__info .name:hover,
	.teckzone-products-carousel-with-category .cat-header .header-link:hover .link-text,
	.teckzone-products-carousel-with-category .categories-box .product-cats a:hover .cat-name,
	.teckzone-products-carousel-with-category .quick-link-box ul a:hover,
	.teckzone-products-carousel-with-banner .cat-header .header-links ul a:hover,
	.teckzone-products-carousel-with-banner .quick-link-box ul a:hover,
	.teckzone-products-carousel-with-banner-2 .cat-header .header-link:hover .link-text,
	.tz-products-grid .product-grid__header .header-link:hover .link-text,
	.teckzone-product-categories .cat-header .cats-link:hover .link-text,
	.teckzone-product-categories .group-item a:hover .cat-name,
	.teckzone-product-categories-2 .group-item a:hover .cat-name,
	.teckzone-product-categories-2 .cat-footer .cats-link:hover .link-text,
	.teckzone-product-categories-3 .cat-header .header-link:hover .link-text,
	.teckzone-product-categories-3 .cat-item__list-cats ul a:hover,
	.teckzone-product-categories-4 .cat-header .cats-link:hover .link-text,
	.teckzone-product-categories-4 .group-item a:hover .cat-name,
	.teckzone-product-categories-carousel .cat-header .header-link:hover .link-text,
	.teckzone-product-categories-carousel ul.product-cats li.cat-item a:hover .cat-name,
	.tz-product-deals-day .cat-header .header-link:hover .link-text,
	.tz-product-deals-grid .cat-header .header-link:hover .link-text,
	.tz-product-with-category .header-cat .extra-links a.extra-link:hover,
	.tz-product-with-category .header-cat .extra-links .header-link:hover .link-text,
	.tz-product-with-category-2 .header-cat .header-link:hover .link-text,
	.tz-product-with-category-2 .categories-box ul.extra-links a:hover,
	.teckzone-list-links .name:hover,
	ul.products li.product .woocommerce-loop-category__title a:hover,
	ul.products li.product .woocommerce-loop-product__title a:hover,
	.tz-catalog-categories .cat-item .cat-item__wrapper:hover .box-title,
	.tz-catalog-categories-2 .cat-item:hover .box-title,
	.woocommerce div.product .product_meta .posted_in a:hover,
	.woocommerce div.product.product-type-variable form.cart .variations .reset_variations,
	.woocommerce-checkout form.checkout .place-order .woocommerce-terms-and-conditions-link,
	.woocommerce-account form.register .woocommerce-privacy-policy-text a,
	.woocommerce-cart table.shop_table tbody .actions button.empty-cart-button,
	.catalog-sidebar .widget_layered_nav_filters ul li.clear a,
	.wcfm-template-themes #wcfmmp-store .widget-area.sidebar .categories_list ul li .children li a:hover,
	.wcfm-template-themes #wcfmmp-stores-lists .widget-area.sidebar .categories_list ul li .children li a:hover{box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 $colors;}
CSS;
}

if ( ! function_exists( 'teckzone_typography_css' ) ) :
	/**
	 * Get typography CSS base on settings
	 *
	 * @since 1.1.6
	 */
	function teckzone_typography_css() {
		$css = '';

		return $css;
	}
endif;