<?php
/**
 * Page Header
 *
 * @package Teckzone
 */

/**
 * Get page header layout
 *
 * @return array
 */

if ( ! function_exists( 'teckzone_get_page_header' ) ) :
	function teckzone_get_page_header() {
		if ( is_404() || teckzone_is_homepage() || is_single() ) {
			return false;
		}

		$page_header = array( 'title', 'breadcrumb' );

		if ( teckzone_is_blog() ) {
			if ( ! intval( teckzone_get_option( 'page_header_blog' ) ) ) {
				return false;
			}
			$page_header = teckzone_get_option( 'page_header_blog_els' );

		} elseif ( is_page() ) {
			if ( ! intval( teckzone_get_option( 'page_header_page' ) ) ) {
				return false;
			}

			$page_layout = get_post_meta( get_the_ID(), 'page_header_layout', true );
			if ( ! empty( $page_layout ) ) {
				$hide_title = get_post_meta( get_the_ID(), 'hide_title', true );
				$hide_breadcrumb = get_post_meta( get_the_ID(), 'hide_breadcrumb', true );

				if ( intval( $hide_title ) && intval( $hide_breadcrumb ) ) {
					return false;
				}

				if ( intval( $hide_breadcrumb ) ) {

					$key = array_search( 'breadcrumb', $page_header );
					if ( $key !== false ) {
						unset( $page_header[ $key ] );
					}
				}

				if ( intval( $hide_title ) ) {

					$key = array_search( 'title', $page_header );
					if ( $key !== false ) {
						unset( $page_header[ $key ] );
					}
				}
			} else {
				$page_header = teckzone_get_option( 'page_header_pages_els' );
			}
		} elseif ( is_search() ) {
			$page_header = array( 'title', 'breadcrumb' );
		} elseif ( teckzone_is_vendor_page() ) {
			$page_header = teckzone_get_option( 'page_header_vendor_els' );
		}

		$page_header = teckzone_custom_page_header( $page_header );

		return $page_header;
	}

endif;

/**
 * Get custom page header layout
 *
 * @return array
 */
if ( ! function_exists( 'teckzone_custom_page_header' ) ) :
	function teckzone_custom_page_header( $page_header ) {

		if ( ! is_singular( 'page' ) && ! is_singular( 'post' ) && ! is_singular( 'product' ) && ! ( function_exists( 'is_shop' ) && is_shop() ) ) {
			return $page_header;
		}
		$post_id = get_the_ID();
		if ( function_exists( 'is_shop' ) && is_shop() ) {
			$post_id = intval( get_option( 'woocommerce_shop_page_id' ) );
		} elseif ( teckzone_is_blog() ) {
			$post_id = intval( get_option( 'page_for_posts' ) );
		}

		if ( empty( $page_header ) ) {
			return false;
		}

		$hide_page_header = get_post_meta( $post_id, 'hide_page_header', true );
		if ( $hide_page_header ) {
			return false;
		}

		if ( get_post_meta( $post_id, 'hide_breadcrumb', true ) ) {
			$key = array_search( 'breadcrumb', $page_header );
			if ( $key !== false ) {
				unset( $page_header[ $key ] );
			}
		}

		if ( get_post_meta( $post_id, 'hide_title', true ) ) {
			$key = array_search( 'title', $page_header );
			if ( $key !== false ) {
				unset( $page_header[ $key ] );
			}
		}

		if ( get_post_meta( $post_id, 'page_header_layout', true ) ) {
			$page_header['page_header_layout'] = get_post_meta( $post_id, 'page_header_layout', true );
		}

		return $page_header;
	}
endif;

/**
 * Get breadcrumbs
 *
 * @since  1.0.0
 *
 * @return string
 */

if ( ! function_exists( 'teckzone_get_breadcrumbs' ) ) :
	function teckzone_get_breadcrumbs() {
		$page_header = teckzone_get_page_header();
		if ( ! $page_header ) {
			return;
		}

		if ( ! in_array( 'breadcrumb', $page_header ) ) {
			return;
		}

		if ( teckzone_is_homepage() || is_front_page() ) {
			return;
		}

		teckzone_breadcrumbs(
			array(
				'taxonomy' => function_exists( 'is_woocommerce' ) && is_woocommerce() ? 'product_cat' : 'category',
			)
		);
	}

endif;