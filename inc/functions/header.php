<?php
/**
 * Custom functions that act on header templates
 *
 * @package Teckzone
 */

/**
 * Register fonts
 *
 * @since  1.0.0
 *
 * @return string
 */

if ( ! function_exists( 'teckzone_fonts_url' ) ):
	function teckzone_fonts_url() {
		$fonts_url = '';

		/* Translators: If there are characters in your language that are not
		* supported by Montserrat, translate this to 'off'. Do not translate
		* into your own language.
		*/
		if ( 'off' !== _x( 'on', 'OpenSans font: on or off', 'teckzone' ) ) {
			$font_families[] = 'Open Sans:400,400i,600,600i,700,700i';
		}

		if ( ! empty( $font_families ) ) {
			$query_args = array(
				'family' => urlencode( implode( '|', $font_families ) ),
				'subset' => urlencode( 'latin,latin-ext' ),
			);

			$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
		}

		return esc_url_raw( $fonts_url );
	}
endif;

/**
 * Get Menu extra search
 *
 * @since  1.0.0
 *
 *
 * @return string
 */
if ( ! function_exists( 'teckzone_extra_search' ) ) :
	function teckzone_header_search() {
		?>
		<form method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>">
			<label>
				<input type="text" name="s" class="search-field"
					   value="<?php echo esc_attr( get_search_query() ); ?>"
					   placeholder="<?php echo apply_filters( 'teckzone_header_search_placeholder', esc_attr__( 'Search anything...', 'teckzone' ) ); ?>" autocomplete="off">
				<button type="submit" class="search-submit"><?php esc_html_e( 'Search', 'teckzone' ) ?></button>
			</label>
		</form>
		<?php
	}

endif;

/**
 * Get Menu extra
 *
 * @since  1.0.0
 *
 *
 * @return string
 */
if ( ! function_exists( 'teckzone_header_elements' ) ) :
	function teckzone_header_elements() {
		$elements = teckzone_get_option( 'header_elements' );

		if ( empty( $elements ) ) {
			return;
		}

		if ( ! class_exists( 'WooCommerce' ) ) {
			return;
		}

		$count = count( $elements );

		$i = 1;
		foreach ( (array) $elements as $element ) {
			get_template_part( 'template-parts/headers/elements/' . $element );

			$i ++;
		}
	}
endif;
