<?php
/**
 * Display numeric pagination
 *
 * @since 1.0
 * @return void
 */
function teckzone_numeric_pagination() {
	global $wp_query;

	if ( $wp_query->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation paging-navigation numeric-navigation">
		<?php

		$big  = 999999999;
		$args = array(
			'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'total'     => $wp_query->max_num_pages,
			'current'   => max( 1, get_query_var( 'paged' ) ),
			'prev_text' => '<i class="icon-chevron-left"></i>',
			'next_text' => '<i class="icon-chevron-right"></i>',
			'type'      => 'plain'
		);

		echo paginate_links( $args );
		?>
	</nav>
	<?php
}

/* Filter function to be used with number_format_i18n filter hook */
if ( ! function_exists( 'teckzone_paginate_links_prefix' ) ) :
	function teckzone_paginate_links_prefix( $format ) {
		$number = intval( $format );
		if ( intval( $number / 10 ) > 0 ) {
			return $format;
		}

		return '0' . $format;
	}
endif;

if ( ! function_exists( 'teckzone_posts_navigation' ) ) :
	/**
	 * Displays posts navigation
	 */
	function teckzone_posts_navigation() {
		$a = sprintf(
			'<span class="load-more-text">%s</span>
			<span class="loading-icon">
				<span class="loading-text">%s</span>
				<span class="loading-bubbles">
					<span class="bubble"><span class="dot"></span></span>
					<span class="bubble"><span class="dot"></span></span>
					<span class="bubble"><span class="dot"></span></span>
				</span>
			</span>',
			esc_html__( 'Load More', 'teckzone' ),
			esc_html__( 'Loading', 'teckzone' )
		);

		$link = get_next_posts_link( $a );

		if ( $link ) {
			$template = '
					<nav class="navigation next-posts-navigation">
						<h4 class="screen-reader-text">%s</h4>
						<div class="nav-links">%s</div>
					</nav>';

			printf( $template, esc_html__( 'Next posts navigation', 'teckzone' ), $link, esc_html__( 'Loading', 'teckzone' ) );
		}
	}
endif;

if ( ! function_exists( 'teckzone_single_post_navigation' ) ) :
	function teckzone_single_post_navigation() {
		if ( ! intval( teckzone_get_option( 'show_post_navigation' ) ) ) {
			return;
		}

		$next_post = get_next_post();
		$prev_post = get_previous_post();

		if ( ! $next_post && ! $prev_post ) {
			return;
		}

		$previous_post_thumbnail_url = get_the_post_thumbnail_url( $prev_post );
		$next_post_thumbnail_url     = get_the_post_thumbnail_url( $next_post );

		$previous_inline_css = $previous_post_thumbnail_url ? 'style="background-image: url( ' . esc_url( $previous_post_thumbnail_url ) . ' )"' : '';
		$next_inline_css     = $next_post_thumbnail_url ? 'style="background-image: url( ' . esc_url( $next_post_thumbnail_url ) . ' )"' : '';

		?>
		<nav class="navigation post-navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Post navigation', 'teckzone' ) ?></h2>

			<div class="nav-links">
				<?php if ( $prev_post ) : ?>
					<div class="nav-previous">
						<div class="nav-previous-wrapper">
							<div class="featured-image" <?php echo ! empty( $previous_inline_css ) ? $previous_inline_css : '' ?>></div>
							<i class="icon-arrow-left"></i>
							<a href="<?php echo esc_url( get_permalink( $prev_post ) ); ?>"><span><?php echo esc_html( $prev_post->post_title ); ?></span></a>
						</div>
					</div>
				<?php endif; ?>

				<?php if ( $prev_post == '' || $next_post == '' ) : ?>
					<div class="none-nav"></div>
				<?php endif; ?>

				<?php if ( $next_post ) : ?>
					<div class="nav-next">
						<div class="nav-next-wrapper">
							<div class="featured-image" <?php echo ! empty( $next_inline_css ) ? $next_inline_css : '' ?>></div>
							<a href="<?php echo esc_url( get_permalink( $next_post ) ); ?>"><span><?php echo esc_html( $next_post->post_title ); ?></span></a>
							<i class="icon-arrow-right"></i>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</nav>
		<?php

	}
endif;