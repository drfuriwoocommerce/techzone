<?php
/**
 * Custom functions for post
 *
 * @package Teckzone
 */

/**
 * Check is blog
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_is_blog' ) ) :
	function teckzone_is_blog() {
		if ( ( is_archive() || is_author() || is_category() || is_home() || is_tag() ) && 'post' == get_post_type() ) {
			return true;
		}

		return false;
	}

endif;

/**
 * Check is catalog
 *
 * @return bool
 */
if ( ! function_exists( 'teckzone_is_catalog' ) ) :
	function teckzone_is_catalog() {
		if ( function_exists( 'is_shop' ) && ( is_shop() || is_product_category() || is_product_tag() || is_tax( 'product_brand' ) || is_tax( 'product_collection' ) || is_tax( 'product_condition' ) ) ) {
			return true;
		}

		return false;
	}
endif;

/**
 * Check homepage
 *
 * @since  1.0
 *
 */

if ( ! function_exists( 'teckzone_is_homepage' ) ) :
	function teckzone_is_homepage() {
		if ( is_page_template( 'template-homepage.php' ) || is_page_template( 'template-homepage-wide.php' ) || is_page_template( 'template-homepage-full-width.php' ) ) {
			return true;
		}

		return false;
	}

endif;

/**
 * Check is vendor page
 *
 * @return bool
 */
if ( ! function_exists( 'teckzone_is_vendor_page' ) ) :
	function teckzone_is_vendor_page() {

		if ( function_exists( 'dokan_is_store_page' ) && dokan_is_store_page() ) {
			return true;
		}

		if ( teckzone_is_wc_vendor_page() ) {
			return true;
		}

		if ( teckzone_is_dc_vendor_store() ) {
			return true;
		}

		if ( function_exists( 'wcfm_is_store_page' ) && wcfm_is_store_page() ) {
			return true;
		}

		if ( class_exists( 'WCFMmp' ) ) {
			if ( wcfmmp_is_stores_list_page() ) {
				return true;
			}
		}

		return false;
	}
endif;

/**
 * Check is vendor page
 *
 * @return bool
 */
if ( ! function_exists( 'teckzone_is_wc_vendor_page' ) ) :
	function teckzone_is_wc_vendor_page() {

		if ( class_exists( 'WCV_Vendors' ) && method_exists( 'WCV_Vendors', 'is_vendor_page' ) ) {
			return WCV_Vendors::is_vendor_page();
		}

		return false;
	}
endif;

/**
 * Check is vendor page
 *
 * @return bool
 */
if ( ! function_exists( 'teckzone_is_dc_vendor_store' ) ) :
	function teckzone_is_dc_vendor_store() {

		if ( ! class_exists( 'WCMp' ) ) {
			return false;
		}

		global $WCMp;
		if ( empty( $WCMp ) ) {
			return false;
		}

		if ( is_tax( $WCMp->taxonomy->taxonomy_name ) ) {
			return true;
		}

		return false;
	}
endif;

/**
 * Check mobile version
 *
 * @since  1.0
 *
 */
if ( ! function_exists( 'teckzone_is_mobile' ) ) :
	function teckzone_is_mobile() {

		if ( ! class_exists( 'Mobile_Detect' ) ) {
			return false;
		}

		$mobile = false;
		$detect = new Mobile_Detect();
		if ( $detect->isMobile() && ! $detect->isTablet() ) {
			$mobile = true;
		}

		return $mobile;
	}
endif;

/**
 * Check have custom header mobile
 *
 * @return bool
 */
if ( ! function_exists( 'teckzone_check_has_custom_header_mobile' ) ) :
	function teckzone_has_custom_header_mobile() {
		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			return false;
		}

		$custom_layout      = get_post_meta( get_the_ID(), 'header_mobile', true );

		if ( ! $custom_layout || empty( $custom_layout ) ) {
			$custom_layout = teckzone_get_option( 'header_mobile' );
		}

		if ( $custom_layout ) {
			return true;
		}
		
		return false;
	}
endif;

/**
 * Check page template
 *
 * @since  1.0
 *
 */

if ( ! function_exists( 'teckzone_is_page_template' ) ) :
	function teckzone_is_page_template() {
		if ( teckzone_is_homepage() ) {
			return true;
		}

		return false;
	}

endif;

/**
 * Get current page URL for layered nav items.
 * @return string
 */
if ( ! function_exists( 'teckzone_get_page_base_url' ) ) :
	function teckzone_get_page_base_url() {
		if ( defined( 'SHOP_IS_ON_FRONT' ) ) {
			$link = home_url( '/' );
		} elseif ( is_post_type_archive( 'product' ) || is_page( wc_get_page_id( 'shop' ) ) ) {
			$link = get_post_type_archive_link( 'product' );
		} elseif ( is_product_category() ) {
			$link = get_term_link( get_query_var( 'product_cat' ), 'product_cat' );
		} elseif ( is_product_tag() ) {
			$link = get_term_link( get_query_var( 'product_tag' ), 'product_tag' );
		} else {
			$queried_object = get_queried_object();
			$link           = get_term_link( $queried_object->slug, $queried_object->taxonomy );
		}

		return $link;
	}
endif;

/**
 * Conditional function to check if current page is the maintenance page.
 *
 * @return bool
 */
function teckzone_is_maintenance_page() {
	if ( ! teckzone_get_option( 'maintenance_enable' ) ) {
		return false;
	}

	if ( current_user_can( 'super admin' ) ) {
		return false;
	}

	$page_id = teckzone_get_option( 'maintenance_page' );

	if ( ! $page_id ) {
		return false;
	}

	return is_page( $page_id );
}

if ( ! function_exists( 'teckzone_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function teckzone_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
		/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'teckzone' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'teckzone_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function teckzone_posted_by() {
		$byline = sprintf(
		/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'teckzone' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'teckzone_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function teckzone_entry_footer() {
		if ( has_tag() ) {
			echo '<div class="post-tags">';
			the_tags( '', '', '' );
			echo '</div>';
		}

		$post_socials = teckzone_get_option( 'show_post_social_share' );
		$socials      = teckzone_get_option( 'post_socials_share' );

		if ( intval( $post_socials ) ) {
			if ( function_exists( 'teckzone_addons_share_link_socials' ) && ( ! empty( $socials ) ) ) {
				echo '<div class="single-post-socials-share"><i class="icon-share2"></i>';
				echo esc_html__( 'Share with:', 'teckzone' );
				echo teckzone_addons_share_link_socials( $socials, get_the_title(), get_the_permalink(), get_the_post_thumbnail() );
				echo '</div>';
			};
		}
	}
endif;

if ( ! function_exists( 'teckzone_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function teckzone_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

            <div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
            </div><!-- .post-thumbnail -->

		<?php else : ?>

            <div class="post-thumbnail">
                <a href="<?php the_permalink(); ?>">
					<?php
					the_post_thumbnail(
						'post-thumbnail', array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
					?>
                </a>
            </div>

		<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'teckzone_meta_date' ) ) :
	function teckzone_meta_date() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);

		$byline = sprintf(
		/* translators: %s: post author. */
			esc_html_x( ' by %s', 'post author', 'teckzone' ),
			'<a class="meta-author" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>'
		);

		return sprintf( '<div class="meta meta-date">%s%s</div>', $time_string, $byline );
	}
endif;

if ( ! function_exists( 'teckzone_meta_cat' ) ) :
	function teckzone_meta_cat() {
		$cats  = get_the_category();
		$count = count( $cats );

		$i      = 0;
		$number = apply_filters( 'teckzone_meta_cat_number', 1 );

		$cat_html = '';
		$output   = array();

		if ( ! is_wp_error( $cats ) && $cats ) {
			foreach ( $cats as $cat ) {
				$output[] = sprintf( '<a href="%s">%s</a>', esc_url( get_category_link( $cat->term_id ) ), esc_html( $cat->cat_name ) );

				$i ++;

				if ( $i > $number || $i > ( $count - 1 ) ) {
					break;
				}

				$output[] = ', ';
			}

			$cat_html = sprintf( '<div class="meta meta-cat"><i class="icon-folder"></i>%s</div>', implode( '', $output ) );
		}

		return $cat_html;
	}
endif;

if ( ! function_exists( 'teckzone_meta_comment' ) ) :
	function teckzone_meta_comment() {
		$number = get_comments_number();

		return sprintf( '<div class="meta meta-comment"><i class="icon-bubbles"></i>%s</div>', number_format_i18n( $number ) );
	}
endif;

if ( ! function_exists( 'teckzone_single_post_meta' ) ) :
	function teckzone_single_post_meta() {
		$metas     = (array) teckzone_get_option( 'post_entry_meta' );
		$meta_html = '';
		foreach ( $metas as $meta ) {
			switch ( $meta ) {
				case 'cat' :
					$meta_html .= teckzone_meta_cat();

					break;
				case 'comment' :
					$meta_html .= teckzone_meta_comment();

					break;
				case 'date' :

					$meta_html .= teckzone_meta_date();

					break;
				default :
			}
		}

		echo '<div class="entry-meta"><div class="meta-wrapper">' . $meta_html . '</div></div>';
	}
endif;

if ( ! function_exists( 'teckzone_blog_meta' ) ) :
	function teckzone_blog_meta() {
		$metas     = (array) teckzone_get_option( 'blog_entry_meta' );
		$meta_html = '';
		foreach ( $metas as $meta ) {
			switch ( $meta ) {
				case 'cat' :
					$meta_html .= teckzone_meta_cat();

					break;
				case 'comment' :
					$meta_html .= teckzone_meta_comment();

					break;
				default :
			}
		}

		echo '<div class="entry-meta"><div class="meta-wrapper">' . $meta_html . '</div></div>';
	}
endif;

/**
 * Get author box
 *
 * @since  1.0
 *
 */
if ( ! function_exists( 'teckzone_author_box' ) ) :
	function teckzone_author_box() {
		if ( ! intval( teckzone_get_option( 'show_author_box' ) ) ) {
			return;
		}

		if ( ! get_the_author_meta( 'description' ) ) {
			return;
		}

		$socials = array(
			'facebook'  => esc_html__( 'Facebook', 'teckzone' ),
			'twitter'   => esc_html__( 'Twitter', 'teckzone' ),
			'google'    => esc_html__( 'Google Plus', 'teckzone' ),
			'pinterest' => esc_html__( 'Pinterest', 'teckzone' ),
			'instagram' => esc_html__( 'Instagram', 'teckzone' ),
		);

		$links = array();
		foreach ( $socials as $social => $name ) {
			$link = get_the_author_meta( $social, get_the_author_meta( 'ID' ) );
			if ( empty( $link ) ) {
				continue;
			}

			if ( $social == 'google' ) {
				$social = 'googleplus';
			}

			$links[] = sprintf(
				'<li><a href="%s" target="_blank"><i class="social_%s"></i><span>%s</span></a></li>',
				esc_url( $link ),
				esc_attr( $social ),
				esc_html( $name )
			);
		}

		$layout = teckzone_get_option( 'single_post_layout' );

		?>

        <div class="post-author">
			<?php if ( $layout == 'full-content' ) : ?>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-xs-12">
					<?php endif ?>
                    <div class="post-author-box clearfix">
                        <div class="post-author-avatar">
							<?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?>
                        </div>
                        <div class="post-author-info">
                            <h3 class="author-name"><?php the_author_meta( 'display_name' ); ?></h3>

                            <p><?php the_author_meta( 'description' ); ?></p>
							<?php
							if ( ! empty( $links ) ) {
								echo sprintf( '<ul class="author-socials">%s</ul>', implode( '', $links ) );
							}
							?>
                        </div>
                    </div>
					<?php if ( $layout == 'full-content' ) : ?>
                </div>
                <!-- .row -->
            </div>
            <!-- .col -->
		<?php endif ?>
        </div>


		<?php
	}
endif;

/**
 * Get post format
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_post_format' ) ) :
	function teckzone_post_format( $blog_view, $size ) {
		$post_format = get_post_format();
		$post_format = $post_format ? $post_format : 'image';
		$css_class   = 'format-' . $post_format;
		$html        = '';

		if ( $blog_view == 'default' || $blog_view == 'grid' ) {
			switch ( get_post_format() ) {
				case 'gallery':
					$html = teckzone_post_format_gallery( $size );
					break;

				case 'quote':
					$html = teckzone_post_format_quote();
					break;

				default:
					$html = teckzone_post_format_image( $blog_view, $size );
					break;
			}
		} elseif ( $blog_view == 'small-thumb' ) {
			switch ( get_post_format() ) {
				case 'gallery':
					$html = teckzone_post_format_gallery( $size );
					break;
				case 'audio':
					$html = teckzone_post_format_audio();
					break;

				case 'link':
					$html = teckzone_post_format_link();
					break;

				case 'quote':
					$html = teckzone_post_format_quote();
					break;

				default:
					$html = teckzone_post_format_image( $blog_view, $size );
					break;
			}
		} else {
			switch ( get_post_format() ) {
				case 'gallery':
					$html = teckzone_post_format_gallery( $size );
					break;
				case 'audio':
					$html = teckzone_post_format_audio();
					break;

				case 'video':
					$html = teckzone_post_format_video();
					break;

				case 'link':
					$html = teckzone_post_format_link();
					break;

				case 'quote':
					$html = teckzone_post_format_quote();
					break;

				default:
					$html = teckzone_post_format_image( $blog_view, $size );
					break;
			}
		}

		if ( $html ) {
			echo "<div class='entry-format $css_class'>$html</div>";
		}

	}
endif;

/**
 * Get post format quote
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_post_format_quote' ) ) :
	function teckzone_post_format_quote() {
		$quote      = get_post_meta( get_the_ID(), 'quote', true );
		$author     = get_post_meta( get_the_ID(), 'quote_author', true );
		$author_url = get_post_meta( get_the_ID(), 'author_url', true );

		if ( ! $quote ) {
			return;
		}

		return sprintf(
			'<blockquote>%s<cite>%s</cite></blockquote>',
			esc_html( $quote ),
			empty( $author_url ) ? $author : '<a href="' . esc_url( $author_url ) . '">' . $author . '</a>'
		);

	}
endif;

/**
 * Get post format link
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_post_format_link' ) ) :
	function teckzone_post_format_link() {
		$title = get_post_meta( get_the_ID(), 'title', true );
		$desc  = get_post_meta( get_the_ID(), 'desc', true );
		$link  = get_post_meta( get_the_ID(), 'url', true );
		$text  = get_post_meta( get_the_ID(), 'url_text', true );

		if ( ! $link ) {
			return;
		}

		if ( $title ) {
			$title = sprintf( '<h3 class="title">%s</h3>', $title );
		}

		if ( $desc ) {
			$desc = sprintf( '<div class="desc">%s</div>', $desc );
		}

		return sprintf( '%s%s<a href="%s" class="link-block">%s</a>', $title, $desc, esc_url( $link ), $text ? $text : $link );

	}
endif;

/**
 * Get post format gallery
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_post_format_gallery' ) ) :
	function teckzone_post_format_gallery( $size ) {
		$image_ids = get_post_meta( get_the_ID(), 'images', false );

		$blog_view = teckzone_get_option( 'blog_view' );

		$dot   = false;
		$arrow = true;

		if ( teckzone_is_blog() && $blog_view != 'default' ) {
			$dot   = true;
			$arrow = false;
		}

		$carousel_settings = array(
			'arrows'     => $arrow,
			'dots'       => $dot,
			'infinite'   => false,
			'responsive' => array(
				array(
					'breakpoint' => 1200,
					'settings'   => array(
						'arrows' => false,
						'dots'   => true,
					)
				),
			)
		);

		$carousel_settings = apply_filters( 'teckzone_post_format_gallery_settings', $carousel_settings );

		if ( empty( $image_ids ) ) {
			$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );

			if ( is_single() ) {
				return wp_get_attachment_image( $post_thumbnail_id, $size );
			} else {
				return '<a class="entry-image" href="' . get_permalink() . '">' . wp_get_attachment_image( $post_thumbnail_id, $size ) . '</a>';
			}


		} else {
			$gallery = array();
			foreach ( $image_ids as $id ) {
				$image = wp_get_attachment_image( $id, $size );
				if ( $image ) {
					if ( is_single() ) {
						$gallery[] = '<li>' . $image . '</li>';
					} else {
						$gallery[] = '<li><a class="entry-image" href="' . get_permalink() . '">' . $image . '</a></li>';
					}
				}
			}

			return '<ul class="slides" data-slick="' . esc_attr( wp_json_encode( $carousel_settings ) ) . '">' . implode( '', $gallery ) . '</ul>';
		}

	}
endif;

/**
 * Get post format video
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_post_format_video' ) ) :
	function teckzone_post_format_video() {
		$video = get_post_meta( get_the_ID(), 'video', true );

		if ( ! $video ) {
			return;
		}

		$video_html = '';

		// If URL: show oEmbed HTML
		if ( filter_var( $video, FILTER_VALIDATE_URL ) ) {
			if ( $oembed = @wp_oembed_get( $video, array( 'width' => 1170 ) ) ) {
				$video_html = $oembed;
			} else {
				$atts = array(
					'src'   => $video,
					'width' => 1170,
				);

				if ( has_post_thumbnail() ) {
					$atts['poster'] = get_the_post_thumbnail_url( get_the_ID(), 'full' );
				}
				$video_html = wp_video_shortcode( $atts );
			}
		} // If embed code: just display
		else {
			$video_html = $video;
		}

		return $video_html;

	}
endif;

/**
 * Get post format audio
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_post_format_audio' ) ) :
	function teckzone_post_format_audio() {
		$audio = get_post_meta( get_the_ID(), 'audio', true );
		if ( ! $audio ) {
			return;
		}

		$html = '';

		// If URL: show oEmbed HTML or jPlayer
		if ( filter_var( $audio, FILTER_VALIDATE_URL ) ) {
			// Try oEmbed first
			if ( $oembed = @wp_oembed_get( $audio ) ) {
				$html .= $oembed;
			} // Use audio shortcode
			else {
				$html .= '<div class="audio-player">' . wp_audio_shortcode( array( 'src' => $audio ) ) . '</div>';
			}
		} // If embed code: just display
		else {
			$html .= $audio;
		}

		return $html;
	}
endif;

/**
 * Get post format image
 *
 * @since  1.0
 */

if ( ! function_exists( 'teckzone_post_format_image' ) ) :
	function teckzone_post_format_image( $blog_view, $size ) {
		$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
		$thumb             = wp_get_attachment_image( $post_thumbnail_id, $size );
		$symbol            = '';
		$post_format       = get_post_format();

		if ( $blog_view == 'default' || $blog_view == 'grid' ) {
			switch ( $post_format ) {
				case 'video':
					$symbol = get_template_directory_uri() . '/images/play-icon.png';
					$symbol = '<span class="post-format-icon"><img src="' . esc_url( $symbol ) . '" alt =""/></span>';
					break;
				case 'audio':
					$symbol = '<span class="post-format-icon"><i class="icon-music-note"></i></span>';
					break;
				case 'link':
					$symbol = '<span class="post-format-icon"><i class="icon-link2"></i></span>';
					break;
				default:
					break;
			}
		} elseif ( $blog_view == 'small-thumb' ) {
			switch ( $post_format ) {
				case 'video':
					$symbol = get_template_directory_uri() . '/images/play-icon.png';
					$symbol = '<span class="post-format-icon"><img src="' . esc_url( $symbol ) . '" alt =""/></span>';
					break;
				default:
					break;
			}
		}

		if ( empty( $thumb ) ) {
			return;
		}

		if ( is_single() ) {
			return $thumb;
		} else {
			return '<a class="entry-image" href="' . get_permalink() . '">' . $symbol . $thumb . '</a>';
		}
	}
endif;

/**
 * Get or display limited words from given string.
 * Strips all tags and shortcodes from string.
 *
 * @since 1.0.0
 *
 * @param integer $num_words The maximum number of words
 * @param string $more More link.
 *
 * @return string|void Limited content.
 */
function teckzone_content_limit( $num_words, $more = "&hellip;" ) {
	$content = get_the_excerpt();

	// Strip tags and shortcodes so the content truncation count is done correctly
	$content = strip_tags(
		strip_shortcodes( $content ), apply_filters(
			'teckzone_content_limit_allowed_tags', '<script>,<style>'
		)
	);

	// Remove inline styles / scripts
	$content = trim( preg_replace( '#<(s(cript|tyle)).*?</\1>#si', '', $content ) );

	// Truncate $content to $max_char
	$content = wp_trim_words( $content, $num_words );

	if ( $more ) {
		$output = sprintf(
			'<p>%s <a href="%s" class="more-link" title="%s">%s</a></p>',
			$content,
			get_permalink(),
			sprintf( esc_html__( 'Continue reading &quot;%s&quot;', 'teckzone' ), the_title_attribute( 'echo=0' ) ),
			esc_html( $more )
		);
	} else {
		$output = sprintf( '<p>%s</p>', $content );
	}

	return $output;
}

/**
 * Retrieves related product terms
 *
 * @param string $term
 *
 * @return array
 */
function teckzone_get_related_terms( $term, $post_id = null ) {
	$post_id     = $post_id ? $post_id : get_the_ID();
	$terms_array = array( 0 );

	$terms = wp_get_post_terms( $post_id, $term );
	foreach ( $terms as $term ) {
		$terms_array[] = $term->term_id;
	}

	return array_map( 'absint', $terms_array );
}

/**
 * Show categories filter
 *
 * @return string
 */

if ( ! function_exists( 'teckzone_taxs_list' ) ) :
	function teckzone_taxs_list( $args ) {
		$args = wp_parse_args(
			$args, array(
				'taxonomy'      => 'category',
				'number'        => 6,
				'button'        => esc_html__( 'All', 'teckzone' ),
				'button_url'    => 'posts' == get_option( 'show_on_front' ) ? home_url() : get_page_link( get_option( 'page_for_posts' ) ),
				'orderby'       => 'count',
				'order'         => 'DESC',
				'custom'        => false,
				'custom_values' => [],
			)
		);

		$cats   = '';
		$output = array();

		$term_args = array(
			'number'  => $args['number'],
			'orderby' => $args['orderby'],
			'order'   => $args['order'],
		);

		$term_id = 0;

		if ( is_tax( $args['taxonomy'] ) || is_category() ) {

			$queried_object = get_queried_object();
			if ( $queried_object ) {
				$term_id = $queried_object->term_id;
			}
		}

		$found       = false;
		$custom_slug = intval( $args['custom'] );

		if ( $custom_slug ) {
			$cats_slug = $args['custom_values'];

			foreach ( $cats_slug as $slug ) {
				$cat = get_term_by( 'slug', $slug, $args['taxonomy'] );

				if ( ! is_wp_error( $cat ) && $cat ) {
					$css_class = '';
					if ( $cat->term_id == $term_id ) {
						$css_class = 'selected';
						$found     = true;
					}
					$cats .= sprintf( '<li><a class="%s" href="%s">%s</a></li>', esc_attr( $css_class ), esc_url( get_term_link( $cat ) ), esc_html( $cat->name ) );
				}
			}
		} else {
			$categories = get_terms( $args['taxonomy'], $term_args );
			if ( ! is_wp_error( $categories ) && $categories ) {
				foreach ( $categories as $cat ) {
					$cat_selected = '';
					if ( $cat->term_id == $term_id ) {
						$cat_selected = 'selected';
						$found        = true;
					}
					$cats .= sprintf( '<li><a href="%s" class="%s">%s</a></li>', esc_url( get_term_link( $cat ) ), esc_attr( $cat_selected ), esc_html( $cat->name ) );
				}
			}
		}

		$cat_selected = $found ? '' : 'selected';

		if ( $cats ) {
			$output[] = sprintf(
				'<ul>
					<li><a href="%s" class="%s">%s</a></li>
					 %s
				</ul>',
				esc_url( $args['button_url'] ),
				esc_attr( $cat_selected ),
				esc_html( $args['button'] ),
				$cats
			);
		}

		if ( $output ) {
			$output = apply_filters( 'teckzone_taxs_list_html', $output );
			echo '<div class="teckzone-taxs-list"><div class="container">' . implode( "\n", $output ) . '</div></div>';
		}
	}

endif;

if ( ! function_exists( 'teckzone_product_video' ) ) :
	function teckzone_product_video() {
		global $product;
		$video_image  = get_post_meta( $product->get_id(), 'video_thumbnail', true );
		$video_url    = get_post_meta( $product->get_id(), 'video_url', true );
		$video_width  = 1024;
		$video_height = 768;
		$video_html   = '';
		if ( $video_image ) {
			$video_thumb = wp_get_attachment_image_src( $video_image, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			$video_thumb = $video_thumb[0];
			// If URL: show oEmbed HTML
			if ( filter_var( $video_url, FILTER_VALIDATE_URL ) ) {

				$atts = array(
					'width'  => $video_width,
					'height' => $video_height
				);

				if ( $oembed = @wp_oembed_get( $video_url, $atts ) ) {
					$video_html = $oembed;
				} else {
					$atts = array(
						'src'    => $video_url,
						'width'  => $video_width,
						'height' => $video_height
					);

					$video_html = wp_video_shortcode( $atts );

				}
			}
			if ( $video_html ) {
				$vid_html      = '<div class="tz-video-wrapper">' . $video_html . '</div>';
				$video_wrapper = sprintf( '<div class="tz-video-content">%s</div>', $vid_html );
				$video_html    = '<div data-thumb="' . esc_url( $video_thumb ) . '" class="woocommerce-product-gallery__image tz-product-video">' . $video_wrapper . '</div>';
			}
		}

		return $video_html;
	}
endif;