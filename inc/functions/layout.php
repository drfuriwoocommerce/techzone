<?php
/**
 * Custom layout functions
 *
 * @package Teckzone
 */


/**
 * Get classes for content area
 *
 * @since  1.0
 *
 * @return string of classes
 */

if ( ! function_exists( 'teckzone_content_container_class' ) ) :
	function teckzone_content_container_class() {
		$container = 'container';

		$get_id = get_the_ID();
		if ( teckzone_is_catalog() ) {
			$get_id = intval(get_option( 'woocommerce_shop_page_id' ));
		} elseif ( teckzone_is_blog() ) {
			$get_id = intval( get_option( 'page_for_posts' ) );
		}

		$container_width_custom = get_post_meta( $get_id, 'page_content_container_width', true );

		if ( is_page_template( 'template-homepage-full-width.php' ) ) {
			$container = 'teckzone-container';
		} elseif ( is_page_template( 'template-homepage-wide.php' ) ) {
			$container = 'teckzone-wide-container';
		} elseif ( is_page() || teckzone_is_blog() ) {
			if( $container_width_custom == 'full-width') {
				$container = 'teckzone-container';
			} elseif( $container_width_custom == 'wide') {
				$container = 'teckzone-wide-container';
			} 

		} elseif ( teckzone_is_catalog() ) {
			$catalog_width = teckzone_get_option( 'catalog_container_width' );
			if( $container_width_custom != 'default' ) {
				$catalog_width = $container_width_custom;
			} 

			if( $catalog_width == 'full-width') {
				$container = 'teckzone-container';
			} elseif( $catalog_width == 'wide') {
				$container = 'teckzone-wide-container';
			} 
		} elseif ( teckzone_is_vendor_page() ) {
			$catalog_vendor_width = teckzone_get_option( 'catalog_vendor_container_width' );
			
			if( $catalog_vendor_width == 'full-width') {
				$container = 'teckzone-container';
			} elseif( $catalog_vendor_width == 'wide') {
				$container = 'teckzone-wide-container';
			} 
		} elseif ( is_singular( 'product' ) ) {
			if( teckzone_get_option( 'product_container_width' ) == 'full-width') {
				$container = 'teckzone-container';
			} elseif( teckzone_get_option( 'product_container_width' ) == 'wide') {
				$container = 'teckzone-wide-container';
			}
		}

		return $container;
	}

endif;


if ( ! function_exists( 'teckzone_get_layout' ) ) :
	function teckzone_get_layout() {
		$layout = teckzone_get_option( 'blog_layout' );

		if ( is_singular( 'post' ) ) {
			$layout = teckzone_get_option( 'single_post_layout' );
		} elseif ( teckzone_is_blog() ) {
			$blog_view = teckzone_get_option( 'blog_view' );
			if ( $blog_view == 'grid' ) {
				$layout = 'full-content';
			}
		} elseif ( is_page() ) {
			$layout = teckzone_get_option( 'page_layout' );
		} elseif ( teckzone_is_catalog() ) {
			$layout = teckzone_get_option( 'catalog_layout' );
		} elseif ( function_exists( 'is_product' ) && is_product() ) {
			$product_layout = teckzone_get_option( 'product_layout' );
			if ( $product_layout == '1' ) {
				$layout = 'full-content';
			} else {
				$layout = teckzone_get_option( 'product_sidebar' );
			}
		} elseif ( is_404() ) {
			$layout = 'full-content';
		}

		return apply_filters( 'teckzone_site_layout', $layout );
	}

endif;

/**
 * Get Bootstrap column classes for content area
 *
 * @since  1.0
 *
 * @return array Array of classes
 */

if ( ! function_exists( 'teckzone_get_content_columns' ) ) :
	function teckzone_get_content_columns( $layout = null ) {
		$layout  = $layout ? $layout : teckzone_get_layout();
		$classes = array( 'col-flex-md-9', 'col-flex-sm-12', 'col-flex-xs-12' );

		if ( $layout == 'full-content' || teckzone_is_vendor_page() ) {
			$classes = array( 'col-flex-xs-12' );
		}

		return $classes;
	}

endif;

/**
 * Echos Bootstrap column classes for content area
 *
 * @since 1.0
 */

if ( ! function_exists( 'teckzone_content_columns' ) ) :
	function teckzone_content_columns( $layout = null ) {
		echo implode( ' ', teckzone_get_content_columns( $layout ) );
	}
endif;
