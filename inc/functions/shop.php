<?php

/**
 * Get catalog layout
 *
 * @since 1.0
 */
if ( ! function_exists( 'teckzone_get_catalog_layout' ) ) :
	function teckzone_get_catalog_layout() {
		$layout = '10';

		if ( is_search() ) {
			$layout = '10';
		} elseif ( function_exists( 'is_shop' ) && is_shop() ) {
			$layout = teckzone_get_option( 'shop_layout' );
		} elseif ( function_exists( 'is_product_category' ) && is_product_category() && teckzone_get_product_category_level() == 0 ) {
			$layout = teckzone_get_option( 'products_cat_level_1_layout' );

			if ( function_exists( 'get_term_meta' ) ) {
				$queried_object = get_queried_object();
				$cat_layout     = get_term_meta ( $queried_object->term_id, 'tz_cat_layout', true );
				$layout         = $cat_layout ? $cat_layout : $layout;
			}
		}

		return apply_filters( 'teckzone_get_catalog_layout', $layout );
	}
endif;

/**
 * Get category level
 *
 * @since 1.0
 */
if ( ! function_exists( 'teckzone_get_product_category_level' ) ) :
	function teckzone_get_product_category_level() {
		global $wp_query;
		$current_cat = $wp_query->get_queried_object();
		if ( empty( $current_cat ) ) {
			return 0;
		}

		$term_id = $current_cat->term_id;

		return count( get_ancestors( $term_id, 'product_cat' ) );
	}
endif;