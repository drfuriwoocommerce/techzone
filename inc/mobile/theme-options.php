<?php
/**
 * Theme options of Mobile.
 *
 * @package Martfury
 */

/**
 * Adds theme options panels of Mobile.
 *
 * @param array $sections Theme options sections.
 *
 * @return array
 */
function teckzone_mobile_customize_panels( $panels ) {
	$panels = array_merge(
		$panels, array(
			'mobile' => array(
				'title'    => esc_html__( 'Mobile', 'teckzone' ),
				'priority' => 90,
			),
		)
	);

	return $panels;
}

add_filter( 'teckzone_customize_panels', 'teckzone_mobile_customize_panels' );

/**
 * Adds theme options sections of Mobile.
 *
 * @param array $sections Theme options sections.
 *
 * @return array
 */
function teckzone_mobile_customize_sections( $sections ) {
	$sections = array_merge(
		$sections, array(
			'general_mobile'         => array(
				'title'       => esc_html__( 'General', 'teckzone' ),
				'description' => '',
				'priority'    => 20,
				'capability'  => 'edit_theme_options',
				'panel'       => 'mobile',
			),
			'homepage_mobile'        => array(
				'title'       => esc_html__( 'Homepage Settings', 'teckzone' ),
				'description' => '',
				'priority'    => 20,
				'capability'  => 'edit_theme_options',
				'panel'       => 'mobile',
			),
			'header_mobile'          => array(
				'title'       => esc_html__( 'Header', 'teckzone' ),
				'description' => '',
				'priority'    => 20,
				'capability'  => 'edit_theme_options',
				'panel'       => 'mobile',
			),
			'navigation_mobile'          => array(
				'title'       => esc_html__( 'Navigation', 'teckzone' ),
				'description' => '',
				'priority'    => 20,
				'capability'  => 'edit_theme_options',
				'panel'       => 'mobile',
			),
			'catalog_mobile'          => array(
				'title'       => esc_html__( 'Catalog Page', 'teckzone' ),
				'description' => '',
				'priority'    => 20,
				'capability'  => 'edit_theme_options',
				'panel'       => 'mobile',
			),
			'product_page_mobile'    => array(
				'title'       => esc_html__( 'Product Page', 'teckzone' ),
				'description' => '',
				'priority'    => 20,
				'capability'  => 'edit_theme_options',
				'panel'       => 'mobile',
			),
			'footer_mobile'          => array(
				'title'       => esc_html__( 'Footer', 'teckzone' ),
				'description' => '',
				'priority'    => 20,
				'capability'  => 'edit_theme_options',
				'panel'       => 'mobile',
			),
		)
	);

	return $sections;
}

add_filter( 'teckzone_customize_sections', 'teckzone_mobile_customize_sections' );

/**
 * Adds theme options of Mobile.
 *
 * @param array $settings Theme options.
 *
 * @return array
 */
function teckzone_mobile_customize_fields( $fields ) {
	$fields = array_merge(
		$fields, array(
			// General
			'enable_mobile_version'             => array(
				'type'     => 'toggle',
				'label'    => esc_html__( 'Mobile Version', 'teckzone' ),
				'section'  => 'general_mobile',
				'default'  => '0',
				'priority' => 20,
			),

			// Homepage Settings
			'homepage_mobile'                   => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Homepage', 'teckzone' ),
				'section'  => 'homepage_mobile',
				'default'  => '',
				'priority' => 20,
				'choices'  => class_exists( 'Kirki_Helper' ) && is_admin() ? Kirki_Helper::get_posts( array(
					'posts_per_page' => - 1,
					'post_type'      => 'page',
				) ) : '',
			),

			// Header
			'header_mobile'           => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Header Mobile', 'teckzone' ),
				'section'  => 'header_mobile',
				'default'  => '',
				'priority' => 10,
				'choices'  => teckzone_customizer_get_posts( array( 'post_type' => 'tz_header' ) ),
				'description'     => esc_html__( 'Select default header layout on mobile version.', 'teckzone' ),
			),
			'header_mobile_inner_page'           => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Header Mobile Inner Page', 'teckzone' ),
				'section'     => 'header_mobile',
				'default'     => '',
				'priority'    => 10,
				'choices'     => teckzone_customizer_get_posts( array( 'post_type' => 'tz_header' ) ),
				'description' => esc_html__( 'Select default header layout for inner page on mobile version. Leave blank to inherit Header Mobile', 'teckzone' ),
			),
			'header_mobile_single_post'           => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Header Mobile Single Post', 'teckzone' ),
				'section'     => 'header_mobile',
				'default'     => '',
				'priority'    => 10,
				'choices'     => teckzone_customizer_get_posts( array( 'post_type' => 'tz_header' ) ),
				'description' => esc_html__( 'Select default header layout for single post on mobile version. Leave blank to inherit Header Mobile', 'teckzone' ),
			),

			// Footer
			'footer_mobile'           => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Footer Mobile', 'teckzone' ),
				'section'  => 'footer_mobile',
				'default'  => '',
				'priority' => 10,
				'choices'  => teckzone_customizer_get_posts( array( 'post_type' => 'tz_footer' ) ),
				'description'     => esc_html__( 'Select default footer layout on mobile version.', 'teckzone' ),
			),

			// Navigation
			'navigation_mobile_template'           => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Navigation Mobile Template', 'teckzone' ),
				'section'  => 'navigation_mobile',
				'default'  => '',
				'priority' => 10,
				'choices'  => teckzone_customizer_get_posts( array( 'post_type' => 'elementor_library' ) ),
			),

			// Catalog Page
			'mobile_catalog_toolbar_elements'             => array(
				'type'    => 'multicheck',
				'label'   => esc_html__( 'Catalog Toolbar Elements', 'teckzone' ),
				'default' => array( 'sort_by', 'view' ),
				'choices' => array(
					'sort_by'       => esc_attr__( 'Sort by', 'teckzone' ),
					'view'          => esc_attr__( 'View', 'teckzone' ),
				),
				'section' => 'catalog_mobile',
			),
			'catalog_custom_1'        => array(
				'type'     => 'custom',
				'section'  => 'catalog_mobile',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'mobile_catalog_brands'  => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Brands', 'teckzone' ),
				'section'     => 'catalog_mobile',
				'default'     => 1,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to show brands on catalog page on mobile.', 'teckzone' ),
			),
			'mobile_catalog_brands_per_show'     => array(
				'type'     => 'number',
				'label'    => esc_html__( 'Brands per show', 'teckzone' ),
				'section'  => 'catalog_mobile',
				'default'  => 10,
				'priority' => 40,
			),
			'mobile_catalog_brands_columns'       => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Columns', 'teckzone' ),
				'section'  => 'catalog_mobile',
				'default'  => '3',
				'priority' => 40,
				'choices'  => array(
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
				),
			),
			'catalog_custom_2'        => array(
				'type'     => 'custom',
				'section'  => 'catalog_mobile',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'catalog_nav_type_mobile'           => array(
				'type'     => 'radio',
				'section'  => 'catalog_mobile',
				'label'    => esc_html__( 'Navigation Type', 'teckzone' ),
				'default'  => 'infinite',
				'priority' => 40,
				'choices'  => array(
					'numeric'  => esc_attr__( 'Numeric', 'teckzone' ),
					'loadmore' => esc_attr__( 'Load More', 'teckzone' ),
					'infinite' => esc_attr__( 'Infinite Scroll', 'teckzone' ),
				),
			),

			// Product Page
			'product_add_to_cart_fixed_mobile'  => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Add to cart fixed', 'teckzone' ),
				'section'     => 'product_page_mobile',
				'default'     => 1,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to enable add to cart button fixed on mobile.', 'teckzone' ),
			),
			'product_layout_custom_1'        => array(
				'type'     => 'custom',
				'section'  => 'product_page_mobile',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'sticky_product_info_mobile'        => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Sticky Product Info', 'teckzone' ),
				'section'     => 'product_page_mobile',
				'default'     => 0,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to enable sticky product info on the product page.', 'teckzone' ),
			),
			'sticky_product_info_offset_mobile'     => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Sticky Product Info Offset', 'teckzone' ),
				'section'     => 'product_page_mobile',
				'default'     => 0,
				'priority'    => 40,
			),
			'product_layout_custom_2'        => array(
				'type'     => 'custom',
				'section'  => 'product_page_mobile',
				'default'  => '<hr>',
				'priority' => 50,
			),
			'product_collapse_tab'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Product Tabs Collapse', 'teckzone' ),
				'section'     => 'product_page_mobile',
				'default'     => 0,
				'priority'    => 50,
				'description' => esc_html__( 'Check this option to show the product tabs collapse on product page.', 'teckzone' ),
			),
			'product_collapse_tab_status'       => array(
				'type'            => 'select',
				'label'           => esc_html__( 'Collapse Status', 'teckzone' ),
				'section'         => 'product_page_mobile',
				'default'         => 'close',
				'priority'        => 50,
				'choices'         => array(
					'close' => esc_html__( 'Close', 'teckzone' ),
					'open'  => esc_html__( 'Open', 'teckzone' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'product_collapse_tab',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
		)
	);

	return $fields;
}

add_filter( 'teckzone_customize_fields', 'teckzone_mobile_customize_fields' );