<?php

/**
 * Hooks for template mobile
 *
 * @package Teckzone
 */
class Teckzone_Mobile {

	function __construct() {

		if ( ! intval( teckzone_is_mobile() ) ) {
			return;
		}

		if ( ! intval( teckzone_get_option( 'enable_mobile_version' ) ) ) {
			return;
		}

		add_filter( 'pre_option_page_on_front', array( $this, 'homepage_mobile_init' ) );

		add_filter( 'body_class', array( $this, 'body_classes' ) );

		remove_action( 'teckzone_header', 'teckzone_show_header' );
		remove_action( 'teckzone_header', 'teckzone_show_header_mobile' );
		remove_action( 'teckzone_after_header', 'teckzone_show_page_header', 20 );
		add_action( 'teckzone_header', array( $this, 'header_mobile' ) );

		remove_action( 'teckzone_footer', 'teckzone_show_footer' );
		add_action( 'teckzone_footer', array( $this, 'footer_mobile' ) );

		add_action( 'wp_footer', array( $this, 'navigation_mobile' ) );

		add_filter( 'teckzone_catalog_toolbar_elements', array( $this, 'mobile_catalog_toolbar_elements' ) );
		add_filter( 'teckzone_catalog_elements', array( $this, 'mobile_catalog_elements' ) );

		add_action( 'woocommerce_before_shop_loop', array( $this, 'mobile_catalog_brands' ), 25 );
		add_action( 'woocommerce_before_shop_loop', array( $this, 'mobile_list_filter' ), 27 );
		add_action( 'woocommerce_before_shop_loop', array( $this, 'mobile_catalog_products_found' ), 29 );

		add_filter( 'teckzone_product_tabs_layout', array( $this, 'product_tabs_layout' ) );
		add_filter( 'teckzone_single_product_breadcrumb', array( $this, 'single_product_breadcrumb' ) );
		add_filter( 'teckzone_sticky_product_info', array( $this, 'sticky_product_info' ) );
		add_filter( 'teckzone_sticky_product_info_offset', array( $this, 'sticky_product_info_offset' ) );

		add_filter( 'teckzone_catalog_nav_type', array( $this, 'catalog_pagination' ) );
		add_filter( 'teckzone_catalog_nav_type_class', array( $this, 'teckzone_catalog_nav_type_class' ) );
	}

	/**
	 * Display homepage mobile
	 *
	 * @since 1.0.0
	 *
	 *  return string
	 */

	function homepage_mobile_init( $value ) {
		$homepage = teckzone_get_option( 'homepage_mobile' );
		$value    = ! empty( $homepage ) ? $homepage : $value;

		return $value;
	}

	function body_classes( $classes ) {
		$classes[] = 'mobile-version';

		if ( ! empty( teckzone_get_option( 'navigation_mobile_template' ) ) ) {
			$classes[] = 'mobile-navigation-enable';
		}

		if ( is_product() ) {
			if ( intval( teckzone_get_option( 'product_add_to_cart_fixed_mobile' ) ) ) {
				$classes[] = 'tz-add-to-cart-fixed';
			}
		}

		return $classes;
	}

	function product_tabs_layout( $layout ) {
		$layout = 2;

		return $layout;
	}

	function single_product_breadcrumb( $output ) {
		$output = '';

		return $output;
	}

	function sticky_product_info() {
		return teckzone_get_option( 'sticky_product_info_mobile' );
	}

	function sticky_product_info_offset() {
		return teckzone_get_option( 'sticky_product_info_offset_mobile' );
	}

	function header_mobile(){
		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			return;
		}

		$elementor_instance = Elementor\Plugin::instance();

		$page_id = get_the_ID();

		if ( function_exists( 'is_shop' ) && is_shop() ) {
			$page_id = get_option( 'woocommerce_shop_page_id' );
		} elseif ( is_home() && ! is_front_page() ) {
			$page_id = get_queried_object_id();
		}

		$header_mobile_layout = teckzone_get_option( 'header_mobile_inner_page' );
		
		if ( teckzone_is_homepage() || is_front_page() ) {
			$header_mobile_layout = teckzone_get_option( 'header_mobile' );
		}

		if ( is_single() ) {
			$header_mobile_layout = teckzone_get_option( 'header_mobile_single_post' );
		}

		if ( is_page() ) {
			if ( get_post_meta( $page_id, 'header_mobile', true ) ) {
				$header_mobile_layout = get_post_meta( $page_id, 'header_mobile', true );
			}
		}
		
		$header_mobile_layout = apply_filters( 'teckzone_header_mobile_layout', $header_mobile_layout );

		if ( empty( $header_mobile_layout ) ) {
			return;
		}

		echo '<div class="header-mobile">'. $elementor_instance->frontend->get_builder_content_for_display( $header_mobile_layout ) .'</div>';
	}

	function footer_mobile(){
		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			return;
		}

		$elementor_instance = Elementor\Plugin::instance();

		$footer_layout = teckzone_get_option( 'footer_mobile' );

		if ( get_post_meta( get_the_ID(), 'footer_mobile', true ) ) {
			$footer_layout = get_post_meta( get_the_ID(), 'footer_mobile', true );
		}

		echo ! empty( $footer_layout ) ? $elementor_instance->frontend->get_builder_content_for_display( $footer_layout ) : '';
	}

	function navigation_mobile(){
		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			return;
		}

		$elementor_instance = Elementor\Plugin::instance();

		$navigation = teckzone_get_option( 'navigation_mobile_template' );

		if ( ! $navigation ) {
			return;
		}

		echo '<div class="tz-navigation-mobile">' . $elementor_instance->frontend->get_builder_content_for_display( $navigation ) . '</div>';
	}

	function mobile_catalog_toolbar_elements( $els ) {
		if ( teckzone_is_catalog() ) {
			$els = ( array ) teckzone_get_option( 'mobile_catalog_toolbar_elements' );
		}

		return $els;
	}

	function mobile_catalog_elements( $els ) {
		$els = [];

		return $els;
	}

	function mobile_catalog_brands() {
		if ( ! intval( teckzone_get_option('mobile_catalog_brands') ) ) {
			return;
		}

		if ( teckzone_is_vendor_page() ) {
			return;
		}

		$columns = teckzone_get_option( 'mobile_catalog_brands_columns' );

		$taxonomy = 'product_brand';
		$term_args = [
			'taxonomy' => $taxonomy,
			'number' => absint( teckzone_get_option( 'mobile_catalog_brands_per_show' ) )
		];

		$terms = get_terms( $term_args );

		if ( is_wp_error( $terms ) || empty( $terms ) ) {
			return;
		}

		$output = [];

		foreach( $terms as $term ) {
			$thumbnail_id = absint( get_term_meta( $term->term_id, 'brand_thumbnail_id', true ) );

			if ( $thumbnail_id ) {
				$output[] = sprintf(
					'<div class="brand-item"><a href="%s">%s</a></div>',
					esc_url( get_term_link( $term->term_id, $taxonomy ) ),
					wp_get_attachment_image( $thumbnail_id, 'full' )
				);
			}
		}

		if ( ! empty( $output ) ) {
			echo '<div class="tz-catalog-brands-mobile columns-' . esc_attr( $columns ) . '"><div class="brands-wrapper">' . implode( '', $output ) . '</div></div>';
		}
	}

	function mobile_catalog_products_found() {
		global $wp_query;
		$total = $wp_query->found_posts;

		echo '<div class="products-found"><span>' . $total . '</span>' . esc_html__( ' Products found', 'teckzone' ) . '</div>';
	}

	function mobile_list_filter() {
		if ( ! class_exists( 'Teckzone_Widget_Layered_Nav_Filters' ) ) {
			return;
		}

		$widget = 'Teckzone_Widget_Layered_Nav_Filters';
		$instance = [
			'title'  => esc_html__( 'Your filters', 'teckzone' ),
		];
		$args  = [
			'before_widget' => '<div class="list-filter-mobile"><div class="widget %s">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		];

		the_widget( $widget, $instance, $args );
	}

	function catalog_pagination(){
		if ( teckzone_is_vendor_page() ) {
			return teckzone_get_option('catalog_nav_type');
		} else {
			return teckzone_get_option('catalog_nav_type_mobile');
		}
	}

	function teckzone_catalog_nav_type_class(){
		return 'catalog-nav-' . teckzone_get_option('catalog_nav_type_mobile');
	}
}