<?php
/**
 * Template Product hooks.
 *
 * @package Teckzone
 */

/**
 * Class of general template.
 */
class Teckzone_WooCommerce_Template_Product {
	/**
	 * Initialize.
	 */
	public static function init() {
		add_action( 'teckzone_after_header', array( __CLASS__, 'single_product_breadcrumb' ) );

		add_action( 'wc_ajax_teckzone_fbt_add_to_cart', array( __CLASS__, 'fbt_add_to_cart' ) );

		add_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'wrapper_before_summary' ), 5 );
		add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'wrapper_after_summary' ), 5 );

		add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'wrapper_open_after_summary' ), 5 );
		add_action( 'woocommerce_after_single_product_summary', array(
			__CLASS__,
			'wrapper_close_after_summary'
		), 999 );

		add_filter( 'woocommerce_product_thumbnails_columns', array( __CLASS__, 'get_product_thumbnails_columns' ) );

		add_filter( 'woocommerce_get_availability_text', array( __CLASS__, 'get_product_availability_text' ), 20, 2 );

		add_action( 'woocommerce_product_meta_end', array( __CLASS__, 'get_product_share_socials' ) );

		add_filter( 'woocommerce_product_description_heading', '__return_false' );
		add_filter( 'woocommerce_product_additional_information_heading', '__return_false' );

		// Change HTML for price
		add_filter( 'woocommerce_format_sale_price', array( __CLASS__, 'format_sale_price' ), 20, 3 );

		// Related Product
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
		if ( intval( teckzone_get_option( 'related_products' ) ) ) {
			add_action( 'teckzone_before_footer', array( __CLASS__, 'get_related_products' ), 20 );
		}
		add_filter( 'woocommerce_output_related_products_args', array( __CLASS__, 'get_related_products_args' ) );

		// Upsells Product
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

		if ( intval( teckzone_get_option( 'upsells_products' ) ) ) {
			add_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 8 );
		}

		add_filter( 'woocommerce_upsell_display_args', array( __CLASS__, 'get_upsell_products_args' ) );

		// Instagram
		add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'product_instagram_photos' ), 12 );

		add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'fbt_product' ), 5 );

		add_filter( 'woocommerce_add_to_cart_redirect', array( __CLASS__, 'buy_now_redirect' ), 99 );

		// Review Section
		add_filter( 'woocommerce_review_gravatar_size', array( __CLASS__, 'tz_review_avatar_size' ) );
		remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10 );
		add_action( 'woocommerce_review_meta', 'woocommerce_review_display_rating', 30 );

		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

		// Add vendors product single
		if ( teckzone_get_option( 'product_layout' ) == '1' ) {
			add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'template_single_summary_header' ), 3 );
		}

		// Add product features
		add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'single_product_features' ), 35 );

		// Remove default tab title
		add_filter(
			'woocommerce_product_additional_information_tab_title', array(
				__CLASS__,
				'teckzone_product_additional_information_tab_title'
			)
		);

		// Single Product Button

		add_action( 'woocommerce_before_add_to_cart_button', array( __CLASS__, 'open_button_group' ), 10 );
		add_action( 'woocommerce_before_single_product', array( __CLASS__, 'group_elements' ), 10 );
		add_action( 'woocommerce_after_add_to_cart_button', array( __CLASS__, 'button_group' ), 50 );
		add_action( 'woocommerce_after_add_to_cart_button', array( __CLASS__, 'close_action_button' ), 70 );
		add_action( 'woocommerce_after_add_to_cart_button', array( __CLASS__, 'close_button_group' ), 100 );

		add_filter( 'yith_wcwl_positions', array( __CLASS__, 'yith_wcwl_positions' ) );

		add_filter( 'tawc_deals_l10n_text', array( __CLASS__, 'deals_l10n_text' ) );
		add_filter( 'tawc_deals_expire_text', array( __CLASS__, 'deals_expire_text' ) );

		if ( teckzone_get_option( 'product_layout' ) == '1' ) {
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

			if ( class_exists( 'TAWC_Deals_Frontend' ) ) {
				remove_action(
					'woocommerce_single_product_summary', array(
					TAWC_Deals_Frontend::instance(),
					'single_product_template',
				), 25
				);
			}

			add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'get_extra_summary' ), 4 );

			add_action(
				'woocommerce_single_product_summary', array(
				__CLASS__,
				'open_single_product_summary_content'
			), 1
			);
			add_action(
				'woocommerce_single_product_summary', array(
				__CLASS__,
				'close_single_product_summary_content'
			), 70
			);

			add_action(
				'woocommerce_single_product_summary', array(
				__CLASS__,
				'open_single_product_summary_sidebar'
			), 70
			);
			add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 80 );
			if ( class_exists( 'TAWC_Deals_Frontend' ) ) {
				add_action(
					'woocommerce_single_product_summary', array(
					TAWC_Deals_Frontend::instance(),
					'single_product_template',
				), 85
				);
			}
			add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 90 );
			add_action(
				'woocommerce_single_product_summary', array(
				__CLASS__,
				'close_single_product_summary_sidebar'
			), 100
			);
		}

		if ( teckzone_get_option( 'product_layout' ) == '3' ) {
			add_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'get_product_header' ), 2 );
		} else {
			add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'get_product_header' ), 2 );
		}

		add_action( 'template_redirect', array( __CLASS__, 'track_product_view' ) );

		add_action( 'wp_footer', array( __CLASS__, 'sticky_product_info' ) );

		// Quick View
		add_action( 'teckzone_before_single_product_quickview_summary', 'woocommerce_show_product_images', 20 );
		add_action( 'teckzone_before_quickview_product', array( __CLASS__, 'group_elements' ), 10 );
		add_action( 'teckzone_single_product_quickview_summary', array(
			__CLASS__,
			'get_quickview_product_header'
		), 10 );
		add_action( 'teckzone_single_product_quickview_summary', array( __CLASS__, 'single_product_brand' ), 15 );
		add_action( 'teckzone_single_product_quickview_summary', 'woocommerce_template_single_price', 15 );
		add_action( 'teckzone_single_product_quickview_summary', 'woocommerce_template_single_excerpt', 20 );
		add_action( 'teckzone_single_product_quickview_summary', 'woocommerce_template_single_add_to_cart', 30 );
		add_action( 'teckzone_single_product_quickview_summary', 'woocommerce_template_single_meta', 40 );
		add_action( 'teckzone_single_product_quickview_summary', array(
			__CLASS__,
			'template_single_summary_header'
		), 15 );


		// single product deal 2
		add_action( 'teckzone_before_single_product_deal_2', array( __CLASS__, 'group_elements' ), 10 );
		add_action( 'teckzone_before_single_product_deal_2_summary', 'woocommerce_show_product_images', 20 );

		add_action( 'teckzone_single_product_deal_2_content', 'Teckzone_WooCommerce_Template_Catalog::template_loop_product_title', 5 );
		add_action( 'teckzone_single_product_deal_2_content', 'woocommerce_template_single_rating', 10 );
		add_action( 'teckzone_single_product_deal_2_content', 'woocommerce_template_single_price', 15 );
		add_action( 'teckzone_single_product_deal_2_content', 'woocommerce_template_single_excerpt', 20 );
		add_action( 'teckzone_single_product_deal_2_content', array(
			__CLASS__,
			'single_product_deals_progress_bar'
		), 25 );
		add_action( 'teckzone_single_product_deal_2_sidebar', array(
			__CLASS__,
			'single_product_deals_countdown'
		), 10 );
		add_action( 'teckzone_single_product_deal_2_sidebar', 'woocommerce_template_single_add_to_cart', 20 );

		// Popup add to cart
		add_action( 'teckzone_before_product_popup_atc_notice', array( __CLASS__, 'get_box_notice' ), 5 );
		add_action( 'teckzone_before_product_popup_atc_recommendation', array(__CLASS__,'get_box_product_recommendation'), 15 );
	}

	/**
	 * Product Breadcrumb
	 */
	public static function single_product_breadcrumb(){
		if ( ! is_product() ) {
			return;
		}

		$container = apply_filters( 'teckzone_single_product_breadcrumb_container', teckzone_content_container_class() );

		ob_start();
		teckzone_breadcrumbs(
			array(
				'taxonomy' => function_exists( 'is_woocommerce' ) && is_woocommerce() ? 'product_cat' : 'category',
			)
		);

		$output = sprintf( 
			'<div class="product-breadcrumb"><div class="%s">%s</div></div>',
			esc_attr( $container ),
			ob_get_clean()
		);

		$output = apply_filters( 'teckzone_single_product_breadcrumb', $output );

		echo wp_kses_post($output);
	}

	/**
	 * Before Summary
	 *
	 * @return void
	 */
	public static function wrapper_before_summary() {
		?>
        <div class="teckzone-product-detail clearfix">
		<?php
	}

	/**
	 * After Summary
	 *
	 * @return void
	 */
	public static function wrapper_after_summary() {
		?>
        </div>
		<?php
	}

	public static function open_single_product_summary_content() {
		echo '<div class="entry-summary-content">';
	}

	public static function close_single_product_summary_content() {
		echo '</div>';
	}

	/**
	 * Before Summary
	 *
	 * @return void
	 */
	public static function open_single_product_summary_sidebar() {
		?>
        <div class="entry-summary-sidebar">
		<?php
	}

	/**
	 * After Summary
	 *
	 * @return void
	 */
	public static function close_single_product_summary_sidebar() {
		?>
        </div>
		<?php
	}

	/**
	 * Before Summary
	 *
	 * @return void
	 */
	public static function wrapper_open_after_summary() {
		?>
        <div class="tz-product-summary">
		<?php
	}

	/**
	 * After Summary
	 *
	 * @return void
	 */
	public static function wrapper_close_after_summary() {
		?>
        </div>
		<?php
	}

	/**
	 * Product Header
	 *
	 * @return void
	 */
	public static function get_quickview_product_header() {
		global $product;
		?>
        <div class="tz-entry-product-header">
            <div class="product-header-left">
				<?php
				if ( function_exists( 'woocommerce_template_single_title' ) ) {
					woocommerce_template_single_title();
				}
				?>
                <div class="product-entry-meta">
					<?php

					if ( function_exists( 'woocommerce_template_single_rating' ) && $product->get_rating_count() ) {
						echo '<div class="meta-rating">';
						woocommerce_template_single_rating();
						echo '</div>';
					}

					self::single_product_condition();
					self::single_product_sku();
					self::template_single_summary_header();
					?>
                </div>
            </div>
        </div>
		<?php
	}

	/**
	 * Product Header
	 *
	 * @return void
	 */
	public static function get_product_header() {
		global $product;
		$product_layout = teckzone_get_option( 'product_layout' );
		?>
        <div class="tz-entry-product-header">
            <div class="product-header-left">
				<?php
				if ( function_exists( 'woocommerce_template_single_title' ) ) {
					woocommerce_template_single_title();
				}
				?>
                <div class="product-entry-meta">
					<?php

					if ( function_exists( 'woocommerce_template_single_rating' ) && $product->get_rating_count() ) {
						echo '<div class="meta-rating">';
						woocommerce_template_single_rating();
						echo '</div>';
					}

					self::single_product_condition();

					if ( $product_layout != '1' ) {
						self::single_product_sku();
					}

					if ( $product_layout == '3' ) {
						self::template_single_summary_header();
					}

					?>
                </div>
            </div>
			<?php
			if ( $product_layout == '3' ) {
				self::single_product_brand();
			}
			?>
        </div>
		<?php
	}

	/**
	 * Extra Summary
	 *
	 * @return void
	 */
	public static function get_extra_summary() {
		if ( teckzone_get_option( 'product_layout' ) != '1' ) {
			return;
		}

		echo '<div class="tz-extra-summary">';
		self::single_product_brand();
		self::single_product_sku();
		echo '</div>';
	}

	/**
	 * Get product SKU
	 */
	public static function single_product_sku() {
		global $product;
		if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
            <div class="meta-sku">
				<span class="meta-label">
				<?php echo apply_filters( 'teckzone_product_sku_meta_label', esc_html__( 'SKU:', 'teckzone' ) ); ?>
			  	</span>
                <span class="meta-value">
                    <?php
                    if ( $sku = $product->get_sku() ) {
	                    echo wp_kses_post( $sku );
                    } else {
	                    esc_html_e( 'N/A', 'teckzone' );
                    }
                    ?>
                </span>
            </div>
		<?php endif;
	}

	/**
	 * Get product condition
	 */
	public static function single_product_condition() {
		global $product;
		$tax  = 'product_condition';
		$id   = $product->get_id();
		$term = get_the_terms( $id, $tax );

		if ( is_wp_error( $term ) || ! $term ) {
			return;
		}

		?>
        <div class="meta-product_condition">
			<span class="meta-label">
				<?php echo apply_filters( 'teckzone_product_condition_meta_label', esc_html__( 'Condition:', 'teckzone' ) ); ?>
			</span>
            <span class="meta-value">
				<?php printf( '<a href="%s">%s</a>', esc_url( get_term_link( $term[0]->term_id, $tax ) ), esc_html( $term[0]->name ) ); ?>
			</span>
        </div>
		<?php
	}

	/**
	 * Get product brand
	 */
	public static function single_product_brand() {
		global $product;
		$tax  = 'product_brand';
		$id   = $product->get_id();
		$term = get_the_terms( $id, $tax );

		if ( is_wp_error( $term ) || ! $term ) {
			return;
		}

		$thumbnail_id = absint( get_term_meta( $term[0]->term_id, 'brand_thumbnail_id', true ) );

		$image_full       = wp_get_attachment_image_src( $thumbnail_id, 'full' );
		$image_full_ratio = apply_filters( 'teckzone_product_brand_meta_thumbnail_ratio', 71 );

		$size = [
			'width'  => floor( $image_full[1] * absint( $image_full_ratio ) / 100 ),
			'height' => floor( $image_full[2] * absint( $image_full_ratio ) / 100 )
		];

		$small_thumbnail_size = apply_filters( 'teckzone_product_brand_meta_thumbnail', array(
			$size['width'],
			$size['height']
		) );

		if ( $thumbnail_id ) : ?>
            <div class="meta-product_brand">
				<?php printf( '<a href="%s">%s</a>', esc_url( get_term_link( $term[0]->term_id, $tax ) ), wp_get_attachment_image( $thumbnail_id, $small_thumbnail_size ) ); ?>
            </div>
		<?php endif;
	}

	/**
	 * Get product features
	 */
	public static function single_product_features() {
		global $product;
		$features = get_post_meta( $product->get_id(), 'product_feature_items', true );

		if ( ! $features ) {
			return;
		}

		echo '<div class="product-extra-content">' . wp_kses_post( $features ) . '</div>';
	}


	/**
	 * Simple stock
	 */
	public static function template_single_summary_header() {
		global $product;
		$output = array();

		ob_start();
		do_action( 'teckzone_single_product_header' );
		$output[] = ob_get_clean();

		$output[] = self::get_sold_by_vendor();

		if ( teckzone_get_option( 'product_layout' ) != '3' ) {
			if ( in_array( $product->get_type(), array( 'simple', 'variable' ) ) ) {
				$output[] = sprintf( '<div class="tz-summary-meta">%s</div>', wc_get_stock_html( $product ) );
			}
		}

		echo implode( ' ', $output );
	}

	public static function get_sold_by_vendor() {

		if ( ! intval( teckzone_get_option( 'single_product_sold_by' ) ) ) {
			return;
		}

		global $product;
		$output = $sold_by_label = $sold_by_name = '';
		if ( function_exists( 'dokan_get_store_url' ) ) {
			$author_id = get_post_field( 'post_author', $product->get_id() );
			$author    = get_user_by( 'id', $author_id );
			$shop_info = get_user_meta( $author_id, 'dokan_profile_settings', true );
			$shop_name = $author->display_name;
			if ( $shop_info && isset( $shop_info['store_name'] ) && $shop_info['store_name'] ) {
				$shop_name = $shop_info['store_name'];
			}

			$sold_by_label = esc_html__( 'Sold by', 'teckzone' );
			$sold_by_name  = '<a href="' . esc_url( dokan_get_store_url( $author_id ) ) . '">' . $shop_name . '</a>';
		}

		elseif ( class_exists( 'WCFMmp' ) ) {
			global $WCFM, $post;

			$vendor_id    = $WCFM->wcfm_vendor_support->wcfm_get_vendor_id_from_product( $post->ID );
			$sold_by_text = apply_filters( 'wcfmmp_sold_by_label', esc_html__( 'Sold by', 'teckzone' ) );
			$store_name   = $WCFM->wcfm_vendor_support->wcfm_get_vendor_store_by_vendor( absint( $vendor_id ) );

			$sold_by_label = $sold_by_text;
			$sold_by_name  = $store_name;
		}

		$output = sprintf(
			'<div class="tz-summary-header">' .
			'<div class="tz-summary-meta">' .
			'<div class="sold-by-meta">' .
			'<span class="sold-by-label">%s </span>' .
			'%s' .
			'</div>' .
			'</div>' .
			'</div>',
			$sold_by_label,
			wp_kses_post( $sold_by_name )
		);

		return $output;
	}

	/**
	 * Product thumbnails columns
	 *
	 * @return int
	 */
	public static function get_product_thumbnails_columns() {
		return intval( teckzone_get_option( 'product_thumbnail_numbers' ) );
	}

	/**
	 * Product availability text
	 *
	 * @return string
	 */
	public static function get_product_availability_text( $availability, $product ) {
		if ( ! $product->get_type() == 'simple' ) {
			return $availability;
		}

		if ( ! $product->managing_stock() && $product->is_in_stock() ) {
			$availability = esc_html__( 'In stock', 'teckzone' );
		}

		return $availability;
	}

	/**
	 * Get product share socials
	 */
	public static function get_product_share_socials() {

		if ( ! function_exists( 'teckzone_addons_share_link_socials' ) ) {
			return;
		}

		if ( ! intval( teckzone_get_option( 'product_share_socials' ) ) ) {
			return;
		}

		$image   = get_the_post_thumbnail_url( get_the_ID(), 'full' );
		$socials = teckzone_get_option( 'product_social_icons' );
		echo '<div class="product_socials socials-with-background">';
		echo teckzone_addons_share_link_socials( $socials, get_the_title(), get_the_permalink(), $image );
		echo '</div>';
	}

	/**
	 * Add product tabs to product pages.
	 *
	 * @return string
	 */
	public static function teckzone_product_additional_information_tab_title( $title ) {
		$title = esc_html__( 'Specifications', 'teckzone' );

		return $title;
	}

	public static function get_related_products() {
		if ( ! is_singular( 'product' ) ) {
			return;
		}
		woocommerce_output_related_products();
	}

	/**
	 * Related products args
	 *
	 * @return array
	 */
	public static function get_related_products_args( $args ) {
		$args['posts_per_page'] = intval( teckzone_get_option( 'related_products_numbers' ) );
		$args['columns']        = intval( teckzone_get_option( 'related_products_columns' ) );

		return $args;
	}

	/**
	 * Upsells products args
	 *
	 * @return array
	 */
	public static function get_upsell_products_args( $args ) {
		$args['posts_per_page'] = intval( teckzone_get_option( 'upsells_products_numbers' ) );

		return $args;
	}

	/**
	 * Wrap button group
	 * Open a div
	 *
	 * @since 1.0
	 */
	public static function open_button_group() {
		echo '<div class="single-button-wrapper">';
	}

	/**
	 * Wrap button group
	 * Close a div
	 *
	 * @since 1.0
	 */
	public static function close_button_group() {
		echo '</div>';
	}

	/**
	 * Wrap button group
	 * Open a div
	 *
	 * @since 1.0
	 */
	public static function open_action_button() {
		echo '<div class="action-buttons">';
	}

	/**
	 * Wrap button group
	 * Close a div
	 *
	 * @since 1.0
	 */
	public static function close_action_button() {
		echo '</div>';
	}

	/**
	 * Wrap button box
	 * Open a div
	 *
	 * @since 1.0
	 */
	public static function open_button_box() {
		echo '<div class="buttons-box">';
	}

	/**
	 * Wrap button box
	 * Close a div
	 *
	 * @since 1.0
	 */
	public static function close_button_box() {
		echo '</div>';
	}

	/**
	 * @since 1.0
	 */
	public static function group_elements() {
		global $product;

		if ( $product->get_type() == 'grouped' || $product->get_type() == 'external' ) {
			add_action( 'woocommerce_before_add_to_cart_button', array( __CLASS__, 'open_action_button' ), 10 );
			add_action( 'woocommerce_before_add_to_cart_button', array( __CLASS__, 'open_button_box' ), 10 );
			add_action( 'woocommerce_before_add_to_cart_button', array( __CLASS__, 'product_vendor_button' ), 10 );
			add_action( 'woocommerce_before_add_to_cart_button', array( __CLASS__, 'product_buy_now_button' ), 20 );
			add_action( 'woocommerce_after_add_to_cart_button', array( __CLASS__, 'close_button_box' ), 45 );

		} else {
			add_action( 'woocommerce_after_add_to_cart_quantity', array( __CLASS__, 'open_action_button' ), 10 );
			add_action( 'woocommerce_after_add_to_cart_quantity', array( __CLASS__, 'open_button_box' ), 10 );
			add_action( 'woocommerce_after_add_to_cart_quantity', array( __CLASS__, 'product_vendor_button' ), 10 );
			add_action( 'woocommerce_after_add_to_cart_quantity', array( __CLASS__, 'product_buy_now_button' ), 20 );
			add_action( 'woocommerce_after_add_to_cart_button', array( __CLASS__, 'close_button_box' ), 45 );
		}
	}

	/**
	 * Wrap button group
	 * Close a div
	 *
	 * @since 1.0
	 */
	public static function button_group() {
		echo '<div class="group-buttons">';
		self::yith_button();
		echo '<div class="divider"></div>';
		self::compare_button();
		echo '</div>';
	}

	/**
	 * Display button vendor
	 *
	 * @since 1.0
	 */
	public static function product_vendor_button() {
		if ( ! intval( teckzone_get_option( 'single_product_sold_by' ) ) ) {
			return;
		}

		global $product;
		$sold_by_url = '';
		if ( function_exists( 'dokan_get_store_url' ) ) {
			$author_id = get_post_field( 'post_author', $product->get_id() );
			$sold_by_url  = esc_url( dokan_get_store_url( $author_id ) );
		}

        elseif ( class_exists( 'WCFMmp' ) ) {
			global $WCFM, $post;

			$vendor_id    = $WCFM->wcfm_vendor_support->wcfm_get_vendor_id_from_product( $post->ID );
			$shop_link     = wcfmmp_get_store_url( $vendor_id );
			$sold_by_url   = apply_filters( 'wcfmmp_vendor_shop_permalink', $shop_link, $vendor_id );
		}

		echo sprintf(
			'<div class="button-vendor">
			<a href="%s"><i class="icon-store"></i>%s</a>
			</div>',
			$sold_by_url,
			esc_html__('Store', 'teckzone')
		);
	}

	/**
	 * Display wishlist button
	 *
	 * @since 1.0
	 */
	public static function yith_button() {

		if ( ! shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
			return;
		}

		echo '<div class="teckzone-wishlist-button">';
		echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
		echo '</div>';
	}

	public static function yith_wcwl_positions( $positions ) {
		$positions['add-to-cart'] = array( 'hook' => '', 'priority' => 31 );

		return $positions;
	}

	/**
	 * Display compare button
	 *
	 * @since 1.0
	 */
	public static function compare_button() {
		Teckzone_WooCommerce_Template_Catalog::product_compare();
	}

	/**
	 * Display buy now button
	 *
	 * @since 1.0
	 */
	public static function product_buy_now_button() {
		global $product;
		if ( ! intval( teckzone_get_option( 'product_buy_now' ) ) ) {
			return;
		}

		if ( $product->get_type() == 'external' ) {
			return;
		}

		echo sprintf( '<button class="buy_now_button button"><i class="icon-power"></i>%s</button>', wp_kses_post( teckzone_get_option( 'product_buy_now_text' ) ) );
	}

	public static function buy_now_redirect( $url ) {

		if ( ! isset( $_REQUEST['buy_now'] ) || $_REQUEST['buy_now'] == false ) {
			return $url;
		}

		if ( empty( $_REQUEST['quantity'] ) ) {
			return $url;
		}

		if ( is_array( $_REQUEST['quantity'] ) ) {
			$quantity_set = false;
			foreach ( $_REQUEST['quantity'] as $item => $quantity ) {
				if ( $quantity <= 0 ) {
					continue;
				}
				$quantity_set = true;
			}

			if ( ! $quantity_set ) {
				return $url;
			}
		}


		$redirect = teckzone_get_option( 'product_buy_now_link' );
		if ( empty( $redirect ) ) {
			return wc_get_checkout_url();
		} else {
			wp_safe_redirect( $redirect );
			exit;
		}
	}

	/**
	 * Getting parts of a price, in html, used by get_price_html.
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function format_sale_price( $price, $regular_price, $sale_price ) {
		$price = '<ins>' . ( is_numeric( $sale_price ) ? wc_price( $sale_price ) : $sale_price ) . '</ins><del>' . ( is_numeric( $regular_price ) ? wc_price( $regular_price ) : $regular_price ) . '</del>';

		return $price;
	}

	public static function deals_l10n_text( $deals ) {
		if ( is_product() && teckzone_get_option( 'product_layout' ) == '1' ) {
			$deals = array(
				'days'    => esc_html__( 'days', 'teckzone' ),
				'hours'   => esc_html__( 'hrs', 'teckzone' ),
				'minutes' => esc_html__( 'mins', 'teckzone' ),
				'seconds' => esc_html__( 'secs', 'teckzone' ),
			);
		}

		return $deals;
	}

	public static function deals_expire_text( $text ) {
		$text = '<strong>' . esc_html__( 'Hurry up!', 'teckzone' ) . '</strong>' . esc_html__( ' Sales ends in ', 'teckzone' ) . '<i class="icon-power"></i>';

		return $text;
	}

	public static function tz_review_avatar_size( $size ) {
		$size = '70';

		return $size;
	}

	/**
	 * Frequently Bought Together
	 */
	public static function fbt_product() {
		global $product;
		if ( ! intval( teckzone_get_option( 'product_fbt' ) ) ) {
			return;
		}

		$columns = intval( teckzone_get_option( 'product_fbt_columns' ) );

		$product_ids = maybe_unserialize( get_post_meta( $product->get_id(), 'tz_pbt_product_ids', true ) );
		$product_ids = apply_filters( 'teckzone_pbt_product_ids', $product_ids, $product );
		if ( empty( $product_ids ) || ! is_array( $product_ids ) ) {
			return;
		}

		$current_product = array( $product->get_id() );
		$product_ids     = array_merge( $current_product, $product_ids );
		$title           = teckzone_get_option( 'product_fbt_title' );

		$total_price  = 0;
		$product_list = array();

		$layout = teckzone_get_layout();
		?>
        <div class="tz-product-fbt" id="tz-product-fbt">
			<?php
			if ( 'full-content' == $layout ) {
				echo '<div class="tz-block-left"></div>';
				echo '<div class="tz-block-right"></div>';
			}
			?>
            <div class="tz-product-fbt__wrapper">
                <h3 class="fbt-title"><?php echo esc_html( $title ); ?></h3>

                <div class="fbt-content">
                    <ul class="products columns-<?php echo esc_attr( $columns ); ?>">
						<?php
						$duplicate_id = 0;
						$index        = 1;
						foreach ( $product_ids as $product_id ) {
							$product_id = apply_filters( 'wpml_object_id', $product_id, 'product' );
							$item       = wc_get_product( $product_id );

							if ( empty( $item ) ) {
								continue;
							}

							if ( $item->is_type( 'variable' ) ) {
								$key = array_search($product_id, $product_ids);
								if ( $key !== false ) {
									unset($product_ids[$key]);
								}
								continue;
							}
			
							if ( $item->get_stock_status() == 'outofstock' ) {
								$key = array_search($product_id, $product_ids);
								if ( $key !== false ) {
									unset($product_ids[$key]);
								}
								continue;
							}

							$data_id = $item->get_id();
							if ( $item->get_parent_id() > 0 ) {
								$data_id = $item->get_parent_id();
							}
							$total_price  += $item->get_price();
							$current_item = '';
							$css          = 'product-list-item';
							if ( $item->get_id() == $product->get_id() ) {
								$current_item = sprintf( '<strong>%s</strong>', esc_html__( 'This item:', 'teckzone' ) );
								$css          .= ' current-item';
							}

							$product_name = $item->get_title() . self::fbt_product_variation( $item );

							$product_list[] = sprintf(
								'<li class="%s">
									<a href="%s" data-id="%s" data-title="%s">
										<span class="p-title">%s %s</span>
									</a>
									<span class="s-price" data-price="%s">(%s)</span>
								</li>',
								esc_attr( $css ),
								esc_url( $item->get_permalink() ),
								esc_attr( $item->get_id() ),
								esc_attr( $product_name ),
								$current_item,
								$product_name,
								esc_attr( $item->get_price() ),
								$item->get_price_html()
							);

							$product_css = 'product';
							if ( $index % $columns == 0 ) {
								$product_css .= ' last';
							}
							?>
                            <li class="<?php echo esc_attr( $product_css ); ?>" 
								data-id="<?php echo esc_attr( $data_id ); ?>"
                                id="fbt-product-<?php echo esc_attr( $item->get_id() ); ?>">
                                <div class="product-thumbnail">
                                    <a class="thumbnail woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                       href="<?php echo esc_url( $item->get_permalink() ) ?>">
										<?php echo wp_get_attachment_image( $item->get_image_id(), 'shop_catalog' ); ?>
                                    </a>
                                </div>
                                <div class="product-detail">
									<?php
									$tax  = 'product_cat';
									$id   = $item->get_id();
									$term = get_the_terms( $id, $tax );
									if ( ! is_wp_error( $term ) && $term ) {
										$url = get_term_link( $term[0]->term_id, $tax );

										echo '<a href="' . esc_url( $url ) . '" class="tz-cat">' . esc_html( $term[0]->name ) . '</a>';
									}

									echo '<h2 class="woocommerce-loop-product__title">' .
									     '<a href="' . esc_url( get_the_permalink( $item->get_id() ) ) . '">' . esc_html( $product_name ) . '</a>' .
									     '</h2>';

									if ( wc_review_ratings_enabled() ) {
										$rating = $item->get_average_rating();
										$count  = $item->get_rating_count();
										$html   = '';
										if ( 0 < $rating ) {
											/* translators: %s: rating */
											$label = sprintf( esc_html__( 'Rated %s out of 5', 'teckzone' ), $rating );
											$html  = '<div class="star-rating" role="img" aria-label="' . esc_attr( $label ) . '">' . wc_get_star_rating_html( $rating, $count ) . '</div>';
											$html  = '<div class="tz-rating">' . $html . '<span class="count">(' . $count . ')</span></div>';
										}

										echo '' . $html;
									}

									if ( $price_html = $item->get_price_html() ) {
										echo '<span class="price">' . $price_html . '</span>';
									}

									if ( $duplicate_id != $data_id ) {
										if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
											echo do_shortcode( '[yith_wcwl_add_to_wishlist link_classes="fbt-wishlist add_to_wishlist single_add_to_wishlist" product_id ="' . $data_id . '"]' );
										}
										$duplicate_id = $data_id;
									}
									?>
                                </div>
                            </li>
							<?php

							$index ++;
						}
						?>
                    </ul>
                </div>
                <div class="fbt-footer">
                    <ul class="products-list">
						<?php echo implode( '', $product_list ); ?>
                    </ul>
                    <div class="product-buttons">
                        <div class="price-box">
                            <span class="label"><?php esc_html_e( 'Price for all: ', 'teckzone' ); ?></span>
                            <span class="s-price tz-total-price"><?php echo wc_price( $total_price ); ?></span>
                            <input type="hidden" data-price="<?php echo esc_attr( $total_price ); ?>"
                                   id="tz-data_price">
                        </div>

						<?php $product_ids = implode( ',', $product_ids ); ?>

                        <form class="fbt-cart" action="<?php echo esc_url( $product->get_permalink() ); ?>"
                              method="post"
                              enctype="multipart/form-data">
                            <button type="submit" name="tz-add-to-cart" value="<?php echo esc_attr( $product_ids ); ?>"
                                    class="tz_add_to_cart_button ajax_add_to_cart">
                                <i class="icon-plus"></i><?php esc_html_e( 'Add all to cart', 'teckzone' ); ?>
                            </button>
                        </form>
						<?php if ( function_exists( 'YITH_WCWL' ) ) : ?>
                            <a href="<?php echo esc_url( $product->get_permalink() ); ?>"
                               class="btn-add-to-wishlist tz-wishlist-button"><i
                                        class="icon-heart"></i><span><?php esc_html_e( 'Add all to Wishlist', 'teckzone' ); ?></span></a>
                            <a href="<?php echo esc_url( self::get_wishlist_url() ); ?>"
                               class="btn-view-to-wishlist tz-wishlist-button"><i
                                        class="icon-heart"></i><span><?php esc_html_e( 'Browse Wishlist', 'teckzone' ); ?></span></a>
						<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
		<?php
	}

	public static function fbt_product_variation( $product ) {
		$current_product_is_variation = $product->is_type( 'variation' );
		if ( ! $current_product_is_variation ) {
			return;
		}
		$attributes = $product->get_variation_attributes();
		$variations = array();
	
		foreach ( $attributes as $key => $attribute ) {
			$key = str_replace( 'attribute_', '', $key );
	
			$terms = get_terms( array(
				'taxonomy'   => sanitize_title( $key ),
				'menu_order' => 'ASC',
				'hide_empty' => false
			) );
	
			foreach ( $terms as $term ) {
				if ( ! is_object( $term ) || ! in_array( $term->slug, array( $attribute ) ) ) {
					continue;
				}
				$variations[] = $term->name;
			}
		}
	
		if ( ! empty( $variations ) ) {
			return ' &ndash; ' . implode( ', ', $variations );
		}
	
	}

	public static function get_wishlist_url() {
		if ( function_exists( 'YITH_WCWL' ) ) {
			return YITH_WCWL()->get_wishlist_url();
		} else {
			$wishlist_page_id = get_option( 'yith_wcwl_wishlist_page_id' );

			return get_the_permalink( $wishlist_page_id );
		}
	}

	/**
	 * AJAX add to cart.
	 */
	public static function fbt_add_to_cart() {
		$product_ids = $_POST['product_ids'];
		$quantity    = 1;
		$product_ids = explode( ',', $product_ids );
		if ( is_array( $product_ids ) ) {
			foreach ( $product_ids as $product_id ) {
				if ( $product_id == 0 ) {
					continue;
				}
				$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
				$product_status    = get_post_status( $product_id );

				if ( $passed_validation && false !== WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {

					do_action( 'woocommerce_ajax_added_to_cart', $product_id );

					if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) {
						wc_add_to_cart_message( array( $product_id => $quantity ), true );
					}


				} else {

					// If there was an error adding to the cart, redirect to the product page to show any errors
					$data = array(
						'error'       => true,
						'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id ),
					);

					wp_send_json( $data );
				}
			}
		}
	}

	/**
	 * Display instagram photos 
	 *
	 * @return string
	 */
	public static function product_instagram_photos() {

		if ( ! intval( teckzone_get_option( 'product_instagram' ) ) ) {
			return;
		}

		if ( ! is_singular( 'product' ) ) {
			return;
		}

		if ( get_query_var( 'edit' ) && is_singular( 'product' ) ) {
			return;
		}

		$numbers    = teckzone_get_option( 'product_instagram_numbers' );
		$title      = teckzone_get_option( 'product_instagram_title' );
		$columns    = teckzone_get_option( 'product_instagram_columns' );
		$container_width = teckzone_get_option( 'product_container_width' );
		
		$instagram_array = self::instagram_get_photos_by_token($numbers);

		$columns = intval( $columns );

		$carousel_settings = array(
			'slidesToShow'   => $columns,
			'slidesToScroll' => $columns,
			'responsive'     => array(
				array(
					'breakpoint' => 1750,
					'settings'   => array(
						'dots'   => $container_width == 'full-width' || $container_width == 'wide' ? true : false,
						'arrows' => $container_width == 'full-width' || $container_width == 'wide' ? true : false,
					)
				),
				array(
					'breakpoint' => 1200,
					'settings'   => array(
						'dots'   => true,
						'arrows' => false,
					)
				),
				array(
					'breakpoint' => 992,
					'settings'   => array(
						'dots'   => true,
						'arrows' => false,
						'slidesToShow'   => 3,
						'slidesToScroll' => 3,
					)
				),
				array(
					'breakpoint' => 768,
					'settings'   => array(
						'dots'           => true,
						'arrows'         => false,
						'slidesToShow'   => 2,
						'slidesToScroll' => 2,
					)
				)
			)
		);

		$carousel_settings = apply_filters( 'teckzone_instagram_products_carousel_settings', $carousel_settings );

		echo '<div class="teckzone-wc-products-carousel teckzone-product-instagram" data-settings="' . esc_attr( wp_json_encode( $carousel_settings ) ) . '">';
		echo sprintf( '<h2>%s</h2>', wp_kses( $title, wp_kses_allowed_html( 'post' ) ) );
		echo '<ul class="products">';

		$output = array();

		if ( is_wp_error( $instagram_array ) ) {
			echo wp_kses_post( $instagram_array->get_error_message() );
		} elseif ( $instagram_array ) {
			$count = 0;
			foreach ( $instagram_array as $instagram_item ) {

				$image_html = sprintf( '<img src="%s" alt="%s">', esc_url( $instagram_item['images']['thumbnail'] ), esc_attr( '' ) );

				$output[] = '<li class="product">' .
				            '<a class="insta-item" href="' . esc_url( $instagram_item['link'] ) . '" target="_blank">' .
				            $image_html . '<i class="icon_social_instagram social_instagram"></i>'  . '</a>' .
				            '</li>' . "\n";

				$count ++;
				$numbers = intval( $numbers );
				if ( $numbers > 0 ) {
					if ( $count == $numbers ) {
						break;
					}
				}
			}

			if ( ! empty( $output ) ) {
				echo implode( '', $output );
			} else {
				esc_html_e( 'Instagram did not return any images.', 'teckzone' );
			}
		} else {
			esc_html_e( 'Instagram did not return any images.', 'teckzone' );
		}

		echo '</ul></div>';
	}

	/**
	 * Get Instagram images
	 *
	 * @param int $limit
	 *
	 * @return array|WP_Error
	 */
	public static function instagram_get_photos_by_token( $limit ) {
		$access_token = teckzone_get_option( 'instagram_token' );

		if ( empty( $access_token ) ) {
			return new WP_Error( 'instagram_no_access_token', esc_html__( 'No access token', 'teckzone' ) );
		}
		$user = self::teckzone_get_instagram_user();
		if ( ! $user || is_wp_error( $user ) ) {
			return $user;
		}
		if (isset($user['error'])) {
			return new WP_Error( 'instagram_no_images', esc_html__( 'Instagram did not return any images. Please check your access token', 'teckzone' ) );
		} else {
			$transient_key = 'teckzone_instagram_photos_' . sanitize_title_with_dashes( $user['username'] . '__' . $limit );
			$images = get_transient( $transient_key );

			$images = array();
			$next = false;
			while ( count( $images ) < $limit ) {
				if ( ! $next ) {
					$fetched = self::teckzone_fetch_instagram_media( $access_token );
				} else {
					$fetched = self::teckzone_fetch_instagram_media( $next );				
				}
				if ( is_wp_error( $fetched ) ) {
					break;
				}

				$images = array_merge( $images, $fetched['images'] );
				$next = $fetched['paging']['cursors']['after'];
			}
			
			if ( ! empty( $images ) ) {
				set_transient( $transient_key, $images, 2 * 3600 ); // Cache for 2 hours.
			}
			if ( ! empty( $images ) ) {

				return $images;

			} else {
				return new WP_Error( 'instagram_no_images', esc_html__( 'Instagram did not return any images.', 'teckzone' ) );
			}
		}
	}
	/**
	 * Fetch photos from Instagram API
	 *
	 * @param  string $access_token
	 * @return array
	 */
	public static function teckzone_fetch_instagram_media( $access_token ) {
		$url = add_query_arg( array(
			'fields'       => 'id,caption,media_type,media_url,permalink,thumbnail_url',
			'access_token' => $access_token,
		), 'https://graph.instagram.com/me/media' );

		$remote = wp_remote_retrieve_body( wp_remote_get( $url ) );
		$data   = json_decode( $remote, true );

		$images = array();
		if ( isset( $data['error'] ) ) {
			return new WP_Error( 'instagram_error', $data['error']['message'] );
		} else {
			foreach ( $data['data'] as $media ) {
				
				 $images[] = array(
					'type'    => $media['media_type'],
					'caption' => isset( $media['caption'] ) ? $media['caption'] : $media['id'],
					'link'    => $media['permalink'],
					'images'  => array(
						'thumbnail' => $media['media_url'],
						'original'  => $media['media_url'],
					),
				);
				
			}
		}
		return array(
			'images' => $images,
			'paging' => $data['paging'],
		);
	}
	/**
	 * Get user data
	 *
	 * @return bool|WP_Error|array
	 */
	public static function teckzone_get_instagram_user() {
		$access_token = teckzone_get_option( 'instagram_token' );
		if ( empty( $access_token ) ) {
			return new WP_Error( 'no_access_token', esc_html__( 'No access token', 'teckzone' ) );
		}
		$user = get_transient( 'teckzone_instagram_user_' . $access_token );
		if ( false === $user ) {
			$url  = add_query_arg( array( 'fields' => 'id,username', 'access_token' => $access_token ), 'https://graph.instagram.com/me' );
			$data = wp_remote_get( $url );
			$data = wp_remote_retrieve_body( $data );
			if ( ! $data ) {
				return new WP_Error( 'no_user_data', esc_html__( 'No user data received', 'teckzone' ) );
			}
			$user = json_decode( $data, true );
			if ( ! empty( $data ) ) {
				set_transient( 'teckzone_instagram_user', $user, 2592000 ); // Cache for one month.
			}
		}
		return $user;
	}


	/**
	 * Track product views.
	 */
	public static function track_product_view() {
		if ( ! is_singular( 'product' ) ) {
			return;
		}

		global $post;

		if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) ) {
			$viewed_products = array();
		} else {
			$viewed_products = (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] );
		}

		if ( ! in_array( $post->ID, $viewed_products ) ) {
			$viewed_products[] = $post->ID;
		}

		if ( sizeof( $viewed_products ) > 15 ) {
			array_shift( $viewed_products );
		}

		// Store for session only
		wc_setcookie( 'woocommerce_recently_viewed', implode( '|', $viewed_products ), time() + 60 * 60 * 24 * 30 );
	}

	public static function sticky_product_info() {

		if ( ! is_singular( 'product' ) ) {
			return;
		}
		$sticky_product = apply_filters( 'teckzone_sticky_product_info', teckzone_get_option( 'sticky_product_info' ) );
		if ( ! intval( $sticky_product ) ) {
			return;
		}

		wc_get_template( 'single-product/sticky-product-info.php' );
	}

	/**
	 * Display countdown and sold items in single product page
	 */
	public static function single_product_deals() {
		global $product;

		if ( ! function_exists( 'tawc_is_deal_product' ) ) {
			return;
		}

		if ( ! tawc_is_deal_product( $product ) ) {
			return;
		}

		$expire_date = ! empty( $product->get_date_on_sale_to() ) ? $product->get_date_on_sale_to()->getOffsetTimestamp() : '';
		$expire_date = apply_filters( 'tawc_deals_expire_timestamp', $expire_date, $product );

		if ( empty( $expire_date ) ) {
			return;
		}

		$now = strtotime( current_time( 'Y-m-d H:i:s' ) );

		$expire = $expire_date - $now;

		$limit     = get_post_meta( $product->get_id(), '_deal_quantity', true );
		$sold      = intval( get_post_meta( $product->get_id(), '_deal_sales_counts', true ) );
		$available = $limit - $sold;

		if ( $expire <= 0 ) {
			return;
		}

		$deals = array(
			'days'    => esc_html__( 'days', 'teckzone' ),
			'hours'   => esc_html__( 'hours', 'teckzone' ),
			'minutes' => esc_html__( 'mins', 'teckzone' ),
			'seconds' => esc_html__( 'secs', 'teckzone' ),
		);

		$expire_text = '<div><strong>' . esc_html__( 'Hurry up!', 'teckzone' ) . '</strong></div>' . '<div>' . esc_html__( 'This deal will end in:', 'teckzone' ) . '</div>';

		$percentage = $sold / $limit * 100;

		?>
        <div class="tawc-deal deal">
			<?php if ( $expire ) : ?>
                <div class="deal-expire-date">
                    <div class="deal-expire-text"><?php echo apply_filters( 'single_product_deals_expire_text', $expire_text ) ?></div>
                    <div class="deal-expire-countdown" data-expire="<?php echo esc_attr( $expire ) ?>"
                         data-text="<?php echo esc_attr( wp_json_encode( $deals ) ) ?>"></div>
                </div>
			<?php endif; ?>

            <div class="deal-sold">
                <div class="deal-sold-text"><?php echo apply_filters( 'single_product_deals_sold_text', esc_html__( 'Sold items', 'teckzone' ) ); ?></div>
                <div class="deal-progress">
                    <div class="progress-bar">
                        <div class="progress-value" style="width: <?php echo esc_attr( $percentage ) . '%' ?>"></div>
                    </div>
                </div>
                <div class="deal-text">
					<span class="available">
						<span class="text"><?php esc_html_e( 'Available: ', 'teckzone' ) ?></span>
						<span class="value"><?php echo wp_kses_post( $available ) ?></span>
					</span>
                    <span class="sold">
						<span class="text"><?php esc_html_e( 'Sold: ', 'teckzone' ) ?></span>
						<span class="value"><?php echo intval($sold) ?></span>
					</span>
                </div>
            </div>
        </div>
		<?php
	}

	public static function single_product_deals_countdown() {
		global $product;

		if ( ! function_exists( 'tawc_is_deal_product' ) ) {
			return;
		}

		if ( ! tawc_is_deal_product( $product ) ) {
			return;
		}

		$expire_date = ! empty( $product->get_date_on_sale_to() ) ? $product->get_date_on_sale_to()->getOffsetTimestamp() : '';
		$expire_date = apply_filters( 'tawc_deals_expire_timestamp', $expire_date, $product );

		if ( empty( $expire_date ) ) {
			return;
		}

		$now = strtotime( current_time( 'Y-m-d H:i:s' ) );

		$expire = $expire_date - $now;

		if ( $expire <= 0 ) {
			return;
		}

		$deals = array(
			'days'    => esc_html__( 'days', 'teckzone' ),
			'hours'   => esc_html__( 'hrs', 'teckzone' ),
			'minutes' => esc_html__( 'mins', 'teckzone' ),
			'seconds' => esc_html__( 'secs', 'teckzone' ),
		);

		$expire_text = '<div><strong>' . esc_html__( 'Hurry up!', 'teckzone' ) . '</strong></div>' . '<div>' . esc_html__( 'This deal will end in:', 'teckzone' ) . '</div>';

		?>
        <div class="tawc-deal deal">
			<?php if ( $expire ) : ?>
                <div class="deal-expire-date">
                    <div class="deal-expire-text"><?php echo apply_filters( 'single_product_deals_2_expire_text', $expire_text ) ?></div>
                    <div class="deal-expire-countdown" data-expire="<?php echo esc_attr( $expire ) ?>"
                         data-text="<?php echo esc_attr( wp_json_encode( $deals ) ) ?>"></div>
                </div>
			<?php endif; ?>
        </div>
		<?php
	}

	public static function single_product_deals_progress_bar() {
		global $product;

		if ( ! function_exists( 'tawc_is_deal_product' ) ) {
			return;
		}

		if ( ! tawc_is_deal_product( $product ) ) {
			return;
		}

		$expire_date = ! empty( $product->get_date_on_sale_to() ) ? $product->get_date_on_sale_to()->getOffsetTimestamp() : '';
		$expire_date = apply_filters( 'tawc_deals_expire_timestamp', $expire_date, $product );

		if ( empty( $expire_date ) ) {
			return;
		}

		$now = strtotime( current_time( 'Y-m-d H:i:s' ) );

		$expire = $expire_date - $now;

		$limit = get_post_meta( $product->get_id(), '_deal_quantity', true );
		$sold  = intval( get_post_meta( $product->get_id(), '_deal_sales_counts', true ) );

		if ( $expire <= 0 ) {
			return;
		}

		$percentage = $sold / $limit * 100;

		?>
        <div class="tawc-deal deal">
            <div class="deal-sold">
                <div class="deal-sold-text"><?php echo apply_filters( 'single_product_deals_2_sold_text', esc_html__( 'Sold items', 'teckzone' ) ); ?></div>
                <div class="deal-progress">
                    <div class="progress-bar">
                        <div class="progress-value" style="width: <?php echo esc_attr( $percentage ) . '%' ?>"></div>
                    </div>
                </div>
                <div class="deal-text">
					<span class="sold">
						<span class="text"><?php esc_html_e( 'Sold: ', 'teckzone' ) ?></span>
						<span class="value"><?php echo intval($sold) ?>/<?php echo ! empty( $limit ) ? $limit : '' ?></span>
					</span>
                </div>
            </div>
        </div>
		<?php
	}

	public static function get_box_notice() {
		global $product;

		$icon = '<i class="icon-checkmark-circle"></i>';
		$title = $product->get_title();
		$afterTitle = esc_html__('successfully added to you cart.','teckzone');

		$btnCart = sprintf('<a href="%s" class="cart-view-btn">%s (%s)</a>',esc_url(wc_get_cart_url()), esc_html__('View Cart','teckzone'),WC()->cart->get_cart_contents_count());

    	echo sprintf('<div class="tz-product-popup-atc__notice"> %s "<span class="title">%s</span>" %s %s</div>',$icon,$title,$afterTitle, $btnCart );

	}

	public static function get_box_product_recommendation() {
		global $product;

		$upsells = $product->get_upsells();

		if ($upsells){
			woocommerce_upsell_display();
		} else {
			woocommerce_output_related_products();
		}

	}
}
