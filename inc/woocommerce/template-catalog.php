<?php
/**
 * Template Product hooks.
 *
 * @package Teckzone
 */

/**
 * Class of general template.
 */
class Teckzone_WooCommerce_Template_Catalog {
	/**
	 * @var string shop view
	 */
	public static $catalog_view;

	/**
	 * @var string catalog layout
	 */
	public static $catalog_layout;

	/**
	 * @var array elements of current page
	 */
	public static $catalog_elements = array();

	/**
	 * Initialize.
	 */
	public static function init() {
		self::general_hooks();
		self::product_loop_content_hooks();
		self::catalog_hooks();
	}

	protected static function general_hooks() {
		// Parse query for shop products per page.
		add_action( 'parse_request', array( __CLASS__, 'parse_request' ) );
		add_filter( 'loop_shop_per_page', array( __CLASS__, 'products_per_page' ) );

		add_filter( 'posts_search', array( __CLASS__, 'product_search_sku' ), 9 );

		// Need an early hook to ajaxify update mini shop cart
		add_filter( 'woocommerce_add_to_cart_fragments', array( __CLASS__, 'add_to_cart_fragments' ) );

		// Add class on product
		add_filter( 'woocommerce_post_class', array( __CLASS__, 'product_classes' ) );

		// Remove shop page title
		add_filter( 'woocommerce_show_page_title', '__return_false' );

		// After output product categories
		add_action(
			'woocommerce_after_output_product_categories', array(
				__CLASS__,
				'teckzone_after_output_product_categories'
			)
		);

		add_action( 'template_redirect', array( __CLASS__, 'teckzone_template_redirect' ) );

		add_action( 'wp_ajax_update_total_price', array( __CLASS__, 'update_total_price' ) );
		add_action( 'wp_ajax_nopriv_update_total_price', array( __CLASS__, 'update_total_price' ) );

		add_action( 'wp_ajax_teckzone_product_quick_view', array( __CLASS__, 'product_quick_view' ) );
		add_action( 'wp_ajax_nopriv_teckzone_product_quick_view', array( __CLASS__, 'product_quick_view', ) );
		add_action( 'wc_ajax_product_quick_view', array( __CLASS__, 'product_quick_view' ) );

		add_action( 'wc_ajax_teckzone_product_popup_add_to_cart', array( __CLASS__, 'product_popup_add_to_cart' ) );
	}

	protected static function product_loop_content_hooks() {
		// Remove product title link
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
		// Remove product link
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
		// Remove add to cart link
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );

		/*------------------
			Product loop
		-------------------*/

		add_action( 'woocommerce_before_shop_loop_item', array( __CLASS__, 'open_product_inner' ), 5 );

		add_action( 'woocommerce_before_shop_loop_item', array( __CLASS__, 'open_product_thumbnail' ), 5 );
		add_action( 'woocommerce_before_shop_loop_item', array( __CLASS__, 'product_link_open' ), 7 );
		add_action( 'woocommerce_before_shop_loop_item_title', array( __CLASS__, 'product_link_close' ), 30 );
		add_action( 'woocommerce_before_shop_loop_item_title', array( __CLASS__, 'product_quickview' ), 35 );
		add_action( 'woocommerce_before_shop_loop_item_title', array( __CLASS__, 'product_attribute' ), 40 );
		add_action( 'woocommerce_before_shop_loop_item_title', array( __CLASS__, 'close_product_thumbnail' ), 50 );

		add_action( 'woocommerce_before_shop_loop_item_title', array( __CLASS__, 'open_product_details' ), 100 );

		add_action( 'woocommerce_shop_loop_item_title', array( __CLASS__, 'open_product_content_box' ), 3 );
		add_action( 'woocommerce_shop_loop_item_title', array( __CLASS__, 'tz_product_category' ), 5 );
		add_action( 'woocommerce_shop_loop_item_title', array( __CLASS__, 'template_loop_product_title' ), 10 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'product_deals_progress_bar' ), 25 );
		add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 30 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'close_product_content_box' ), 45 );

		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'open_product_right_box' ), 50 );
		add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 60 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'product_button' ), 90 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'close_product_right_box' ), 100 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'open_product_details_hover' ), 150 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'product_button' ), 180 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'product_attribute' ), 190 );
		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'close_product_details_hover' ), 200 );

		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'close_product_details' ), 250 );

		add_action( 'woocommerce_after_shop_loop_item', array( __CLASS__, 'close_product_inner' ), 300 );

		// Change HTML
		add_filter(
			'woocommerce_product_add_to_cart_text', array(
			__CLASS__,
			'external_product_add_to_cart_text'
		), 20, 2
		);
		add_filter( 'woocommerce_product_get_rating_html', array( __CLASS__, 'product_get_rating_html' ) );
		add_filter( 'woocommerce_get_price_html', array( __CLASS__, 'product_get_price_html' ), 20, 2 );

		/*----------------------
			Product list loop
		-----------------------*/
		add_action( 'teckzone_before_product_list_thumbnail', array( __CLASS__, 'open_product_inner' ), 5 );

		add_action( 'teckzone_product_list_thumbnail', array( __CLASS__, 'product_link_open' ), 10 );
		add_action( 'teckzone_product_list_thumbnail', 'woocommerce_template_loop_product_thumbnail', 20 );
		add_action( 'teckzone_product_list_thumbnail', array( __CLASS__, 'product_link_close' ), 100 );


		add_action( 'teckzone_product_list_details', array( __CLASS__, 'template_loop_product_title' ), 10 );
		add_action( 'teckzone_product_list_details', 'woocommerce_template_loop_rating', 30 );
		add_action( 'teckzone_product_list_details', 'woocommerce_template_loop_price', 40 );

		add_action( 'teckzone_after_product_list_details', array( __CLASS__, 'close_product_inner' ), 500 );

		/*----------------------
			Product 2 loop
		-----------------------*/
		add_action( 'teckzone_product_thumbnail_2', array( __CLASS__, 'product_link_open' ), 10 );
		add_action( 'teckzone_product_thumbnail_2', 'woocommerce_template_loop_product_thumbnail', 20 );
		add_action( 'teckzone_product_thumbnail_2', array( __CLASS__, 'product_link_close' ), 100 );

		add_action( 'teckzone_product_details_2', array( __CLASS__, 'tz_product_category' ), 5 );
		add_action( 'teckzone_product_details_2', array( __CLASS__, 'template_loop_product_title' ), 10 );
		add_action( 'teckzone_product_details_2', 'woocommerce_template_loop_rating', 30 );
		add_action( 'teckzone_product_details_2', 'woocommerce_template_loop_price', 40 );
	}

	protected static function catalog_hooks() {
		// Remove catalog ordering
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
		remove_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );

		/*-----------------------
			Catalog content
		-----------------------*/

		add_action( 'woocommerce_before_main_content', array( __CLASS__, 'catalog_products_main' ), 5 );

		// Catalog Title
		add_action( 'woocommerce_archive_description', array( __CLASS__, 'teckzone_catalog_title' ), 10 );

		// Catalog Header
		add_action( 'woocommerce_archive_description', array( __CLASS__, 'catalog_products_header' ), 30 );

		add_action( 'woocommerce_before_shop_loop', array( __CLASS__, 'shop_toolbar' ), 20 );

		add_action( 'woocommerce_before_shop_loop', array( __CLASS__, 'before_shop_loop' ), 40 );
		add_action( 'woocommerce_before_shop_loop', array( __CLASS__, 'shop_loading' ), 60 );

		add_action( 'woocommerce_after_shop_loop', array( __CLASS__, 'after_shop_loop' ), 100 );

		// Catalog Pagination
		remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination' );
		add_action( 'woocommerce_after_shop_loop', array( __CLASS__, 'pagination' ) );
		add_filter( 'woocommerce_pagination_args', array( __CLASS__, 'pagination_args' ) );
	}

	public static function teckzone_template_redirect() {
		self::$catalog_view     = isset( $_COOKIE['catalog_view'] ) ? $_COOKIE['catalog_view'] : teckzone_get_option( 'catalog_view' );
		self::$catalog_layout   = teckzone_get_catalog_layout();
		self::$catalog_elements = self::get_catalog_elements();
	}

	/**
	 * Get catalog elements
	 *
	 * @since 1.0
	 */
	public static function get_catalog_elements() {
		$elements = array();
		if ( function_exists( 'is_shop' ) && is_shop() ) {
			$elements = (array) teckzone_get_option( 'shop_els' );

		} elseif ( function_exists( 'is_product_category' ) && is_product_category() && teckzone_get_product_category_level() == 0 ) {
			$elements = (array) teckzone_get_option( 'products_cat_level_1_els' );

			if ( function_exists( 'get_term_meta' ) ) {
				$queried_object = get_queried_object();
				$term_id        = $queried_object->term_id;
				$elements       = (array) get_term_meta( $term_id, 'product_cat_1_els', true );
			}
		}

		$elements = apply_filters( 'teckzone_catalog_elements', $elements );

		return $elements;
	}

	/**
	 * Ajaxify update cart viewer
	 *
	 * @since 1.0
	 *
	 * @param array $fragments
	 *
	 * @return array
	 */
	public static function add_to_cart_fragments( $fragments ) {
		global $woocommerce;

		if ( empty( $woocommerce ) ) {
			return $fragments;
		}

		$fragments['.teckzone-mini-cart-counter'] = '<span class="mini-item-counter teckzone-mini-cart-counter">' . WC()->cart->get_cart_contents_count() . '</span>';

		$fragments['.teckzone-price-total'] = '<span class="teckzone-price-total">' . WC()->cart->get_cart_total() . '</span>';

		return $fragments;
	}

	/**
	 * Parse request to change the shop columns and products per page
	 */
	public static function parse_request() {
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
			return;
		}

		if ( isset( $_REQUEST['products_per_page'] ) ) {
			wc_setcookie( 'products_per_page', intval( $_REQUEST['products_per_page'] ) );
		}
	}

	/**
	 * Change the products per page.
	 *
	 * @param  int $per_page The default value.
	 *
	 * @return int
	 */
	public static function products_per_page( $per_page ) {
		if ( is_search() ) {
			if ( isset( $_POST['search_per_page'] ) ) {
				$per_page = intval( $_REQUEST['search_per_page'] );
			}
		} else {
			if ( ! empty( $_REQUEST['products_per_page'] ) ) {
				$per_page = intval( $_REQUEST['products_per_page'] );
			} elseif ( ! empty( $_COOKIE['products_per_page'] ) ) {
				$per_page = intval( $_COOKIE['products_per_page'] );
			}
		}

		return $per_page;
	}

	public static function is_deal_product() {
		global $product;

		// It must be a sale product first
		if ( ! $product->is_on_sale() ) {
			return false;
		}

		// Only support product type "simple" and "external"
		if ( ! $product->is_type( 'simple' ) && ! $product->is_type( 'external' ) ) {
			return false;
		}

		$expire_date = ! empty( $product->get_date_on_sale_to() ) ? $product->get_date_on_sale_to()->getOffsetTimestamp() : '';
		$expire_date = apply_filters( 'tawc_deals_expire_timestamp', $expire_date, $product );

		if ( empty( $expire_date ) ) {
			return false;
		}

		$now    = strtotime( current_time( 'Y-m-d H:i:s' ) );
		$expire = $expire_date - $now;

		$deal_quantity = get_post_meta( $product->get_id(), '_deal_quantity', true );

		if ( $expire > 0 && $deal_quantity > 0 ) {
			return true;
		}

		return false;
	}

	public static function product_classes( $classes ) {
		if ( self::is_deal_product() ) {
			$classes[] = 'is-deal-product';
		}

		return $classes;
	}

	/**
	 * After Output Product Categories
	 */
	public static function teckzone_after_output_product_categories() {
		$html = '<li class="divider"></li>';

		return $html;
	}

	public static function open_product_inner() {
		echo '<div class="product-inner">';
	}

	public static function close_product_inner() {
		echo '</div>';
	}

	public static function open_product_details() {
		echo '<div class="product-details">';
	}

	public static function close_product_details() {
		echo '</div>';
	}

	public static function open_product_thumbnail() {
		echo '<div class="product-thumbnail teckzone-product-thumbnail">';
	}

	public static function close_product_thumbnail() {
		echo '</div>';
	}

	public static function product_link_open() {
		echo '<a href="' . esc_url( get_the_permalink() ) . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">';
	}

	public static function product_link_close() {
		echo '</a>';
	}

	public static function open_product_content_box() {
		echo '<div class="product-content-box">';
	}

	public static function close_product_content_box() {
		echo '</div>';
	}

	public static function open_product_right_box() {
		echo '<div class="product-right-box">';
	}

	public static function close_product_right_box() {
		echo '</div>';
	}

	public static function open_product_details_hover() {
		echo '<div class="product-details-hover">';
	}

	public static function close_product_details_hover() {
		echo '</div>';
	}

	/**
	 * Shop loading
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function before_shop_loop() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}
		echo '<div id="teckzone-shop-content" class="teckzone-shop-content">';
	}

	/**
	 * Shop loading
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function after_shop_loop() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}
		echo '</div>';
	}

	public static function product_button() {
		echo '<div class="product-button">';

		if ( function_exists( 'woocommerce_template_loop_add_to_cart' ) ) {
			woocommerce_template_loop_add_to_cart();
		}

		echo '<div class="group">';

		if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
			echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
		}

		echo '<div class="divider"></div>';

		self::product_compare();

		echo '</div>';
		echo '</div>';

	}

	/**
	 * WooCommerce product quickview
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	public static function product_quickview() {
		global $product;

		echo '<a href="' . $product->get_permalink() . '" data-id="' . esc_attr( $product->get_id() ) . '"  class="teckzone-product-quick-view button hidden-sm hidden-xs">
		<i class="p-icon icon-expand" title="' . esc_attr__( 'Quick View', 'teckzone' ) . '" data-rel="tooltip"></i>
		</a>';
	}

	/**
	 * WooCommerce product compare
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	public static function product_compare() {
		global $product;

		if ( ! class_exists( 'YITH_Woocompare' ) ) {
			return;
		}

		$button_text = get_option( 'yith_woocompare_button_text', esc_html__( 'Compare', 'teckzone' ) );
		$product_id  = $product->get_id();
		$url_args    = array(
			'action' => 'yith-woocompare-add-product',
			'id'     => $product_id,
		);
		$lang        = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : false;
		if ( $lang ) {
			$url_args['lang'] = $lang;
		}

		$css_class   = 'compare';
		$cookie_name = 'yith_woocompare_list';
		if ( function_exists( 'is_multisite' ) && is_multisite() ) {
			$cookie_name .= '_' . get_current_blog_id();
		}
		$the_list = isset( $_COOKIE[ $cookie_name ] ) ? json_decode( $_COOKIE[ $cookie_name ] ) : array();
		if ( in_array( $product_id, $the_list ) ) {
			$css_class          .= ' added';
			$url_args['action'] = 'yith-woocompare-view-table';
			$button_text        = apply_filters( 'yith_woocompare_compare_added_label', esc_html__( 'Added', 'teckzone' ) );
		}

		$url = esc_url_raw( add_query_arg( $url_args, home_url() ) );
		echo '<div class="compare-button tz-compare-button">';
		printf( '<a href="%s" class="%s" title="%s" data-product_id="%d"><span>%s</span></a>', esc_url( $url ), esc_attr( $css_class ), esc_html( $button_text ), $product_id, $button_text );
		echo '</div>';
	}

	/**
	 * Display buy now button
	 *
	 * @since 1.0
	 */
	public static function product_buy_now_button() {
		global $product;
		if ( ! intval( teckzone_get_option( 'product_buy_now' ) ) ) {
			return;
		}

		if ( $product->get_type() == 'external' ) {
			return;
		}

		echo sprintf( '<button class="buy_now_button button">%s</button>', wp_kses_post( teckzone_get_option( 'product_buy_now_text' ) ) );

	}

	/**
	 * Shop loading
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function shop_loading() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}
		echo '<div class="teckzone-catalog-loading">
				<span class="teckzone-loader"></span>
			</div>';
	}

	/**
	 * HTML for rating
	 *
	 * @since 1.0
	 */
	public static function product_get_rating_html( $html ) {
		if ( empty( $html ) ) {
			return $html;
		}

		global $product;

		if ( empty( $product ) ) {
			return $html;
		}
		$count = $product->get_rating_count();

		$rating = '<div class="tz-rating">';
		$rating .= $html;
		$rating .= '<span class="count">(' . $count . ')</span>';
		$rating .= '</div>';

		return $rating;
	}

	/**
	 * HTML for price
	 *
	 * @since 1.0
	 */
	public static function product_get_price_html( $price, $product ) {
		if ( is_admin() ) {
			return $price;
		}

		if ( $product->get_type() == 'variable' || $product->get_type() == 'grouped' ) {
			return $price;
		}

		if ( $product->is_on_sale() ) {
			$price           = wc_format_sale_price( wc_get_price_to_display( $product, array( 'price' => $product->get_regular_price() ) ), wc_get_price_to_display( $product ) ) . $product->get_price_suffix();
			$percentage      = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 ) . '%';
			$percentage_html = intval( teckzone_get_option( 'sale_percentage' ) ) ? sprintf( '<span class="sale">%s %s</span>', $percentage, esc_html__( 'off', 'teckzone' ) ) : '';
			$price           = sprintf( '%s %s', $price, $percentage_html );
		}

		return $price;
	}

	/**
	 * HTML for button text
	 *
	 * @since 1.0
	 */
	public static function external_product_add_to_cart_text( $text, $product ) {
		if ( ! $product->get_type() == 'external' ) {
			return $text;
		}

		$number = apply_filters( 'tz_catalog_external_button_length', 3 );
		$text   = wp_trim_words( $text, $number );

		return $text;
	}

	/**
	 * Get product Categories
	 */
	public static function tz_product_category() {
		global $product;
		$tax  = 'product_cat';
		$id   = $product->get_id();
		$term = get_the_terms( $id, $tax );

		if ( is_wp_error( $term ) || ! $term ) {
			return;
		}

		$url = get_term_link( $term[0]->term_id, $tax );

		echo '<a href="' . esc_url( $url ) . '" class="tz-cat">' . esc_html( $term[0]->name ) . '</a>';
	}

	/**
	 * Show the product title in the product loop. By default this is an H2.
	 */
	public static function template_loop_product_title() {
		echo '<h2 class="woocommerce-loop-product__title"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h2>';
	}

	/**
	 * Products pagination.
	 */
	public static function pagination() {
		$nav_type = apply_filters( 'teckzone_catalog_nav_type', teckzone_get_option( 'catalog_nav_type' ) );

		if ( 'numeric' == $nav_type ) {
			woocommerce_pagination();
		} elseif ( get_next_posts_link() ) {
			$classes = array(
				'woocommerce-navigation',
				'next-posts-navigation',
				'ajax-navigation',
				'ajax-' . $nav_type,
			);

			$button_html = sprintf(
				'<span class="load-more-text">%s</span>
				<span class="loading-icon">
					<span class="loading-text">%s</span>
					<span class="loading-bubbles">
						<span class="bubble"><span class="dot"></span></span>
						<span class="bubble"><span class="dot"></span></span>
						<span class="bubble"><span class="dot"></span></span>
					</span>
				</span>',
				esc_html__( 'Load More', 'teckzone' ),
				esc_html__( 'Loading', 'teckzone' )
			);

			echo '<nav class="' . esc_attr( implode( ' ', $classes ) ) . '">';
			echo '<div class="nav-links">';
			next_posts_link( $button_html );
			echo '</div>';
			echo '</nav>';
		}
	}

	public static function pagination_args( $args ) {
		if ( ! teckzone_is_catalog() ) {
			return $args;
		}

		$args['prev_text'] = '<i class="icon-chevron-left"></i>';
		$args['next_text'] = '<i class="icon-chevron-right"></i>';

		return $args;
	}

	/**
	 * Display product attribute
	 *
	 * @since 1.0
	 */
	public static function product_attribute() {
		global $product;

		if ( $product->get_type() != 'variable' ) {
			return;
		}

		$default_attribute = sanitize_title( teckzone_get_option( 'product_attribute' ) );

		if ( $default_attribute == '' || $default_attribute == 'none' ) {
			return;
		}

		$default_attribute = 'pa_' . $default_attribute;

		$attributes         = maybe_unserialize( get_post_meta( $product->get_id(), '_product_attributes', true ) );
		$product_attributes = maybe_unserialize( get_post_meta( $product->get_id(), 'attributes_extra', true ) );

		if ( $product_attributes == 'none' ) {
			return;
		}

		if ( $product_attributes == '' ) {
			$product_attributes = $default_attribute;
		}

		$variations = self::get_variations( $product_attributes );
		if ( ! $attributes ) {
			return;
		}

		foreach ( $attributes as $attribute ) {

			if ( sanitize_title( $attribute['name'] ) == $product_attributes ) {
				$attr_type = '';

				if ( function_exists( 'TA_WCVS' ) ) {
					$attr = TA_WCVS()->get_tax_attribute( $attribute['name'] );
					if ( $attr ) {
						$attr_type = $attr->attribute_type;
					}
				}

				$extra_css = ' tz-attr-swatches--' . $attr_type;

				$columns = apply_filters( 'teckzone_image_attribute_columns', 4 );

				echo '<div class="tz-attr-swatches' . esc_attr( $extra_css ) . '">';
				echo '<div class="tz-attr-swatches__wrapper" data-columns="' . esc_attr( $columns ) . '">';
				if ( $attribute['is_taxonomy'] ) {
					$post_terms = wp_get_post_terms( $product->get_id(), $attribute['name'] );
					foreach ( $post_terms as $term ) {
						$css_class = 'tz-swatch-item';

						if ( is_wp_error( $term ) || empty( $term ) ) {
							continue;
						}

						if ( ! isset( $term->slug ) ) {
							continue;
						}

						if ( $variations && isset( $variations[ $term->slug ] ) ) {
							$attachment_id = $variations[ $term->slug ];
							$attachment    = wp_get_attachment_image_src( $attachment_id, 'shop_catalog' );
							$image_srcset  = wp_get_attachment_image_srcset( $attachment_id, 'shop_catalog' );

							$img_src = $attachment ? $attachment[0] : '';

							echo '' . self::swatch_html( $term, $attr_type, $img_src, $css_class, $image_srcset );
						}
					}
				}
				echo '</div>';
				echo '</div>';
				break;
			}
		}

	}

	/**
	 * Print HTML of a single swatch
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function swatch_html( $term, $attr_type, $img_src, $css_class, $image_srcset ) {
		$html = '';
		$name = $term->name;

		switch ( $attr_type ) {
			case 'color':
				$color = get_term_meta( $term->term_id, 'color', true );
				list( $r, $g, $b ) = sscanf( $color, "#%02x%02x%02x" );
				$html = sprintf(
					'<span class="swatch swatch-color %s" data-src="%s" data-src-set="%s" title="%s"><span class="sub-swatch" style="background-color:%s;color:%s;"></span> </span>',
					esc_attr( $css_class ),
					esc_url( $img_src ),
					esc_attr( $image_srcset ),
					esc_attr( $name ),
					esc_attr( $color ),
					"rgba($r,$g,$b,0.5)"
				);
				break;

			case 'image':
				$image = get_term_meta( $term->term_id, 'image', true );
				if ( $image ) {
					$image = wp_get_attachment_image_src( $image );
					$image = $image ? $image[0] : WC()->plugin_url() . '/assets/images/placeholder.png';
					$html  = sprintf(
						'<span class="swatch swatch-image %s" data-src="%s" data-src-set="%s" title="%s"><img src="%s" alt="%s"></span>',
						esc_attr( $css_class ),
						esc_url( $img_src ),
						esc_attr( $image_srcset ),
						esc_attr( $name ),
						esc_url( $image ),
						esc_attr( $name )
					);
				}

				break;

			default:
				$label = get_term_meta( $term->term_id, 'label', true );
				$label = $label ? $label : $name;
				$html  = sprintf(
					'<span class="swatch swatch-label %s" data-src="%s" data-src-set="%s" title="%s">%s</span>',
					esc_attr( $css_class ),
					esc_url( $img_src ),
					esc_attr( $image_srcset ),
					esc_attr( $name ),
					esc_html( $label )
				);
				break;

		}

		return $html;
	}

	/**
	 * Get variations
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function get_variations( $default_attribute ) {
		global $product;

		$variations = array();
		if ( $product->get_type() == 'variable' ) {
			$args = array(
				'post_parent' => $product->get_id(),
				'post_type'   => 'product_variation',
				'orderby'     => 'menu_order',
				'order'       => 'ASC',
				'fields'      => 'ids',
				'post_status' => 'publish',
				'numberposts' => - 1,
			);

			if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
				$args['meta_query'][] = array(
					'key'     => '_stock_status',
					'value'   => 'instock',
					'compare' => '=',
				);
			}

			$thumbnail_id = get_post_thumbnail_id();

			$posts = get_posts( $args );

			foreach ( $posts as $post_id ) {
				$attachment_id = get_post_thumbnail_id( $post_id );
				$attribute     = self::get_variation_attributes( $post_id, 'attribute_' . $default_attribute );

				if ( ! $attachment_id ) {
					$attachment_id = $thumbnail_id;
				}

				if ( $attribute ) {
					$variations[ $attribute[0] ] = $attachment_id;
				}

			}

		}

		return $variations;
	}

	/**
	 * Get variation attribute
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function get_variation_attributes( $child_id, $attribute ) {
		global $wpdb;

		$values = array_unique(
			$wpdb->get_col(
				$wpdb->prepare(
					"SELECT meta_value FROM {$wpdb->postmeta} WHERE meta_key = %s AND post_id IN (" . $child_id . ")",
					$attribute
				)
			)
		);

		return $values;
	}

	/**
	 * Catalog Main
	 */
	public static function catalog_products_main() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( empty( self::$catalog_elements ) ) {
			return;
		}

		if ( self::$catalog_layout == '1' ) {
			self::catalog_banners_group();
		}
	}

	/**
	 * Catalog Header
	 */
	public static function catalog_products_header() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( empty( self::$catalog_elements ) ) {
			return;
		}

		if ( self::$catalog_layout == '1' ) {
			self::catalog_categories();
			self::catalog_brands();
			self::catalog_products_carousel();
		} elseif ( self::$catalog_layout == '2' ) {
			self::catalog_banners_group();
			self::catalog_categories();
			self::catalog_brands();
			self::catalog_products_carousel();
		}
	}

	/**
	 * Catalog Banners
	 */
	public static function catalog_banners_group() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		$banners_carousel = self::catalog_banners_carousel();
		$banners_grid     = self::catalog_banners_grid();

		if ( ! $banners_carousel && ! $banners_grid ) {
			return;
		}

		$css = self::$catalog_layout == '1' ? 'col-flex-xs-12' : '';

		echo '<div class="catalog-banners-group ' . esc_attr( $css ) . '">';
		echo wp_kses_post( $banners_carousel );
		echo wp_kses_post( $banners_grid );
		echo '</div>';
	}

	/**
	 * Catalog Banners
	 */
	public static function catalog_banners_carousel() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( ! in_array( 'banners_carousel', self::$catalog_elements ) ) {
			return;
		}

		$banners  = (array) teckzone_get_option( 'shop_banners_carousel' );
		$autoplay = intval( teckzone_get_option( 'shop_banners_carousel_autoplay' ) );

		if ( function_exists( 'is_product_category' ) && is_product_category() && teckzone_get_product_category_level() == 0 ) {
			$banners  = (array) teckzone_get_option( 'products_cat_level_1_banners_carousel' );
			$autoplay = intval( teckzone_get_option( 'products_cat_level_1_banners_carousel_autoplay' ) );
		}

		$output = array();

		if ( function_exists( 'is_product_category' ) && is_product_category() ) {
			$queried_object = get_queried_object();
			$term_id        = $queried_object->term_id;
			$banners_ids    = get_term_meta( $term_id, 'tz_cat_banners_id', true );
			$banners_links  = get_term_meta( $term_id, 'tz_cat_banners_link', true );

			if ( $banners_ids ) {
				$thumbnail_ids = explode( ',', $banners_ids );
				$banners_links = explode( "\n", $banners_links );
				$i             = 0;
				foreach ( $thumbnail_ids as $thumbnail_id ) {
					if ( empty( $thumbnail_id ) ) {
						continue;
					}

					$image = wp_get_attachment_image( $thumbnail_id, 'full' );

					if ( empty( $image ) ) {
						continue;
					}
					if ( $image ) {
						$link = $link_html = '';

						if ( $banners_links && isset( $banners_links[ $i ] ) ) {
							$link = preg_replace( '/<br \/>/iU', '', $banners_links[ $i ] );
						}

						$output[] = sprintf(
							'<li><a href="%s">%s</a></li>',
							esc_url( $link ),
							$image
						);
					}

					$i ++;
				}
			}
		}

		if ( empty( $output ) && $banners ) {
			foreach ( $banners as $banner ) {
				$image    = isset( $banner['image'] ) && $banner['image'] ? wp_get_attachment_image( $banner['image'], 'full' ) : '';
				$output[] = sprintf(
					'<li><a href="%s">%s</a></li>',
					esc_url( isset( $banner['link_url'] ) && $banner['link_url'] ? $banner['link_url'] : '#' ),
					$image
				);
			}

		}

		if ( $output ) {
			return sprintf(
				'<div class="tz-catalog-banners"><ul id="tz-catalog-banners" data-autoplay="%s">%s</ul></div>',
				esc_attr( $autoplay ),
				implode( ' ', $output )
			);
		}

	}

	/**
	 * Catalog Banners
	 */
	public static function catalog_banners_grid() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( ! in_array( 'banners_grid', self::$catalog_elements ) ) {
			return;
		}

		$banners = (array) teckzone_get_option( 'shop_banners_grid' );
		$columns = intval( teckzone_get_option( 'shop_banners_grid_columns' ) );

		if ( function_exists( 'is_product_category' ) && is_product_category() && teckzone_get_product_category_level() == 0 ) {
			$banners = (array) teckzone_get_option( 'products_cat_level_1_banners_grid' );
			$columns = intval( teckzone_get_option( 'products_cat_level_1_banners_grid_columns' ) );
		}

		$output = array();

		if ( function_exists( 'is_product_category' ) && is_product_category() ) {
			$queried_object = get_queried_object();
			$term_id        = $queried_object->term_id;
			$banners_ids    = get_term_meta( $term_id, 'tz_cat_banners_2_id', true );
			$banners_links  = get_term_meta( $term_id, 'tz_cat_banners_2_link', true );
			$columns        = absint( get_term_meta( $term_id, 'tz_cat_banners_2_columns', true ) );

			if ( $banners_ids ) {
				$thumbnail_ids = explode( ',', $banners_ids );
				$banners_links = explode( "\n", $banners_links );
				$i             = 0;
				foreach ( $thumbnail_ids as $thumbnail_id ) {
					if ( empty( $thumbnail_id ) ) {
						continue;
					}

					$image = wp_get_attachment_image( $thumbnail_id, 'full' );

					if ( empty( $image ) ) {
						continue;
					}
					if ( $image ) {
						$link = $link_html = '';

						if ( $banners_links && isset( $banners_links[ $i ] ) ) {
							$link = preg_replace( '/<br \/>/iU', '', $banners_links[ $i ] );
						}

						$output[] = sprintf(
							'<div class="item col-flex-md-%s col-flex-xs-6"><a href="%s">%s</a></div>',
							esc_attr( floor( 12 / $columns ) ),
							esc_url( $link ),
							$image
						);
					}

					$i ++;
				}
			}
		}

		if ( empty( $output ) && $banners ) {
			foreach ( $banners as $banner ) {
				$image    = wp_get_attachment_image( $banner['image'], 'full' );
				$output[] = sprintf(
					'<div class="item col-flex-md-%s"><a href="%s">%s</a></div>',
					esc_attr( floor( 12 / $columns ) ),
					esc_url( $banner['link_url'] ),
					$image
				);
			}
		}

		if ( $output ) {
			return sprintf(
				'<div class="tz-catalog-banners-grid"><div class="row-flex">%s</div></div>',
				implode( ' ', $output )
			);
		}
	}

	/**
	 * Catalog Title
	 */
	public static function teckzone_catalog_title() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( ! in_array( 'title', self::$catalog_elements ) ) {
			return;
		}

		if ( is_search() ) {
			return;
		}

		echo '<div class="tz-catalog-header">';

		the_archive_title( '<h2 class="tz-catalog-title">', '</h2>' );

		if ( is_post_type_archive( 'product' ) && in_array( absint( get_query_var( 'paged' ) ), array(
				0,
				1
			), true ) ) {
			$shop_page = get_post( wc_get_page_id( 'shop' ) );
			if ( $shop_page ) {
				$description = wc_format_content( $shop_page->post_content );
				if ( $description ) {
					echo '<div class="page-description">' . $description . '</div>'; // WPCS: XSS ok.
				}
			}
		}

		if ( is_product_taxonomy() && 0 === absint( get_query_var( 'paged' ) ) ) {
			$term = get_queried_object();

			if ( $term && ! empty( $term->description ) ) {
				echo '<div class="term-description">' . wc_format_content( $term->description ) . '</div>'; // WPCS: XSS ok.
			}
		}

		echo '</div>';
	}

	/**
	 * Catalog categories
	 */
	public static function catalog_categories() {
		if ( function_exists( 'is_shop' ) && is_shop() ) {
			self::catalog_categories_style_1();
		} elseif ( function_exists( 'is_product_category' ) && is_product_category() ) {
			self::catalog_categories_style_2();
		}
	}

	/**
	 * Catalog categories style 1
	 */
	public static function catalog_categories_style_1() {
		if ( ! in_array( 'categories', self::$catalog_elements ) ) {
			return;
		}

		$cats_number    = teckzone_get_option( 'shop_categories_number' );
		$subcats_number = apply_filters( 'teckzone_categories_box_number_subcats', 5 );
		$cats_order     = teckzone_get_option( 'shop_categories_orderby' );
		$columns        = teckzone_get_option( 'shop_categories_columns' );

		if ( intval( $cats_number ) < 1 ) {
			return;
		}

		$atts = array(
			'taxonomy'   => 'product_cat',
			'hide_empty' => 1,
			'number'     => $cats_number,
			'parent'     => 0,
		);

		$atts['menu_order'] = false;
		if ( $cats_order == 'order' ) {
			$atts['menu_order'] = 'asc';
		} else {
			$atts['orderby'] = $cats_order;
			if ( $cats_order == 'count' ) {
				$atts['order'] = 'desc';
			}
		}

		$terms = get_terms( $atts );

		if ( is_wp_error( $terms ) || ! $terms ) {
			return;
		}

		$output = array();
		foreach ( $terms as $term ) {

			$term_list = array();
			$item_css  = '';

			if ( $subcats_number ) {
				$atts        = array(
					'taxonomy'   => 'product_cat',
					'hide_empty' => 1,
					'orderby'    => $cats_order,
					'number'     => $subcats_number,
					'parent'     => $term->term_id,
				);
				$child_terms = get_terms( $atts );
				if ( ! is_wp_error( $child_terms ) && $child_terms ) {
					$term_list[] = '<ul class="child-list">';
					foreach ( $child_terms as $child ) {
						$term_list[] = sprintf(
							'<li><a href="%s">%s</a></li>',
							esc_url( get_term_link( $child->term_id, 'product_cat' ) ),
							$child->name
						);
					}

					$term_list[] = sprintf(
						'<li class="parent"><a href="%s">%s<i class="icon-chevron-right"></i></a></li>',
						esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
						apply_filters( 'teckzone_catalog_categories_parent_text', esc_html__( 'See All', 'teckzone' ) )
					);

					$term_list[] = '</ul>';
				} else {
					$item_css .= 'no-child';
				}
			}

			$thumbnail_id         = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );
			$small_thumbnail_size = apply_filters( 'teckzone_category_archive_thumbnail_size', 'shop_catalog' );

			$image_html = '';
			if ( $thumbnail_id ) {
				$image_html = sprintf(
					'<a class="thumbnail" href="%s">%s</a>',
					esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
					wp_get_attachment_image( $thumbnail_id, $small_thumbnail_size )
				);
			} else {
				$item_css .= ' no-thumb';
			}

			$cat_content = sprintf(
				'<a href="%s" class="box-title">%s</a>',
				esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
				esc_html( $term->name )
			);

			$output[] = sprintf(
				'<div class="cat-item %s">' .
				'<div class="cat-item__wrapper">' .
				'<div class="cat-content">' .
				'%s%s' .
				'</div>' .
				'%s' .
				'</div>' .
				'</div>',
				esc_attr( $item_css ),
				$image_html,
				$cat_content,
				implode( '', $term_list )
			);
		}

		if ( $output ) {
			printf(
				'<div class="tz-catalog-categories columns-%s"><div class="catalog-categories__wrapper">%s</div></div>',
				esc_attr( absint( $columns ) ),
				implode( ' ', $output )
			);
		}
	}

	/**
	 * Catalog categories style 2
	 */
	public static function catalog_categories_style_2() {
		if ( ! in_array( 'categories', self::$catalog_elements ) ) {
			return;
		}

		$cats_number = teckzone_get_option( 'products_cat_level_1_categories_number' );
		$cats_order  = teckzone_get_option( 'products_cat_level_1_categories_orderby' );
		$columns     = teckzone_get_option( 'products_cat_level_1_categories_columns' );

		if ( intval( $cats_number ) < 1 ) {
			return;
		}

		$atts = array(
			'taxonomy'   => 'product_cat',
			'hide_empty' => 1,
			'number'     => $cats_number,
		);

		$atts['menu_order'] = false;
		if ( $cats_order == 'order' ) {
			$atts['menu_order'] = 'asc';
		} else {
			$atts['orderby'] = $cats_order;
			if ( $cats_order == 'count' ) {
				$atts['order'] = 'desc';
			}
		}

		$parent = 0;
		global $wp_query;
		$current_cat = $wp_query->get_queried_object();
		if ( $current_cat ) {
			$parent = $current_cat->term_id;
		}

		$atts['parent'] = $parent;

		$terms = get_terms( $atts );

		if ( is_wp_error( $terms ) || ! $terms ) {
			return;
		}

		$output = array();
		foreach ( $terms as $term ) {

			$item_css = '';

			$thumbnail_id         = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );
			$small_thumbnail_size = apply_filters( 'teckzone_category_archive_thumbnail_size', 'shop_catalog' );

			$image_html = '';
			if ( $thumbnail_id ) {
				$image_html = sprintf(
					'<span class="thumbnail" href="%s">%s</span>',
					esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
					wp_get_attachment_image( $thumbnail_id, $small_thumbnail_size )
				);
			} else {
				$item_css .= ' no-thumb';
			}

			$cat_content = sprintf(
				'<span href="%s" class="box-title">%s</span>',
				esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
				esc_html( $term->name )
			);

			$count = sprintf( _n( '%s Item', '%s Items', $term->count, 'teckzone' ), number_format_i18n( $term->count ) );
			$count = sprintf( '<span class="cat-count">%s</span>', $count );

			$output[] = sprintf(
				'<div class="cat-item %s">' .
				'<a class="cat-item__wrapper" href="%s">' .
				'%s' .
				'<span class="cat-content">' .
				'%s%s' .
				'</span>' .
				'</a>' .
				'</div>',
				esc_attr( $item_css ),
				esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
				$image_html,
				$cat_content,
				$count
			);
		}

		if ( $output ) {
			printf(
				'<div class="tz-catalog-categories-2 columns-%s"><div class="catalog-categories__wrapper">%s</div></div>',
				esc_attr( absint( $columns ) ),
				implode( ' ', $output )
			);
		}
	}

	/**
	 * Catalog products carousel
	 */
	public static function catalog_products_carousel() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( ! in_array( 'products_carousel', self::$catalog_elements ) ) {
			return;
		}

		$carousels = teckzone_get_option( 'shop_products_carousel' );

		if ( function_exists( 'is_product_category' ) && is_product_category() && teckzone_get_product_category_level() == 0 ) {
			$carousels = teckzone_get_option( 'products_cat_level_1_products_carousel' );
		}

		if ( empty( $carousels ) ) {
			return;
		}

		if ( ! is_array( $carousels ) ) {
			return;
		}

		foreach ( $carousels as $carousel ) {
			$title = sprintf( '<h2 class="title">%s</h2>', esc_html( $carousel['title'] ) );

			$title .= '<div class="slick-arrows"></div>';

			$columns       = isset( $carousel['columns'] ) ? absint( $carousel['columns'] ) : 5;
			$autoplaySpeed = absint( $carousel['autoplay'] );
			$autoplay      = $autoplaySpeed > 0 ? true : false;

			$slug = '';

			if ( function_exists( 'is_product_category' ) && is_product_category() ) {
				global $wp_query;
				$current_cat = $wp_query->get_queried_object();
				$slug        = $current_cat->slug;
			}

			$atts = array(
				'per_page' => intval( $carousel['number'] ),
				'type'     => $carousel['type'],
				'columns'  => $columns,
				'category' => $slug,
			);

			$carousel_settings = array(
				'slidesToShow'   => $columns,
				'slidesToScroll' => $columns,
				'autoplay'       => $autoplay,
				'infinite'       => $autoplay,
				'autoplaySpeed'  => $autoplaySpeed,
				'speed'          => 800
			);

			$responsive_settings = [];

			$responsive_settings['1200'] = [
				'breakpoint' => 1200,
				'settings'   => array(
					'dots'           => true,
					'arrows'         => false,
					'slidesToShow'   => 4,
					'slidesToScroll' => 4,
				)
			];

			$responsive_settings['992'] = [
				'breakpoint' => 992,
				'settings'   => array(
					'dots'           => true,
					'arrows'         => false,
					'slidesToShow'   => 3,
					'slidesToScroll' => 3,
				)
			];

			$responsive_settings['768'] = [
				'breakpoint' => 768,
				'settings'   => array(
					'dots'           => true,
					'arrows'         => false,
					'slidesToShow'   => 2,
					'slidesToScroll' => 2,
				)
			];

			$get_id                 = intval( get_option( 'woocommerce_shop_page_id' ) );
			$container_width_custom = get_post_meta( $get_id, 'page_content_container_width', true );
			$catalog_width          = teckzone_get_option( 'catalog_container_width' );
			if ( $container_width_custom == 'default' ) {
				$catalog_width = $catalog_width;
			} else {
				$catalog_width = $container_width_custom;
			}

			if ( $catalog_width == 'full-width' || $catalog_width == 'wide' ) {
				$responsive_settings['1500'] = [
					'breakpoint' => 1500,
					'settings'   => array(
						'slidesToShow'   => $columns > 5 ? 5 : $columns,
						'slidesToScroll' => $columns > 5 ? 5 : $columns,
					)
				];
			}

			krsort( $responsive_settings );

			foreach ( $responsive_settings as $responsive_setting ) {
				$carousel_settings['responsive'][] = $responsive_setting;
			}

			printf(
				'<div class="tz-products-top-carousel catalog-view-grid" data-settings="%s">
 					<div class="carousel-header">%s</div>
 					%s
				</div>',
				esc_attr( wp_json_encode( $carousel_settings ) ),
				$title,
				self::product_loop( $atts )
			);
		}
	}

	/**
	 * Catalog categories
	 */
	public static function catalog_brands() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( ! in_array( 'brands', self::$catalog_elements ) ) {
			return;
		}

		$title    = teckzone_get_option( 'shop_brands_title' );
		$number   = intval( teckzone_get_option( 'shop_brands_number' ) );
		$columns  = intval( teckzone_get_option( 'shop_brands_columns' ) );
		$autoplay = intval( teckzone_get_option( 'shop_brands_autoplay' ) );

		if ( function_exists( 'is_product_category' ) && is_product_category() && teckzone_get_product_category_level() == 0 ) {
			$title    = teckzone_get_option( 'products_cat_level_1_brands_title' );
			$number   = intval( teckzone_get_option( 'products_cat_level_1_brands_number' ) );
			$columns  = intval( teckzone_get_option( 'products_cat_level_1_brands_columns' ) );
			$autoplay = intval( teckzone_get_option( 'products_cat_level_1_brands_autoplay' ) );
		}

		$atts = array(
			'taxonomy'   => 'product_brand',
			'hide_empty' => 1,
			'number'     => $number,
		);

		$terms                = get_terms( $atts );
		$small_thumbnail_size = apply_filters( 'teckzone_brand_archive_thumbnail_size', 'full' );
		$output               = array();

		if ( is_wp_error( $terms ) ) {
			return;
		}

		if ( empty( $terms ) || ! is_array( $terms ) ) {
			return;
		}

		$term_counts = array();

		if ( is_tax( 'product_brand' ) ) {
			$term_counts = teckzone_get_filtered_term_product_counts( wp_list_pluck( $terms, 'term_id' ), 'product_brand', 'pa_' );
		}

		$found = 0;
		foreach ( $terms as $term ) {
			if ( is_tax( 'product_brand' ) ) {
				$count = isset( $term_counts[ $term->term_id ] ) ? $term_counts[ $term->term_id ] : 0;

				if ( $found >= $number ) {
					break;
				}

				if ( $count === 0 ) {
					continue;
				}

				$found ++;
			}
			$thumbnail_id = absint( get_term_meta( $term->term_id, 'brand_thumbnail_id', true ) );
			if ( $thumbnail_id ) {
				$output[] = sprintf(
					'<div class="brand-item">' .
					'<a href="%s">%s</a>' .
					'</div>',
					esc_url( get_term_link( $term->term_id, 'product_brand' ) ),
					wp_get_attachment_image( $thumbnail_id, $small_thumbnail_size )
				);
			}
		}

		$title = sprintf( '<h2 class="title">%s</h2>', esc_html( $title ) );
		$title .= '<div class="slick-arrows"></div>';

		$carousel_settings = array(
			'slidesToShow'   => $columns,
			'slidesToScroll' => $columns,
			'autoplay'       => $autoplay > 0 ? true : false,
			'infinite'       => $autoplay > 0 ? true : false,
			'autoplaySpeed'  => $autoplay,
			'speed'          => 800,
			'responsive'     => array(
				array(
					'breakpoint' => 992,
					'settings'   => array(
						'slidesToShow'   => 4,
						'slidesToScroll' => 4,
					)
				),
				array(
					'breakpoint' => 768,
					'settings'   => array(
						'slidesToShow'   => 3,
						'slidesToScroll' => 3,
					)
				),
				array(
					'breakpoint' => 481,
					'settings'   => array(
						'slidesToShow'   => 2,
						'slidesToScroll' => 2,
					)
				)
			)
		);

		if ( $output ) {
			printf(
				'<div class="tz-catalog-brands">
					<div class="carousel-header">%s</div>
					<div class="brand-wrapper" data-slick="%s">%s</div>
				</div>',
				$title,
				esc_attr( wp_json_encode( $carousel_settings ) ),
				implode( ' ', $output )
			);
		}
	}

	/**
	 * Get shop toolbar
	 *
	 */
	public static function shop_toolbar() {
		if ( ! teckzone_is_catalog() && ! teckzone_is_vendor_page() ) {
			return;
		}
		global $wp_query;

		$els = (array) teckzone_get_option( 'toolbar_elements' );

		if ( teckzone_is_vendor_page() ) {
			$els = teckzone_get_option( 'catalog_vendor_toolbar_els' );
		}

		$els = apply_filters( 'teckzone_catalog_toolbar_elements', $els );

		if ( empty( $els ) ) {
			return;
		}

		$css_class = array( 'tz-catalog-toolbar catalog-toolbar' );

		if ( count( $els ) > 1 ) {
			$css_class[] = 'multiple';
		}

		$products_found = $page = $sort_by = $view = '';

		if ( in_array( 'total_product', $els ) ) {
			if ( intval( teckzone_is_mobile() ) ) {
				if ( intval( teckzone_get_option( 'enable_mobile_version' ) ) ) {
					if ( teckzone_is_vendor_page() ) {
						$products_found = '<a href="#" class="hidden-lg hidden-md tz-filter-mobile tz-vendor-infor-mobile" id="tz-vendor-infor-mobile"><i class="icon-equalizer"></i><span>Info</span></a>';
					}
				}
			} else {
				$total          = $wp_query->found_posts;
				$products_found = '<div class="products-found"><span>' . $total . '</span>' . esc_html__( ' Products Found', 'teckzone' ) . '</div>';
			}
		}

		if ( in_array( 'page', $els ) ) {
			$total_page = $wp_query->max_num_pages;
			$paged      = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			$pages_url = [];
			for ( $x = 1; $x <= $total_page; $x ++ ) {
				$pages_url[] = '<input id="tz-catalog-url-page-' . esc_attr( $x ) . '" type="hidden" value="' . esc_attr( esc_url( get_pagenum_link( $x ) ) ) . '">';
			}

			$paged_html = '<form class="tz-toolbar-pagination" method="post">' .
			              '<input id="tz-catalog-page-number" type="number" value="' . esc_attr( $paged ) . '" min="1" max="' . esc_attr( $total_page ) . '" step="1">' .
			              implode( "\n", $pages_url ) .
			              '</form>';
			$page       = '<div class="current-page">' . esc_html__( 'Page ', 'teckzone' ) . $paged_html . esc_html__( ' of ', 'teckzone' ) . $total_page . '</div>';
		}

		if ( in_array( 'sort_by', $els ) ) {
			ob_start();
			woocommerce_catalog_ordering();
			$sort_by = ob_get_clean();
		}

		if ( in_array( 'view', $els ) ) {
			$view = self::shop_view();
		}

		$top = $bottom = '';

		if ( ! teckzone_is_vendor_page() ) {
			if ( ! empty( $products_found ) || ! empty( $page ) ) {
				$top = sprintf( '<div class="catalog-toolbar__top">%s%s</div>', $products_found, $page );
			}
		}

		$filter = '';

		if ( teckzone_get_option( 'catalog_layout' ) != 'full-content' ) {
			$filter = '<div id="catalog-filter-mobile" class="catalog-filter-mobile">' .
			          '<a href="#"><i class="icon-equalizer"></i><span class="filter-text">' . esc_html__( 'Filter', 'teckzone' ) . '</span></a>' .
			          '</div>';
		}

		if ( ! empty( $sort_by ) || ! empty( $view ) ) {
			$bottom = sprintf( '<div class="catalog-toolbar__bottom clearfix">%s%s%s</div>', $view, $sort_by, $filter );
		}

		if ( teckzone_is_vendor_page() ) {
			if ( ! empty( $products_found ) || ! empty( $view ) ) {
				$bottom = sprintf( '<div class="catalog-toolbar__bottom clearfix">%s%s</div>', $products_found, $view );
			}
		}

		printf(
			'<div id="tz-catalog-toolbar" class="%s">%s%s</div>',
			esc_attr( implode( ' ', $css_class ) ),
			$top,
			$bottom
		);
	}

	public static function shop_view() {
		$per_page = array( '12', '16', '20' );
		$per_page = apply_filters( 'teckzone_shop_view_per_page', $per_page );

		if ( ! empty( $_REQUEST['products_per_page'] ) ) {
			$current = intval( $_REQUEST['products_per_page'] );
		} elseif ( ! empty( $_COOKIE['products_per_page'] ) ) {
			$current = intval( $_COOKIE['products_per_page'] );
		} else {
			$current = wc_get_loop_prop( 'per_page' );
		}

		$label = apply_filters( 'teckzone_shop_view_label', esc_html__( 'View:', 'teckzone' ) );

		$per_page[] = $current;
		$per_page   = array_unique( $per_page );
		$per_page   = array_filter( $per_page );
		asort( $per_page );

		$output = array();
		foreach ( $per_page as $value ) {
			$class    = $value == $current ? 'active' : '';
			$output[] = sprintf(
				'<li><a href="%s" class="%s">%s</a></li>',
				esc_url( add_query_arg( array( 'products_per_page' => $value ) ) ),
				esc_attr( $class ),
				esc_html( $value )
			);
		}

		$html = '<ul>' . implode( '', $output ) . '</ul>';

		$list_current     = self::$catalog_view == 'list' ? 'current' : '';
		$grid_current     = self::$catalog_view == 'grid' ? 'current' : '';
		$extended_current = self::$catalog_view == 'extended' ? 'current' : '';

		$per_page_view = '<label>' . esc_html__( 'View:', 'teckzone' ) . '</label>';
		if ( ! teckzone_is_vendor_page() ) {
			$per_page_view = sprintf(
				'<label>%s</label>
				<ul class="per-page"><li class="current">%s%s</li></ul>',
				$label,
				$current,
				$html
			);
		}

		return sprintf(
			'<div class="tz-shop-view shop-view">
                %s
				<div class="shop-view__icon">
					<a href="#" class="grid %s" data-view="grid" data-type="grid"><i class="icon-icons"></i></a>
					<span class="sep first"></span>
					<a href="#" class="extended %s" data-view="extended" data-type="extended"><i class="icon-grid"></i></a>
					<span class="sep last"></span>
					<a href="#" class="list %s" data-view="list" data-type="list"><i class="icon-list4"></i></a>
				</div>
			</div>',
			! empty( $per_page_view ) ? $per_page_view : '',
			esc_attr( $grid_current ),
			esc_attr( $extended_current ),
			esc_attr( $list_current )
		);
	}

	/**
	 *product_quick_view
	 */
	public static function product_quick_view() {
		if ( apply_filters( 'teckzone_check_ajax_referer', true ) ) {
			check_ajax_referer( '_teckzone_nonce', 'nonce' );
		}
		ob_start();
		if ( isset( $_POST['product_id'] ) && ! empty( $_POST['product_id'] ) ) {
			$product_id      = $_POST['product_id'];
			$original_post   = $GLOBALS['post'];
			$GLOBALS['post'] = get_post( $product_id ); // WPCS: override ok.
			setup_postdata( $GLOBALS['post'] );
			wc_get_template_part( 'content', 'product-quick-view' );
			$GLOBALS['post'] = $original_post; // WPCS: override ok.

		}
		$output = ob_get_clean();
		wp_send_json_success( $output );
		die();
	}

	public static function product_deals_progress_bar() {
		global $product;

		if ( ! function_exists( 'tawc_is_deal_product' ) ) {
			return;
		}

		if ( ! tawc_is_deal_product( $product ) ) {
			return;
		}

		$expire_date = ! empty( $product->get_date_on_sale_to() ) ? $product->get_date_on_sale_to()->getOffsetTimestamp() : '';
		$expire_date = apply_filters( 'tawc_deals_expire_timestamp', $expire_date, $product );

		if ( empty( $expire_date ) ) {
			return;
		}

		$now = strtotime( current_time( 'Y-m-d H:i:s' ) );

		$expire = $expire_date - $now;

		$limit = get_post_meta( $product->get_id(), '_deal_quantity', true );
		$sold  = intval( get_post_meta( $product->get_id(), '_deal_sales_counts', true ) );

		if ( $expire <= 0 ) {
			return;
		}

		$percentage = $sold / $limit * 100;

		?>
        <div class="tawc-deal deal">
            <div class="deal-sold">
                <div class="deal-progress">
                    <div class="progress-bar">
                        <div class="progress-value" style="width: <?php echo esc_attr( $percentage ) . '%' ?>"></div>
                    </div>
                </div>
                <div class="deal-text">
					<span class="sold">
						<span class="text"><?php esc_html_e( 'Sold: ', 'teckzone' ) ?></span>
						<span class="value"><?php echo intval( $sold ) ?>
                            /<?php echo ! empty( $limit ) ? $limit : '' ?></span>
					</span>
                </div>
            </div>
        </div>
		<?php
	}

	/**
	 * Search SKU
	 *
	 * @since 1.0
	 */
	public static function product_search_sku( $where ) {
		global $pagenow, $wpdb, $wp;

		if ( ( is_admin() && 'edit.php' != $pagenow )
		     || ! is_search()
		     || ! isset( $wp->query_vars['s'] )
		     || ( isset( $wp->query_vars['post_type'] ) && 'product' != $wp->query_vars['post_type'] )
		     || ( isset( $wp->query_vars['post_type'] ) && is_array( $wp->query_vars['post_type'] ) && ! in_array( 'product', $wp->query_vars['post_type'] ) )
		) {
			return $where;
		}
		$search_ids = array();
		$terms      = explode( ',', $wp->query_vars['s'] );

		foreach ( $terms as $term ) {
			//Include the search by id if admin area.
			if ( is_admin() && is_numeric( $term ) ) {
				$search_ids[] = $term;
			}
			// search for variations with a matching sku and return the parent.

			$sku_to_parent_id = $wpdb->get_col( $wpdb->prepare( "SELECT p.post_parent as post_id FROM {$wpdb->posts} as p join {$wpdb->postmeta} pm on p.ID = pm.post_id and pm.meta_key='_sku' and pm.meta_value LIKE '%%%s%%' where p.post_parent <> 0 group by p.post_parent", wc_clean( $term ) ) );

			//Search for a regular product that matches the sku.
			$sku_to_id = $wpdb->get_col( $wpdb->prepare( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key='_sku' AND meta_value LIKE '%%%s%%';", wc_clean( $term ) ) );

			$search_ids = array_merge( $search_ids, $sku_to_id, $sku_to_parent_id );
		}

		$search_ids = array_filter( array_map( 'absint', $search_ids ) );

		if ( sizeof( $search_ids ) > 0 ) {
			$where = str_replace( ')))', ") OR ({$wpdb->posts}.ID IN (" . implode( ',', $search_ids ) . "))))", $where );
		}

		return $where;
	}

	protected static function product_loop( $atts ) {
		global $woocommerce_loop;
		$query_args = self::get_query_args( $atts );

		$products = new WP_Query( $query_args );

		$columns = isset( $atts['columns'] ) ? absint( $atts['columns'] ) : null;

		if ( $columns ) {
			$woocommerce_loop['columns'] = $columns;
		}

		ob_start();

		if ( $products->have_posts() ) {
			woocommerce_product_loop_start();

			while ( $products->have_posts() ) : $products->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile; // end of the loop.

			woocommerce_product_loop_end();
		}

		$html = '<div class="woocommerce">' . ob_get_clean() . '</div>';

		woocommerce_reset_loop();
		wp_reset_postdata();

		return $html;
	}

	protected static function get_query_args( $atts ) {
		$args = array(
			'post_type'              => 'product',
			'post_status'            => 'publish',
			'orderby'                => get_option( 'woocommerce_default_catalog_orderby' ),
			'order'                  => 'DESC',
			'ignore_sticky_posts'    => 1,
			'posts_per_page'         => $atts['per_page'],
			'meta_query'             => WC()->query->get_meta_query(),
			'update_post_term_cache' => false,
			'update_post_meta_cache' => false,
		);

		if ( version_compare( WC()->version, '3.0.0', '>=' ) ) {
			$args['tax_query'] = WC()->query->get_tax_query();
		}

		// Ordering
		if ( 'menu_order' == $args['orderby'] || 'price' == $args['orderby'] ) {
			$args['order'] = 'ASC';
		}

		if ( 'price-desc' == $args['orderby'] ) {
			$args['orderby'] = 'price';
		}

		if ( method_exists( WC()->query, 'get_catalog_ordering_args' ) ) {
			$ordering_args   = WC()->query->get_catalog_ordering_args( $args['orderby'], $args['order'] );
			$args['orderby'] = $ordering_args['orderby'];
			$args['order']   = $ordering_args['order'];

			if ( $ordering_args['meta_key'] ) {
				$args['meta_key'] = $ordering_args['meta_key'];
			}
		}

		if ( ! empty( $atts['category'] ) ) {
			$args['tax_query'][] = array(
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'slug', //This is optional, as it defaults to 'term_id'
					'terms'    => $atts['category'],
					'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				),
			);
		}

		if ( isset( $atts['brand'] ) && $atts['brand'] ) {
			$args['tax_query'][] = array(
				array(
					'taxonomy' => 'product_brand',
					'field'    => 'slug', //This is optional, as it defaults to 'term_id'
					'terms'    => $atts['brand'],
					'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				),
			);
		}

		if ( isset( $atts['type'] ) ) {
			switch ( $atts['type'] ) {
				case 'recent':
					$args['order']   = 'DESC';
					$args['orderby'] = 'date';

					unset( $args['update_post_meta_cache'] );
					break;

				case 'featured':
					if ( version_compare( WC()->version, '3.0.0', '<' ) ) {
						$args['meta_query'][] = array(
							'key'   => '_featured',
							'value' => 'yes',
						);
					} else {
						$args['tax_query'][] = array(
							'taxonomy' => 'product_visibility',
							'field'    => 'name',
							'terms'    => 'featured',
							'operator' => 'IN',
						);
					}

					unset( $args['update_post_meta_cache'] );
					break;

				case 'sale':
					$args['post__in'] = array_merge( array( 0 ), wc_get_product_ids_on_sale() );
					break;

				case 'best_selling':
					$args['meta_key'] = 'total_sales';
					$args['orderby']  = 'meta_value_num';
					$args['order']    = 'DESC';
					break;

				case 'top_rated':
					$args['meta_key'] = '_wc_average_rating';
					$args['orderby']  = 'meta_value_num';
					$args['order']    = 'DESC';
					break;
			}
		}

		return $args;
	}

	/**
	 *    Product get template popup add to cart
	 */
	public static function product_popup_add_to_cart() {
		ob_start();

		if ( isset( $_POST['product_id'] ) && ! empty( $_POST['product_id'] ) ) {
			$product_id      = $_POST['product_id'];
			$original_post   = $GLOBALS['post'];
			$GLOBALS['post'] = get_post( $product_id ); // WPCS: override ok.
			setup_postdata( $GLOBALS['post'] );
			wc_get_template_part( 'content', 'product-popup-add-to-cart' );
			$GLOBALS['post'] = $original_post; // WPCS: override ok.
		}

		$output = ob_get_clean();
		wp_send_json_success( $output );
		die();

	}
}

