<?php
/**
 * General template hooks.
 *
 * @package Teckzone
 */

/**
 * Class of general template.
 */
class Teckzone_WooCommerce_Template {
	/**
	 * Initialize.
	 */
	public static function init() {
		// Disable the default WooCommerce stylesheet.
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'scripts' ), 20 );
		add_filter( 'body_class', array( __CLASS__, 'body_class' ) );

		// Add catalog ajax loader after #page
		add_action( 'teckzone_before_header', array( __CLASS__, 'catalog_ajax_loader' ) );

		// Remove default WooCommerce wrapper.
		remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
		remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
		add_action( 'woocommerce_before_main_content', array( __CLASS__, 'wrapper_before' ) );
		add_action( 'woocommerce_after_main_content', array( __CLASS__, 'wrapper_after' ) );

		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash' );
		if ( intval( teckzone_get_option( 'catalog_badges' ) ) ) {
			add_action( 'woocommerce_before_shop_loop_item_title', array( __CLASS__, 'product_ribbons' ) );
			add_action( 'teckzone_product_thumbnail_2', array( __CLASS__, 'product_ribbons' ), 50 );
		}

		add_action( 'woocommerce_after_product_gallery', array( __CLASS__, 'single_product_ribbons' ) );

		// Remove breadcrumb, use theme's instead
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

		// Orders account
		add_action( 'woocommerce_account_dashboard', 'woocommerce_account_orders', 5 );
		add_action( 'woocommerce_account_dashboard', 'woocommerce_account_edit_address', 15 );

		// Change position cross sell
		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
		if ( intval( teckzone_get_option( 'cross_sells_products' ) ) ) {
			add_action( 'woocommerce_after_cart', 'woocommerce_cross_sell_display' );
		}

		// Change columns and total of cross sell
		add_filter( 'woocommerce_cross_sells_columns', array( __CLASS__, 'cross_sells_columns' ) );
		add_filter( 'woocommerce_cross_sells_total', array( __CLASS__, 'cross_sells_numbers' ) );

		// Wishlist
		add_filter( 'yith_wcwl_add_to_cart_label', array( __CLASS__, 'teckzone_wishlist_add_to_cart_label' ) );

		add_action( 'yith_wcwl_before_wishlist_share', array( __CLASS__, 'teckzone_before_wishlist_share' ), 50 );
		add_action( 'yith_wcwl_after_wishlist_share', array( __CLASS__, 'teckzone_after_wishlist_share' ) );

		add_filter( 'yith_wcwl_loop_positions', array( __CLASS__, 'wcwl_loop_positions' ) );
		add_filter( 'yith_wcwl_button_icon', array( __CLASS__, 'wcwl_button_icon' ) );
		add_filter( 'yith_wcwl_button_added_icon', array( __CLASS__, 'wcwl_button_added_icon' ) );
		add_filter( 'yith_wcwl_remove_from_wishlist_label', array( __CLASS__, 'wcwl_remove_from_wishlist_label' ) );
		add_filter( 'yith_wcwl_add_to_wishlist_options', array( __CLASS__, 'replace_default_wishlist_label' ) );
	}

	/**
	 * WooCommerce specific scripts & stylesheets.
	 *
	 * @return void
	 */
	public static function scripts() {
		wp_enqueue_style( 'teckzone-woocommerce', get_template_directory_uri() . '/woocommerce.css' );
		if ( wp_script_is( 'wc-add-to-cart-variation', 'registered' ) ) {
			wp_enqueue_script( 'wc-add-to-cart-variation' );
		}

		wp_add_inline_style( 'teckzone-woocommerce', self::get_inline_style() );
	}


	/**
	 * Add 'woocommerce-active' class to the body tag.
	 *
	 * @param  array $classes CSS classes applied to the body tag.
	 *
	 * @return array $classes modified to include 'woocommerce-active' class.
	 */
	public static function body_class( $classes ) {
		$classes[] = 'woocommerce-active';

		// Adds a class of product layout.
		if ( is_product() ) {
			$product_layout = teckzone_get_option( 'product_layout' );
			$classes[]      = 'product-layout-' . $product_layout;

			if( teckzone_get_option( 'product_container_width' ) == 'full-width') {
				$classes[] = 'single-product--full-width';
			} elseif( teckzone_get_option( 'product_container_width' ) == 'wide') {
				$classes[] = 'single-product--full-width single-product--wide';
			} else {
				$classes[] = 'single-product--standard';
			}

			$sticky_product = apply_filters( 'teckzone_sticky_product_info', teckzone_get_option( 'sticky_product_info' ) );
			if ( intval( $sticky_product ) ) {
				$classes[] = 'sticky-header-info';
			}
		}

		if ( teckzone_is_catalog() ) {
			$catalog_layout = teckzone_get_option( 'catalog_layout' );
			$catalog_view   = isset( $_COOKIE['catalog_view'] ) ? $_COOKIE['catalog_view'] : teckzone_get_option( 'catalog_view' );
			$catalog_nav_class   = 'catalog-nav-' . teckzone_get_option( 'catalog_nav_type' );

			$classes[] = 'teckzone-catalog-page';
			$classes[] = 'catalog-' . $catalog_layout;
			$classes[] = 'catalog-view-' . apply_filters( 'teckzone_catalog_view', $catalog_view );
			$classes[] = apply_filters( 'teckzone_catalog_nav_type_class', $catalog_nav_class );

			$get_id = intval(get_option( 'woocommerce_shop_page_id' ));
			$container_width_custom = get_post_meta( $get_id, 'page_content_container_width', true );
			$catalog_width = teckzone_get_option( 'catalog_container_width' );
			if( $container_width_custom == 'default' ) {
				$catalog_width = $catalog_width;
			} else {
				$catalog_width = $container_width_custom;
			}

			if( $catalog_width == 'full-width') {
				$classes[] = 'catalog-full-width';
			} elseif( $catalog_width == 'wide') {
				$classes[] = 'catalog-full-width catalog-wide';
			} else {
				$classes[] = 'catalog-standard';
			}

			if ( intval( teckzone_get_option( 'show_excerpt' ) ) ) {
				$classes[] = 'catalog-show-excerpt';
			}

			if ( intval( teckzone_get_option( 'catalog_ajax_filter' ) ) ) {
				$classes[] = 'catalog-ajax-filter';
			}
		}

		if (intval(teckzone_get_option('added_to_cart_notice'))) {
			$classes[] = 'teckzone-atc-notice-'.teckzone_get_option('added_to_cart_notice_type');
		}

		return $classes;
	}

	public static function catalog_ajax_loader() {
		if ( ! teckzone_is_catalog() ) {
			return;
		}

		if ( ! intval( teckzone_get_option( 'catalog_ajax_filter' ) ) ) {
			return;
		}

		?>
		<div id="tz-catalog-ajax-loader" class="tz-catalog-ajax-loader fade-in"><div class="teckzone-loading"></div></div>
		<?php
	}

	/**
	 * Before Content.
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 */
	public static function wrapper_before() {
		?>
		<div id="primary" class="content-area <?php teckzone_content_columns(); ?>">
		<main id="main" class="site-main">
		<?php
	}

	/**
	 * After Content.
	 * Closes the wrapping divs.
	 */
	public static function wrapper_after() {
		?>
		</main><!-- #main -->
		</div><!-- #primary -->
		<?php
	}

	public static function product_ribbons() {
		global $product;

		$badges = (array) teckzone_get_option( 'badges' );

		if ( empty( $badges ) ) {
			return;
		}
		$output              = array();
		$custom_badges       = maybe_unserialize( get_post_meta( $product->get_id(), 'custom_badges_text', true ) );
		$custom_badges_bg    = get_post_meta( $product->get_id(), 'custom_badges_bg', true );
		$custom_badges_color = get_post_meta( $product->get_id(), 'custom_badges_color', true );

		if ( $custom_badges ) {
			$style    = '';
			$bg_color = ! empty( $custom_badges_bg ) ? 'background-color:' . $custom_badges_bg . ' !important;' : '';
			$color    = ! empty( $custom_badges_color ) ? 'color:' . $custom_badges_color . ' !important;' : '';

			if ( $bg_color || $color ) {
				$style = 'style="' . $color . $bg_color . '"';
			}

			$output[] = '<span class="custom ribbon"' . $style . '>' . esc_html( $custom_badges ) . '</span>';

		} else {
			if ( ! $product->is_in_stock() && in_array( 'outofstock', $badges ) ) {
				$outofstock = teckzone_get_option( 'outofstock_text' );
				if ( ! $outofstock ) {
					$outofstock = esc_html__( 'Out Of Stock', 'teckzone' );
				}
				$output[] = '<span class="out-of-stock ribbon">' . esc_html( $outofstock ) . '</span>';
			} elseif ( $product->is_on_sale() && in_array( 'sale', $badges ) ) {
				$percentage = 0;
				$save       = 0;
				if ( $product->get_type() == 'variable' ) {
					$available_variations = $product->get_available_variations();
					$percentage           = 0;
					$save                 = 0;

					for ( $i = 0; $i < count( $available_variations ); $i ++ ) {
						$variation_id     = $available_variations[$i]['variation_id'];
						$variable_product = new WC_Product_Variation( $variation_id );
						$regular_price    = $variable_product->get_regular_price();
						$sales_price      = $variable_product->get_sale_price();
						if ( empty( $sales_price ) ) {
							continue;
						}
						$max_percentage = $regular_price ? round( ( ( ( $regular_price - $sales_price ) / $regular_price ) * 100 ) ) : 0;
						$max_save       = $regular_price ? $regular_price - $sales_price : 0;

						if ( $percentage < $max_percentage ) {
							$percentage = $max_percentage;
						}

						if ( $save < $max_save ) {
							$save = $max_save;
						}
					}
				} elseif ( $product->get_type() == 'simple' || $product->get_type() == 'external' ) {
					$percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
					$save       = $product->get_regular_price() - $product->get_sale_price();
				}
				if ( $percentage ) {
					$output[] = '<span class="onsale ribbon">' . $percentage . '% ' . esc_html__( 'off', 'teckzone' ) . '</span>';
				}

			} elseif ( $product->is_featured() && in_array( 'hot', $badges ) ) {
				$hot = teckzone_get_option( 'hot_text' );
				if ( ! $hot ) {
					$hot = esc_html__( 'Hot', 'teckzone' );
				}
				$output[] = '<span class="featured ribbon">' . esc_html( $hot ) . '</span>';
			} elseif ( ( time() - ( 60 * 60 * 24 * teckzone_get_option( 'product_newness' ) ) ) < strtotime( get_the_time( 'Y-m-d' ) ) && in_array( 'new', $badges ) ||
				get_post_meta( $product->get_id(), '_is_new', true ) == 'yes'
			) {
				$new = teckzone_get_option( 'new_text' );
				if ( ! $new ) {
					$new = esc_html__( 'New', 'teckzone' );
				}
				$output[] = '<span class="newness ribbon">' . esc_html( $new ) . '</span>';
			}
		}

		if ( $output ) {
			echo sprintf( '<span class="ribbons">%s</span>', implode( '', $output ) );
		}
	}

	public static function single_product_ribbons() {
		global $product;

		$output = '';

		$format = array(
			'decimal_separator'  => wc_get_price_decimal_separator(),
			'thousand_separator' => wc_get_price_thousand_separator(),
			'decimals'           => wc_get_price_decimals(),
		);

		if ( $product->is_on_sale() ) {
			$percentage = 0;
			$save       = 0;
			if ( $product->get_type() == 'variable' ) {
				$available_variations = $product->get_available_variations();
				$percentage           = 0;
				$save                 = 0;

				for ( $i = 0; $i < count( $available_variations ); $i ++ ) {
					$variation_id     = $available_variations[$i]['variation_id'];
					$variable_product = new WC_Product_Variation( $variation_id );
					$regular_price    = $variable_product->get_regular_price();
					$sales_price      = $variable_product->get_sale_price();
					if ( empty( $sales_price ) ) {
						continue;
					}
					$max_percentage = $regular_price ? round( ( ( ( $regular_price - $sales_price ) / $regular_price ) * 100 ) ) : 0;
					$max_save       = $regular_price ? $regular_price - $sales_price : 0;

					if ( $percentage < $max_percentage ) {
						$percentage = $max_percentage;
					}

					if ( $save < $max_save ) {
						$save = $max_save;
					}
				}
			} elseif ( $product->get_type() == 'simple' || $product->get_type() == 'external' ) {
				$percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
				$save       = $product->get_regular_price() - $product->get_sale_price();
			}
			if ( $save ) {
				$save   = number_format( $save, $format['decimals'], $format['decimal_separator'], $format['thousand_separator'] );
				$output = '<span class="onsale ribbon"><span>' . esc_html__( 'Save', 'teckzone' ) . '</span>' . get_woocommerce_currency_symbol() . $save . '</span>';
			}
		}

		if ( $output ) {
			echo sprintf( '<span class="ribbons">%s</span>', $output );
		}
	}

	/**
	 * Change number of columns when display cross sells products
	 *
	 * @param  int $cross_columns
	 *
	 * @return int
	 */
	public static function cross_sells_columns( $cross_columns ) {
		return intval( teckzone_get_option( 'cross_sells_products_columns' ) );
	}

	/**
	 * Change number of columns when display cross sells products
	 *
	 * @param  int $cross_numbers
	 *
	 * @return int
	 */
	public static function cross_sells_numbers( $cross_numbers ) {
		return intval( teckzone_get_option( 'cross_sells_products_numbers' ) );
	}

	/**
	 * Get inline style
	 */
	public static function get_inline_style() {
		$custom_badge = teckzone_get_option( 'custom_badge' );

		$inline_css = '';
		if ( intval( $custom_badge ) ) {
			$color    = teckzone_get_option( 'custom_badge_color' );
			$bg_color = teckzone_get_option( 'custom_badge_bg_color' );

			if ( $bg_color ) {
				$inline_css .= '.woocommerce .ribbons .ribbon {background-color:' . $bg_color . ' !important;}';
			}

			if ( $color ) {
				$inline_css .= '.woocommerce .ribbons .ribbon {color:' . $color . ' !important;}';
			}

		} else {
			// Hot
			$hot_bg_color = teckzone_get_option( 'hot_bg_color' );
			$hot_color    = teckzone_get_option( 'hot_color' );
			if ( ! empty( $hot_bg_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon.featured {background-color:' . $hot_bg_color . ';}';
			}

			if ( ! empty( $hot_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon.featured {color:' . $hot_color . ';}';
			}

			// Out of stock
			$outofstock_bg_color = teckzone_get_option( 'outofstock_bg_color' );
			$outofstock_color    = teckzone_get_option( 'outofstock_color' );

			if ( ! empty( $outofstock_bg_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon.out-of-stock {background-color:' . $outofstock_bg_color . ';}';
			}

			if ( ! empty( $outofstock_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon.out-of-stock {color:' . $outofstock_color . ';}';
			}

			// New
			$new_bg_color = teckzone_get_option( 'new_bg_color' );
			$new_color    = teckzone_get_option( 'new_color' );

			if ( ! empty( $new_bg_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon {background-color:' . $new_bg_color . ';}';
			}

			if ( ! empty( $new_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon {color:' . $new_color . ';}';
			}

			// Sale
			$sale_bg_color = teckzone_get_option( 'sale_bg_color' );
			$sale_color    = teckzone_get_option( 'sale_color' );

			if ( ! empty( $sale_bg_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon.onsale {background-color:' . $sale_bg_color . ';}';
			}

			if ( ! empty( $sale_color ) ) {
				$inline_css .= '.woocommerce .ribbons .ribbon.onsale {color:' . $sale_color . ';}';
			}
		}

		// Add to Cart
		$woo_button_custom           = teckzone_get_option( 'woo_button_custom' );
		$woo_button_color            = teckzone_get_option( 'woo_button_color' );
		$woo_button_background_color = teckzone_get_option( 'woo_button_background_color' );

		if ( intval( $woo_button_custom ) && $woo_button_color ) {
			$inline_css .= 'ul.products li.product .product-button .button {color:' . $woo_button_color . ' !important;}';
			$inline_css .= 'ul.products li.product .product-button .button:before {border-color:' . $woo_button_color . ' transparent ' . $woo_button_color . ' transparent !important;}';
		}

		if ( intval( $woo_button_custom ) && $woo_button_background_color ) {
			$inline_css .= 'ul.products li.product .product-button .button {background-color:' . $woo_button_background_color . ' !important;}';
		}

		// Single Product Buy Now Button
		$buy_now_custom                  = teckzone_get_option( 'buy_now_custom' );
		$buy_now_button_color            = teckzone_get_option( 'buy_now_button_color' );
		$buy_now_button_background_color = teckzone_get_option( 'buy_now_button_background_color' );

		if ( intval( $buy_now_custom ) && $buy_now_button_color ) {
			$inline_css .= '.woocommerce div.product form.cart .buy_now_button {color:' . $buy_now_button_color . ' !important;}';
		}

		if ( intval( $buy_now_custom ) && $buy_now_button_background_color ) {
			$inline_css .= '.woocommerce div.product form.cart .buy_now_button {background-color:' . $buy_now_button_background_color . ' !important;}';
		}

		// Single Product Add To Cart Button
		$add_to_cart_custom                  = teckzone_get_option( 'add_to_cart_custom' );
		$add_to_cart_button_color            = teckzone_get_option( 'add_to_cart_button_color' );
		$add_to_cart_button_background_color = teckzone_get_option( 'add_to_cart_button_background_color' );

		if ( intval( $add_to_cart_custom ) && $add_to_cart_button_color ) {
			$inline_css .= '.woocommerce div.product form.cart .single_add_to_cart_button {color:' . $add_to_cart_button_color . ' !important;}';
			$inline_css .= '.woocommerce div.product form.cart .single_add_to_cart_button.loading:before {border-color:' . $add_to_cart_button_color . ' transparent ' . $add_to_cart_button_color . ' transparent !important;}';
		}

		if ( intval( $add_to_cart_custom ) && $add_to_cart_button_background_color ) {
			$inline_css .= '.woocommerce div.product form.cart .single_add_to_cart_button {background-color:' . $woo_button_background_color . ' !important;}';
		}

		return $inline_css;
	}

	// Wishlist

	public static function teckzone_before_wishlist_share() {
		echo '<div class="socials-with-background socials-with-content">';
	}

	public static function teckzone_after_wishlist_share() {
		echo '</div>';
	}

	public static function teckzone_wishlist_add_to_cart_label( $label ) {
		global $product;
		if ( $product->get_stock_status() == 'outofstock' || $product->get_type() == 'grouped' ) {

			$label = esc_html__( 'Read More', 'teckzone' );
		}

		if ( $product->get_type() == 'external' ) {
			$label = esc_html__( 'Buy Product', 'teckzone' );
		}

		return $label;
	}

	public static function wcwl_loop_positions() {
		return 'shortcode';
	}

	public static function wcwl_button_icon( $icon ) {
		if ( ! $icon ) {
			$icon = 'icon-heart';
		}

		return $icon;
	}

	public static function wcwl_button_added_icon( $icon ) {
		if ( ! $icon ) {
			$icon = 'fa-heart';
		}

		return $icon;
	}

	public static function wcwl_remove_from_wishlist_label( $label ) {
		$label = esc_html__( 'Remove', 'teckzone' );

		return $label;
	}

	public static function replace_default_wishlist_label( $options ) {
		$options['add_to_wishlist']['add_to_wishlist_text']['default'] = esc_html__( 'Wishlist', 'teckzone' );
		$options['add_to_wishlist']['browse_wishlist_text']['default'] = esc_html__( 'Wishlist', 'teckzone' );

		return $options;
	}
}
