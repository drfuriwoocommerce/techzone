<?php
/**
 * Theme options of WooCommerce.
 *
 * @package Teckzone
 */

/**
 * Get product attributes
 *
 * @return string
 */
function teckzone_product_attributes() {
	$output = array();
	if ( function_exists( 'wc_get_attribute_taxonomies' ) ) {
		$attributes_tax = wc_get_attribute_taxonomies();
		if ( $attributes_tax ) {
			$output['none'] = esc_html__( 'None', 'teckzone' );

			foreach ( $attributes_tax as $attribute ) {
				$output[$attribute->attribute_name] = $attribute->attribute_label;
			}

		}
	}

	return $output;
}

/**
 * Adds theme options panels of WooCommerce.
 *
 * @param array $panels Theme options panels.
 *
 * @return array
 */
function teckzone_woocommerce_customize_panels( $panels ) {
	$panels['shop'] = array(
		'priority' => 200,
		'title'    => esc_html__( 'Catalog Page', 'teckzone' ),
	);

	$panels['single_product'] = array(
		'priority' => 200,
		'title'    => esc_html__( 'Product Page', 'teckzone' ),
	);

	return $panels;
}

add_filter( 'teckzone_customize_panels', 'teckzone_woocommerce_customize_panels' );

/**
 * Adds theme options sections of WooCommerce.
 *
 * @param array $sections Theme options sections.
 *
 * @return array
 */
function teckzone_woocommerce_customize_sections( $sections ) {
	$sections = array_merge(
		$sections, array(
			'product_notifications'           => array(
				'title'    => esc_html__( 'Product Notifications', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'woocommerce',
			),
			'catalog'                  => array(
				'title'    => esc_html__( 'Catalog General', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'shop',
			),
			'toolbar'                  => array(
				'title'    => esc_html__( 'Catalog Toolbar', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'shop',
			),
			'catalog_page'             => array(
				'title'       => esc_html__( 'Shop Page', 'teckzone' ),
				'description' => '',
				'priority'    => 60,
				'panel'       => 'shop',
			),
			'product_cat_level_1_page' => array(
				'title'       => esc_html__( 'Category Level 1 Page', 'teckzone' ),
				'description' => '',
				'priority'    => 60,
				'panel'       => 'shop',
			),
			// Single product
			'single_product'           => array(
				'title'    => esc_html__( 'Single Product', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'single_product',
			),
			'single_product_button'           => array(
				'title'    => esc_html__( 'Button', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'single_product',
			),
			'upsells_product'          => array(
				'title'    => esc_html__( 'Upsells Products', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'single_product',
			),
			'related_product'          => array(
				'title'    => esc_html__( 'Related Products', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'single_product',
			),
			'instagram_photos'         => array(
				'title'    => esc_html__( 'Instagram Photos', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'single_product',
			),
			'cross_sells_product'      => array(
				'title'    => esc_html__( 'Cross-sells Products', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'shop',
			),
			'product_fbt'              => array(
				'title'    => esc_html__( 'Frequently Bought Together', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'single_product',
			),

			// Woocommerce
			'cart_page'                => array(
				'title'    => esc_html__( 'Cart Page', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'woocommerce',
			),
			'account_page'                => array(
				'title'    => esc_html__( 'Account Page', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'woocommerce',
			),
			'badge'                    => array(
				'title'       => esc_html__( 'Badges', 'teckzone' ),
				'description' => '',
				'priority'    => 60,
				'panel'       => 'woocommerce',
				'capability'  => 'edit_theme_options',
			),
			'catalog_widget'           => array(
				'title'    => esc_html__( 'Widget', 'teckzone' ),
				'priority' => 60,
				'panel'    => 'woocommerce',
			),
		)
	);

	return $sections;
}

add_filter( 'teckzone_customize_sections', 'teckzone_woocommerce_customize_sections' );

/**
 * Adds theme options of WooCommerce.
 *
 * @param array $settings Theme options.
 *
 * @return array
 */
function teckzone_woocommerce_customize_fields( $fields ) {
	// Product page.
	$fields = array_merge(
		$fields, array(
			'added_to_cart_notice'         => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Added to Cart Notification', 'teckzone' ),
				'description' => esc_html__( 'Display a notification when a product is added to cart', 'teckzone' ),
				'section'     => 'product_notifications',
				'priority'    => 70,
				'default'     => 1,
			),
			'added_to_cart_notice_type'               => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Added to Cart Notification Type', 'teckzone' ),
				'default'     => 'default',
				'section'     => 'product_notifications',
				'priority'    => 70,
				'description' => esc_html__( 'Select type for notification.', 'teckzone' ),
				'choices'     => array(
					'default' => esc_html__( 'Default', 'teckzone' ),
					'popup'   => esc_html__( 'Popup', 'teckzone' ),
				),
			),
			'cart_notice_auto_hide'        => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Cart Notification Auto Hide', 'teckzone' ),
				'description' => esc_html__( 'How many seconds you want to hide the notification.', 'teckzone' ),
				'section'     => 'product_notifications',
				'priority'    => 70,
				'active_callback' => array(
					array(
						'setting'  => 'added_to_cart_notice_type',
						'operator' => '==',
						'value'    => 'default',
					),
				),
				'default'     => 3,
			),
			'cart_notice_auto_hide_custom'            => array(
				'type'     => 'custom',
				'section'  => 'product_notifications',
				'default'  => '<hr>',
				'priority' => 70,
			),
			'added_to_wishlist_notice'                    => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Added to Wislist Notification', 'teckzone' ),
				'description' => esc_html__( 'Display a notification when a product is added to wishlist', 'teckzone' ),
				'section'     => 'product_notifications',
				'priority'    => 70,
				'default'     => 1,
			),
			'wishlist_notice_auto_hide'                   => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Wishlist Notification Auto Hide', 'teckzone' ),
				'description' => esc_html__( 'How many seconds you want to hide the notification.', 'teckzone' ),
				'section'     => 'product_notifications',
				'priority'    => 70,
				'default'     => 3,
			),

			// Catalog General
			'catalog_layout'               => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Catalog Layout', 'teckzone' ),
				'default'     => 'full-content',
				'section'     => 'catalog',
				'priority'    => 70,
				'description' => esc_html__( 'Select layout for catalog.', 'teckzone' ),
				'choices'     => array(
					'sidebar-content' => esc_html__( 'Left Sidebar', 'teckzone' ),
					'content-sidebar' => esc_html__( 'Right Sidebar', 'teckzone' ),
					'full-content'    => esc_html__( 'Full Content', 'teckzone' ),
				),
			),
			'catalog_view'                 => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Catalog View', 'teckzone' ),
				'default'  => 'grid',
				'section'  => 'catalog',
				'priority' => 70,
				'choices'  => array(
					'grid'     => esc_html__( 'Grid', 'teckzone' ),
					'list'     => esc_html__( 'List', 'teckzone' ),
					'extended' => esc_html__( 'Extended', 'teckzone' ),
				),
			),
			'catalog_layout_container_width'      => array(
				'type'     => 'custom',
				'section'  => 'catalog',
				'default'  => '<hr>',
				'priority' => 70,
			),
			'catalog_container_width'                 => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Container Width', 'teckzone' ),
				'default'  => 'normal',
				'section'  => 'catalog',
				'priority' => 70,
				'choices'  => array(
					'normal'     => esc_html__( 'Normal', 'teckzone' ),
					'full-width'     => esc_html__( 'Full Width', 'teckzone' ),
					'wide' => esc_html__( 'Wide', 'teckzone' ),
				),
			),
			'shop_custom'                  => array(
				'type'     => 'custom',
				'section'  => 'catalog',
				'default'  => '<hr>',
				'priority' => 70,
			),
			'show_excerpt'                 => array(
				'type'     => 'toggle',
				'label'    => esc_html__( 'Show Excerpt', 'teckzone' ),
				'section'  => 'catalog',
				'default'  => 0,
				'priority' => 70
			),
			'sale_percentage'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Sale Percentage', 'teckzone' ),
				'section'     => 'catalog',
				'description' => esc_html__( 'Show Sale Percentage next to Price', 'teckzone' ),
				'default'     => 0,
				'priority'    => 70
			),
			'product_attribute'            => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Product Attribute', 'teckzone' ),
				'section'     => 'catalog',
				'default'     => 'none',
				'priority'    => 70,
				'choices'     => teckzone_product_attributes(),
				'description' => esc_html__( 'Show product attribute in the product item.', 'teckzone' ),
			),
			'shop_custom_2'                => array(
				'type'     => 'custom',
				'section'  => 'catalog',
				'default'  => '<hr>',
				'priority' => 70
			),
			'catalog_ajax_filter'          => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Ajax For Filtering', 'teckzone' ),
				'section'     => 'catalog',
				'description' => esc_html__( 'Check this option to use ajax for filtering in the catalog page.', 'teckzone' ),
				'default'     => 1,
				'priority'    => 70
			),
			'shop_custom_3'                => array(
				'type'     => 'custom',
				'section'  => 'catalog',
				'default'  => '<hr>',
				'priority' => 70
			),
			'woo_button_custom'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Add to Cart Custom Color', 'teckzone' ),
				'section'     => 'catalog',
				'default'     => 0,
				'priority'    => 70,
			),
			'woo_button_color'             => array(
				'type'        => 'color',
				'label'       => esc_html__( 'Color', 'teckzone' ),
				'description' => esc_html__( 'Add to Cart button color.', 'teckzone' ),
				'default'     => '',
				'section'     => 'catalog',
				'priority'    => 70,
				'active_callback' => array(
					array(
						'setting'  => 'woo_button_custom',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
			'woo_button_background_color'  => array(
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'teckzone' ),
				'description' => esc_html__( 'Add to Cart button background color.', 'teckzone' ),
				'default'     => '',
				'section'     => 'catalog',
				'priority'    => 70,
				'active_callback' => array(
					array(
						'setting'  => 'woo_button_custom',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),

			'shop_custom_4'                => array(
				'type'     => 'custom',
				'section'  => 'catalog',
				'default'  => '<hr>',
				'priority' => 70,
				'active_callback' => array(
					array(
						'setting'  => 'catalog_layout',
						'operator' => 'in',
						'value'    => array( 'sidebar-content','content-sidebar' ),
					),
				),
			),
			'catalog_sticky_sidebar'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Sticky Sidebar', 'teckzone' ),
				'section'     => 'catalog',
				'default'     => 0,
				'priority'    => 70,
				'active_callback' => array(
					array(
						'setting'  => 'catalog_layout',
						'operator' => 'in',
						'value'    => array( 'sidebar-content','content-sidebar' ),
					),
				),
			),
			'catalog_sticky_sidebar_offset'    => array(
				'type'     => 'number',
				'label'    => esc_html__( 'Offset', 'teckzone' ),
				'section'  => 'catalog',
				'default'  => 10,
				'priority' => 70,
				'choices'  => [
					'min'  => 0,
					'max'  => 1000,
					'step' => 1,
				],
				'active_callback' => array(
					array(
						'setting'  => 'catalog_layout',
						'operator' => 'in',
						'value'    => array( 'sidebar-content','content-sidebar' ),
					),
				),
			),
			'shop_custom_5'                => array(
				'type'     => 'custom',
				'section'  => 'catalog',
				'default'  => '<hr>',
				'priority' => 70,
			),
			'catalog_nav_type'           => array(
				'type'     => 'radio',
				'section'  => 'catalog',
				'label'    => esc_html__( 'Navigation Type', 'teckzone' ),
				'default'  => 'numeric',
				'priority' => 70,
				'choices'  => array(
					'numeric'  => esc_attr__( 'Numeric', 'teckzone' ),
					'loadmore' => esc_attr__( 'Load More', 'teckzone' ),
					'infinite' => esc_attr__( 'Infinite Scroll', 'teckzone' ),
				),
			),

			//Shop Toolbar
			'toolbar_elements'             => array(
				'type'    => 'multicheck',
				'label'   => esc_html__( 'Elements', 'teckzone' ),
				'default' => array( 'total_product', 'page', 'sort_by', 'view' ),
				'choices' => array(
					'total_product' => esc_attr__( 'Total Products', 'teckzone' ),
					'page'          => esc_attr__( 'Page', 'teckzone' ),
					'sort_by'       => esc_attr__( 'Sort by', 'teckzone' ),
					'view'          => esc_attr__( 'View', 'teckzone' ),
				),
				'section' => 'toolbar',
			),

			// Badge
			'catalog_badges'               => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Catalog Badges', 'teckzone' ),
				'section'     => 'badge',
				'default'     => 1,
				'priority'    => 20,
				'description' => esc_html__( 'Check this to show badges in the catalog page.', 'teckzone' ),
			),
			'badges'                       => array(
				'type'        => 'multicheck',
				'label'       => esc_html__( 'Badges', 'teckzone' ),
				'section'     => 'badge',
				'default'     => array( 'hot', 'new', 'sale', 'outofstock' ),
				'priority'    => 20,
				'choices'     => array(
					'hot'        => esc_html__( 'Hot', 'teckzone' ),
					'new'        => esc_html__( 'New', 'teckzone' ),
					'sale'       => esc_html__( 'Sale', 'teckzone' ),
					'outofstock' => esc_html__( 'Out Of Stock', 'teckzone' ),
				),
				'description' => esc_html__( 'Select which badges you want to show', 'teckzone' ),
			),
			'hot_badge_custom'             => array(
				'type'     => 'custom',
				'label'    => '<hr/>',
				'default'  => '<h2>' . esc_html__( 'Hot Badge', 'teckzone' ) . '</h2>',
				'section'  => 'badge',
				'priority' => 20,
			),
			'hot_text'                     => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Text', 'teckzone' ),
				'section'         => 'badge',
				'default'         => 'Hot',
				'priority'        => 20,
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'hot',
					),
				),
			),
			'hot_color'                    => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'hot',
					),
				),
			),
			'hot_bg_color'                 => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Background Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'hot',
					),
				),
			),
			'outofstock_badge_custom'      => array(
				'type'     => 'custom',
				'label'    => '<hr/>',
				'default'  => '<h2>' . esc_html__( 'Out of stock Badge', 'teckzone' ) . '</h2>',
				'section'  => 'badge',
				'priority' => 20,
			),
			'outofstock_text'              => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Text', 'teckzone' ),
				'section'         => 'badge',
				'default'         => 'Out Of Stock',
				'priority'        => 20,
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'outofstock',
					),
				),
			),
			'outofstock_color'             => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'outofstock',
					),
				),
			),
			'outofstock_bg_color'          => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Background Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'outofstock',
					),
				),
			),
			'new_badge_custom'             => array(
				'type'     => 'custom',
				'label'    => '<hr/>',
				'default'  => '<h2>' . esc_html__( 'New Badge', 'teckzone' ) . '</h2>',
				'section'  => 'badge',
				'priority' => 20,
			),
			'new_text'                     => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Text', 'teckzone' ),
				'section'         => 'badge',
				'default'         => 'New',
				'priority'        => 20,
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'new',
					),
				),
			),
			'new_color'                    => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'new',
					),
				),
			),
			'new_bg_color'                 => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Background Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'new',
					),
				),
			),
			'product_newness'              => array(
				'type'            => 'number',
				'label'           => esc_html__( 'Product Newness', 'teckzone' ),
				'section'         => 'badge',
				'default'         => 3,
				'priority'        => 20,
				'description'     => esc_html__( 'Display the "New" badge for how many days?', 'teckzone' ),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'new',
					),
				),
			),
			'sale_badge_custom'            => array(
				'type'     => 'custom',
				'label'    => '<hr/>',
				'default'  => '<h2>' . esc_html__( 'Sale Badge', 'teckzone' ) . '</h2>',
				'section'  => 'badge',
				'priority' => 20,
			),
			'sale_color'                   => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'sale',
					),
				),
			),
			'sale_bg_color'                => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Background Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'badges',
						'operator' => 'contains',
						'value'    => 'sale',
					),
				),
			),

			'custom_badge_custom'          => array(
				'type'     => 'custom',
				'label'    => '<hr/>',
				'default'  => '<h2>' . esc_html__( 'Custom Badge', 'teckzone' ) . '</h2>',
				'section'  => 'badge',
				'priority' => 20,
			),
			'custom_badge'                 => array(
				'type'     => 'toggle',
				'label'    => esc_html__( 'Custom Badge', 'teckzone' ),
				'section'  => 'badge',
				'default'  => 0,
				'priority' => 20,
			),
			'custom_badge_color'           => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'custom_badge',
						'operator' => '=',
						'value'    => 1,
					),
				),
			),
			'custom_badge_bg_color'        => array(
				'type'            => 'color',
				'label'           => esc_html__( 'Background Color', 'teckzone' ),
				'default'         => '',
				'section'         => 'badge',
				'priority'        => 20,
				'choices'         => array(
					'alpha' => true,
				),
				'active_callback' => array(
					array(
						'setting'  => 'custom_badge',
						'operator' => '=',
						'value'    => 1,
					),
				),
			),

			//Product
			'product_image_zoom'           => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Image Zoom', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => 1,
				'description' => esc_html__( 'Check this option to show a bigger size product image on mouseover', 'teckzone' ),
				'priority'    => 40,
			),
			'product_images_lightbox'      => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Images Lightbox', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => 1,
				'description' => esc_html__( 'Check this option to open product gallery images in a lightbox', 'teckzone' ),
				'priority'    => 40,
			),
			'product_add_to_cart_ajax'     => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Add to cart with AJAX', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => 1,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to enable add to cart with AJAX on the product page.', 'teckzone' ),
			),
			'sticky_product_info'          => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Sticky Product Info', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => 0,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to enable sticky product info on the product page.', 'teckzone' ),
			),
			'sticky_product_info_offset'     => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Sticky Product Info Offset', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => 0,
				'priority'    => 40,
			),
			'product_layout_custom'        => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'product_layout'               => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Layout', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => '1',
				'priority'    => 40,
				'choices'     => array(
					'1' => esc_html__( 'Layout 1', 'teckzone' ),
					'2' => esc_html__( 'Layout 2', 'teckzone' ),
					'3' => esc_html__( 'Layout 3', 'teckzone' ),
				),
				'description' => esc_html__( 'Select layout for product page.', 'teckzone' ),
			),
			'product_sidebar'              => array(
				'type'            => 'select',
				'label'           => esc_html__( 'Sidebar', 'teckzone' ),
				'section'         => 'single_product',
				'default'         => 'full-content',
				'priority'        => 40,
				'choices'         => array(
					'full-content'    => esc_html__( 'No Sidebar', 'teckzone' ),
					'content-sidebar' => esc_html__( 'Content Sidebar', 'teckzone' ),
					'sidebar-content' => esc_html__( 'Sidebar Content', 'teckzone' ),
				),
				'description'     => esc_html__( 'Select default sidebar for product page.', 'teckzone' ),
				'active_callback' => array(
					array(
						'setting'  => 'product_layout',
						'operator' => 'in',
						'value'    => array( '2', '3' ),
					),
				),
			),
			'product_layout_container_width'      => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'product_container_width'                 => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Container Width', 'teckzone' ),
				'default'  => 'normal',
				'section'  => 'single_product',
				'priority' => 40,
				'choices'  => array(
					'normal'     => esc_html__( 'Normal', 'teckzone' ),
					'full-width'     => esc_html__( 'Full Width', 'teckzone' ),
					'wide' => esc_html__( 'Wide', 'teckzone' ),
				),
			),
			'product_layout_container_width_2'      => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'product_thumbnail_layout'     => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Product Thumbnail Layout', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => 'vertical',
				'priority'    => 40,
				'choices'     => array(
					'vertical'   => esc_html__( 'Vertical', 'teckzone' ),
					'horizontal' => esc_html__( 'Horizontal', 'teckzone' ),
				),
				'description' => esc_html__( 'Select layout for product page.', 'teckzone' ),
			),
			'product_thumbnail_numbers'    => array(
				'type'     => 'number',
				'label'    => esc_html__( 'Numbers of Product Thumbnail', 'teckzone' ),
				'section'  => 'single_product',
				'default'  => 5,
				'priority' => 40,
				'choices'  => [
					'min'  => 0,
					'max'  => 10,
					'step' => 1,
				],
			),
			'product_layout_custom_4'      => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'product_socials_custom'       => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 40,
			),
			'product_share_socials'        => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Product Share Socials', 'teckzone' ),
				'section'     => 'single_product',
				'default'     => 1,
				'description' => esc_html__( 'Check this option to show share socials in the single product.', 'teckzone' ),
				'priority'    => 40,
			),
			'product_social_icons'         => array(
				'type'            => 'multicheck',
				'label'           => esc_html__( 'Socials', 'teckzone' ),
				'section'         => 'single_product',
				'default'         => array( 'twitter', 'facebook', 'google', 'pinterest', 'linkedin', ),
				'priority'        => 40,
				'choices'         => array(
					'facebook'  => esc_html__( 'Facebook', 'teckzone' ),
					'twitter'   => esc_html__( 'Twitter', 'teckzone' ),
					'pinterest' => esc_html__( 'Pinterest', 'teckzone' ),
					'linkedin'  => esc_html__( 'Linkedin', 'teckzone' ),
					'google'    => esc_html__( 'Google Plus', 'teckzone' ),
					'tumblr'    => esc_html__( 'Tumblr', 'teckzone' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'product_share_socials',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),

			// Single Product Button
			'product_buy_now'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Buy Now Button', 'teckzone' ),
				'section'     => 'single_product_button',
				'default'     => 1,
				'description' => esc_html__( 'Show buy now in the single product.', 'teckzone' ),
				'priority'    => 40,
			),
			'product_buy_now_text'         => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Buy Now Text', 'teckzone' ),
				'description'     => esc_html__( 'Enter Buy not button text.', 'teckzone' ),
				'section'         => 'single_product_button',
				'default'         => esc_html__( 'Buy Now', 'teckzone' ),
				'priority'        => 40,
				'active_callback' => array(
					array(
						'setting'  => 'product_buy_now',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
			'product_buy_now_link'         => array(
				'type'            => 'textarea',
				'label'           => esc_html__( 'Buy Now Link', 'teckzone' ),
				'section'         => 'single_product_button',
				'default'         => '',
				'priority'        => 40,
				'active_callback' => array(
					array(
						'setting'  => 'product_buy_now',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
			'buy_now_custom'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Custom Color', 'teckzone' ),
				'section'     => 'single_product_button',
				'default'     => 0,
				'priority'    => 40,
				'active_callback' => array(
					array(
						'setting'  => 'product_buy_now',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
			'buy_now_button_color'             => array(
				'type'        => 'color',
				'label'       => esc_html__( 'Color', 'teckzone' ),
				'description' => esc_html__( 'Buy Now button color.', 'teckzone' ),
				'default'     => '',
				'section'     => 'single_product_button',
				'priority'    => 40,
				'active_callback' => array(
					array(
						'setting'  => 'product_buy_now',
						'operator' => '==',
						'value'    => 1,
					),
					array(
						'setting'  => 'buy_now_custom',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
			'buy_now_button_background_color'  => array(
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'teckzone' ),
				'description' => esc_html__( 'Buy Now button background color.', 'teckzone' ),
				'default'     => '',
				'section'     => 'single_product_button',
				'priority'    => 40,
				'active_callback' => array(
					array(
						'setting'  => 'product_buy_now',
						'operator' => '==',
						'value'    => 1,
					),
					array(
						'setting'  => 'buy_now_custom',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
			'single_product_button_custom'             => array(
				'type'     => 'custom',
				'label'    => '<hr/>',
				'section'  => 'single_product_button',
				'priority' => 40,
			),
			'add_to_cart_custom'              => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Add to Cart Custom Color', 'teckzone' ),
				'section'     => 'single_product_button',
				'default'     => 0,
				'priority'    => 40,
			),
			'add_to_cart_button_color'             => array(
				'type'        => 'color',
				'label'       => esc_html__( 'Color', 'teckzone' ),
				'description' => esc_html__( 'Add to Cart button color.', 'teckzone' ),
				'default'     => '',
				'section'     => 'single_product_button',
				'priority'    => 40,
				'active_callback' => array(
					array(
						'setting'  => 'add_to_cart_custom',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
			'add_to_cart_button_background_color'  => array(
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'teckzone' ),
				'description' => esc_html__( 'Add to Cart background color.', 'teckzone' ),
				'default'     => '',
				'section'     => 'single_product_button',
				'priority'    => 40,
				'active_callback' => array(
					array(
						'setting'  => 'add_to_cart_custom',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),

			// Upsells Products
			'upsells_products'             => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Upsells Products', 'teckzone' ),
				'section'     => 'upsells_product',
				'default'     => 0,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to show upsells products in single product page', 'teckzone' ),
			),
			'upsells_products_title'       => array(
				'type'     => 'text',
				'label'    => esc_html__( 'Upsells Products Title', 'teckzone' ),
				'section'  => 'upsells_product',
				'default'  => esc_html__( 'You may also like', 'teckzone' ),
				'priority' => 40,
			),
			'upsells_products_columns'     => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Upsells Products Columns', 'teckzone' ),
				'section'     => 'upsells_product',
				'default'     => '4',
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many columns of upsells products you want to show on single product page', 'teckzone' ),
				'choices'     => array(
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
					'7' => esc_html__( '7 Columns', 'teckzone' ),
				),
			),
			'upsells_products_numbers'     => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Upsells Products Numbers', 'teckzone' ),
				'section'     => 'upsells_product',
				'default'     => 6,
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many numbers of upsells products you want to show on single product page', 'teckzone' ),
			),
			// Related Products
			'related_products'             => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Related Products', 'teckzone' ),
				'section'     => 'related_product',
				'default'     => 0,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to show related products in single product page', 'teckzone' ),
			),
			'related_products_title'       => array(
				'type'     => 'text',
				'label'    => esc_html__( 'Related Products Title', 'teckzone' ),
				'section'  => 'related_product',
				'default'  => esc_html__( 'Related products', 'teckzone' ),
				'priority' => 40,
			),
			'related_products_columns'     => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Related Products Columns', 'teckzone' ),
				'section'     => 'related_product',
				'default'     => '4',
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many columns of related products you want to show on single product page', 'teckzone' ),
				'choices'     => array(
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
					'7' => esc_html__( '7 Columns', 'teckzone' ),
				),
			),
			'related_products_numbers'     => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Related Products Numbers', 'teckzone' ),
				'section'     => 'related_product',
				'default'     => 6,
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many numbers of related products you want to show on single product page', 'teckzone' ),
			),
			// Products Instagram
			'product_instagram'            => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Instagram Photos', 'teckzone' ),
				'section'     => 'instagram_photos',
				'default'     => 1,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to show instagram photos in single product page', 'teckzone' ),
			),
			'instagram_token'              => array(
				'type'            => 'textarea',
				'label'           => esc_html__( 'Access Token', 'teckzone' ),
				'section'         => 'instagram_photos',
				'default'         => '',
				'priority'        => 40,
				'description'     => esc_html__( 'Enter your Access Token', 'teckzone' ),
			),
			'product_instagram_title'      => array(
				'type'     => 'textarea',
				'label'    => esc_html__( 'Product Instagram Title', 'teckzone' ),
				'section'  => 'instagram_photos',
				'default'  => esc_html__( 'See It Styled On Instagram', 'teckzone' ),
				'priority' => 40,
			),
			'product_instagram_columns'    => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Instagram Photos Columns', 'teckzone' ),
				'section'     => 'instagram_photos',
				'default'     => '5',
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many columns of Instagram Photos you want to show on single product page', 'teckzone' ),
				'choices'     => array(
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
					'7' => esc_html__( '7 Columns', 'teckzone' ),
				),
			),
			'product_instagram_numbers'    => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Instagram Photos Numbers', 'teckzone' ),
				'section'     => 'instagram_photos',
				'default'     => 10,
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many Instagram Photos you want to show on single product page.', 'teckzone' ),
			),

			// Frequently Bought Together Products
			'product_fbt'                  => array(
				'type'     => 'toggle',
				'label'    => esc_html__( 'Show Frequently Bought Together', 'teckzone' ),
				'section'  => 'product_fbt',
				'default'  => 1,
				'priority' => 40,
			),
			'product_fbt_title'            => array(
				'type'     => 'text',
				'label'    => esc_html__( 'Frequently Bought Together Title', 'teckzone' ),
				'section'  => 'product_fbt',
				'default'  => esc_html__( 'Frequently Bought Together', 'teckzone' ),
				'priority' => 40,
			),
			'product_fbt_columns'          => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Columns', 'teckzone' ),
				'default'  => '4',
				'section'  => 'product_fbt',
				'priority' => 40,
				'choices'  => array(
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
				),
			),

			// Cart Page
			'clear_cart_button'            => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Clear Cart Button', 'teckzone' ),
				'default'     => 0,
				'section'     => 'cart_page',
				'description' => esc_html__( 'Enable to show Clear Cart button on cart page.', 'teckzone' ),
				'priority'    => 20,
			),
			'quantity_ajax'                => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Update Quantity AJAX', 'teckzone' ),
				'default'     => 0,
				'section'     => 'cart_page',
				'description' => esc_html__( 'Change qty in the cart page with AJAX', 'teckzone' ),
				'priority'    => 20,
			),

			// Cross Products
			'cart_page_custom'             => array(
				'type'     => 'custom',
				'label'    => '<hr/>',
				'default'  => '<h2>' . esc_html__( 'Cross Sells Products', 'teckzone' ) . '</h2>',
				'section'  => 'cart_page',
				'priority' => 40,
			),
			'cross_sells_products'         => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Cross Sells Products', 'teckzone' ),
				'section'     => 'cart_page',
				'default'     => 1,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to show cross-sells products in the cart page', 'teckzone' ),
			),
			'cross_sells_products_title'   => array(
				'type'     => 'text',
				'label'    => esc_html__( 'Cross Sells Products Title', 'teckzone' ),
				'section'  => 'cart_page',
				'default'  => esc_html__( 'You may be interested in...', 'teckzone' ),
				'priority' => 40,
			),
			'cross_sells_products_columns' => array(
				'type'        => 'select',
				'label'       => esc_html__( 'Cross Sells Products Columns', 'teckzone' ),
				'section'     => 'cart_page',
				'default'     => '4',
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many columns of cross-sells products you want to show on the cart page', 'teckzone' ),
				'choices'     => array(
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
					'7' => esc_html__( '7 Columns', 'teckzone' ),
				),
			),
			'cross_sells_products_numbers' => array(
				'type'        => 'number',
				'label'       => esc_html__( 'Cross Sells Products Numbers', 'teckzone' ),
				'section'     => 'cart_page',
				'default'     => 6,
				'priority'    => 40,
				'description' => esc_html__( 'Specify how many numbers of cross-sells products you want to show on the cart page', 'teckzone' ),
			),

			// Account Page
			'account_page_layout' => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Login - Register Layout', 'teckzone' ),
				'section'  => 'account_page',
				'default'  => 'default',
				'priority' => 40,
				'choices'  => array(
					'default'   => esc_html__( 'Default', 'teckzone' ),
					'promotion' => esc_html__( 'With Promotion', 'teckzone' ),
				),
			),

			'account_page_promotion_template'           => array(
				'type'     => 'select',
				'label'    => esc_html__( 'Promotion Template', 'teckzone' ),
				'section'  => 'account_page',
				'default'  => '',
				'priority' => 40,
				'choices'  => teckzone_customizer_get_posts( array( 'post_type' => 'elementor_library' ) ),
				'active_callback' => array(
					array(
						'setting'  => 'account_page_layout',
						'operator' => '==',
						'value'    => 'promotion',
					),
				),
			),

			// Catalog Widget
			'collapse_the_widget'          => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Widget Collapse', 'teckzone' ),
				'section'     => 'catalog_widget',
				'default'     => 0,
				'priority'    => 40,
				'description' => esc_html__( 'Check this option to collapse the widget on catalog sidebar.', 'teckzone' ),
			),
			'collapse_the_widget_status'   => array(
				'type'            => 'select',
				'label'           => esc_html__( 'Status', 'teckzone' ),
				'section'         => 'catalog_widget',
				'default'         => 'open',
				'priority'        => 40,
				'choices'         => array(
					'close' => esc_html__( 'Close', 'teckzone' ),
					'open'  => esc_html__( 'Open', 'teckzone' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'collapse_the_widget',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
		)
	);

	$catalog_layouts = array(
		'shop_layout'                 => array(
			'section'     => 'catalog_page',
			'field'       => 'shop',
			'label'       => esc_html__( 'Shop Layout', 'teckzone' ),
			'description' => esc_html__( 'Select default layout for shop.', 'teckzone' ),
		),
		'products_cat_level_1_layout' => array(
			'section'     => 'product_cat_level_1_page',
			'field'       => 'products_cat_level_1',
			'label'       => esc_html__( 'Category Layout', 'teckzone' ),
			'description' => esc_html__( 'Select default layout for category level 1.', 'teckzone' ),
		),
	);

	$catalog_fields = array();
	foreach ( $catalog_layouts as $layout ) {
		$catalog_fields[$layout['field'] . '_layout'] = array(
			'type'        => 'select',
			'label'       => $layout['label'],
			'default'     => '10',
			'section'     => $layout['section'],
			'description' => $layout['description'],
			'priority'    => 70,
			'choices'     => array(
				'10' => esc_html__( 'Default', 'teckzone' ),
				'1'  => esc_html__( 'Layout 1', 'teckzone' ),
				'2'  => esc_html__( 'Layout 2', 'teckzone' ),
			),
		);

		$catalog_fields[$layout['field'] . '_els'] = array(
			'type'        => 'multicheck',
			'label'       => $layout['label'] . ' ' . esc_html__( 'Elements', 'teckzone' ),
			'default'     => array( 'banners_carousel', 'banners_grid', 'title', 'categories', 'brands', 'products_carousel' ),
			'section'     => $layout['section'],
			'priority'    => 70,
			'description' => esc_html__( 'Select which elements you want to show.', 'teckzone' ),
			'choices'     => array(
				'banners_carousel'  => esc_html__( 'Banners Carousel', 'teckzone' ),
				'banners_grid'      => esc_html__( 'Banners Grid', 'teckzone' ),
				'title'             => esc_html__( 'Title', 'teckzone' ),
				'brands'            => esc_html__( 'Brands', 'teckzone' ),
				'categories'        => esc_html__( 'Categories Box', 'teckzone' ),
				'products_carousel' => esc_html__( 'Products Carousel', 'teckzone' ),
			),
		);

		/**
		 * Elements
		 */
		// Banner Carousel
		$catalog_fields[$layout['field'] . 'custom'] = array(
			'type'            => 'custom',
			'label'           => '<hr/>',
			'default'         => '<h2>' . esc_html__( 'Banners Carousel', 'teckzone' ) . '</h2>',
			'section'         => $layout['section'],
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'banners_carousel',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_banners_carousel'] = array(
			'type'            => 'repeater',
			'label'           => esc_html__( 'Banners', 'teckzone' ),
			'section'         => $layout['section'],
			'default'         => '',
			'priority'        => 70,
			'row_label'       => array(
				'type'  => 'text',
				'value' => esc_html__( 'Banner', 'teckzone' ),
			),
			'fields'          => array(
				'image'    => array(
					'type'    => 'image',
					'label'   => esc_html__( 'Image', 'teckzone' ),
					'default' => '',
				),
				'link_url' => array(
					'type'    => 'text',
					'label'   => esc_html__( 'Link(URL)', 'teckzone' ),
					'default' => '',
				),
			),
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'banners_carousel',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_banners_carousel_autoplay'] = array(
			'type'            => 'number',
			'label'           => esc_html__( 'Autoplay', 'teckzone' ),
			'description'     => esc_html__( 'Duration of animation between slides (in ms). Enter the value is 0 or empty if you want the slider is not autoplay', 'teckzone' ),
			'section'         => $layout['section'],
			'default'         => '0',
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'banners_carousel',
				),
			),
		);

		// Banner Grid
		$catalog_fields[$layout['field'] . '_banner_custom_2'] = array(
			'type'            => 'custom',
			'label'           => '<hr/>',
			'default'         => '<h2>' . esc_html__( 'Banners Grid', 'teckzone' ) . '</h2>',
			'section'         => $layout['section'],
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'banners_grid',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_banners_grid'] = array(
			'type'            => 'repeater',
			'label'           => esc_html__( 'Banners', 'teckzone' ),
			'section'         => $layout['section'],
			'default'         => '',
			'priority'        => 70,
			'row_label'       => array(
				'type'  => 'text',
				'value' => esc_html__( 'Banner', 'teckzone' ),
			),
			'fields'          => array(
				'image'    => array(
					'type'    => 'image',
					'label'   => esc_html__( 'Image', 'teckzone' ),
					'default' => '',
				),
				'link_url' => array(
					'type'    => 'text',
					'label'   => esc_html__( 'Link(URL)', 'teckzone' ),
					'default' => '',
				),
			),
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'banners_grid',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_banners_grid_columns'] = array(
			'type'            => 'select',
			'label'           => esc_html__( 'Columns', 'teckzone' ),
			'default'         => '2',
			'section'         => $layout['section'],
			'priority'        => 70,
			'choices'         => array(
				'1' => esc_html__( '1 Column', 'teckzone' ),
				'2' => esc_html__( '2 Columns', 'teckzone' ),
				'3' => esc_html__( '3 Columns', 'teckzone' ),
				'4' => esc_html__( '4 Columns', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'banners_grid',
				),
			),
		);

		// Categories Box
		$catalog_fields[$layout['field'] . '_categories_custom'] = array(
			'type'            => 'custom',
			'label'           => '<hr/>',
			'default'         => '<h2>' . esc_html__( 'Categories Box', 'teckzone' ) . '</h2>',
			'section'         => $layout['section'],
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'categories',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_categories_number'] = array(
			'type'            => 'number',
			'label'           => esc_html__( 'Categories Number', 'teckzone' ),
			'section'         => $layout['section'],
			'default'         => 10,
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'categories',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_categories_columns'] = array(
			'type'            => 'select',
			'label'           => esc_html__( 'Columns', 'teckzone' ),
			'default'         => '5',
			'section'         => $layout['section'],
			'priority'        => 70,
			'choices'         => array(
				'4' => esc_html__( '4 Columns', 'teckzone' ),
				'5' => esc_html__( '5 Columns', 'teckzone' ),
				'6' => esc_html__( '6 Columns', 'teckzone' ),
				'7' => esc_html__( '7 Columns', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'categories',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_categories_orderby'] = array(
			'type'            => 'select',
			'label'           => esc_html__( 'Order By', 'teckzone' ),
			'section'         => $layout['section'],
			'default'         => 'order',
			'priority'        => 70,
			'choices'         => array(
				'order' => esc_html__( 'Category Order', 'teckzone' ),
				'name'  => esc_html__( 'Name', 'teckzone' ),
				'count' => esc_html__( 'Count', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'categories',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_brands_custom'] = array(
			'type'            => 'custom',
			'label'           => '<hr/>',
			'default'         => '<h2>' . esc_html__( 'Brands Sliders', 'teckzone' ) . '</h2>',
			'section'         => $layout['section'],
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'brands',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_brands_title'] = array(
			'type'            => 'text',
			'label'           => esc_html__( 'Title', 'teckzone' ),
			'default'         => esc_html__( 'Featured Brands', 'teckzone' ),
			'section'         => $layout['section'],
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'brands',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_brands_number'] = array(
			'type'            => 'number',
			'label'           => esc_html__( 'Brands Number', 'teckzone' ),
			'section'         => $layout['section'],
			'default'         => 10,
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'brands',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_brands_columns'] = array(
			'type'            => 'select',
			'label'           => esc_html__( 'Columns', 'teckzone' ),
			'default'         => '5',
			'section'         => $layout['section'],
			'priority'        => 70,
			'choices'         => array(
				'3' => esc_html__( '3 Columns', 'teckzone' ),
				'4' => esc_html__( '4 Columns', 'teckzone' ),
				'5' => esc_html__( '5 Columns', 'teckzone' ),
				'6' => esc_html__( '6 Columns', 'teckzone' ),
				'7' => esc_html__( '7 Columns', 'teckzone' ),
			),
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'brands',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_brands_autoplay'] = array(
			'type'            => 'number',
			'label'           => esc_html__( 'Autoplay', 'teckzone' ),
			'default'         => '',
			'section'         => $layout['section'],
			'description'     => esc_html__( 'Duration of animation between slides (in ms). Enter the value is 0 or empty if you want the slider is not autoplay', 'teckzone' ),
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'brands',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_featured_custom'] = array(
			'type'            => 'custom',
			'label'           => '<hr/>',
			'default'         => '<h2>' . esc_html__( 'Products Carousel', 'teckzone' ) . '</h2>',
			'section'         => $layout['section'],
			'priority'        => 70,
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'products_carousel',
				),
			),
		);

		$catalog_fields[$layout['field'] . '_products_carousel'] = array(
			'type'            => 'repeater',
			'label'           => '',
			'section'         => $layout['section'],
			'default'         => '',
			'priority'        => 70,
			'row_label'       => array(
				'type'  => 'text',
				'value' => esc_html__( 'Products Carousel', 'teckzone' ),
			),
			'fields'          => array(
				'title'    => array(
					'type'    => 'text',
					'label'   => esc_html__( 'Title', 'teckzone' ),
					'default' => '',
				),
				'number'   => array(
					'type'    => 'number',
					'label'   => esc_html__( 'Number', 'teckzone' ),
					'default' => 6,
				),
				'columns'  => array(
					'type'    => 'select',
					'label'   => esc_html__( 'Columns', 'teckzone' ),
					'default' => '4',
					'choices' => array(
						'3' => esc_html__( '3 Columns', 'teckzone' ),
						'4' => esc_html__( '4 Columns', 'teckzone' ),
						'5' => esc_html__( '5 Columns', 'teckzone' ),
						'6' => esc_html__( '6 Columns', 'teckzone' ),
						'7' => esc_html__( '7 Columns', 'teckzone' ),
					),
				),
				'autoplay' => array(
					'type'        => 'number',
					'label'       => esc_html__( 'Autoplay', 'teckzone' ),
					'default'     => '',
					'description' => esc_html__( 'Duration of animation between slides (in ms). Enter the value is 0 or empty if you want the slider is not autoplay', 'teckzone' ),
				),
				'type'     => array(
					'type'    => 'select',
					'label'   => esc_html__( 'Type', 'teckzone' ),
					'default' => 'featured',
					'choices' => array(
						'featured'     => esc_html__( 'Featured Products', 'teckzone' ),
						'best_selling' => esc_html__( 'Best Seller Products', 'teckzone' ),
						'sale'         => esc_html__( 'Sale Products', 'teckzone' ),
						'recent'       => esc_html__( 'Recent Products', 'teckzone' ),
						'top_rated'    => esc_html__( 'Top Rated Products', 'teckzone' ),
					),
				),
			),
			'active_callback' => array(
				array(
					'setting'  => $layout['field'] . '_els',
					'operator' => 'contains',
					'value'    => 'products_carousel',
				),
			),
		);
	}

	$fields = array_merge( $fields, $catalog_fields );

	return $fields;
}

add_filter( 'teckzone_customize_fields', 'teckzone_woocommerce_customize_fields' );

