<?php
/**
 * Functions and Hooks for product meta box data
 *
 * @package Teckzone
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Teckzone_WooCommerce_Settings class.
 */
class Teckzone_WooCommerce_Settings {

	/**
	 * Constructor.
	 */
	public static function init() {
		if ( ! function_exists( 'is_woocommerce' ) ) {
			return false;
		}
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ) );

		// Add form
		add_action( 'woocommerce_product_data_panels', array( __CLASS__, 'product_meta_fields' ) );
		add_action( 'woocommerce_product_data_tabs', array( __CLASS__, 'product_meta_tab' ) );
		add_action( 'woocommerce_process_product_meta', array( __CLASS__, 'product_meta_fields_save' ) );

		add_action( 'wp_ajax_product_meta_fields', array( __CLASS__, 'instance_product_meta_fields' ) );
		add_action( 'wp_ajax_nopriv_product_meta_fields', array( __CLASS__, 'instance_product_meta_fields' ) );
	}

	public static function  enqueue_scripts( $hook ) {
		$screen = get_current_screen();
		if ( in_array( $hook, array( 'post.php', 'post-new.php' ) ) && $screen->post_type == 'product' ) {
			wp_enqueue_script( 'teckzone_wc_settings_js', get_template_directory_uri() . '/js/backend/woocommerce.js', array( 'jquery' ), '20190717', true );
			wp_enqueue_style( 'teckzone_wc_settings_style', get_template_directory_uri() . "/css/woocommerce-settings.css", array(), '20190717' );
		}
	}

	/**
	 * Get product data fields
	 *
	 */
	public static function instance_product_meta_fields() {
		$post_id = $_POST['post_id'];
		ob_start();
		self::create_product_extra_fields( $post_id );
		$response = ob_get_clean();
		wp_send_json_success( $response );
		die();
	}

	/**
	 * Product data tab
	 */
	public static function product_meta_tab( $product_data_tabs ) {

		$product_data_tabs['teckzone_attributes_extra'] = array(
			'label'  => esc_html__( 'Extra', 'teckzone' ),
			'target' => 'product_attributes_extra',
			'class'  => 'product-attributes-extra'
		);

		$product_data_tabs['teckzone_pbt_product'] = array(
			'label'  => esc_html__( 'Frequently Bought Together', 'teckzone' ),
			'target' => 'teckzone_pbt_product_data',
			'class'  => array( 'hide_if_grouped', 'hide_if_external', 'hide_if_bundle' ),
		);

		return $product_data_tabs;
	}

	/**
	 * Add product data fields
	 *
	 */
	public static function product_meta_fields() {
		global $post;
		self::create_product_extra_fields( $post->ID );
	}

	/**
	 * product_meta_fields_save function.
	 *
	 * @param mixed $post_id
	 */
	public static function product_meta_fields_save( $post_id ) {

		if ( isset( $_POST['attributes_extra'] ) ) {
			$woo_data = $_POST['attributes_extra'];
			update_post_meta( $post_id, 'attributes_extra', $woo_data );
		}

		if ( isset( $_POST['custom_badges_text'] ) ) {
			$woo_data = $_POST['custom_badges_text'];
			update_post_meta( $post_id, 'custom_badges_text', $woo_data );
		}

		if ( isset( $_POST['custom_badges_bg'] ) ) {
			$woo_data = $_POST['custom_badges_bg'];
			update_post_meta( $post_id, 'custom_badges_bg', $woo_data );
		}

		if ( isset( $_POST['custom_badges_color'] ) ) {
			$woo_data = $_POST['custom_badges_color'];
			update_post_meta( $post_id, 'custom_badges_color', $woo_data );
		}

		if ( isset( $_POST['_is_new'] ) ) {
			$woo_data = $_POST['_is_new'];
			update_post_meta( $post_id, '_is_new', $woo_data );
		} else {
			update_post_meta( $post_id, '_is_new', 0 );
		}

		if ( isset( $_POST['tz_pbt_product_ids'] ) ) {
			$woo_data = $_POST['tz_pbt_product_ids'];
			update_post_meta( $post_id, 'tz_pbt_product_ids', $woo_data );
		} else {
			update_post_meta( $post_id, 'tz_pbt_product_ids', 0 );
		}
	}

	/**
	 * Create product meta fields
	 *
	 * @param $post_id
	 */
	public static function create_product_extra_fields( $post_id ) {
		global $post;

		$post_custom = get_post_custom( $post_id );


		echo '<div id="product_attributes_extra" class="panel woocommerce_options_panel">';
		$attributes = maybe_unserialize( get_post_meta( $post_id, '_product_attributes', true ) );
		if ( ! $attributes ) : ?>
			<div id="message" class="inline notice woocommerce-message">
				<p><?php esc_html_e( 'You need to add attributes on the Attributes tab.', 'teckzone' ); ?></p>
			</div>

		<?php else :
			$options         = array();
			$options['']     = esc_html__( 'Default', 'teckzone' );
			$options['none'] = esc_html__( 'None', 'teckzone' );
			foreach ( $attributes as $attribute ) {
				$options[sanitize_title( $attribute['name'] )] = wc_attribute_label( $attribute['name'] );
			}
			woocommerce_wp_select(
				array(
					'id'       => 'attributes_extra',
					'label'    => esc_html__( 'Product Attribute', 'teckzone' ),
					'desc_tip' => esc_html__( 'Show product attribute for each item listed under the item name.', 'teckzone' ),
					'options'  => $options
				)
			);

		endif;
		woocommerce_wp_text_input(
			array(
				'id'       => 'custom_badges_text',
				'label'    => esc_html__( 'Custom Badge Text', 'teckzone' ),
				'desc_tip' => esc_html__( 'Enter this optional to show your badges.', 'teckzone' ),
			)
		);

		$bg_color       = ( isset( $post_custom['custom_badges_bg'][0] ) ) ? $post_custom['custom_badges_bg'][0] : '';
		woocommerce_wp_text_input(
			array(
				'id'       => 'custom_badges_bg',
				'label'    => esc_html__( 'Custom Badge Background', 'teckzone' ),
				'desc_tip' => esc_html__( 'Pick background color for your badge', 'teckzone' ),
				'value'    => $bg_color,
			)
		);

		$color       = ( isset( $post_custom['custom_badges_color'][0] ) ) ? $post_custom['custom_badges_color'][0] : '';
		woocommerce_wp_text_input(
			array(
				'id'       => 'custom_badges_color',
				'label'    => esc_html__( 'Custom Badge Color', 'teckzone' ),
				'desc_tip' => esc_html__( 'Pick color for your badge', 'teckzone' ),
				'value'    => $color,
			)
		);

		woocommerce_wp_checkbox(
			array(
				'id'          => '_is_new',
				'label'       => esc_html__( 'New product?', 'teckzone' ),
				'description' => esc_html__( 'Enable to set this product as a new product. A "New" badge will be added to this product.', 'teckzone' ),
			)
		);
		echo '</div>';
		
		?>
		<div id="teckzone_pbt_product_data" class="panel woocommerce_options_panel">

			<div class="options_group">

				<p class="form-field">
					<label for="tz_pbt_product_ids"><?php esc_html_e( 'Select Products', 'teckzone' ); ?></label>
					<select class="wc-product-search" multiple="multiple" style="width: 50%;" id="tz_pbt_product_ids" name="tz_pbt_product_ids[]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'teckzone' ); ?>" data-action="woocommerce_json_search_products_and_variations" data-exclude="<?php echo intval( $post->ID ); ?>">
						<?php
						$product_ids = maybe_unserialize( get_post_meta( $post->ID, 'tz_pbt_product_ids', true ) );

						if( $product_ids && is_array( $product_ids ) ) {
							foreach ( $product_ids as $product_id ) {
								$product = wc_get_product( $product_id );
								if ( is_object( $product ) ) {
									echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true, false ) . '>' . wp_kses_post( $product->get_formatted_name() ) . '</option>';
								}
							}
						}

						?>
					</select> <?php echo wc_help_tip( esc_html__( 'Select products for "Frequently bought together" group.', 'teckzone' ) ); ?>
				</p>

			</div>

		</div>
		<?php
	}

}