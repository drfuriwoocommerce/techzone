<?php
/**
 * Hooks of checkout.
 *
 * @package Teckzone
 */

/**
 * Class of checkout template.
 */
class Teckzone_WooCommerce_Template_Checkout {
	/**
	 * Initialize.
	 */
	public static function init() {
		add_action( 'woocommerce_checkout_order_review', array( __CLASS__, 'open_review_order_table' ), 8 );
		add_action( 'woocommerce_checkout_order_review', array( __CLASS__, 'close_review_order_table' ), 12 );
		add_action( 'woocommerce_checkout_process', array( __CLASS__, 'tz_confirm_password_checkout_validation' ) );
		add_filter( 'woocommerce_checkout_fields', array( __CLASS__, 'tz_confirm_password_checkout_field' ) );
	}

	public static function open_review_order_table() {
		echo '<div class="tz-review-order-table">';
	}

	public static function close_review_order_table() {
		echo '</div>';
	}

	public static function tz_confirm_password_checkout_validation() {
		if ( ! is_user_logged_in() && ( WC()->checkout->must_create_account || ! empty( $_POST['createaccount'] ) ) ) {
			if ( strcmp( $_POST['account_password'], $_POST['account_password2'] ) !== 0 ) {
				wc_add_notice( esc_html__( 'Passwords do not match.', 'teckzone' ), 'error' );
			}
		}
	}

	public static function tz_confirm_password_checkout_field( $fields ) {
		if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) {
			$fields['account']['account_password2'] = array(
				'type'        => 'password',
				'label'       => _x( 'Retype Password', 'Label - Checkout field', 'teckzone' ),
				'required'    => true,
				'class'       => array( 'form-row-wide' ),
				'placeholder' => _x( 'Retype Password', 'Placeholder - Checkout field', 'teckzone' ),
			);
		}

		return $fields;
	}
}