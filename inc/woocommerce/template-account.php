<?php
/**
 * Hooks of account.
 *
 * @package Teckzone
 */

/**
 * Class of account template.
 */
class Teckzone_WooCommerce_Template_Account {
	/**
	 * Initialize.
	 */
	public static function init() {
		add_filter( 'woocommerce_registration_errors', array( __CLASS__, 'tz_registration_errors_validation' ), 10, 3 );
		add_action( 'woocommerce_register_form', array( __CLASS__, 'tz_register_form_confirm_password' ), 8 );
		add_action( 'woocommerce_account_navigation', array( __CLASS__, 'account_info' ), 5 );

		if ( class_exists( 'NextendSocialLogin' ) ) {
			add_action( 'woocommerce_login_form_end', 'NextendSocialLogin::addLoginFormButtons' );
			add_action( 'woocommerce_register_form_end', 'NextendSocialLogin::addLoginFormButtons' );
		}

		add_action( 'teckzone_after_login_form', array( __CLASS__, 'account_promotion' ) );
	}

	public static function tz_registration_errors_validation( $reg_errors, $username, $email ) {
		global $woocommerce;
		extract( $_POST );

		if ( strcmp( $password, $password2 ) !== 0 ) {
			return new WP_Error( 'registration-error', esc_html__( 'Passwords do not match.', 'teckzone' ) );
		}

		return $reg_errors;
	}

	public static function tz_register_form_confirm_password() {
		if ( 'no' !== get_option( 'woocommerce_registration_generate_password' ) ) {
			return;
		}

		?>
		<p class="form-row form-row-wide">
			<input type="password" required class="input-text" name="password2" id="reg_password2" value="<?php echo ! empty( $_POST['password2'] ) ? esc_attr( $_POST['password2'] ) : '' ?>"
				   placeholder="<?php echo esc_attr_x( 'Retype Password', 'Placeholder - Register Form', 'teckzone' ); ?>" />
		</p>
		<?php
	}

	/**
	 * Get Account Info
	 */
	public static function account_info() {
		$user = get_user_by( 'ID', get_current_user_id() );
		if ( empty( $user ) ) {
			return;
		}

		?>
		<div class="account-info">
			<div class="account-avatar">
				<?php echo get_avatar( get_current_user_id(), 70 ); ?>
			</div>
			<div class="account-name">
				<span><?php esc_html_e( 'Hello!', 'teckzone' ); ?></span>

				<h3><?php echo esc_html( $user->display_name ); ?></h3>
			</div>
		</div>
		<?php
	}

	public static function account_promotion() {
		if ( ! class_exists( 'Elementor\Plugin' ) ) {
			return;
		}

		$elementor_instance = Elementor\Plugin::instance();
		$layout = teckzone_get_option( 'account_page_layout' );

		if ( $layout != 'promotion' ) {
			return;
		}

		$promotion = teckzone_get_option( 'account_page_promotion_template' );

		if ( ! $promotion ) {
			return;
		}

		echo '<div class="col-md-7 col-sm-12 col-xs-12 login-promotion">'
			. $elementor_instance->frontend->get_builder_content_for_display( $promotion )
			. '</div>';
	}
}