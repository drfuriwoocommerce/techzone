<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package Teckzone
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
 *
 * @return void
 */
function teckzone_woocommerce_setup() {
	add_theme_support( 'woocommerce', array(
		'product_grid' => array(
			'default_rows'    => 4,
			'min_rows'        => 2,
			'max_rows'        => 20,
			'default_columns' => 4,
			'min_columns'     => 2,
			'max_columns'     => 7,
		),
	) );

	if ( intval( teckzone_get_option( 'product_image_zoom' ) ) ) {
		add_theme_support( 'wc-product-gallery-zoom' );
	}

	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme',  'teckzone_woocommerce_setup' );



/**
 * WooCommerce initialize.
 */
function teckzone_woocommerce_init() {
	if ( is_admin() ) {
		Teckzone_WooCommerce_Settings::init();
	}
	Teckzone_WooCommerce_Template::init();
	Teckzone_WooCommerce_Template_Catalog::init();
	Teckzone_WooCommerce_Template_Product::init();
	Teckzone_WooCommerce_Template_Checkout::init();
	Teckzone_WooCommerce_Template_Cart::init();
	Teckzone_WooCommerce_Template_Account::init();
}

add_action( 'wp_loaded', 'teckzone_woocommerce_init' );

require get_theme_file_path( '/inc/woocommerce/settings.php' );
require get_theme_file_path( '/inc/woocommerce/theme-options.php' );
require get_theme_file_path( '/inc/woocommerce/template.php' );
require get_theme_file_path( '/inc/woocommerce/template-product.php' );
require get_theme_file_path( '/inc/woocommerce/template-catalog.php' );
require get_theme_file_path( '/inc/woocommerce/template-checkout.php' );
require get_theme_file_path( '/inc/woocommerce/template-cart.php' );
require get_theme_file_path( '/inc/woocommerce/template-account.php' );