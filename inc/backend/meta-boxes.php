<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/**
 * Enqueue script for handling actions with meta boxes
 *
 * @since 1.0
 *
 * @param string $hook
 */
function teckzone_meta_box_scripts( $hook ) {
	if ( in_array( $hook, array( 'post.php', 'post-new.php' ) ) ) {
		wp_enqueue_script( 'teckzone-meta-boxes', get_template_directory_uri() . "/js/backend/meta-boxes.js", array( 'jquery' ), '20181210', true );
	}
}

add_action( 'admin_enqueue_scripts', 'teckzone_meta_box_scripts' );

/**
 * Registering meta boxes
 *
 * Using Meta Box plugin: http://www.deluxeblogtips.com/meta-box/
 *
 * @see http://www.deluxeblogtips.com/meta-box/docs/define-meta-boxes
 *
 * @param array $meta_boxes Default meta boxes. By default, there are no meta boxes.
 *
 * @return array All registered meta boxes
 */
function teckzone_register_meta_boxes( $meta_boxes ) {
	// Post format's meta box
	$meta_boxes[] = array(
		'id'       => 'post-format-settings',
		'title'    => esc_html__( 'Format Details', 'teckzone' ),
		'pages'    => array( 'post' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields'   => array(
			array(
				'name'             => esc_html__( 'Image', 'teckzone' ),
				'id'               => 'image',
				'type'             => 'image_advanced',
				'class'            => 'image',
				'max_file_uploads' => 1,
			),
			array(
				'name'  => esc_html__( 'Gallery', 'teckzone' ),
				'id'    => 'images',
				'type'  => 'image_advanced',
				'class' => 'gallery',
			),
			array(
				'name'  => esc_html__( 'Audio', 'teckzone' ),
				'id'    => 'audio',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 2,
				'class' => 'audio',
			),
			array(
				'name'  => esc_html__( 'Video', 'teckzone' ),
				'id'    => 'video',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 2,
				'class' => 'video',
			),
			array(
				'name'  => esc_html__( 'Title', 'teckzone' ),
				'id'    => 'title',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'link',
			),
			array(
				'name'  => esc_html__( 'Description', 'teckzone' ),
				'id'    => 'desc',
				'type'  => 'textarea',
				'cols'  => 40,
				'rows'  => 2,
				'class' => 'link',
			),
			array(
				'name'  => esc_html__( 'Link', 'teckzone' ),
				'id'    => 'url',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'link',
			),
			array(
				'name'  => esc_html__( 'Text', 'teckzone' ),
				'id'    => 'url_text',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'link',
			),
			array(
				'name'  => esc_html__( 'Quote', 'teckzone' ),
				'id'    => 'quote',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 2,
				'class' => 'quote',
			),
			array(
				'name'  => esc_html__( 'Author', 'teckzone' ),
				'id'    => 'quote_author',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'quote',
			),
			array(
				'name'  => esc_html__( 'Author URL', 'teckzone' ),
				'id'    => 'author_url',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'quote',
			),
		),
	);

	// Product Features
	$meta_boxes[] = array(
		'id'       => 'product-extra-content-settings',
		'title'    => esc_html__( 'Product Extra Content', 'teckzone' ),
		'pages'    => array( 'product' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields'   => array(
			array(
				'name'    => esc_html__( 'Content', 'teckzone' ),
				'id'      => 'product_feature_items',
				'type'    => 'wysiwyg',
				'options' => array(
					'textarea_rows' => 5,
					'teeny'         => true,
				),
			),
		),
	);
	$meta_boxes[] = array(
		'id'       => 'product-videos',
		'title'    => esc_html__( 'Product Video', 'teckzone' ),
		'pages'    => array( 'product' ),
		'context'  => 'side',
		'priority' => 'low',
		'fields'   => array(
			array(
				'name' => esc_html__( 'Video URL', 'teckzone' ),
				'id'   => 'video_url',
				'type' => 'oembed',
				'std'  => false,
				'desc' => esc_html__( 'Enter URL of Youtube or Vimeo or specific filetypes such as mp4, webm, ogv.', 'teckzone' ),
			),
			array(
				'name'             => esc_html__( 'Video Thumbnail', 'teckzone' ),
				'id'               => 'video_thumbnail',
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'std'              => false,
				'desc'             => esc_html__( 'Add video thumbnail', 'teckzone' ),
			),
			array(
				'name'    => esc_html__( 'Video Position', 'teckzone' ),
				'id'      => 'video_position',
				'type'    => 'select',
				'options' => array(
					'1' => esc_html__( 'The last product gallery', 'teckzone' ),
					'2' => esc_html__( 'The first product gallery', 'teckzone' ),
				),
			),
		),
	);

	$meta_boxes[] = array(
		'id'       => 'product-360-view',
		'title'    => esc_html__( 'Product 360 View', 'teckzone' ),
		'pages'    => array( 'product' ),
		'context'  => 'side',
		'priority' => 'low',
		'fields'   => array(
			array(
				'id'   => 'product_360_view',
				'type' => 'image_advanced',
			),
		),
	);

	$meta_boxes[] = array(
		'id'       => 'tz-page-layouts',
		'title'    => esc_html__( 'Page Layouts', 'teckzone' ),
		'pages'    => array( 'page' ),
		'context'  => 'side',
		'priority' => 'low',
		'fields'   => array(
			array(
				'name'    => esc_html__( 'Header', 'teckzone' ),
				'id'      => 'header_layout',
				'type'    => 'select',
				'options' => teckzone_customizer_get_posts( array( 'post_type' => 'tz_header' ) ),
			),
			array(
				'name'    => esc_html__( 'Footer', 'teckzone' ),
				'id'      => 'footer_layout',
				'type'    => 'select',
				'options' => teckzone_customizer_get_posts( array( 'post_type' => 'tz_footer' ) ),
			),
		),
	);
	$meta_boxes[] = array(
		'id'       => 'tz-page-layouts-mobile',
		'title'    => esc_html__( 'Page Layouts Mobile', 'teckzone' ),
		'pages'    => array( 'page' ),
		'context'  => 'side',
		'priority' => 'low',
		'fields'   => array(
			array(
				'name'    => esc_html__( 'Header', 'teckzone' ),
				'id'      => 'header_mobile',
				'type'    => 'select',
				'options' => teckzone_customizer_get_posts( array( 'post_type' => 'tz_header' ) ),
				'desc'    => esc_html__( 'Select default header layout for this page on mobile version.', 'teckzone' ),
			),
			array(
				'name'    => esc_html__( 'Footer', 'teckzone' ),
				'id'      => 'footer_mobile',
				'type'    => 'select',
				'options' => teckzone_customizer_get_posts( array( 'post_type' => 'tz_footer' ) ),
				'desc'    => esc_html__( 'Select default footer layout for this page on mobile version.', 'teckzone' ),
			),
		),
	);

	// Page Header
	$meta_boxes[] = array(
		'id'       => 'page-header-settings',
		'title'    => esc_html__( 'Page Header Settings', 'teckzone' ),
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name'             => esc_html__( 'Hide Page Header', 'teckzone' ),
				'id'               => 'hide_page_header',
				'type'             => 'checkbox',
				'std'              => false,
			),
			array(
				'name'    => esc_html__( 'Layout', 'teckzone' ),
				'id'      => 'page_header_layout',
				'type'    => 'select',
				'std'     => '',
				'options' => array(
					'' => esc_html__( 'Default', 'teckzone' ),
					'1' => esc_html__( 'Layout 1', 'teckzone' ),
					'2' => esc_html__( 'Layout 2', 'teckzone' ),
				),
				'class'   => 'page-header-layout',
			),
			array(
				'name' => esc_html__( 'Hide Title', 'teckzone' ),
				'id'   => 'hide_title',
				'type' => 'checkbox',
				'std'  => false,
				'class'   => 'page-header-hide-title',
			),
			array(
				'name' => esc_html__( 'Hide Breadcrumb', 'teckzone' ),
				'id'   => 'hide_breadcrumb',
				'type' => 'checkbox',
				'std'  => false,
				'class'   => 'page-header-hide-breadcrumb',
			),
			array(
				'name'    => esc_html__( 'Custom Spacing', 'teckzone' ),
				'id'      => 'page_header_custom_spacing',
				'type'    => 'select',
				'std'     => 'default',
				'options' => array(
					'default' => esc_html__( 'Default', 'teckzone' ),
					'custom' => esc_html__( 'Custom', 'teckzone' ),
				),
				'class'   => 'page-header-custom-spacing',
			),
			array(
				'name'             => esc_html__( 'Padding Top', 'teckzone' ),
				'id'               => 'page_header_top_padding',
				'type'             => 'slider',
				'suffix' 		   => ' px',
				'js_options'       => array(
					'min'          => 0,
					'max'          => 500,
				),
				'std'              => false,
				'class'   => 'page-header-padding-top',
			),
			array(
				'name'             => esc_html__( 'Padding Bottom', 'teckzone' ),
				'id'               => 'page_header_bottom_padding',
				'type'             => 'slider',
				'suffix' 		   => ' px',
				'js_options'       => array(
					'min'          => 0,
					'max'          => 500,
				),
				'std'              => false,
				'class'   => 'page-header-padding-bottom',
			),
		)
	);

	// Content Spacing
	$meta_boxes[] = array(
		'id'       => 'page-content-top-spacing-settings',
		'title'    => esc_html__( 'Content Spacing', 'teckzone' ),
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name'    => esc_html__( 'Page Content Top Spacing', 'teckzone' ),
				'id'      => 'page_content_top_spacing',
				'type'    => 'select',
				'std'     => 'default',
				'options' => array(
					'default' => esc_html__( 'Default', 'teckzone' ),
					'no-spacing' => esc_html__( 'No Spacing', 'teckzone' ),
					'custom' => esc_html__( 'Custom', 'teckzone' ),
				),
				'class'   => 'page-content-top-spacing',
			),
			array(
				'name'             => esc_html__( 'Padding Top', 'teckzone' ),
				'id'               => 'page_content_top_padding',
				'type'             => 'slider',
				'suffix' 		   => ' px',
				'js_options'       => array(
					'min'          => 0,
					'max'          => 500,
				),
				'std'              => false,
				'class'   => 'page-content-top-padding',
			),
			array(
				'name'    => esc_html__( 'Page Content Bottom Spacing', 'teckzone' ),
				'id'      => 'page_content_bottom_spacing',
				'type'    => 'select',
				'std'     => 'default',
				'options' => array(
					'default' => esc_html__( 'Default', 'teckzone' ),
					'no-spacing' => esc_html__( 'No Spacing', 'teckzone' ),
					'custom' => esc_html__( 'Custom', 'teckzone' ),
				),
				'class'   => 'page-content-bottom-spacing',
			),
			array(
				'name'             => esc_html__( 'Padding Bottom', 'teckzone' ),
				'id'               => 'page_content_bottom_padding',
				'type'             => 'slider',
				'suffix' 		   => ' px',
				'js_options'       => array(
					'min'          => 0,
					'max'          => 500,
				),
				'std'              => 110,
				'class'   => 'page-content-bottom-padding',
			),
		)
	);

	// Content Container Width
	$meta_boxes[] = array(
		'id'       => 'page-content-container-width-settings',
		'title'    => esc_html__( 'Content Container Width', 'teckzone' ),
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name'    => esc_html__( 'Page Content Container Width', 'teckzone' ),
				'id'      => 'page_content_container_width',
				'type'    => 'select',
				'std'     => 'default',
				'options' => array(
					'default' => esc_html__( 'Default', 'teckzone' ),
					'normal' => esc_html__( 'Normal', 'teckzone' ),
					'full-width' => esc_html__( 'Full Width', 'teckzone' ),
					'wide' => esc_html__( 'Wide', 'teckzone' ),
				),
				'class'   => 'page-content-container-width',
			),
		)
	);

	return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'teckzone_register_meta_boxes' );