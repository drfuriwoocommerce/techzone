<?php
/**
 * Register post types
 *
 * @package Teckzone
 */

/**
 * Class Product Categtory
 */
class Teckzone_Product_Cat {

	/**
	 * Construction function
	 *
	 * @since 1.0.0
	 *
	 * @return Teckzone_Product_Cat
	 */

	/**
	 * @var string placeholder image
	 */
	public $placeholder_img_src;

	/**
	 * Banner Grid Columns
	 *
	 * @var array
	 */
	private $banner_grid_columns = array();

	/**
	 * Category Layout
	 *
	 * @var array
	 */
	private $cat_layout = array();

	function __construct() {

		if ( ! function_exists( 'is_woocommerce' ) ) {
			return false;
		}

		$this->cat_layout = array(
			'10' => esc_html__( 'Default', 'teckzone' ),
			'1'  => esc_html__( 'Layout 1', 'teckzone' ),
			'2'  => esc_html__( 'Layout 2', 'teckzone' ),
		);

		$this->banner_grid_columns = array(
			'1' => esc_html__( '1 Column', 'teckzone' ),
			'2' => esc_html__( '2 Columns', 'teckzone' ),
			'3' => esc_html__( '3 Columns', 'teckzone' ),
			'4' => esc_html__( '4 Columns', 'teckzone' ),
		);

		// Register custom post type and custom taxonomy
		add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );

		$this->placeholder_img_src = get_template_directory_uri() . '/images/placeholder.png';
		// Add form
		add_action( 'product_cat_add_form_fields', array( $this, 'add_category_fields' ), 30 );
		add_action( 'product_cat_edit_form_fields', array( $this, 'edit_category_fields' ), 20 );
		add_action( 'created_term', array( $this, 'save_category_fields' ), 20, 3 );
		add_action( 'edit_term', array( $this, 'save_category_fields' ), 20, 3 );
	}

	function register_admin_scripts( $hook ) {
		$screen = get_current_screen();
		if ( ( $hook == 'edit-tags.php' && ( $screen->taxonomy == 'product_cat' || $screen->taxonomy == 'product_brand' ) ) || ( $hook == 'term.php' && $screen->taxonomy == 'product_cat' ) ) {
			wp_enqueue_media();
			wp_enqueue_script( 'teckzone_product_cat_js', get_template_directory_uri() . "/js/backend/product-cat.js", array( 'jquery' ), '20190407', true );
			wp_enqueue_style( 'teckzone_product_cat_style', get_template_directory_uri() . "/css/product-cat.css", array(), '20190407' );
		}
	}

	/**
	 * Category thumbnail fields.
	 */
	function add_category_fields() {
		?>
		<div class="form-field tz-cat-banners-group">
			<label><?php esc_html_e( 'Category Layout', 'teckzone' ); ?></label>

			<div class="tz-cat-layout">
				<select id="tz_cat_layout" name="tz_cat_layout">
					<?php
					foreach ( $this->cat_layout as $key => $value ) {
						?>
						<option value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $value ); ?></option>
						<?php
					}
					?>
				</select>
			</div>
			<div class="clear"></div>
		</div>
		<div class="form-field tz-cat-banners-group">
			<label><?php esc_html_e( 'Banners Carousel', 'teckzone' ); ?></label>

			<div id="tz_cat_banners" class="tz-cat-banners">
				<ul class="tz-cat-images"></ul>
				<input type="hidden" id="tz_cat_banners_id" class="tz_cat_banners_id" name="tz_cat_banners_id" />
				<button type="button" data-multiple="1" data-delete="<?php esc_attr_e( 'Delete image', 'teckzone' ); ?>"
						data-text="<?php esc_attr_e( 'Delete', 'teckzone' ); ?>"
						class="upload_images_button button"><?php esc_html_e( 'Upload/Add Images', 'teckzone' ); ?></button>
			</div>
			<div class="clear"></div>
		</div>
		<div class="form-field tz-cat-banners-group">
			<label><?php esc_html_e( 'Banners Carousel Link', 'teckzone' ); ?></label>

			<div id="tz_cat_banners_link" class="tz-cat-banners_link">
				<textarea id="tz_cat_banners_link" cols="50" rows="3" name="tz_cat_banners_link"></textarea>

				<p class="description"><?php esc_html_e( 'Enter links for each banner here. Divide links with linebreaks (Enter).', 'teckzone' ); ?></p>
			</div>
			<div class="clear"></div>
		</div>

		<div class="form-field tz-cat-banners-2-group">
			<label><?php esc_html_e( 'Banners Grid', 'teckzone' ); ?></label>

			<div id="tz_cat_banners_2" class="tz-cat-banners_2">
				<ul class="tz-cat-images"></ul>
				<input type="hidden" id="tz_cat_banners_2_id" class="tz_cat_banners_2_id" name="tz_cat_banners_2_id" />
				<button type="button" data-multiple="1" data-delete="<?php esc_attr_e( 'Delete image', 'teckzone' ); ?>"
						data-text="<?php esc_attr_e( 'Delete', 'teckzone' ); ?>"
						class="upload_images_button button"><?php esc_html_e( 'Upload/Add Images', 'teckzone' ); ?></button>
			</div>
			<div class="clear"></div>
		</div>

		<div class="form-field tz-cat-banners-2-group">
			<label><?php esc_html_e( 'Banners Grid Link', 'teckzone' ); ?></label>

			<div id="tz_cat_banners_2_link" class="tz-cat-banners_2_link">
				<textarea id="tz_cat_banners_2_link" cols="50" rows="3" name="tz_cat_banners_2_link"></textarea>

				<p class="description"><?php esc_html_e( 'Enter links for each banner here. Divide links with linebreaks (Enter).', 'teckzone' ); ?></p>
			</div>
			<div class="clear"></div>
		</div>
		<div class="form-field tz-cat-banners-2-group">
			<label><?php esc_html_e( 'Banners Grid Columns', 'teckzone' ); ?></label>

			<div class="tz_cat_banners_2_columns">
				<select id="tz_cat_banners_2_columns" name="tz_cat_banners_2_columns">
					<?php
					foreach ( $this->banner_grid_columns as $key => $value ) {
						?>
						<option value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $value ); ?></option>
						<?php
					}
					?>
				</select>
			</div>
			<div class="clear"></div>
		</div>
		<div class="form-field tz-cat-banners-group" id="tz-custom-elements">
			<label><?php esc_html_e( 'Elements', 'teckzone' ); ?></label>

			<div class="tz-custom-elements" id="tz-custom-elements-1">
				<p>
					<input type="checkbox" checked="checked" name="product_cat_1_els[]" id="product_cat_1_banners_carousel"
						   value="banners_carousel">
					<label for="product_cat_1_banners_carousel"><?php esc_html_e( 'Banners Carousel', 'teckzone' ); ?></label>
				</p>

				<p>
					<input type="checkbox" checked="checked" name="product_cat_1_els[]" id="product_cat_1_banners_grid"
						   value="banners_grid">
					<label for="product_cat_1_banners_grid"><?php esc_html_e( 'Banners Grid', 'teckzone' ); ?></label>
				</p>

				<p>
					<input type="checkbox" checked="checked" name="product_cat_1_els[]" id="product_cat_1_title"
						   value="title">
					<label for="product_cat_1_title"><?php esc_html_e( 'Title', 'teckzone' ); ?></label>
				</p>

				<p>
					<input type="checkbox" checked="checked" name="product_cat_1_els[]" id="product_cat_1_categories"
						   value="categories">
					<label for="product_cat_1_categories"><?php esc_html_e( 'Categories Box', 'teckzone' ); ?></label>
				</p>

				<p>
					<input type="checkbox" checked="checked" name="product_cat_1_els[]" id="product_cat_1_brands"
						   value="brands">
					<label for="product_cat_1_brands"><?php esc_html_e( 'Brands', 'teckzone' ); ?></label>
				</p>

				<p>
					<input type="checkbox" checked="checked" name="product_cat_1_els[]" id="product_cat_1_carousel"
						   value="products_carousel">
					<label for="product_cat_1_carousel"><?php esc_html_e( 'Product Carousel', 'teckzone' ); ?></label>
				</p>
			</div>
			<div class="clear"></div>
		</div>

		<?php
	}

	/**
	 * Edit category thumbnail field.
	 *
	 * @param mixed $term Term (category) being edited
	 */
	function edit_category_fields( $term ) {
		$cat_layout          = get_term_meta( $term->term_id, 'tz_cat_layout', true );
		$thumbnail_ids       = get_term_meta( $term->term_id, 'tz_cat_banners_id', true );
		$thumbnail_2_ids     = get_term_meta( $term->term_id, 'tz_cat_banners_2_id', true );
		$banners_link        = get_term_meta( $term->term_id, 'tz_cat_banners_link', true );
		$banners_2_link      = get_term_meta( $term->term_id, 'tz_cat_banners_2_link', true );
		$banner_grid_columns = get_term_meta( $term->term_id, 'tz_cat_banners_2_columns', true );
		$product_cat_1_els   = get_term_meta( $term->term_id, 'product_cat_1_els', true );
		$product_cat_1_els   = $product_cat_1_els ? $product_cat_1_els : array();

		?>
		<tr class="form-field tz-cat-banners-group">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Category Layout', 'teckzone' ); ?></label></th>
			<td>
				<div class="tz-cat-layout">
					<select id="tz_cat_layout" name="tz_cat_layout">
						<?php
						foreach ( $this->cat_layout as $key => $value ) {
							$selected = ( $key == $cat_layout ) ? 'selected=selected' : '';
							?>
							<option value="<?php echo esc_attr( $key ); ?>" <?php echo esc_attr( $selected ); ?>><?php echo esc_html( $value ); ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<tr class="form-field tz-cat-banners-group">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Banners Carousel', 'teckzone' ); ?></label></th>
			<td>
				<div id="tz_cat_banners" class="tz-cat-banners">
					<ul class="tz-cat-images">
						<?php

						if ( $thumbnail_ids ) {
							$thumbnails = explode( ',', $thumbnail_ids );
							foreach ( $thumbnails as $thumbnail_id ) {
								if ( empty( $thumbnail_id ) ) {
									continue;
								}
								$image = wp_get_attachment_thumb_url( $thumbnail_id );
								if ( empty( $image ) ) {
									continue;
								}
								?>
								<li class="image" data-attachment_id="<?php echo esc_attr( $thumbnail_id ); ?>">
									<img src="<?php echo esc_url( $image ); ?>" width="100px" height="100px" />
									<ul class="actions">
										<li>
											<a href="#" class="delete"
											   title="<?php esc_attr_e( 'Delete image', 'teckzone' ); ?>"><?php esc_html_e( 'Delete', 'teckzone' ); ?></a>
										</li>
									</ul>
								</li>
								<?php
							}
						}

						?>
					</ul>
					<input type="hidden" id="tz_cat_banners_id" class="tz_cat_banners_id" name="tz_cat_banners_id"
						   value="<?php echo esc_attr( $thumbnail_ids ); ?>" />
					<button type="button" data-multiple="1"
							data-delete="<?php esc_attr_e( 'Delete image', 'teckzone' ); ?>"
							data-text="<?php esc_attr_e( 'Delete', 'teckzone' ); ?>"
							class="upload_images_button button"><?php esc_html_e( 'Upload/Add Images', 'teckzone' ); ?></button>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<tr class="form-field tz-cat-banners-group">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Banners Carousel Link', 'teckzone' ); ?></label></th>
			<td>
				<div id="tz_cat_banners_link" class="tz-cat-banners-link">
                    <textarea id="tz_cat_banners_link" cols="50" rows="4"
							  name="tz_cat_banners_link"><?php echo esc_html( $banners_link ); ?></textarea>

					<p class="description"><?php esc_html_e( 'Enter links for each banner here. Divide links with linebreaks (Enter).', 'teckzone' ); ?></p>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<tr class="form-field tz-cat-banners-2-group">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Banners Grid', 'teckzone' ); ?></label></th>
			<td>
				<div id="tz_cat_banners_2" class="tz-cat-banners_2">
					<ul class="tz-cat-images">
						<?php

						if ( $thumbnail_2_ids ) {
							$thumbnails = explode( ',', $thumbnail_2_ids );
							foreach ( $thumbnails as $thumbnail_id ) {
								if ( empty( $thumbnail_id ) ) {
									continue;
								}
								$image = wp_get_attachment_thumb_url( $thumbnail_id );
								if ( empty( $image ) ) {
									continue;
								}
								?>
								<li class="image" data-attachment_id="<?php echo esc_attr( $thumbnail_id ); ?>">
									<img src="<?php echo esc_url( $image ); ?>" width="100px" height="100px" />
									<ul class="actions">
										<li>
											<a href="#" class="delete"
											   title="<?php esc_attr_e( 'Delete image', 'teckzone' ); ?>"><?php esc_html_e( 'Delete', 'teckzone' ); ?></a>
										</li>
									</ul>
								</li>
								<?php
							}
						}

						?>
					</ul>
					<input type="hidden" id="tz_cat_banners_2_id" class="tz_cat_banners_2_id" name="tz_cat_banners_2_id"
						   value="<?php echo esc_attr( $thumbnail_2_ids ); ?>" />
					<button type="button" data-multiple="1"
							data-delete="<?php esc_attr_e( 'Delete image', 'teckzone' ); ?>"
							data-text="<?php esc_attr_e( 'Delete', 'teckzone' ); ?>"
							class="upload_images_button button"><?php esc_html_e( 'Upload/Add Images', 'teckzone' ); ?></button>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<tr class="form-field tz-cat-banners-2-group">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Banners Grid Link', 'teckzone' ); ?></label></th>
			<td>
				<div id="tz_cat_banners_2_link" class="tz-cat-banners-2-link">
                    <textarea id="tz_cat_banners_2_link" cols="50" rows="4"
							  name="tz_cat_banners_2_link"><?php echo esc_html( $banners_2_link ); ?></textarea>

					<p class="description"><?php esc_html_e( 'Enter links for each banner here. Divide links with linebreaks (Enter).', 'teckzone' ); ?></p>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<tr class="form-field tz-cat-banners-2-group">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Banner Grid Columns', 'teckzone' ); ?></label></th>
			<td>
				<div class="tz_cat_banners_2_columns">
					<select id="tz_cat_banners_2_columns" name="tz_cat_banners_2_columns">
						<?php
						foreach ( $this->banner_grid_columns as $key => $value ) {
							$selected = ( $key == $banner_grid_columns ) ? 'selected=selected' : '';
							?>
							<option value="<?php echo esc_attr( $key ); ?>" <?php echo esc_attr( $selected ); ?>><?php echo esc_html( $value ); ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<tr class="form-field tz-cat-banners-group" id="tz-custom-elements">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Elements', 'teckzone' ); ?></label></th>
			<td>
				<div class="tz-custom-elements" id="tz-custom-elements-1">
					<p>
						<input type="checkbox" <?php checked( in_array( 'banners_carousel', $product_cat_1_els ) ); ?>
							   name="product_cat_1_els[]" id="product_cat_1_banners_carousel" value="banners_carousel">
						<label for="product_cat_1_banners_carousel"><?php esc_html_e( 'Banners Carousel', 'teckzone' ); ?></label>
					</p>

					<p>
						<input type="checkbox" <?php checked( in_array( 'banners_grid', $product_cat_1_els ) ); ?>
							   name="product_cat_1_els[]" id="product_cat_1_banners_grid" value="banners_grid">
						<label for="product_cat_1_banners_grid"><?php esc_html_e( 'Banners Grid', 'teckzone' ); ?></label>
					</p>

					<p>
						<input type="checkbox" <?php checked( in_array( 'title', $product_cat_1_els ) ); ?>
							   name="product_cat_1_els[]" id="product_cat_1_title" value="title">
						<label for="product_cat_1_title"><?php esc_html_e( 'Title', 'teckzone' ); ?></label>
					</p>

					<p>
						<input type="checkbox" <?php checked( in_array( 'categories', $product_cat_1_els ) ); ?>
							   name="product_cat_1_els[]" id="product_cat_1_categories" value="categories">
						<label for="product_cat_1_categories"><?php esc_html_e( 'Categories Box', 'teckzone' ); ?></label>
					</p>

					<p>
						<input type="checkbox" <?php checked( in_array( 'brands', $product_cat_1_els ) ); ?>
							   name="product_cat_1_els[]" id="product_cat_1_brands" value="brands">
						<label for="product_cat_1_brands"><?php esc_html_e( 'Brands', 'teckzone' ); ?></label>
					</p>

					<p>
						<input type="checkbox" <?php checked( in_array( 'products_carousel', $product_cat_1_els ) ); ?>
							   name="product_cat_1_els[]" id="product_cat_1_carousel" value="products_carousel">
						<label for="product_cat_1_carousel"><?php esc_html_e( 'Product Carousel', 'teckzone' ); ?></label>
					</p>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
		<?php
	}

	/**
	 * save_category_fields function.
	 *
	 * @param mixed  $term_id Term ID being saved
	 * @param mixed  $tt_id
	 * @param string $taxonomy
	 */
	function save_category_fields( $term_id, $tt_id = '', $taxonomy = '' ) {

		if ( 'product_cat' === $taxonomy && function_exists( 'update_term_meta' ) ) {
			if ( isset( $_POST['tz_cat_layout'] ) ) {
				update_term_meta( $term_id, 'tz_cat_layout', $_POST['tz_cat_layout'] );
			}

			if ( isset( $_POST['tz_cat_banners_id'] ) ) {
				update_term_meta( $term_id, 'tz_cat_banners_id', $_POST['tz_cat_banners_id'] );
			}

			if ( isset( $_POST['tz_cat_banners_link'] ) ) {
				update_term_meta( $term_id, 'tz_cat_banners_link', $_POST['tz_cat_banners_link'] );
			}

			if ( isset( $_POST['tz_cat_banners_2_id'] ) ) {
				update_term_meta( $term_id, 'tz_cat_banners_2_id', $_POST['tz_cat_banners_2_id'] );
			}

			if ( isset( $_POST['tz_cat_banners_2_link'] ) ) {
				update_term_meta( $term_id, 'tz_cat_banners_2_link', $_POST['tz_cat_banners_2_link'] );
			}

			if ( isset( $_POST['tz_cat_banners_2_columns'] ) ) {
				update_term_meta( $term_id, 'tz_cat_banners_2_columns', $_POST['tz_cat_banners_2_columns'] );
			}

			$els_1 = array();
			if ( isset( $_POST['product_cat_1_els'] ) ) {
				$els_1 = $_POST['product_cat_1_els'];
			}

			update_term_meta( $term_id, 'product_cat_1_els', $els_1 );
		}
	}

}

function teckzone_product_cat_init() {
	new Teckzone_Product_Cat;
}

add_action( 'init', 'teckzone_product_cat_init' );