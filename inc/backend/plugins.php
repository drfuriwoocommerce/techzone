<?php
/**
 * Register required, recommended plugins for theme
 *
 * @package Teckzone
 */

/**
 * Register required plugins
 *
 * @since  1.0
 */
function teckzone_register_required_plugins() {
	$plugins = array(
		array(
			'name'               => esc_html__( 'Meta Box', 'teckzone' ),
			'slug'               => 'meta-box',
			'required'           => true,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'Kirki', 'teckzone' ),
			'slug'               => 'kirki',
			'required'           => true,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'WooCommerce', 'teckzone' ),
			'slug'               => 'woocommerce',
			'required'           => true,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'Elementor Page Builder', 'teckzone' ),
			'slug'               => 'elementor',
			'required'           => true,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'Teckzone Addons', 'teckzone' ),
			'slug'               => 'teckzone-addons',
			'source'             => esc_url( 'http://demo2.drfuri.com/plugins/teckzone-addons.zip' ),
			'required'           => true,
			'force_activation'   => false,
			'force_deactivation' => false,
			'version'            => '1.0.0',
		),
		array(
			'name'               => esc_html__( 'Contact Form 7', 'teckzone' ),
			'slug'               => 'contact-form-7',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'MailChimp for WordPress', 'teckzone' ),
			'slug'               => 'mailchimp-for-wp',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'YITH WooCommerce Wishlist', 'teckzone' ),
			'slug'               => 'yith-woocommerce-wishlist',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'YITH WooCommerce Compare', 'teckzone' ),
			'slug'               => 'yith-woocommerce-compare',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => esc_html__( 'Variation Swatches for WooCommerce Pro', 'teckzone' ),
			'slug'               => 'variation-swatches-for-woocommerce-pro',
			'source'             => esc_url( 'http://demo2.drfuri.com/plugins/variation-swatches-for-woocommerce-pro.zip' ),
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
			'version'            => '1.0.5',
		),
		array(
			'name'               => esc_html__( 'Woocommerce Deals', 'teckzone' ),
			'slug'               => 'woocommerce-deals',
			'source'             => esc_url( 'http://demo2.drfuri.com/plugins/woocommerce-deals.zip' ),
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
			'version'            => '1.0.9',
		),
	);
	$config  = array(
		'domain'       => 'teckzone',
		'default_path' => '',
		'menu'         => 'install-required-plugins',
		'has_notices'  => true,
		'is_automatic' => false,
		'message'      => '',
		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'teckzone' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'teckzone' ),
			'installing'                      => esc_html__( 'Installing Plugin: %s', 'teckzone' ),
			'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'teckzone' ),
			'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'teckzone' ),
			'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'teckzone' ),
			'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'teckzone' ),
			'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'teckzone' ),
			'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'teckzone' ),
			'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'teckzone' ),
			'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'teckzone' ),
			'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'teckzone' ),
			'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'teckzone' ),
			'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'teckzone' ),
			'return'                          => esc_html__( 'Return to Required Plugins Installer', 'teckzone' ),
			'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'teckzone' ),
			'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'teckzone' ),
			'nag_type'                        => 'updated',
		),
	);

	tgmpa( $plugins, $config );
}

add_action( 'tgmpa_register', 'teckzone_register_required_plugins' );
