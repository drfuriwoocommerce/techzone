<?php

/**
 * Class menu walker
 *
 * @package Teckzone
 */
class Teckzone_Mobile_Menu_Walker extends Walker_Nav_Menu {
	/**
	 * Background Item
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $style = '';

	/**
	 * Start the element output.
	 * Display item description text and classes
	 *
	 * @see   Walker::start_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$item_icon         = get_post_meta( $item->ID, 'tamm_menu_item_icon', true );
		$item_icon_size    = get_post_meta( $item->ID, 'tamm_menu_item_icon_size', true );
		$item_icon_spacing = get_post_meta( $item->ID, 'tamm_menu_item_icon_spacing', true );

		$item_badge                = get_post_meta( $item->ID, 'tamm_menu_item_badge', true );
		$item_badge_text           = get_post_meta( $item->ID, 'tamm_menu_item_badge_text', true );
		$item_badge_bg             = get_post_meta( $item->ID, 'tamm_menu_item_badge_bg', true );
		$item_badge_color          = get_post_meta( $item->ID, 'tamm_menu_item_badge_color', true );
		$item_badge_padding_top    = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_top', true );
		$item_badge_padding_bottom = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_bottom', true );
		$item_badge_padding_left   = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_left', true );
		$item_badge_padding_right  = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_right', true );

		$item_color      = get_post_meta( $item->ID, 'tamm_menu_item_color', true );
		$item_color_menu = get_post_meta( $item->ID, 'tamm_menu_item_color_menu', true );
		$item_font_weight = get_post_meta( $item->ID, 'tamm_menu_item_font_weight', true );

		$classes   = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$this->style = '';
		$inline      = '';

		if ( $inline ) {
			$this->style = 'style="' . $inline . '"';;
		}

		/**
		 * Filter the arguments for a single nav menu item.
		 *
		 * @since 4.4.0
		 *
		 * @param array  $args  An array of arguments.
		 * @param object $item  Menu item data object.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

		/**
		 * Add active class for current menu item
		 */
		$active_classes = array(
			'current-menu-item',
			'current-menu-parent',
			'current-menu-ancestor',
		);
		$is_active      = array_intersect( $classes, $active_classes );
		if ( ! empty( $is_active ) ) {
			$classes[] = 'active';
		}

		/**
		 * Filter the CSS class(es) applied to a menu item's list item element.
		 *
		 * @since 3.0.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		/**
		 * Filter the ID applied to a menu item's list item element.
		 *
		 * @since 3.0.1
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */

		$output .= $indent . '<li' . $class_names . '>';

		$atts           = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target ) ? $item->target : '';
		$atts['rel']    = ! empty( $item->xfn ) ? $item->xfn : '';
		$atts['href']   = ! empty( $item->url ) ? $item->url : '';

		$atts['class'] = '';
		/**
		 * Add attributes for menu item link when this is not mega menu item
		 */
		if ( in_array( 'menu-item-has-children', $classes ) ) {
			$atts['class']         = 'dropdown-toggle';
			$atts['role']          = 'button';
			$atts['data-toggle']   = 'dropdown';
			$atts['aria-haspopup'] = 'true';
			$atts['aria-expanded'] = 'false';
		}

		if ( $item_icon ) {
			$atts['class'] .= ' has-icon';
		}


		/**
		 * Filter the HTML attributes applied to a menu item's anchor element.
		 *
		 * @since 3.6.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array  $atts   {
		 *                       The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
		 *
		 * @type string  $title  Title attribute.
		 * @type string  $target Target attribute.
		 * @type string  $rel    The rel attribute.
		 * @type string  $href   The href attribute.
		 * }
		 *
		 * @param object $item   The current menu item.
		 * @param array  $args   An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth  Depth of menu item. Used for padding.
		 */
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		/** This filter is documented in wp-includes/post-template.php */
		$title = apply_filters( 'the_title', $item->title, $item->ID );

		/**
		 * Filter a menu item's title.
		 *
		 * @since 4.4.0
		 *
		 * @param string $title The menu item's title.
		 * @param object $item  The current menu item.
		 * @param array  $args  An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

		$badge = array();
		if ( $item_badge ) {
			$badge_style = '';
			if ( $item_badge_bg ) {
				$badge_style .= 'background-color:' . $item_badge_bg;
			}
			if ( $item_badge_color ) {
				$badge_style .= ';color:' . $item_badge_color;
			}

			if ( $item_badge_padding_top ) {
				$badge_style .= ';padding-top:' . $item_badge_padding_top;
			}

			if ( $item_badge_padding_bottom ) {
				$badge_style .= ';padding-bottom:' . $item_badge_padding_bottom;
			}

			if ( $item_badge_padding_left ) {
				$badge_style .= ';padding-left:' . $item_badge_padding_left;
			}

			if ( $item_badge_padding_right ) {
				$badge_style .= ';padding-right:' . $item_badge_padding_right;
			}

			$badge_style = $badge_style ? 'style="' . $badge_style . '"' : '';
			$badge[]     = '<span class="item-badge" ' . $badge_style . ' >';
			$badge[]     = empty( $item_badge_text ) ? esc_html__( 'New', 'teckzone' ) : $item_badge_text;
			$badge[]     = '</span>';
		}

		// Item
		$style_item = $style_item_inline = '';
		if ( $item_color ):
			$style_item = $item_color_menu ? 'color:' . $item_color_menu . ';' : '';
		endif;

		if ( $item_font_weight ) {
			$style_item .= 'font-weight:' . $item_font_weight . ';';
		}

		if ( $style_item ) {
			$style_item_inline = ' style="' . $style_item . '"';
		}

		// Icon
		$icon_style = $icon_inline = '';

		if ( $item_icon_size ) {
			$icon_inline .= 'font-size:' . $item_icon_size . ';';
		}

		if ( $item_icon_spacing ) {
			$icon_inline .= 'margin-right:' . $item_icon_spacing . ';';
		}

		if ( $icon_inline ) {
			$icon_style = 'style="' . $icon_inline . '"';
		}

		$item_output = ! empty( $args->before ) ? $args->before : '';
		$item_output .= '<a' . $attributes . $style_item_inline . '>';
		$item_output .= $item_icon ? '<i class="' . esc_attr( $item_icon ) . '" ' . $icon_style . '></i> ' : '';
		$item_output .= ( ! empty( $args->link_before ) ? $args->link_before : '' ) . $title . implode( $badge ) . ( ! empty( $args->link_after ) ? $args->link_after : '' );
		$item_output .= '<span class="menu-separator"></span>';
		$item_output .= '</a>';
		$item_output .= ! empty( $args->after ) ? $args->after : '';

		/**
		 * Filter a menu item's starting output.
		 *
		 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
		 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
		 * no filter for modifying the opening and closing `<li>` for a menu item.
		 *
		 * @since 3.0.0
		 *
		 * @param string $item_output The menu item's starting HTML output.
		 * @param object $item        Menu item data object.
		 * @param int    $depth       Depth of menu item. Used for padding.
		 * @param array  $args        An array of {@see wp_nav_menu()} arguments.
		 */
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @see   Walker::end_el()
	 *
	 * @since 1.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Page data object. Not used.
	 * @param int    $depth  Depth of page. Not Used.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}
}