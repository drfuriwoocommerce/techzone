<div id="tamm-panel-mega" class="tamm-panel-mega tamm-panel">
	<p class="mr-tamm-panel-box">
		<label>
			<input type="checkbox" name="{{ taMegaMenu.getFieldName( 'mega', data.data['menu-item-db-id'] ) }}"
			       value="1" {{ data.megaData.mega ? 'checked="checked"' : '' }} >
			<?php esc_html_e( 'Mega Menu', 'teckzone' ) ?>
        </label>
    </p>

    <p class="mr-tamm-panel-box-large">
        <label>
			<?php esc_html_e( 'Mega Width', 'teckzone' ) ?><br>
            <input type="text" name="{{ taMegaMenu.getFieldName( 'mega_width', data.data['menu-item-db-id'] ) }}" placeholder="100%" value="{{ data.megaData.mega_width }}">
        </label>
    </p>

    <p class="mr-tam-panel-align">
        <label>    <?php esc_html_e( 'Mega Menu Align', 'teckzone' ) ?></label>
        <select name="{{ taMegaMenu.getFieldName( 'align_mega', data.data['menu-item-db-id'] ) }}">
            <option value="" {{
            '' == data.megaData.align_mega ? 'selected="selected"' : '' }}><?php esc_html_e( 'Select default', 'teckzone' ); ?></option>
	        <option value="left" {{
            'left' == data.megaData.align_mega ? 'selected="selected"' : '' }}><?php esc_html_e( 'Left edge of Parent', 'teckzone' ); ?></option>
            <option value="center" {{
            'center' == data.megaData.align_mega ? 'selected="selected"' : '' }}><?php esc_html_e( 'Center edge of Parent', 'teckzone' ); ?></option>
            <option value="right" {{
            'right' == data.megaData.align_mega ? 'selected="selected"' : '' }}><?php esc_html_e( 'Right edge of Parent', 'teckzone' ); ?></option>
        </select>
    </p>
    <hr>
    <p class="mr-tamm-panel-box-large">
        <button type="button" class="tz-edit-mega-menu button-primary button-large"
                data-menu-id="{{data.data['menu-item-db-id']}}">    <?php esc_html_e( 'Edit Mega Menu Content', 'teckzone' ) ?></button>
    </p>
</div>
