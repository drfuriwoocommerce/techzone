<div id="tamm-panel-badge" class="tamm-panel-badge tamm-panel">
    <div class="tamm-col-6 tamm-col-left">
        <p class="mr-tamm-panel-box">
            <label>
                <input type="checkbox" name="{{ taMegaMenu.getFieldName( 'badge', data.data['menu-item-db-id'] ) }}"
                       value="1" {{ data.megaData.badge ? 'checked="checked"' : '' }} >
                <?php esc_html_e( 'Show Badge', 'teckzone' ) ?>
            </label>
        </p>
        <p class="mr-tamm-panel-box">
            <label><?php esc_html_e( 'Badge Text', 'teckzone' ) ?></label><br>
            <input type="text"
                   name="{{ taMegaMenu.getFieldName( 'badge_text', data.data['menu-item-db-id'] ) }}"
                   value="{{ data.megaData.badge_text }}">
        </p>
        <p class="background-color">
            <label><?php esc_html_e( 'Badge Background Color', 'teckzone' ) ?></label><br>
            <input type="text" class="background-color-picker"
                   name="{{ taMegaMenu.getFieldName( 'badge_bg', data.data['menu-item-db-id'] ) }}"
                   value="{{ data.megaData.badge_bg }}">
        </p>
        <p class="background-color">
            <label><?php esc_html_e( 'Badge Color', 'teckzone' ) ?></label><br>
            <input type="text" class="background-color-picker"
                   name="{{ taMegaMenu.getFieldName( 'badge_color', data.data['menu-item-db-id'] ) }}"
                   value="{{ data.megaData.badge_color }}">
        </p>
    </div>

    <div class="tamm-col-6 tamm-col-right">
        <p class="badge-padding-field">
            <label><?php esc_html_e( 'Padding', 'teckzone' ) ?></label><br>
            <label>
                <input type="text" value="{{ data.megaData.badge_padding.top }}" name="{{ taMegaMenu.getFieldName( 'badge_padding_top', data.data['menu-item-db-id'] ) }}"
                       size="4" placeholder="3px"><br>
                <span class="description"><?php esc_html_e( 'Top', 'teckzone' ) ?></span>
            </label>
            &nbsp;
            <label>
                <input type="text" value="{{ data.megaData.badge_padding.bottom }}" name="{{ taMegaMenu.getFieldName( 'badge_padding_bottom', data.data['menu-item-db-id'] ) }}"
                       size="4" placeholder="3px"><br>
                <span class="description"><?php esc_html_e( 'Bottom', 'teckzone' ) ?></span>
            </label>
            &nbsp;
            <label>
                <input type="text" value="{{ data.megaData.badge_padding.left }}" name="{{ taMegaMenu.getFieldName( 'badge_padding_left', data.data['menu-item-db-id'] ) }}"
                       size="4" placeholder="5px"><br>
                <span class="description"><?php esc_html_e( 'Left', 'teckzone' ) ?></span>
            </label>
            &nbsp;
            <label>
                <input type="text" value="{{ data.megaData.badge_padding.right }}" name="{{ taMegaMenu.getFieldName( 'badge_padding_right', data.data['menu-item-db-id'] ) }}"
                       size="4" placeholder="5px"><br>
                <span class="description"><?php esc_html_e( 'Right', 'teckzone' ) ?></span>
            </label>
        </p>
    </div>
    <div class="clear"></div>
</div>