<div id="tamm-panel-color" class="tamm-panel-color tamm-panel">
    <div class="tamm-col-6 tamm-col-left">
        <p class="mr-tamm-panel-box">
            <label>
                <input type="checkbox" name="{{ taMegaMenu.getFieldName( 'color', data.data['menu-item-db-id'] ) }}"
                       value="1" {{ data.megaData.color ? 'checked="checked"' : '' }} >
                <?php esc_html_e( 'Show Custom color', 'teckzone' ) ?>
            </label>
        </p>
        <p class="background-color">
            <label><?php esc_html_e( 'Item Color', 'teckzone' ) ?></label><br>
            <input type="text" class="background-color-picker"
                   name="{{ taMegaMenu.getFieldName( 'color_menu', data.data['menu-item-db-id'] ) }}"
                   value="{{ data.megaData.color_menu }}">
        </p>
    </div>
    <div class="tamm-col-6 tamm-col-right">
        <label>    <?php esc_html_e( 'Font Weight', 'teckzone' ) ?></label>
        <select name="{{ taMegaMenu.getFieldName( 'font_weight', data.data['menu-item-db-id'] ) }}">
            <option value="" {{ '' == data.megaData.font_weight ? 'selected="selected"' : '' }}><?php esc_html_e( 'Select default', 'teckzone' ); ?></option>
            <option value="300" {{ '300' == data.megaData.font_weight ? 'selected="selected"' : '' }}><?php esc_html_e( '300', 'teckzone' ); ?></option>
            <option value="400" {{ '400' == data.megaData.font_weight ? 'selected="selected"' : '' }}><?php esc_html_e( '400', 'teckzone' ); ?></option>
            <option value="600" {{ '600' == data.megaData.font_weight ? 'selected="selected"' : '' }}><?php esc_html_e( '600', 'teckzone' ); ?></option>
            <option value="700" {{ '700' == data.megaData.font_weight ? 'selected="selected"' : '' }}><?php esc_html_e( '700', 'teckzone' ); ?></option>
            <option value="800" {{ '800' == data.megaData.font_weight ? 'selected="selected"' : '' }}><?php esc_html_e( '800', 'teckzone' ); ?></option>
        </select>
    </div>
    <div></div>
</div>