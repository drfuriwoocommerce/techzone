<# if ( data.depth == 0 ) { #>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Mega Menu Content', 'teckzone' ) ?>"
   data-panel="mega"><?php esc_html_e( 'Mega Menu', 'teckzone' ) ?></a>
<div class="separator"></div>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Icon', 'teckzone' ) ?>"
   data-panel="icon"><?php esc_html_e( 'Icon', 'teckzone' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Badge', 'teckzone' ) ?>"
   data-panel="badge"><?php esc_html_e( 'Badge', 'teckzone' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Color Item', 'teckzone' ) ?>"
   data-panel="color"><?php esc_html_e( 'Color', 'teckzone' ) ?></a>
<# } else if ( data.depth == 1 ) { #>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Menu Icon', 'teckzone' ) ?>"
   data-panel="icon"><?php esc_html_e( 'Icon', 'teckzone' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Badge', 'teckzone' ) ?>"
   data-panel="badge"><?php esc_html_e( 'Badge', 'teckzone' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Color Item', 'teckzone' ) ?>"
   data-panel="color"><?php esc_html_e( 'Color', 'teckzone' ) ?></a>
<# } else { #>
<a href="#" class="media-menu-item " data-title="<?php esc_attr_e( 'Menu Icon', 'teckzone' ) ?>"
   data-panel="icon"><?php esc_html_e( 'Icon', 'teckzone' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Badge', 'teckzone' ) ?>"
   data-panel="badge"><?php esc_html_e( 'Badge', 'teckzone' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Color Item', 'teckzone' ) ?>"
   data-panel="color"><?php esc_html_e( 'Color', 'teckzone' ) ?></a>
<# } #>
