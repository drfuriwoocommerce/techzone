var taMegaMenu;

(function ( $, _ ) {
	'use strict';

	var api,
		wp = window.wp;

	api = taMegaMenu = {
		init: function () {
			api.$body = $( 'body' );
			api.$modal = $( '#tamm-settings' );
			api.$elementorModal = $( '#tamm-settings-elementor' );
			api.itemData = {};
			api.templates = {
				menus: wp.template( 'tamm-menus' ),
				title: wp.template( 'tamm-title' ),
				mega : wp.template( 'tamm-mega' ),
				icon : wp.template( 'tamm-icon' ),
				badge: wp.template( 'tamm-badge' ),
				color: wp.template( 'tamm-color' )
			};

			api.frame = wp.media( {
				library: {
					type: 'image'
				}
			} );

			this.initActions();
		},

		initActions: function () {
			api.$body
				.on( 'click', '.opensettings', this.openModal )
				.on( 'click', '.tamm-modal-backdrop, .tamm-modal-close, .tamm-button-cancel', this.closeModal )
				.on( 'click', '.tz-tamm-modal-elementor-backdrop, .tz-tamm-modal-elementor-close', this.closeMenuContent );

			api.$modal
				.on( 'click', '.tamm-menu a', this.switchPanel )
				.on( 'click', '.tamm-button-save', this.saveChanges )
				.on( 'click', '.tz-edit-mega-menu', this.openMenuContent );
		},

		openModal: function () {
			api.getItemData( this );

			api.$modal.show();
			api.$body.addClass( 'modal-open' );
			api.render();

			return false;
		},

		openMenuContent: function () {
			api.getItemData( this );
			api.$elementorModal.show();
			api.$body.addClass( 'modal-open' );

			var menuId = $( this ).data( 'menu-id' ),
				$iframeId = $( '#tz-mega-menu-content-frame-' + menuId ),
				iframeSrc = '?tz_tamm_menu_id=' + menuId + '&tz_tamm_mega_elementor=true',
				iframe = '<iframe id="tz-mega-menu-content-frame-' + menuId + '" src="' + iframeSrc + '" class="tz-mega-menu-content-frame active"></iframe>';

			$( '.tz-mega-menu-content-frame' ).removeClass( 'active' );
			if ( $iframeId.length < 1 ) {
				api.$elementorModal.find( '.media-modal-content' ).append( iframe );
			}
			$iframeId.addClass( 'active' );

			api.render();

			return false;
		},

		closeModal      : function () {
			api.$modal.hide().find( '.tamm-content' ).html( '' );
			api.$body.removeClass( 'modal-open' );
			return false;
		},
		closeMenuContent: function () {
			api.$elementorModal.hide();
			api.$body.removeClass( 'modal-open' );
			return false;
		},


		switchPanel: function ( e ) {
			e.preventDefault();

			var $el = $( this ),
				panel = $el.data( 'panel' );

			$el.addClass( 'active' ).siblings( '.active' ).removeClass( 'active' );
			api.openSettings( panel );
		},

		render: function () {
			// Render menu
			api.$modal.find( '.tamm-frame-menu .tamm-menu' ).html( api.templates.menus( api.itemData ) );

			var $activeMenu = api.$modal.find( '.tamm-menu a.active' );

			// Render content
			this.openSettings( $activeMenu.data( 'panel' ) );
		},

		openSettings: function ( panel ) {
			var $content = api.$modal.find( '.tamm-frame-content .tamm-content' ),
				$panel = $content.children( '#tamm-panel-' + panel );

			if ( $panel.length ) {
				$panel.addClass( 'active' ).siblings().removeClass( 'active' );
			} else {
				$content.append( api.templates[panel]( api.itemData ) );
				$content.children( '#tamm-panel-' + panel ).addClass( 'active' ).siblings().removeClass( 'active' );

				if ( 'icon' == panel ) {
					api.initIconFields();
				}

				if ( 'badge' == panel ) {
					api.$modal.find( '.background-color-picker' ).wpColorPicker();
				}

				if ( 'color' == panel ) {
					api.$modal.find( '.background-color-picker' ).wpColorPicker();
				}
			}

			// Render title
			var title = api.$modal.find( '.tamm-frame-menu .tamm-menu a[data-panel=' + panel + ']' ).data( 'title' );
			api.$modal.find( '.tamm-frame-title' ).html( api.templates.title( { title: title } ) );
		},


		initIconFields: function () {
			var $input = api.$modal.find( '#tamm-icon-input' ),
				$preview = api.$modal.find( '#tamm-selected-icon' ),
				$icons = api.$modal.find( '.tamm-icon-selector .icons i' );

			api.$modal.on( 'click', '.tamm-icon-selector .icons i', function () {
				var $el = $( this ),
					icon = $el.data( 'icon' );

				$el.addClass( 'active' ).siblings( '.active' ).removeClass( 'active' );

				$input.val( icon );
				$preview.html( '<i class="' + icon + '"></i>' );
			} );

			$preview.on( 'click', 'i', function () {
				$( this ).remove();
				$input.val( '' );
			} );

			api.$modal.on( 'keyup', '.tamm-icon-search', function ( e ) {
				var term = $( this ).val().toUpperCase();

				if ( !term ) {
					$icons.show();
				} else {
					$icons.hide().filter( function () {
						return $( this ).data( 'icon' ).toUpperCase().indexOf( term ) > -1;
					} ).show();
				}
			} );
		},

		getItemData: function ( menuItem ) {
			var $menuItem = $( menuItem ).closest( 'li.menu-item' ),
				$menuData = $menuItem.find( '.tamm-data' ),
				children = $menuItem.childMenuItems();

			api.itemData = {
				depth          : $menuItem.menuItemDepth(),
				megaData       : {
					mega         : $menuData.data( 'mega' ),
					icon         : $menuData.data( 'icon' ),
					icon_size    : $menuData.data( 'icon_size' ),
					icon_spacing : $menuData.data( 'icon_spacing' ),
					align_mega   : $menuData.data( 'align_mega' ),
					badge        : $menuData.data( 'badge' ),
					badge_text   : $menuData.data( 'badge_text' ),
					badge_bg     : $menuData.data( 'badge_bg' ),
					badge_color  : $menuData.data( 'badge_color' ),
					badge_padding: {
						top   : $menuData.data( 'badge_padding_top' ),
						bottom: $menuData.data( 'badge_padding_bottom' ),
						left  : $menuData.data( 'badge_padding_left' ),
						right : $menuData.data( 'badge_padding_right' )
					},
					mega_width   : $menuData.data( 'mega_width' ),
					color        : $menuData.data( 'color' ),
					color_menu   : $menuData.data( 'color_menu' ),
					font_weight  : $menuData.data( 'font_weight' ),
				},
				data           : $menuItem.getItemData(),
				children       : [],
				originalElement: $menuItem.get( 0 )
			};

			if ( !_.isEmpty( children ) ) {
				_.each( children, function ( item ) {
					var $item = $( item ),
						$itemData = $item.find( '.tamm-data' ),
						depth = $item.menuItemDepth();

					api.itemData.children.push( {
						depth          : depth,
						subDepth       : depth - api.itemData.depth - 1,
						data           : $item.getItemData(),
						megaData       : {
							mega         : $itemData.data( 'mega' ),
							icon         : $itemData.data( 'icon' ),
							icon_size    : $itemData.data( 'icon_size' ),
							icon_spacing : $itemData.data( 'icon_spacing' ),
							badge        : $itemData.data( 'badge' ),
							badge_text   : $itemData.data( 'badge_text' ),
							badge_bg     : $itemData.data( 'badge_bg' ),
							badge_color  : $itemData.data( 'badge_color' ),
							badge_padding: {
								top   : $itemData.data( 'badge_padding_top' ),
								bottom: $itemData.data( 'badge_padding_bottom' ),
								left  : $itemData.data( 'badge_padding_left' ),
								right : $itemData.data( 'badge_padding_right' )
							},
							align_mega   : $itemData.data( 'align_mega' ),
							mega_width   : $itemData.data( 'mega_width' ),
							color        : $itemData.data( 'color' ),
							color_menu   : $itemData.data( 'color_menu' ),
							font_weight  : $itemData.data( 'font_weight' ),
						},
						originalElement: item
					} );
				} );
			}

		},

		setItemData: function ( item, data, depth ) {
			if ( !_.has( data, 'mega' ) ) {
				data.mega = false;
			}

			var $dataHolder = $( item ).find( '.tamm-data' );

			$dataHolder.data( data );

		},

		getFieldName: function ( name, id ) {
			name = name.split( '.' );
			name = '[' + name.join( '][' ) + ']';

			return 'menu-item[' + id + ']' + name;
		},

		saveChanges: function () {
			var data = api.$modal.find( '.tamm-content :input' ).serialize(),
				$spinner = api.$modal.find( '.tamm-toolbar .spinner' );

			$spinner.addClass( 'is-active' );

			$.post( ajaxurl, {
				action: 'tamm_save_menu_item_data',
				data  : data
			}, function ( res ) {
				if ( !res.success ) {
					return;
				}


				var data = res.data['menu-item'];

				// Update parent menu item
				if ( _.has( data, api.itemData.data['menu-item-db-id'] ) ) {
					api.setItemData( api.itemData.originalElement, data[api.itemData.data['menu-item-db-id']], 0 );
				}

				_.each( api.itemData.children, function ( menuItem ) {
					if ( !_.has( data, menuItem.data['menu-item-db-id'] ) ) {
						return;
					}

					api.setItemData( menuItem.originalElement, data[menuItem.data['menu-item-db-id']], 1 );
				} );

				$spinner.removeClass( 'is-active' );
				api.closeModal();
			} );
		}
	};

	$( function () {
		taMegaMenu.init();
	} );
})( jQuery, _ );