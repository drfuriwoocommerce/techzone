<?php
/**
 * Customize and add more fields for mega menu
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Walker_Nav_Menu_Edit' ) ) {
	require_once ABSPATH . 'wp-admin/includes/nav-menu.php';
}

class Teckzone_Mega_Menu_Walker_Edit extends Walker_Nav_Menu_Edit {
	/**
	 * Start the element output.
	 *
	 * @see   Walker_Nav_Menu::start_el()
	 * @since 3.0.0
	 *
	 * @global int   $_wp_nav_menu_max_depth
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   Not used.
	 * @param int    $id     Not used.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$item_icon         = get_post_meta( $item->ID, 'tamm_menu_item_icon', true );
		$item_icon_size    = get_post_meta( $item->ID, 'tamm_menu_item_icon_size', true );
		$item_icon_spacing = get_post_meta( $item->ID, 'tamm_menu_item_icon_spacing', true );

		$item_mega  = get_post_meta( $item->ID, 'tamm_menu_item_mega', true );
		$item_align = get_post_meta( $item->ID, 'tamm_menu_item_align_mega', true );

		$item_badge                = get_post_meta( $item->ID, 'tamm_menu_item_badge', true );
		$item_badge_text           = get_post_meta( $item->ID, 'tamm_menu_item_badge_text', true );
		$item_badge_bg             = get_post_meta( $item->ID, 'tamm_menu_item_badge_bg', true );
		$item_badge_color          = get_post_meta( $item->ID, 'tamm_menu_item_badge_color', true );
		$item_badge_padding_top    = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_top', true );
		$item_badge_padding_bottom = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_bottom', true );
		$item_badge_padding_left   = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_left', true );
		$item_badge_padding_right  = get_post_meta( $item->ID, 'tamm_menu_item_badge_padding_right', true );

		$item_mega_width = get_post_meta( $item->ID, 'tamm_menu_item_mega_width', true );

		$item_color      = get_post_meta( $item->ID, 'tamm_menu_item_color', true );
		$item_color_menu = get_post_meta( $item->ID, 'tamm_menu_item_color_menu', true );
		$item_font_weight = get_post_meta( $item->ID, 'tamm_menu_item_font_weight', true );


		$item_output = '';
		parent::start_el( $item_output, $item, $depth, $args );

		$dom                  = new DOMDocument();
		$dom->validateOnParse = true;
		$dom->loadHTML( mb_convert_encoding( $item_output, 'HTML-ENTITIES', 'UTF-8' ) );

		$xpath = new DOMXPath( $dom );

		// Remove spaces in href attribute
		$anchors = $xpath->query( "//a" );

		foreach ( array_reverse( iterator_to_array( $anchors ) ) as $anchor ) {
			$anchor->setAttribute( 'href', trim( $anchor->getAttribute( 'href' ) ) );
		}

		// Add more menu item data
		$settings = $xpath->query( "//*[@id='menu-item-settings-" . $item->ID . "']" )->item( 0 );

		if ( $settings ) {
			$data = $dom->createElement( 'span' );
			$data->setAttribute( 'class', 'hidden tamm-data' );
			$data->setAttribute( 'data-mega', intval( $item_mega ) );
			$data->setAttribute( 'data-icon', esc_attr( $item_icon ) );
			$data->setAttribute( 'data-icon_size', esc_attr( $item_icon_size ) );
			$data->setAttribute( 'data-icon_spacing', esc_attr( $item_icon_spacing ) );
			$data->setAttribute( 'data-badge', esc_attr( $item_badge ) );
			$data->setAttribute( 'data-badge_text', esc_attr( $item_badge_text ) );
			$data->setAttribute( 'data-badge_bg', esc_attr( $item_badge_bg ) );
			$data->setAttribute( 'data-badge_color', esc_attr( $item_badge_color ) );
			$data->setAttribute( 'data-badge_padding_top', esc_attr( $item_badge_padding_top ) );
			$data->setAttribute( 'data-badge_padding_bottom', esc_attr( $item_badge_padding_bottom ) );
			$data->setAttribute( 'data-badge_padding_left', esc_attr( $item_badge_padding_left ) );
			$data->setAttribute( 'data-badge_padding_right', esc_attr( $item_badge_padding_right ) );
			$data->setAttribute( 'data-align_mega', esc_attr( $item_align ) );
			$data->setAttribute( 'data-mega_width', esc_attr( $item_mega_width ) );

			$data->setAttribute( 'data-color', esc_attr( $item_color ) );
			$data->setAttribute( 'data-color_menu', esc_attr( $item_color_menu ) );
			$data->setAttribute( 'data-font_weight', esc_attr( $item_font_weight ) );

			$settings->appendChild( $data );
		}

		// Add settings link
		$cancel = $xpath->query( "//*[@id='cancel-" . $item->ID . "']" )->item( 0 );

		if ( $cancel ) {
			$link            = $dom->createElement( 'a' );
			$link->nodeValue = esc_html__( 'Settings', 'teckzone' );
			$link->setAttribute( 'class', 'item-config-mega opensettings submitcancel hide-if-no-js' );
			$link->setAttribute( 'href', '#' );
			$sep            = $dom->createElement( 'span' );
			$sep->nodeValue = ' | ';
			$sep->setAttribute( 'class', 'meta-sep hide-if-no-js' );

			$cancel->parentNode->insertBefore( $link, $cancel );
			$cancel->parentNode->insertBefore( $sep, $cancel );
		}


		$output .= $dom->saveHTML();
	}
}

class Teckzone_Mega_Menu_Edit {
	/**
	 * Teckzone_Mega_Menu_Edit constructor.
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );
		add_action( 'admin_footer-nav-menus.php', array( $this, 'modal' ) );
		add_action( 'admin_footer-nav-menus.php', array( $this, 'elementor_buider_modal' ) );
		add_action( 'admin_footer-nav-menus.php', array( $this, 'templates' ) );
		add_action( 'wp_ajax_tamm_save_menu_item_data', array( $this, 'save_menu_item_data' ) );
	}

	/**
	 * Load scripts on Menus page only
	 *
	 * @param string $hook
	 */
	public function scripts( $hook ) {
		if ( 'nav-menus.php' !== $hook ) {
			return;
		}

		wp_register_style( 'teckzone-mega-font-linearicons', get_template_directory_uri() . '/css/linearicons.min.css', array(), '1.0' );
		wp_register_style(
			'teckzone-mega-menu', get_template_directory_uri() . '/inc/mega-menu/css/mega-menu.css', array(
			'media-views',
			'wp-color-picker',
			'teckzone-mega-font-linearicons'
		), '20160530'
		);
		wp_enqueue_style( 'teckzone-mega-menu' );

		wp_register_script(
			'teckzone-mega-menu', get_template_directory_uri() . '/inc/mega-menu/js/mega-menu.js', array(
			'jquery',
			'jquery-ui-resizable',
			'wp-util',
			'backbone',
			'underscore',
			'wp-color-picker'
		), '20160530', true
		);
		wp_enqueue_media();
		wp_enqueue_script( 'teckzone-mega-menu' );
	}

	/**
	 * Prints HTML of modal on footer
	 */
	public function modal() {
		?>
		<div id="tamm-settings" tabindex="0" class="tamm-settings tz-tamm-settings">
			<div class="tamm-modal media-modal wp-core-ui">
				<button type="button" class="button-link media-modal-close tamm-modal-close">
                    <span class="media-modal-icon"><span
							class="screen-reader-text"><?php esc_html_e( 'Close', 'teckzone' ) ?></span></span>
				</button>
				<div class="media-modal-content">
					<div class="tamm-frame-menu media-frame-menu">
						<div class="tamm-menu media-menu"></div>
					</div>
					<div class="tamm-frame-title media-frame-title"></div>
					<div class="tamm-frame-content media-frame-content">
						<div class="tamm-content">
							<!--							<span class="spinner"></span>-->
						</div>
					</div>
					<div class="tamm-frame-toolbar media-frame-toolbar">
						<div class="tamm-toolbar media-toolbar">
							<div class="tamm-toolbar-primary media-toolbar-primary search-form">
								<button type="button"
										class="button tamm-button tamm-button-save media-button button-primary button-large"><?php esc_html_e( 'Save Changes', 'teckzone' ) ?></button>
								<button type="button"
										class="button tamm-button tamm-button-cancel media-button button-secondary button-large"><?php esc_html_e( 'Cancel', 'teckzone' ) ?></button>
								<span class="spinner"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="media-modal-backdrop tamm-modal-backdrop"></div>
		</div>
		<?php
	}

	public function elementor_buider_modal() {
		?>
		<div id="tamm-settings-elementor" tabindex="0" class="tamm-settings tz-tamm-settings-elementor">
			<div class="tamm-modal media-modal wp-core-ui">
				<button type="button" class="button-link media-modal-close tz-tamm-modal-elementor-close">
                    <span class="media-modal-icon"><span
							class="screen-reader-text"><?php esc_html_e( 'Close', 'teckzone' ) ?></span></span>
				</button>
				<div class="media-modal-content">
					<div class="teckzone-loading"></div>
				</div>
			</div>
			<div class="media-modal-backdrop tz-tamm-modal-elementor-backdrop"></div>
		</div>
		<?php
	}

	/**
	 * Prints underscore template on footer
	 */
	public function templates() {
		$templates = apply_filters(
			'tamm_js_templates', array(
				'menus',
				'title',
				'mega',
				'icon',
				'badge',
				'color'
			)
		);

		foreach ( $templates as $template ) {
			$file = apply_filters( 'tamm_js_template_path', plugin_dir_path( __FILE__ ) . 'tmpl/' . $template . '.php', $template );
			?>
			<script type="text/template" id="tmpl-tamm-<?php echo esc_attr( $template ) ?>">
				<?php
				if ( file_exists( $file ) ) {
					include $file;
				}
				?>
			</script>
			<?php
		}
	}

	/**
	 * Ajax function to save menu item data
	 */
	public function save_menu_item_data() {
		$_POST['data'] = stripslashes_deep( $_POST['data'] );
		parse_str( $_POST['data'], $data );


		$i = 0;
		// Save menu item data
		foreach ( $data['menu-item'] as $id => $meta ) {

			// Update meta value for checkboxes
			$keys = array_keys( $meta );

			if ( $i == 0 ) {
				if ( in_array( 'mega', $keys ) ) {
					update_post_meta( $id, 'tamm_menu_item_mega', true );
				} else {
					delete_post_meta( $id, 'tamm_menu_item_mega' );
				}
			}

			if ( in_array( 'badge', $keys ) ) {
				update_post_meta( $id, 'tamm_menu_item_badge', true );
			} else {
				delete_post_meta( $id, 'tamm_menu_item_badge' );
			}

			if ( in_array( 'color', $keys ) ) {
				update_post_meta( $id, 'tamm_menu_item_color', true );
			} else {
				delete_post_meta( $id, 'tamm_menu_item_color' );
			}

			foreach ( $meta as $key => $value ) {
				$key = str_replace( '-', '_', $key );
				update_post_meta( $id, 'tamm_menu_item_' . $key, $value );
			}

			$i ++;
		}


		do_action( 'teckzone_save_menu_item_data', $data );

		wp_send_json_success( $data );
	}
}
