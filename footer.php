<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Teckzone
 */

?>

<?php do_action( 'teckzone_before_site_content_close' ); ?>
</div><!-- #content -->
<?php do_action( 'teckzone_before_footer' ) ?>
<footer id="colophon" class="site-footer">
	<?php do_action( 'teckzone_footer' ); ?>
</footer><!-- #colophon -->
<?php do_action( 'teckzone_after_footer' ) ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
