(function ( $ ) {
	'use strict';

	var teckzone = teckzone || {};
	teckzone.init = function () {
		teckzone.$body = $( document.body ),
			teckzone.$window = $( window ),
			teckzone.$header = $( '#site-header' );

		this.preLoader();
		this.teckzoneCustom();

		// Header
		this.magicLine();
		this.megaMenu();
		this.menuMobile();
		this.searchPanel();
		this.toggleCartPanel();

		// Blog
		this.postEntryFormat();
		this.loadMorePosts();
		this.viewPort();

		// Styling
		this.Tabs();
		this.newLetterPopup();

		// Catalog
		this.productCategoriesWidget();
		this.productAttribute();
		this.variationImagesCarousel();
		this.shopView();
		this.searchLayeredNav();
		this.filterAjax();
		this.collapseTheWidget();
		this.productCatWidget();
		this.catalogBanners();
		this.productsTopCarousel();
		this.brandsCarousel();
		this.updateWishlistCounter();
		this.addWishlist();
		this.addCompare();
		this.catalogToolbarPagination();
		this.catalogOpenCartMini();
		this.showCatelogFilterPanel();
		this.catalogSorting();
		this.catalogStickySidebar();
		this.loadMoreProducts();
		this.productPopupATC();

		// Single Product
		this.productQuantity();
		this.productThumbnail();
		this.productVideo();
		this.productGallery();
		this.productDegree();
		this.productVariation();
		this.addToCartAjax();
		this.buyNow();
		this.hoverProductTabs();
		this.productsCarousel();
		this.fbtProduct();
		this.fbtAddToCartAjax();
		this.fbtAddToWishlistAjax();
		this.cartQuantityAjax();
		this.stickyProductInfo();
		this.productQuickView();
		this.wooTabToggle();
		
		// Lazy load
		this.lazyLoadImages();

		// Mobile
		this.mobileNavBottomSpacing();
		this.goBackToPreviouslyPage();

		// Vendor
        this.filterSidebar();
	};

	teckzone.goBackToPreviouslyPage = function() {
		var $seleclor = $( '.teckzone-go-back' );
	
		$seleclor.each( function(){
			var $this = $( this );
	
			$this.on( 'click', 'a', function(e) {
				e.preventDefault();
	
				window.history.go(-1);
				$(window).on('popstate', function(e){
					window.location.reload(true);
				});
			});
		} );
	};
	

	teckzone.preLoader = function () {
		if ( !teckzone.$body.hasClass( 'tz-preloader' ) ) {
			return;
		}

		var $preloader = $( '#teckzone-preloader' );

		if ( !$preloader.length ) {
			return;
		}

		if (teckzone.$body.hasClass('elementor-editor-active')) {
            $preloader.addClass('fade-in');
            return;
        }

        $( document ).ready(function() {
            $preloader.addClass('fade-in');
        });
	};

	teckzone.Tabs = function () {
		var $tabs = $( '.teckzone-tabs' );

		$tabs.each( function () {
			var $el = $( this ).find( '.tabs-nav a' ),
				$panels = $( this ).find( '.tabs-panel' );

			$el.filter( ':first' ).addClass( 'active' );
			$( this ).find( '.tabs-nav' ).find( 'li' ).filter( ':first' ).addClass( 'active' );
			$panels.filter( ':first' ).addClass( 'active' );

			$el.on( 'click', function ( e ) {
				e.preventDefault();

				var $tab = $( this ),
					index = $tab.parent().index();

				if ( $tab.hasClass( 'active' ) ) {
					return;
				}

				$tabs.find( '.tabs-nav a' ).removeClass( 'active' );
				$tab.addClass( 'active' );
				$panels.removeClass( 'active' );
				$panels.filter( ':eq(' + index + ')' ).addClass( 'active' );
			} );
		} );
	};

	teckzone.teckzoneCustom = function () {
		teckzone.$window.on( 'resize', function () {
			var $el = $( '#tz-product-fbt, #comments' ),
				width = $el.width(),
				wWidth = $( '#content' ).width(),
				space = 0;

			if ( wWidth > width ) {
				space = (wWidth - width) / 2;
			}

			$el.find( '.tz-block-left' ).css( {
				'left' : space * -1,
				'width': space
			} );

			$el.find( '.tz-block-right' ).css( {
				'right': space * -1,
				'width': space
			} );
		} ).trigger( 'resize' );
	};

	teckzone.magicLine = function () {
		var $el, leftPos, newWidth, $origWidth, childWidth,
			$mainNav = teckzone.$header.find( 'ul.menu, ul.tz-nav-menu' );

		if ( ! $mainNav.length ) {
			return;
		}

		if (teckzone.$window.width() < 767) {
			return;
		}

		$mainNav.find( 'li:last-child' ).addClass( 'last-child' );
		$mainNav.append( '<li id="tz-menu-item__magic-line" class="tz-menu-item__magic-line"></li>' );
		var $magicLine = $( '#tz-menu-item__magic-line' );

		$origWidth = 0;

		var left = 0;
		if ( typeof $mainNav.children( 'li.active' ).position() !== 'undefined' ) {
			left = $mainNav.children( 'li.active' ).position().left;
		}

        $( document ).ready(function() {
			if ( $mainNav.children( 'li.current-menu-item, li.current-menu-ancestor, li.current-menu-parent' ).length ) {
				childWidth = $mainNav.children( 'li.current-menu-item, li.current-menu-ancestor, li.current-menu-parent' ).outerWidth();
				$magicLine
					.width( childWidth )
					.css( 'left', left )
					.data( 'origLeft', left )
					.data( 'origWidth', $magicLine.width() );

				$origWidth = $magicLine.data( 'origWidth' );
			}

			$mainNav.children( 'li' ).on( 'mouseenter', function () {
				$el = $( this );
				newWidth = $el.outerWidth();
				leftPos = $el.position().left;
				$magicLine.stop().animate( {
					left : leftPos,
					width: newWidth
				} );
			} );

			$mainNav.children( 'li' ).on( 'mouseleave', function () {
				$magicLine.stop().animate( {
					left : $magicLine.data( 'origLeft' ),
					width: $origWidth
				} );

			} );
		} );
	};

	teckzone.megaMenu = function () {
		var $menu = teckzone.$header.find( 'ul.menu' ),
			$megaMenu = $menu.find( '.is-mega-menu' ),
			menuWidth = $menu.innerWidth();

		$megaMenu.each(function(){
			var $el = $( this ),
				$dropdownSubMenu = $el.find( 'ul.dropdown-submenu' ),
				megaMenuWidth = $el.outerWidth(),
				megaMenuPositionLeft = $el.position().left,
				megaMenuPositionRight = menuWidth - megaMenuPositionLeft - megaMenuWidth,
				dropdownSubMenuWidth = $dropdownSubMenu.outerWidth();

			if ( $el.hasClass( 'has-custom-align' ) ) {
				return;
			}

			var dropdownSubMenuWidthHalf = dropdownSubMenuWidth * 1/2;

			if ( megaMenuPositionLeft < dropdownSubMenuWidthHalf ) {
				$dropdownSubMenu.css({
					left: 0
				});
			} else {
				if ( megaMenuPositionRight < dropdownSubMenuWidthHalf ) {
					$dropdownSubMenu.css({
						left: 'auto',
						right: 0
					});
				} else {
					$el.addClass( 'p-relative custom-align-center' );
				}
			}
		});
	};

	teckzone.searchPanel = function () {
		var $headerSearch = teckzone.$header.find( '.header-element--search' );

		$headerSearch.on( 'click', 'a.open-header-search', function(e){
			e.preventDefault;

			var $this = $(this);

			$this.siblings( '.search-panel-content').addClass( 'open' );
			$headerSearch.addClass( 'display-search-panel' );
			$(document.body).toggleClass('open-search-mobile').removeClass('open-cart-mobile open-menu-mobile');
		} );

		$headerSearch.on( 'click', 'a.close-search-panel', function(e){
			e.preventDefault;
			var $this = $(this);

			$this.closest( '.search-panel-content').removeClass( 'open' );
			$(document.body).removeClass( 'display-search-panel' );
			$(document.body).removeClass('open-off-canvas');
		} );

		$headerSearch.find( '.teckzone-off-canvas-layer' ).on('click', function () {
			var $this = $(this);

			$headerSearch.removeClass('display-search-panel');
			$this.siblings('.search-panel-content').removeClass('open');
			$(document.body).removeClass('open-off-canvas');
		});
	};

	teckzone.toggleCartPanel = function () {
		if (teckzone.$window.width() >= 1200) {
			return;
		}

		var $header_cart = teckzone.$header.find( '.header-element--cart' );
		
		$header_cart.on( 'click', 'a.cart-contents', function(e){
			e.preventDefault();
			var $this = $(this);

			$this.siblings('.mini-cart-content').addClass('open');
			$header_cart.addClass('display-cart-panel');
			$(document.body).toggleClass('open-cart-mobile').removeClass('open-menu-mobile open-search-mobile');
		});

		$header_cart.find('.teckzone-off-canvas-layer').on('click', function (e) {
			$header_cart.find('.mini-cart-content').removeClass('open');
			$header_cart.removeClass('display-cart-panel');
			$(document.body).removeClass('open-off-canvas');
		});

		$header_cart.on('click', 'a.close-cart-panel', function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest('.mini-cart-content').removeClass('open');
			$header_cart.removeClass('display-cart-panel');
			$(document.body).removeClass('open-off-canvas');
		});
	};

	teckzone.menuMobile = function () {
		var $mobileMenu = $( '.header-main-wrapper .tz-menu-mobile' );

		$mobileMenu.on( 'click', '.menu-icon-js', function(event) {
			var $this = $( this );

			event.preventDefault();

			$this.siblings('.menu-mobile-wrapper').addClass( 'open' );
			$mobileMenu.addClass( 'display-mobile-menu' );
			$(document.body).removeClass('open-cart-mobile open-search-mobile').trigger('teckzone_toggle_menu_mobile');
		} );

		$mobileMenu.find( '.teckzone-off-canvas-layer' ).on('click', function (e) {
			var $this = $( this );

			$mobileMenu.removeClass( 'display-mobile-menu' );
			$mobileMenu.find('.menu-mobile-wrapper').removeClass( 'open' );
			$(document.body).removeClass('open-off-canvas');
		} );

		$mobileMenu.on('click', '.close-canvas-mobile-panel', function (e) {
			var $this = $( this );
			e.preventDefault();

			$mobileMenu.removeClass( 'display-mobile-menu' );
			$this.closest('.menu-mobile-wrapper').removeClass( 'open' );
			$(document.body).removeClass('open-off-canvas');
		});

		$mobileMenu.find('.menu .menu-item-has-children').prepend('<span class="toggle-menu-children"><i class="icon-chevron-down"></i></span>');

		$mobileMenu.on('click', '.toggle-menu-children', function (e) {
			e.preventDefault();
			openSubMenus($(this));
		});

		function openSubMenus($el) {
			$el.closest('li').siblings().find('ul').slideUp();
			$el.closest('li').siblings().removeClass('active');
			$el.closest('li').siblings().find('li').removeClass('active');

			$el.closest('li').children('ul').slideToggle();
			$el.closest('li').toggleClass('active');
		}
	};

	teckzone.postEntryFormat = function () {
		$( '.single-post-entry-format, .blog-wrapper' ).find( '.entry-format' ).fitVids( { customSelector: 'iframe, video' } );

		var $selector = $( '.format-gallery' ).find( 'ul.slides' );
		$selector.each( function () {
			var $el = $( this );
			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};
			$el.slick( options );
		} );

	};

	/**
	 * Blog Load Ajax
	 */
	teckzone.loadMorePosts = function () {
		$( document.body ).on( 'click', '.navigation.next-posts-navigation a', function ( event ) {
			event.preventDefault();

			if ( $( this ).data( 'requestRunning' ) ) {
				return;
			}

			$( this ).data( 'requestRunning', true );

			var $el = $( this ),
				$navigation = $el.closest( '.navigation' ),
				$row = $navigation.prev( '.row-flex' ),
				url = $el.attr( 'href' );

			if ( $el.hasClass( 'loading' ) ) {
				return;
			}

			$el.addClass( 'loading' );

			$.get( url, function ( response ) {
				var $content = $( '#main', response ),
					$posts = $( '.hentry', $content ),
					$nav = $( '.next-posts-navigation', $content );

				$posts.each( function ( index, post ) {
					$( post ).css( 'animation-delay', index * 100 + 'ms' );
				} );

				$posts.addClass( 'animated teckzoneFadeInUp' );
				$row.append( $posts );

				if ( $nav.length ) {
					$el.replaceWith( $( 'a', $nav ) );
				} else {
					$el.removeClass( 'loading' );
					$navigation.fadeOut();
				}

				$( document.body ).trigger( 'teckzone_posts_loaded', [$posts, true] );
			} );
		} );
	};

	// Viewport
	teckzone.viewPort = function () {
		if ( 'infinite' !== teckzoneData.blog.nav_type ) {
			return;
		}

		teckzone.$window.on( 'scroll', function () {
			if ( teckzone.$body.find( '.next-posts-navigation' ).is( ':in-viewport' ) ) {
				teckzone.$body.find( '.next-posts-navigation a' ).click();
			}
		} ).trigger( 'scroll' );
	};

	// Newsletter popup

	teckzone.newLetterPopup = function () {
		var $modal = $( '#tz-newsletter-popup' ),
			days = parseInt( teckzoneData.nl_days ),
			seconds = parseInt( teckzoneData.nl_seconds );

		if ( days > 0 && document.cookie.match( /^(.*;)?\s*tz_newletter\s*=\s*[^;]+(.*)?$/ ) ) {
			return;
		}

		if ( $modal.length < 1 ) {
			return;
		}

        $( document ).ready(function() {
			setTimeout( function () {
				$modal.addClass( 'open' );
			}, seconds * 1000 );
		} );

		$modal.on( 'click', '.close-modal', function ( e ) {
			e.preventDefault();
			closeNewsLetter( days );
			$modal.removeClass( 'open' );
			$modal.fadeOut();
		} );

		$modal.on( 'click', '.n-close', function ( e ) {
			e.preventDefault();
			closeNewsLetter( 30 );
			$modal.removeClass( 'open' );
			$modal.fadeOut();
		} );

		$modal.find( '.mc4wp-form' ).submit( function () {
			closeNewsLetter( days );
		} );

		$modal.find( '.formkit-form' ).submit( function () {
			closeNewsLetter( days );
		} );

		function closeNewsLetter( days ) {
			var date = new Date(),
				value = date.getTime();

			date.setTime( date.getTime() + (days * 24 * 60 * 60 * 1000) );

			document.cookie = 'tz_newletter=' + value + ';expires=' + date.toGMTString() + ';path=/';
		}
	};

	// Filter Ajax
	teckzone.filterAjax = function () {
		if ( !teckzone.$body.hasClass( 'catalog-ajax-filter' ) ) {
			return;
		}

		var $ajaxPreloader = $( '#tz-catalog-ajax-loader' );

		$ajaxPreloader.addClass('fade-in');

		$( document.body ).on( 'price_slider_change', function ( event, ui ) {
			var form = $( '.price_slider' ).closest( 'form' ).get( 0 ),
				$form = $( form ),
				url = $form.attr( 'action' ) + '?' + $form.serialize();

			$( document.body ).trigger( 'teckzone_catelog_filter_ajax', url, $( this ) );
		} );

		teckzone.$body.on( 'click', '.tz_widget_product_categories a, .tz-widget-layered-nav a, .widget_rating_filter a, .widget_layered_nav a, .widget_layered_nav_filters a, ul.woocommerce-ordering a:not(.tz-cancel-order), ul.per-page a', function ( e ) {
			e.preventDefault();
			var url = $( this ).attr( 'href' );
			$( document.body ).trigger( 'teckzone_catelog_filter_ajax', url, $( this ) );
		} );

		$( document.body ).on( 'teckzone_catelog_filter_ajax', function ( e, url, element ) {

			var $content = $( '#content' ),
				$headerMobile = $( '.header-mobile' ),
				$pageHeader = $( '.page-header' );
				

			$ajaxPreloader.removeClass('fade-in');

			if ( '?' == url.slice( -1 ) ) {
				url = url.slice( 0, -1 );
			}

			url = url.replace( /%2C/g, ',' );

			history.pushState( null, null, url );

			$( document.body ).trigger( 'teckzone_ajax_filter_before_send_request', [url, element] );

			if ( teckzone.ajaxXHR ) {
				teckzone.ajaxXHR.abort();
			}

			teckzone.ajaxXHR = $.get( url, function ( res ) {

				$content.replaceWith( $( res ).find( '#content' ) );
				$pageHeader.html( $( res ).find( '.page-header' ).html() );
				$headerMobile.html( $( res ).find( '.header-mobile' ).html() );

				$( document.body ).trigger( 'teckzone_ajax_filter_request_success', [res, url] );
			}, 'html' );

		} );

		$( document.body ).on( 'teckzone_ajax_filter_request_success', function () {
			teckzone.shopView();
			teckzone.searchLayeredNav();
			teckzone.productCategoriesWidget();
			teckzone.productAttribute();
			teckzone.variationImagesCarousel();
			teckzone.collapseTheWidget();
			teckzone.productCatWidget();
			teckzone.catalogBanners();
			teckzone.brandsCarousel();
			teckzone.productsTopCarousel();
			teckzone.priceSlider();
			teckzone.catalogSorting();
			teckzone.showCatelogFilterPanel();
			teckzone.goBackToPreviouslyPage();

			$ajaxPreloader.addClass('fade-in');
		} );
	};

	teckzone.searchLayeredNav = function () {

		var $widgets = $( '.tz-widget-layered-nav' );

		if ( $widgets.length < 1 ) {
			return;
		}

		$widgets.find( '.tz-widget-layered-nav-scroll' ).each( function () {
			var heightUL = $( this ).data( 'height' );
			if ( $( this ).height() > parseInt( heightUL ) ) {
				$( this ).slimScroll( {
					height       : heightUL,
					railVisible  : true,
					alwaysVisible: true,
					size         : '7px',
					color        : '#c1c1c1',
					railColor    : '#eeeeee',
					railOpacity  : 1
				} );
			}
		} );

		$widgets.on( 'keyup', '.tz-input-search-nav', function ( e ) {
			var valid = false;

			if ( typeof e.which == 'undefined' ) {
				valid = true;
			} else if ( typeof e.which == 'number' && e.which > 0 ) {
				valid = !e.ctrlKey && !e.metaKey && !e.altKey;
			}

			if ( !valid ) {
				return;
			}

			var val = $( this ).val();

			if ( typeof val === 'number' ) {
				val = '' + val;
			}

			var filter = val.toUpperCase(),
				widget = $( this ).closest( '.tz-widget-layered-nav' ),
				ul = widget.find( '.woocommerce-widget-layered-nav-list' ),
				items = ul.children( '.wc-layered-nav-term' );

			items.each( function () {
				var a = $( this ).find( 'a' ).data( 'title' );

				if ( typeof a === 'number' ) {
					a = '' + a;
				}

				a = a.toUpperCase();

				if ( a.indexOf( filter ) > -1 ) {
					$( this ).show();
				} else {
					$( this ).hide();
				}
			} );
			var heightUL = ul.data( 'height' );
			if ( ul.height() < parseInt( heightUL ) ) {
				widget.addClass( 'no-scroll' );
			} else {
				widget.removeClass( 'no-scroll' );
			}
		} );

	};

	// Get price js slider
	teckzone.priceSlider = function () {
		// woocommerce_price_slider_params is required to continue, ensure the object exists
		if ( typeof woocommerce_price_slider_params === 'undefined' ) {
			return false;
		}

		if ( $( '.catalog-sidebar' ).find( '.widget_price_filter' ).length <= 0 ) {
			return false;
		}

		// Get markup ready for slider
		$( 'input#min_price, input#max_price' ).hide();
		$( '.price_slider, .price_label' ).show();

		// Price slider uses jquery ui
		var min_price = $( '.price_slider_amount #min_price' ).data( 'min' ),
			max_price = $( '.price_slider_amount #max_price' ).data( 'max' ),
			current_min_price = parseInt( min_price, 10 ),
			current_max_price = parseInt( max_price, 10 );

		if ( $( '.price_slider_amount #min_price' ).val() != '' ) {
			current_min_price = parseInt( $( '.price_slider_amount #min_price' ).val(), 10 );
		}
		if ( $( '.price_slider_amount #max_price' ).val() != '' ) {
			current_max_price = parseInt( $( '.price_slider_amount #max_price' ).val(), 10 );
		}

		$( document.body ).on( 'price_slider_create price_slider_slide', function ( event, min, max ) {
			if ( woocommerce_price_slider_params.currency_pos === 'left' ) {

				$( '.price_slider_amount span.from' ).html( woocommerce_price_slider_params.currency_symbol + min );
				$( '.price_slider_amount span.to' ).html( woocommerce_price_slider_params.currency_symbol + max );

			} else if ( woocommerce_price_slider_params.currency_pos === 'left_space' ) {

				$( '.price_slider_amount span.from' ).html( woocommerce_price_slider_params.currency_symbol + ' ' + min );
				$( '.price_slider_amount span.to' ).html( woocommerce_price_slider_params.currency_symbol + ' ' + max );

			} else if ( woocommerce_price_slider_params.currency_pos === 'right' ) {

				$( '.price_slider_amount span.from' ).html( min + woocommerce_price_slider_params.currency_symbol );
				$( '.price_slider_amount span.to' ).html( max + woocommerce_price_slider_params.currency_symbol );

			} else if ( woocommerce_price_slider_params.currency_pos === 'right_space' ) {

				$( '.price_slider_amount span.from' ).html( min + ' ' + woocommerce_price_slider_params.currency_symbol );
				$( '.price_slider_amount span.to' ).html( max + ' ' + woocommerce_price_slider_params.currency_symbol );

			}

			$( document.body ).trigger( 'price_slider_updated', [min, max] );
		} );
		if ( typeof $.fn.slider !== 'undefined' ) {
			$( '.price_slider' ).slider( {
				range  : true,
				animate: true,
				min    : min_price,
				max    : max_price,
				values : [current_min_price, current_max_price],
				create : function () {

					$( '.price_slider_amount #min_price' ).val( current_min_price );
					$( '.price_slider_amount #max_price' ).val( current_max_price );

					$( document.body ).trigger( 'price_slider_create', [current_min_price, current_max_price] );
				},
				slide  : function ( event, ui ) {

					$( 'input#min_price' ).val( ui.values[0] );
					$( 'input#max_price' ).val( ui.values[1] );

					$( document.body ).trigger( 'price_slider_slide', [ui.values[0], ui.values[1]] );
				},
				change : function ( event, ui ) {

					$( document.body ).trigger( 'price_slider_change', [ui.values[0], ui.values[1]] );
				}
			} );
		}
	};

	// Product Attribute
	teckzone.productAttribute = function () {
		var oImgSrc = '',
			oImgSrcSet = '';
		teckzone.$body.on( 'mouseover', '.tz-swatch-item', function ( e ) {
			e.preventDefault();
			var $mainImages = $( this ).closest( 'li.product' ).find( '.teckzone-product-thumbnail' ),
				$oriImage = $mainImages.find( '.woocommerce-loop-product__link img' );

			oImgSrc = $oriImage.attr( 'src' );
			oImgSrcSet = $oriImage.attr( 'srcset' );

			var imgSrc = $( this ).attr( 'data-src' ),
				imgSrcSet = $( this ).attr( 'data-src-set' );

			$oriImage.attr( 'src', imgSrc );

			if ( imgSrcSet ) {
				$oriImage.attr( 'srcset', imgSrcSet );
			}


		} ).on( 'mouseout', '.tz-swatch-item', function ( e ) {
			e.preventDefault();
			var $mainImages = $( this ).closest( 'li.product' ).find( '.teckzone-product-thumbnail' ),
				$oriImage = $mainImages.find( '.woocommerce-loop-product__link img' );

			if ( oImgSrc ) {
				$oriImage.attr( 'src', oImgSrc );
			}

			if ( oImgSrcSet ) {
				$oriImage.attr( 'srcset', oImgSrcSet );
			}
		} );

		teckzone.$body.on( 'mouseover', '.tz-attr-swatches', function ( e ) {
			e.preventDefault();
			var $mainImages = $( this ).closest( 'li.product' ).find( '.teckzone-product-thumbnail' );
			$mainImages.addClass( 'hover-swatch' );
		} ).on( 'mouseout', '.tz-attr-swatches', function ( e ) {
			e.preventDefault();
			var $mainImages = $( this ).closest( 'li.product' ).find( '.teckzone-product-thumbnail' );
			$mainImages.removeClass( 'hover-swatch' );
		} );
	};

	// Product Categories
	teckzone.productCategoriesWidget = function () {
		var $categories = $( '.tz_widget_product_categories' );

		if ( $categories.length <= 0 ) {
			return;
		}

		$categories.find( 'ul.children' ).closest( 'li' ).prepend( '<span class="cat-menu-close"><i class="icon-chevron-down"></i></span>' );

		$categories.find( 'li.current-cat-parent, li.current-cat, li.current-cat-ancestor' ).addClass( 'opened' ).children( '.children' ).show();

		$categories.on( 'click', '.cat-menu-close', function ( e ) {
			e.preventDefault();
			$( this ).closest( 'li' ).children( '.children' ).slideToggle();
			$( this ).closest( 'li' ).toggleClass( 'opened' );
		} )
	};

	teckzone.variationImagesCarousel = function () {
		var $product = $( '.woocommerce ul.products li.product' );

		$product.each( function () {
			var $el = $( this ),
				$slider = $el.find( '.tz-attr-swatches--image .tz-attr-swatches__wrapper' );

			$slider.each( function () {
				var $this = $( this ),
					columns = $this.data( 'columns' );

				if ( $this.children().length < columns ) {
					return;
				}

				$this.slick( {
					rtl           : (teckzoneData.direction === 'true'),
					slidesToShow  : columns,
					slidesToScroll: columns,
					infinite      : false,
					prevArrow     : '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow     : '<span class="icon-chevron-right slick-next-arrow"></span>'
				} );
			} );
		} );
	};

	// Shop View
	teckzone.shopView = function () {
		$( '.tz-shop-view' ).on( 'click', '.shop-view__icon a', function ( e ) {
			e.preventDefault();
			var $el = $( this ),
				view = $el.data( 'view' );

			if ( $el.hasClass( 'current' ) ) {
				return;
			}

			$el.addClass( 'current' ).siblings().removeClass( 'current' );
			teckzone.$body.removeClass( 'catalog-view-grid catalog-view-list catalog-view-extended' ).addClass( 'catalog-view-' + view );

			document.cookie = 'catalog_view=' + view + ';domain=' + window.location.host + ';path=/';

			teckzone.$body.trigger( 'teckzone_shop_view_after_change' );
		} );
	};

	// Collapse The
	teckzone.collapseTheWidget = function () {
		if ( teckzoneData.collapse_the_widget.collapse != '1' ) {
			return;
		}

		var $widget = $( '.catalog-sidebar .widget' );

		$widget.each( function () {
			var $this = $( this );

			if ( $this.length <= 0 ) {
				return;
			}

			if ( $this.hasClass('.tz_widget_product_categories') || $this.hasClass('.widget_product_categories') ) {
				return;
			}

			$this.find( '.widget-title' ).addClass( 'show-icon' );

			if ( teckzoneData.collapse_the_widget.status == 'close' ) {
				$this.find( '.widget-title' ).siblings().addClass( 'closed' );
				$this.find( '.widget-title' ).addClass( 'non-active' );
			} else {
				$this.find( '.widget-title' ).addClass( 'active' );
			}

			$this.on( 'click', '.widget-title', function ( e ) {
				e.preventDefault();
				$( this ).siblings().slideToggle( 400 );
				$( this ).siblings().toggleClass( 'opened closed' );
				$( this ).toggleClass( 'active non-active' );
			} );
		} );
	};

	teckzone.productCatWidget = function () {
		var $widget = $( '.tz_widget_product_categories' );

		$widget.each( function () {
			var $el = $( this );

			var size_li = $el.find( 'ul.product-categories > li' ).size(),
				catsNumber = parseInt( $el.find( 'input.cats-number' ).val(), 10 ),
				perShow = parseInt( $el.find( 'input.per-show' ).val(), 10 ),
				x = catsNumber;

			$el.find( 'ul.product-categories > li:lt(' + x + ')' ).show();

			$el.on( 'click', '.show-more', function () {
				x = (x + perShow <= size_li) ? x + perShow : size_li;

				$el.find( 'ul.product-categories > li:lt(' + x + ')' ).show();

				if ( x == size_li ) {
					$el.find( '.show-more' ).hide();
					$el.find( '.show-less' ).show();
				}
			} );

			$el.on( 'click', '.show-less', function () {
				x = (x - perShow < 0) ? catsNumber : x - perShow;

				$el.find( 'ul.product-categories > li' ).not( ':lt(' + x + ')' ).hide();

				if ( x == catsNumber ) {
					$el.find( '.show-more' ).show();
					$el.find( '.show-less' ).hide();
				}
			} );
		} );
	};

	// Catalog Banners Carousel
	teckzone.catalogBanners = function () {
		var $banners = $( '#tz-catalog-banners' );

		if ( $banners.length <= 0 ) {
			return;
		}

		var autoplay = $banners.data( 'autoplay' ),
			infinite = false,
			speed = 1000;

		if ( autoplay > 0 ) {
			infinite = true;
			speed = autoplay;
			autoplay = true;
		} else {
			autoplay = false;
		}

		$banners.slick( {
			rtl           : (teckzoneData.direction === 'true'),
			slidesToShow  : 1,
			slidesToScroll: 1,
			autoplaySpeed : speed,
			autoplay      : autoplay,
			infinite      : infinite,
			arrows        : true,
			dots          : true,
			prevArrow     : '<span class="icon-chevron-left slick-prev-arrow"></span>',
			nextArrow     : '<span class="icon-chevron-right slick-next-arrow"></span>'
		} );
	};

	// Products Top Carousel
	teckzone.productsTopCarousel = function () {
		var $products = $( '.tz-products-top-carousel' );

		if ( !$products.length ) {
			return;
		}

		$products.each( function () {
			var $el = $( this ),
				$slider = $el.find( 'ul.products' ),
				dataSettings = $el.data( 'settings' );

			if ( $slider.children().length < dataSettings.slidesToShow ) {
				return;
			}

			var options = {
				arrows      : true,
				dots        : false,
				prevArrow   : '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow   : '<span class="icon-chevron-right slick-next-arrow"></span>',
				appendArrows: $el.find( '.slick-arrows' )
			};

			var data = JSON.stringify( dataSettings );

			$slider.attr( 'data-slick', data );
			$slider.slick( options );
		} );
	};

	// Products Top Carousel
	teckzone.brandsCarousel = function () {
		var $parent = $( '.tz-catalog-brands' ),
			$selector = $parent.find( '.brand-wrapper' );

		var options = {
			arrows      : true,
			dots        : false,
			prevArrow   : '<span class="icon-chevron-left slick-prev-arrow"></span>',
			nextArrow   : '<span class="icon-chevron-right slick-next-arrow"></span>',
			appendArrows: $parent.find( '.slick-arrows' )
		};

		$selector.slick( options );
	};

	teckzone.productQuantity = function () {
		teckzone.$body.on( 'click', '.quantity .increase, .quantity .decrease', function ( e ) {
			e.preventDefault();

			var $this = $( this ),
				$qty = $this.siblings( '.qty' ),
				step = parseInt( $qty.attr( 'step' ), 10 ),
				current = parseInt( $qty.val(), 10 ),
				min = parseInt( $qty.attr( 'min' ), 10 ),
				max = parseInt( $qty.attr( 'max' ), 10 );

			min = min ? min : 1;
			max = max ? max : current + 1;

			if ( $this.hasClass( 'decrease' ) && current > min ) {
				$qty.val( current - step );
				$qty.trigger( 'change' );
			}
			if ( $this.hasClass( 'increase' ) && current < max ) {
				$qty.val( current + step );
				$qty.trigger( 'change' );
			}
		} );
	};

	/**
	 * Change product quantity
	 */
	teckzone.productThumbnail = function () {
		var $gallery = $( '.woocommerce-product-gallery' ),
			$video = $gallery.find( '.woocommerce-product-gallery__image.tz-product-video' ),
			$thumbnail = $gallery.find( '.flex-control-thumbs' ),
			$divProduct = $gallery.closest( '.product' );

		if ( $(document.body).hasClass( 'mobile-version' ) ) {
			return;
		}

		$gallery.imagesLoaded( function () {
			setTimeout( function () {
				if ( ! $thumbnail.length ) {
					return;
				}
				var columns = $gallery.data( 'columns' );
				var count = $thumbnail.find( 'li' ).length;
				if ( count > columns ) {
					var options = {
						rtl           : teckzoneData.isRTL === '1',
						slidesToShow  : columns,
						slidesToScroll: 1,
						focusOnSelect : true,
						infinite      : false,
						prevArrow     : '<span class="icon-chevron-left slick-prev-arrow"></span>',
						nextArrow     : '<span class="icon-chevron-right slick-next-arrow"></span>',
						vertical      : false
					};

					if ( $divProduct.hasClass( 'product-thumbnail-vertical' ) ) {
						options.vertical = true;
						options.prevArrow = '<span class="icon-chevron-up slick-prev-arrow"></span>';
						options.nextArrow = '<span class="icon-chevron-down slick-next-arrow"></span>';
					}

					$thumbnail.slick( options );
				} else {
					$thumbnail.addClass( 'no-slick' );
				}

				if ( $video.length > 0 ) {
					$( '.woocommerce-product-gallery' ).addClass( 'has-video' );
					if ( $( '.woocommerce-product-gallery' ).hasClass( 'video-first' ) ) {
						$thumbnail.find( 'li' ).first().append( '<div class="i-video"><div class="i-inner"></div><div class="i-play"></div></div>' );
					} else {
						$thumbnail.find( 'li' ).last().append( '<div class="i-video"><div class="i-inner"></div><div class="i-play"></div></div>' );
					}
				}

			}, 100 );

		} );

	};

	teckzone.productVideo = function () {
		var $gallery = $( '.woocommerce-product-gallery' );
		var $video = $gallery.find( '.woocommerce-product-gallery__image.tz-product-video' );
		var $thumbnail = $gallery.find( '.flex-control-thumbs' );

		if ( $video.length < 1 ) {
			return;
		}

		var found = false,
			last = false;

		$thumbnail.on( 'click', 'li', function () {

			var $video = $gallery.find( '.tz-product-video' );

			var thumbsCount = $( this ).siblings().length;

			last = true;
			if ( $( this ).index() == thumbsCount ) {
				last = false;
				found = false;
			}

			if ( !found && last ) {
				var $iframe = $video.find( 'iframe' ),
					$wp_video = $video.find( 'video.wp-video-shortcode' );

				if ( $iframe.length > 0 ) {
					$iframe.attr( 'src', $iframe.attr( 'src' ) );
				}
				if ( $wp_video.length > 0 ) {
					$wp_video[0].pause();
				}
				found = true;
			}

			return false;

		} );

		$thumbnail.find( 'li' ).on( 'click', '.i-video', function ( e ) {
			e.preventDefault();
			$( this ).closest( 'li' ).find( 'img' ).trigger( 'click' );
		} );
	};

	/**
	 * Show photoSwipe lightbox
	 */
	teckzone.productGallery = function () {
		var $images = $( '.woocommerce-product-gallery' );

        if (typeof teckzoneData.product_gallery === 'undefined' || teckzoneData.product_gallery != '1') {
            $images.on( 'click', '.woocommerce-product-gallery__image', function ( e ) {
                return false;
            } );

            return
        }

		if ( !$images.length ) {
			return;
		}

		$images.find( '.woocommerce-product-gallery__image' ).on( 'mouseenter', function () {
			$( this ).closest( '.woocommerce-product-gallery' ).find( '.ms-image-view' ).removeClass( 'hide' );
			$( this ).closest( '.woocommerce-product-gallery' ).find( '.ms-image-zoom' ).addClass( 'hide' );
		} );

		$images.find( '.woocommerce-product-gallery__image' ).on( 'mouseleave', function () {
			$( this ).closest( '.woocommerce-product-gallery' ).find( '.ms-image-view' ).addClass( 'hide' );
			$( this ).closest( '.woocommerce-product-gallery' ).find( '.ms-image-zoom' ).removeClass( 'hide' );
		} );

		$images.on( 'click', '.woocommerce-product-gallery__image', function ( e ) {
			e.preventDefault();

			if ( $( this ).hasClass( 'tz-product-video' ) ) {
				return false;
			}

			var items = [];
			var $links = $( this ).closest( '.woocommerce-product-gallery' ).find( '.woocommerce-product-gallery__image' );
			$links.each( function () {
				var $el = $( this );

				if ( $el.hasClass( 'tz-product-video' ) ) {
					items.push( {
						html: $el.find( '.tz-video-content' ).html()
					} );

				} else {
					items.push( {
						src: $el.children( 'a' ).attr( 'href' ),
						w  : $el.find( 'a img' ).attr( 'data-large_image_width' ),
						h  : $el.find( 'a img' ).attr( 'data-large_image_height' )
					} );
				}
			} );

			var index = $links.index( $( this ) ),
				options = {
					index              : index,
					bgOpacity          : 0.85,
					showHideOpacity    : true,
					mainClass          : 'pswp--minimal-dark',
					barsSize           : { top: 0, bottom: 0 },
					captionEl          : false,
					fullscreenEl       : false,
					shareEl            : false,
					tapToClose         : true,
					tapToToggleControls: false
				};

			var lightBox = new PhotoSwipe( document.getElementById( 'pswp' ), window.PhotoSwipeUI_Default, items, options );
			lightBox.init();

			lightBox.listen( 'close', function () {
				$( '.tz-video-wrapper' ).find( 'iframe' ).each( function () {
					$( this ).attr( 'src', $( this ).attr( 'src' ) );
				} );

				$( '.tz-video-wrapper' ).find( 'video' ).each( function () {
					$( this )[0].pause();
				} );
			} );

            return false;
		} );
	};

	/**
	 * Show product 360 degree
	 */
	teckzone.productDegree = function () {
		var $product_degrees = $( '.woocommerce-product-gallery .product-degree-images' );

		if ( $product_degrees.length < 1 ) {
			return;
		}

		if ( teckzoneData.product_degree.length < 1 ) {
			return;
		}
		var degree = '',
			$pswp = $( '#product-degree-pswp' );

		$product_degrees.on( 'click', function ( e ) {
			e.preventDefault();
			teckzone.openModal( $pswp );

			if ( $pswp.hasClass( 'init' ) ) {
				return;
			}

			$pswp.addClass( 'init' );

			var imgArray = teckzoneData.product_degree.split( ',' ),
				images = [];

			for ( var i = 0; i < imgArray.length; i++ ) {
				images.push( imgArray[i] );
			}

			degree = $pswp.find( '.tz-product-gallery-degree' ).ThreeSixty( {
				totalFrames : images.length, // Total no. of image you have for 360 slider
				endFrame    : images.length, // end frame for the auto spin animation
				currentFrame: 1, // This the start frame for auto spin
				imgList     : $pswp.find( '.product-degree-images' ), // selector for image list
				progress    : '.tz-gallery-degree-spinner', // selector to show the loading progress
				imgArray    : images, // path of the image assets
				height      : 530,
				width       : 1070,
				navigation  : true,
				onReady		: function(){
					$pswp.find( '.degree-pswp-close' ).show();
				}
			} );

			$pswp.find( '.product-degree-images' ).imagesLoaded( function () {
				$pswp.find( '.nav_bar' ).removeClass( 'hide' );
			} );

			$pswp.on( 'click', '.degree-pswp-close, .degree-pswp-bg', function () {
				degree.stop();
				$( this ).removeClass( 'play' );
			} );
		} );

		$pswp.on( 'click', '.degree-pswp-close, .degree-pswp-bg', function () {
			teckzone.closeModal( $pswp );
		} );
	};

	teckzone.productVariation = function () {
		var $form = $( '.variations_form' );
		teckzone.$body.on( 'tawcvs_initialized', function () {
			$form.unbind( 'tawcvs_no_matching_variations' );
			$form.on( 'tawcvs_no_matching_variations', function ( event, $el ) {
				event.preventDefault();

				$form.find( '.woocommerce-variation.single_variation' ).show();
				if ( typeof wc_add_to_cart_variation_params !== 'undefined' ) {
					$form.find( '.single_variation' ).slideDown( 200 ).html( '<p>' + wc_add_to_cart_variation_params.i18n_no_matching_variations_text + '</p>' );
				}
			} );

		} );

		$form.find( 'td.value' ).on( 'change', 'select', function () {
			var value = $( this ).find( 'option:selected' ).text();
			$( this ).closest( 'tr' ).find( 'td.label .teckzone-attr-value' ).html( value );
		} );
	};

	// Add to cart ajax
	teckzone.addToCartAjax = function () {

		if ( teckzoneData.add_to_cart_ajax == '0' ) {
			return;
		}

		var found = false;
		teckzone.$body.find( 'form.cart' ).on( 'click', '.single_add_to_cart_button', function ( e ) {
			var $el = $( this ),
				$elID = $el.val(),
				$cartForm = $el.closest( 'form.cart' ),
                $productTitle = $el.closest('.entry-summary').find('.product_title');

			if ( $el.hasClass( 'has-buy-now' ) ) {
				return;
			}

			if ( $el.closest('.product').hasClass('product-type-external') ) {
				return;
			}

			if ( $cartForm.length > 0 ) {
				e.preventDefault();
			} else {
				return;
			}

			if ( $el.hasClass( 'disabled' ) ) {
				return;
			}

			$el.addClass( 'loading' );
			if ( found ) {
				return;
			}
			found = true;

			var formdata = $cartForm.serializeArray(),
				currentURL = window.location.href;

			if ( $el.val() != '' ) {
				formdata.push( { name: $el.attr( 'name' ), value: $el.val() } );
			}
			$.ajax( {
				url    : window.location.href,
				method : 'post',
				data   : formdata,
				error  : function () {
					window.location = currentURL;
				},
				success: function ( response ) {
					if ( !response ) {
						window.location = currentURL;
					}

					if ( typeof wc_add_to_cart_params !== 'undefined' ) {
						if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
							window.location = wc_add_to_cart_params.cart_url;
							return;
						}
					}

					$( document.body ).trigger( 'updated_wc_div' );
					$( document.body ).on( 'wc_fragments_refreshed', function () {
						$el.removeClass( 'loading' );
					} );

                    var $message = '',
                        className = 'success',
                        $content = false;
                    if ($(response).find('.woocommerce-message').length > 0) {
                        $message = $(response).find('.woocommerce-message').html();
                    }

                    if ($(response).find('.woocommerce-error').length > 0) {
                        $message = $(response).find('.woocommerce-error').html();
                        className = 'error';
                    }

                    if ($(response).find('.woocommerce-info').length > 0) {
                        $message = $(response).find('.woocommerce-info').html();
                    }

                    if (!$message) {
                        $content = $productTitle.find('a').html();
                    }

                	teckzone.addedToCartNotice($message, $content, true, className, false);
        			teckzone.singlePopupATC($elID);

                    found = false;
				}
			} );

		} );

	};

	teckzone.addedToCartNotice = function ($message, $content, single, className, multiple) {
		if ( teckzoneData.added_to_cart_notice === 'undefined'  || !$.fn.notify ) {
			return;
		}

		if (!$(document.body).hasClass('teckzone-atc-notice-default')) {
        	return;
        }

        if ($content === false) {
            $content = ' ';
        } else {
            if (multiple) {
                $content += ' ' + teckzoneData.added_to_cart_notice.added_to_cart_texts;
            } else {
                $content += ' ' + teckzoneData.added_to_cart_notice.added_to_cart_text;
            }
        }

		$message += '<a href="' + teckzoneData.added_to_cart_notice.cart_view_link + '" class="btn-button">' + teckzoneData.added_to_cart_notice.cart_view_text + '</a>';

		if ( single ) {
			$message = '<div class="message-box">' + $message + '</div>';
		}

		$.notify.addStyle( 'teckzone', {
			html: '<div><i class="icon-checkmark-circle message-icon"></i><span data-notify-text/>' + $message + '<span class="close icon-cross2"></span> </div>'
		} );
		$.notify( $content, {
			autoHideDelay: teckzoneData.added_to_cart_notice.cart_notice_auto_hide,
			className    : className,
			style        : 'teckzone',
			showAnimation: 'fadeIn',
			hideAnimation: 'fadeOut'
		} );
	};

	  /**
	 * Toggle product popup add to cart
	 */
	teckzone.productPopupATC = function () {

		var $modal = $( '#tz-popup-add-to-cart-modal' ),
			$product = $modal.find( '.product-modal-content' );

		if (!$(document.body).hasClass('teckzone-atc-notice-popup')) {
        	return;
        }        

		$(document.body)
	 		.on('added_to_cart', function (event, fragments, cart_hash, $thisbutton) {
				var $id = $thisbutton.data('product_id');

				teckzone.openModal( $modal );
	 			$modal.addClass( 'loading' ).removeClass( 'loaded' );
	 			teckzone.getDataAJAXPopupATC($modal, $product, $id);
        });
		
		$modal.on( 'click', '.close-modal, .tz-modal-overlay', function ( e ) {
			e.preventDefault();
			teckzone.closeModal( $modal );
		} )
	};

	teckzone.singlePopupATC = function ($id){
		var $modal = $( '#tz-popup-add-to-cart-modal' ),
			$product = $modal.find( '.product-modal-content' );

		if (!$(document.body).hasClass('teckzone-atc-notice-popup')) {
        	return;
        } 

		$modal.addClass( 'loading' ).removeClass( 'loaded' );
		teckzone.openModal( $modal );

		teckzone.getDataAJAXPopupATC($modal, $product, $id);

		$modal.on( 'click', '.close-modal, .tz-modal-overlay', function ( e ) {
			e.preventDefault();
			teckzone.closeModal( $modal );
		} )
	};

	teckzone.getDataAJAXPopupATC = function ($modal, $product, $id){

		$.ajax({
			url: teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'teckzone_product_popup_add_to_cart' ),
			type: 'POST',
			data    : {
				nonce     : teckzoneData.nonce,
				product_id: $id
			},
		    success: function(response){
		    	if ( ! response ) {
					return;
				}

				$product.show().html( response.data );
				teckzone.productsCarousel();

				$modal.removeClass( 'loading' ).addClass( 'loaded' );
		    }
		})
	};

	teckzone.fbtPopupATC = function ($title, $id){
		var $modal = $( '#tz-popup-add-to-cart-modal' ),
			$product = $modal.find( '.product-modal-content' );

		if (!$(document.body).hasClass('teckzone-atc-notice-popup')) {
        	return;
        }        

		$modal.addClass( 'loading' ).removeClass( 'loaded' );
		teckzone.openModal( $modal );

		$.ajax({
			url: teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'teckzone_product_popup_add_to_cart' ),
			type: 'POST',
			data    : {
				nonce     : teckzoneData.nonce,
				product_id: $id
			},
		    success: function(response){
		    	if ( ! response ) {
					return;
				}

				$product.show().html( response.data ).find('.tz-product-popup-atc__notice .title').replaceWith($title);
				teckzone.productsCarousel();


				$modal.removeClass( 'loading' ).addClass( 'loaded' );
		    }
		})

		$modal.on( 'click', '.close-modal, .tz-modal-overlay', function ( e ) {
			e.preventDefault();
			teckzone.closeModal( $modal );
		} )
	};

	teckzone.buyNow = function () {
		teckzone.$body.find( 'form.cart' ).on( 'click', '.buy_now_button', function ( e ) {
			e.preventDefault();
			var $form = $( this ).closest( 'form.cart' ),
				is_disabled = $( this ).is( ':disabled' );

			if ( is_disabled ) {
				jQuery( 'html, body' ).animate( {
						scrollTop: $( this ).offset().top - 200
					}, 900
				);
			} else {
				$form.append( '<input type="hidden" value="true" name="buy_now" />' );
				$form.find( '.single_add_to_cart_button' ).addClass( 'has-buy-now' );
				$form.find( '.single_add_to_cart_button' ).trigger( 'click' );
			}
		} );
	};

	/**
	 * Append WC Tab magic line
	 */
	teckzone.hoverProductTabs = function () {
		var $el, leftPos, newWidth, $origWidth, childWidth,
			$mainNav = $( '.single-product .woocommerce-tabs' ).find( 'ul.wc-tabs' );

		if ( $mainNav.length < 1 ) {
			return;
		}

		$mainNav.append( '<li id="tz-wc-tab__magic-line" class="tz-wc-tab__magic-line"></li>' );
		var $magicLine = $( '#tz-wc-tab__magic-line' );

        $( document ).ready(function() {
			childWidth = $mainNav.children( 'li.active' ).outerWidth();
			$magicLine
				.width( childWidth )
				.css( 'left', $mainNav.children( 'li.active' ).position().left )
				.data( 'origLeft', $magicLine.position().left )
				.data( 'origWidth', $magicLine.width() );

			$origWidth = $magicLine.data( 'origWidth' );

			$mainNav.children( 'li' ).on( 'mouseenter', function () {
				$el = $( this );
				newWidth = $el.outerWidth();
				leftPos = $el.position().left;
				$magicLine.stop().animate( {
					left : leftPos,
					width: newWidth
				} );

			} );

			$mainNav.children( 'li' ).on( 'mouseleave', function () {
				$magicLine.stop().animate( {
					left : $magicLine.data( 'origLeft' ),
					width: $origWidth
				} );

			} );

			$mainNav.on( 'click', 'li', function () {
				$el = $( this );
				$origWidth = newWidth = $el.outerWidth();
				leftPos = $el.position().left;
				$magicLine.stop().animate( {
					left : leftPos,
					width: newWidth
				} );
				$magicLine
					.data( 'origLeft', leftPos )
					.data( 'origWidth', newWidth );
			} );
		});
	};

	// Products Carousel
	teckzone.productsCarousel = function () {
		var $products = $( '.teckzone-wc-products-carousel' );

		$products.each( function () {
			var $el = $( this ),
				$slider = $el.find( 'ul.products' ),
				dataSettings = $el.data( 'settings' );

			var options = {
				arrows   : true,
				dots     : false,
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			var data = JSON.stringify( dataSettings );

			$slider.attr( 'data-slick', data );
			$slider.not('.slick-initialized').slick( options );
		} );
	};

	teckzone.fbtProduct = function () {
		if (!teckzone.$body.hasClass('single-product')) {
            return;
        }

		var $fbtProducts = $( '#tz-product-fbt' );

		if ( $fbtProducts.length <= 0 ) {
			return;
		}

		var $priceAt = $fbtProducts.find( '.tz-total-price .woocommerce-Price-amount' ),
			$button = $fbtProducts.find( '.tz_add_to_cart_button' ),
			totalPrice = parseFloat( $fbtProducts.find( '#tz-data_price' ).data( 'price' ) ),
			currency = teckzoneData.currency_param.currency_symbol,
			thousand = teckzoneData.currency_param.thousand_sep,
			decimal = teckzoneData.currency_param.decimal_sep,
			price_decimals = teckzoneData.currency_param.price_decimals,
			currency_pos = teckzoneData.currency_param.currency_pos;

		$fbtProducts.find( '.products-list' ).on( 'click', 'a', function ( e ) {
			e.preventDefault();
			var id = $( this ).data( 'id' );
			$( this ).closest( 'li' ).toggleClass( 'uncheck' );
			var currentPrice = parseFloat( $( this ).closest( 'li' ).find( '.s-price' ).data( 'price' ) );
			if ( $( this ).closest( 'li' ).hasClass( 'uncheck' ) ) {
				$fbtProducts.find( '#fbt-product-' + id ).addClass( 'un-active' );
				totalPrice -= currentPrice;

			} else {
				$fbtProducts.find( '#fbt-product-' + id ).removeClass( 'un-active' );
				totalPrice += currentPrice;
			}

			var $product_ids = '0';
			$fbtProducts.find( '.products-list li' ).each( function () {
				if ( !$( this ).hasClass( 'uncheck' ) ) {
					$product_ids += ',' + $( this ).find( 'a' ).data( 'id' );
				}
			} );

			$button.attr( 'value', $product_ids );

			$priceAt.html( formatNumber( totalPrice ) );
		} );


		function formatNumber(number) {
            var n = number;
            if (parseInt(price_decimals) > 0) {
                number = number.toFixed(price_decimals) + '';
                var x = number.split('.');
                var x1 = x[0],
                    x2 = x.length > 1 ? decimal + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + thousand + '$2');
                }

                n = x1 + x2
            }


            switch (currency_pos) {
                case 'left' :
                    return currency + n;
                    break;
                case 'right' :
                    return n + currency;
                    break;
                case 'left_space' :
                    return currency + ' ' + n;
                    break;
                case 'right_space' :
                    return n + ' ' + currency;
                    break;
            }
        }
	};

	// Add to cart ajax
	teckzone.fbtAddToCartAjax = function () {
		var $fbtProducts = $( '#tz-product-fbt' );

		if ( $fbtProducts.length <= 0 ) {
			return;
		}

		$fbtProducts.on( 'click', '.tz_add_to_cart_button.ajax_add_to_cart', function ( e ) {
			e.preventDefault();

			var $singleBtn = $( this );
			$singleBtn.addClass( 'loading' );

			var currentURL = window.location.href,
				pro_title = '',
				i = 0;

			$fbtProducts.find( '.products-list li' ).each( function () {
				if ( !$( this ).hasClass( 'uncheck' ) ) {
					if ( i > 0 ) {
						pro_title += ',';
					}
					pro_title += ' ' + $( this ).find( 'a' ).data( 'title' );
					i++;
				}
			} );

			var $id = $singleBtn.attr( 'value' ).split(",").pop();

			var data = {
				nonce: teckzoneData.nonce,
				product_ids: $singleBtn.attr( 'value' )
			},

			ajax_url = wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'teckzone_fbt_add_to_cart');

			$.post(
				ajax_url,
                data,
				function ( response ) {
					if ( typeof wc_add_to_cart_params !== 'undefined' ) {
						if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
							window.location = wc_add_to_cart_params.cart_url;
							return;
						}
					}

					$( document.body ).trigger( 'updated_wc_div' );
					$( document.body ).on( 'wc_fragments_refreshed', function () {
						$singleBtn.removeClass( 'loading' );
					} );

					teckzone.addedToCartNotice('', pro_title, false, 'success', true);
					if ($id && $id !== '0') {
						teckzone.fbtPopupATC(pro_title,$id);
					}
				}
			);

		} );

	};

    teckzone.catalogOpenCartMini = function () {

        $(document.body).on('added_to_cart', function (event, fragments, cart_hash, $thisbutton) {

        	if ($thisbutton.hasClass('tz_add_to_cart_button')) {
        		return;
        	}

            var product_title = $thisbutton.attr('data-title'),
                $message = '';

            teckzone.addedToCartNotice($message, product_title, false, 'success', false);
        });
	};
	
	// Catalog filter panel
	teckzone.showCatelogFilterPanel = function () {
		$('#catalog-filter-mobile').on('click', 'a', function (e) {
            e.preventDefault();
           
            teckzone.$body.addClass('display-catalog-filter');
            $('.catalog-sidebar').addClass('open');
		});
		
		teckzone.$body.on( 'click', 'a.close-catalog-filter', function(e){
			e.preventDefault;

			var $this = $(this);

			$this.closest( '.catalog-sidebar').removeClass( 'open' );
			$(document.body).removeClass( 'display-mobile-menu display-search-panel display-cart-panel display-catalog-filter' );
		} );

		$( '.teckzone-off-canvas-layer' ).on('click', function (e) {
			$(document.body).removeClass('display-mobile-menu display-search-panel display-cart-panel display-catalog-filter');
			$('.catalog-sidebar').removeClass('open');
		});

		$(document.body).on('teckzone_ajax_filter_request_success', function(){
			$(document.body).removeClass('display-mobile-menu display-search-panel display-cart-panel display-catalog-filter');
			$('.catalog-sidebar').removeClass('open');
		});
	}

	teckzone.catalogSorting = function () {
        if (teckzone.$window.width() > 767) {
			return;
		}

        var $sortingMobile = $('#tz-catalog-sorting-mobile');

        $('#tz-catalog-toolbar').on('click', '.woocommerce-ordering', function (e) {
            e.preventDefault();
            $sortingMobile.addClass('tz-active');
        });

        $sortingMobile.on('click', 'a', function (e) {
			e.preventDefault();

            $sortingMobile.removeClass('tz-active');
            $sortingMobile.find('a').removeClass('active');
            $(this).addClass('active');
        });

    };

	teckzone.catalogStickySidebar = function () {
        if (typeof teckzoneData.catalog_sticky_sidebar === 'undefined' || teckzoneData.catalog_sticky_sidebar.sticky_sidebar != '1') {
            return
		}

		if ( teckzone.$window.width() < 1025 ) {
			return;
		}

		if ($.fn.stick_in_parent ) {
            var $sidebar = $( '.catalog-sidebar .catalog-filter-mobile-wrapper' ),
				offSet = teckzoneData.catalog_sticky_sidebar.offset;

			$sidebar.stick_in_parent({
				parent: '.site-content',
				offset_top: offSet
			});
        }
	};
	
	/**
	 * Ajax load more products.
	 */
	teckzone.loadMoreProducts = function() {
		// Handles click load more button.
		$( document.body ).on( 'click', '.woocommerce-navigation.ajax-navigation a', function( event ) {
			event.preventDefault();

			var $el = $( this );

			if ( $el.hasClass( 'loading' ) ) {
				return;
			}

			$el.addClass( 'loading' );

			loadProducts( $( this ) );
		} );

		// Infinite scroll.
		if ( $( document.body ).hasClass( 'catalog-nav-infinite' ) ) {
			$( window ).on( 'scroll', function() {
				if ($( document.body ).find('.woocommerce-navigation.ajax-navigation').is(':in-viewport')) {
					$( document.body ).find('.woocommerce-navigation.ajax-navigation a').trigger('click');
				}
			} ).trigger('scroll');
		}

		/**
		 * Ajax load products.
		 *
		 * @param jQuery $el Button element.
		 */
		function loadProducts( $el ) {
			var $nav = $el.closest( '.woocommerce-navigation' ),
				url = $el.attr( 'href' );

			$.get( url, function( response ) {
				var $content = $( '#main', response ),
					$list = $( 'ul.products', $content ),
					$products = $list.children(),
					$newNav = $( '.woocommerce-navigation.ajax-navigation', $content );

				$products.each( function( index, product ) {
					$( product ).css( 'animation-delay', index * 100 + 'ms' );
				} );

				$products.appendTo( $nav.prev( 'ul.products' ) );
				$products.addClass( 'animated teckzoneFadeInUp' );

				if ( $newNav.length ) {
					$el.replaceWith( $( 'a', $newNav ) );
				} else {
					$nav.fadeOut( function() {
						$nav.remove();
					} );
				}

				$( document.body ).trigger( 'teckzone_products_loaded', [$products, true] );
				$( document.body ).trigger('yith_wcwl_init');
			} );
		}
	};

	// Add to wishlist  ajax
	teckzone.fbtAddToWishlistAjax = function () {
		var $fbtProducts = $( '#tz-product-fbt' ),
			i = 0;

		if ( !$fbtProducts.length ) {
			return;
		}

		var product_ids = getProductIds();

		if ( product_ids.length == 0 ) {
			$fbtProducts.find( '.btn-view-to-wishlist' ).addClass( 'showed' );
			$fbtProducts.find( '.btn-add-to-wishlist' ).addClass( 'hided' );
		}

		$fbtProducts.on( 'click', '.btn-add-to-wishlist', function ( e ) {
			e.preventDefault();

			var $singleBtn = $( this );
			product_ids = getProductIds();

			if ( product_ids.length == 0 ) {
				return;
			}

            var pro_title = '',
                index = 0;
            $fbtProducts.find('.products-list li').each(function () {
                if (!$(this).hasClass('uncheck')) {
                    if (index > 0) {
                        pro_title += ',';
                    }
                    pro_title += ' ' + $(this).find('a').data('title');
                    index++;
                }
            });

			$singleBtn.addClass( 'loading' );
			wishlistCallBack( product_ids[i] );

			teckzone.$body.on( 'added_to_wishlist', function () {
				if ( product_ids.length > i ) {
					wishlistCallBack( product_ids[i] );
				} else if ( product_ids.length == i ) {
					$fbtProducts.find( '.btn-view-to-wishlist' ).addClass( 'showed' );
					$fbtProducts.find( '.btn-add-to-wishlist' ).addClass( 'hided' );
                    teckzone.addedToWishlistNotice('', pro_title, false, 'success', true);
                    $singleBtn.removeClass('loading');
				}
			} );

		} );

        function getProductIds() {
            var product_ids = [];
            $fbtProducts.find('li.product').each(function () {
                if (!$(this).hasClass('un-active') && !$(this).hasClass('product-buttons') && !$(this).find('.yith-wcwl-add-to-wishlist').hasClass('exists')) {
                    if (product_ids.indexOf($(this).data('id')) == -1) {
                        product_ids.push($(this).data('id'));
                    }
                }

            });

            return product_ids;
        }

		function wishlistCallBack( id ) {
			var $product = $fbtProducts.find( '.add-to-wishlist-' + id );
			$product.find( '.yith-wcwl-add-button .add_to_wishlist' ).trigger( 'click' );
			i++;
			return i;
		}

	};

	// add wishlist
	teckzone.addWishlist = function () {
		$( 'ul.products li.product .yith-wcwl-add-button' ).on( 'click', 'a', function () {
			$( this ).addClass( 'loading' );
		} );

		teckzone.$body.on( 'added_to_wishlist', function (e, $el_wrap) {
            e.preventDefault();
			$( 'ul.products li.product .yith-wcwl-add-button a' ).removeClass( 'loading' );
            if ($el_wrap.hasClass('fbt-wishlist')) {
                return;
            }
            var content = $el_wrap.data('product-title');
            teckzone.addedToWishlistNotice('', content, false, 'success');
		} );
	};

	// update wishlist count
	teckzone.updateWishlistCounter = function () {
		var $counter = $( '.teckzone-wishlist .mini-item-counter, .header-element--wishlist .mini-item-counter' );

		if ( ! $counter.length ) {
			return;
		}

		$( document.body ).on( 'added_to_wishlist', function() {
			$counter.text( function() {
				return parseInt( this.innerText, 10 ) + 1;
			} );
		} ).on( 'removed_from_wishlist', function() {
			$counter.text( function() {
				var counter = parseInt( this.innerText, 10 ) - 1;
				return Math.max( 0, counter );
			} );
		} );
	}

    teckzone.addedToWishlistNotice = function ($message, $content, single, className, multiple) {
        if (typeof teckzoneData.added_to_wishlist_notice === 'undefined' || !$.fn.notify) {
            return;
        }

        if (multiple) {
            $content += ' ' + teckzoneData.added_to_wishlist_notice.added_to_wishlist_texts;
        } else {
            $content += ' ' + teckzoneData.added_to_wishlist_notice.added_to_wishlist_text;
        }

        $message += '<a href="' + teckzoneData.added_to_wishlist_notice.wishlist_view_link + '" class="btn-button">' + teckzoneData.added_to_wishlist_notice.wishlist_view_text + '</a>';

        if (single) {
            $message = '<div class="message-box">' + $message + '</div>';
        }

        $.notify.addStyle('teckzone', {
            html: '<div><i class="icon-checkmark-circle message-icon"></i><span data-notify-text/>' + $message + '<span class="close icon-cross2"></span> </div>'
        });
        $.notify($content, {
            autoHideDelay: teckzoneData.added_to_wishlist_notice.wishlist_notice_auto_hide,
            className: className,
            style: 'teckzone',
            showAnimation: 'fadeIn',
            hideAnimation: 'fadeOut'
        });
    };

	// Compare Button
	teckzone.addCompare = function () {

		teckzone.$body.on( 'click', 'a.compare:not(.added)', function ( e ) {
			e.preventDefault();

			var $el = $( this );
			$el.addClass( 'loading' );

			$el.closest( '.product-inner, .single-button-wrapper' ).find( '.compare:not(.loading)' ).trigger( 'click' );

			var compare = false;

			if ( $( this ).hasClass( 'added' ) ) {
				compare = true;
			}

			if ( compare === false ) {
				var compare_counter = teckzone.$header.find( '.mini-compare-counter' ).html();
				compare_counter = parseInt( compare_counter, 10 ) + 1;

				setTimeout( function () {
					teckzone.$header.find( '.mini-compare-counter' ).html( compare_counter );
					$el.removeClass( 'loading' );
				}, 2000 );
			}
		} );

		$( document ).find( '.compare-list' ).on( 'click', '.remove a', function ( e ) {
			e.preventDefault();
			var compare_counter = $( '.mini-compare-counter', window.parent.document ).html();
			compare_counter = parseInt( compare_counter, 10 ) - 1;
			if ( compare_counter < 0 ) {
				compare_counter = 0;
			}

			$( '.mini-compare-counter', window.parent.document ).html( compare_counter );

			$( 'a.compare' ).removeClass( 'loading' );
		} );

		$( '.yith-woocompare-widget' ).on( 'click', 'li a.remove', function ( e ) {
			e.preventDefault();
			var compare_counter = teckzone.$header.find( '.mini-compare-counter' ).html();
			compare_counter = parseInt( compare_counter, 10 ) - 1;
			if ( compare_counter < 0 ) {
				compare_counter = 0;
			}

			setTimeout( function () {
				teckzone.$header.find( '.mini-compare-counter' ).html( compare_counter );
			}, 2000 );

		} );

		$( '.yith-woocompare-widget' ).on( 'click', 'a.clear-all', function ( e ) {
			e.preventDefault();
			setTimeout( function () {
				teckzone.$header.find( '.mini-compare-counter' ).html( '0' );
			}, 2000 );
		} );
	};

	teckzone.catalogToolbarPagination = function () {
		$( '.tz-toolbar-pagination' ).on( 'submit', function () {
			var pageNumber = $( '#tz-catalog-page-number' ).val(),
				pageUrl = $( '#tz-catalog-url-page-' + pageNumber ).val();

			window.location.href = pageUrl;
			return false;
		} );
	};

	/**
	 * Change product quantity in cart page
	 */
	teckzone.cartQuantityAjax = function () {

		if ( !teckzone.$body.hasClass( 'woocommerce-cart' ) ) {
			return;
		}

		if ( teckzoneData.quantity_ajax != '1' ) {
			return;
		}

		teckzone.$body.on( 'click', '.quantity .increase, .quantity .decrease', function ( e ) {
			e.preventDefault();
			teckzone.$body.find( 'button[name="update_cart"]' ).trigger( 'click' );
		} );


		teckzone.$body.on( 'keyup', '.quantity .qty', function ( e ) {
			e.preventDefault();
			teckzone.$body.find( 'button[name="update_cart"]' ).trigger( 'click' );
		} );
	};

	/**
	 * Open modal
	 *
	 * @param $modal
	 */
	teckzone.openModal = function ( $modal ) {
		$modal.fadeIn();
		$modal.addClass( 'open' );
	};

	/**
	 * Close modal
	 */
	teckzone.closeModal = function ( $modal ) {
		$modal.fadeOut( function () {
			$( this ).removeClass( 'open' );
		} );
	};

	teckzone.stickyProductInfo = function () {
		if ( !teckzone.$body.hasClass( 'sticky-header-info' ) ) {
			return;
		}

		var $sticky_header = $( '#sticky-product-info-wrapper' ),
			$wc_tabs = $( 'div.product' ).find( '.woocommerce-tabs' ),
			sticky_height = $sticky_header.outerHeight( true ),
			$product_summary = $( 'div.product' ).find( '.tz-product-summary' ),
			$entry_cat = $( 'div.product' ).find( '.entry-summary' ).find( '.cart' ),
			topSection = 0;

		$sticky_header.find( '.sc-tabs' ).on( 'click', 'a', function ( e ) {
			e.preventDefault();
			var target = $( this ).data( 'tab' );
			$( this ).closest( '.sc-tabs' ).find( 'a' ).removeClass( 'active' );
			$( this ).addClass( 'active' );
			if ( teckzone.$body.hasClass( 'mobile-version' ) ) {
				var $tab = $( '#tab-' + target );
				if ( $tab.length > 0 ) {
					topSection = $tab.offset().top - sticky_height - 60;
					$( 'html, body' ).stop().animate( {
							scrollTop: topSection
						},
						400
					);
				}
			} else {
				if ( $wc_tabs.length > 0 ) {
					$wc_tabs.find( '.' + target + '_tab a' ).trigger( 'click' );
					topSection = $wc_tabs.offset().top - sticky_height - 60;
					$( 'html, body' ).stop().animate( {
							scrollTop: topSection
						},
						400
					);
				}
			}
		} );

		$wc_tabs.find( '.wc-tabs' ).on( 'click', 'a', function ( e ) {
			e.preventDefault();
			var id = $( this ).attr( 'href' );
			id = id ? id.replace( '#', '' ) : id;
			if ( id ) {
				$sticky_header.find( '.sc-tabs' ).find( 'a' ).removeClass( 'active' );
				$sticky_header.find( '.sc-tabs .' + id ).addClass( 'active' );
			}
		} );

		$sticky_header.find( '.sc-product-cart' ).on( 'click', '.button', function ( e ) {
			e.preventDefault();
			if ( $entry_cat.length > 0 ) {
				var topSection = $entry_cat.offset().top - sticky_height - 50;
				$( 'html, body' ).stop().animate( {
						scrollTop: topSection
					},
					400
				);
			}

		} );
		var offSet = 150;
		if ( teckzone.$body.hasClass( 'mobile-version' ) ) {
			teckzone.$window.on( 'scroll', function () {
				$sticky_header.find( '.sc-tabs li a' ).removeClass( 'active' );
				$sticky_header.find( '.sc-tabs li' ).each( function () {
					var $el = $( this ).find( 'a' );
					var currentTab = $el.attr( 'href' );
					if ( $( currentTab ).is( ':in-viewport(' + offSet + ')' ) ) {
						$el.addClass( 'active' );
					}
				} );

			} );
		}

		if ( $product_summary.length < 1 ) {
			return;
		}

		var top_sumary = 0;
		teckzone.$window.on( 'scroll', function () {
			if ( teckzone.$body.hasClass( 'mobile-version' ) ) {
				var hHeader = teckzone.$header.outerHeight( true );
				top_sumary = hHeader;
			} else {
				top_sumary = $product_summary.offset().top - 300;
			}
			if ( teckzone.$window.scrollTop() > top_sumary ) {
				$sticky_header.addClass( 'viewport' );
			} else {
				$sticky_header.removeClass( 'viewport' );
			}
		} );

		// Top Offset
		var $wpAdminBar = $( '#wpadminbar' ),
			topOffSet = $sticky_header.data( 'top_offset' );
        
        if ($wpAdminBar.length && 'top' && 'fixed' === $wpAdminBar.css('position')) {
            topOffSet += $wpAdminBar.height();
		}
		
		$sticky_header.css({
			'top' : topOffSet
		});
	};

	/**
	 * Toggle product quick view
	 */
	teckzone.productQuickView = function () {
		var $modal = $( '#tz-quick-view-modal' ),
			$product = $modal.find( '.product-modal-content' ),
			ajax_url = teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'product_quick_view' );

		if ( ajax_url == '' ) {
			return;
		}

		teckzone.$body.on( 'click', '.teckzone-product-quick-view', function ( e ) {
			e.preventDefault();

			var $a = $( this ),
				id = $a.data( 'id' );

			$product.hide().html( '' );
			$modal.addClass( 'loading' ).removeClass( 'loaded' );
			teckzone.openModal( $modal );

			$.ajax( {
				url     : ajax_url,
				dataType: 'json',
				method  : 'post',
				data    : {
					action    : 'teckzone_product_quick_view',
					nonce     : teckzoneData.nonce,
					product_id: id
				},
				success : function ( response ) {
					$product.show().append( response.data );
					$modal.removeClass( 'loading' ).addClass( 'loaded' );
					var $gallery = $product.find( '.woocommerce-product-gallery' ),
						$variation = $( '.variations_form' ),
						$buttons = $product.find( 'form.cart .actions-button' ),
						$buy_now = $buttons.find( '.buy_now_button' );

					$gallery.removeAttr( 'style' );
					$gallery.imagesLoaded( function () {
						$gallery.find( '.woocommerce-product-gallery__wrapper' ).not( '.slick-initialized' ).slick( {
							slidesToShow  : 1,
							slidesToScroll: 1,
							infinite      : false,
							dots          : true,
							prevArrow     : '<span class="icon-chevron-left slick-prev-arrow"></span>',
							nextArrow     : '<span class="icon-chevron-right slick-next-arrow"></span>'
						} );
					} );

					if ( $buy_now.length > 0 ) {
						$buttons.prepend( $buy_now );
					}

					$gallery.find( '.woocommerce-product-gallery__image' ).on( 'click', function ( e ) {
						e.preventDefault();
					} );

					if ( typeof wc_add_to_cart_variation_params !== 'undefined' ) {
						$variation.each( function () {
							$( this ).wc_variation_form();
						} );
					}

					if ( typeof $.fn.tawcvs_variation_swatches_form !== 'undefined' ) {
						$variation.tawcvs_variation_swatches_form();
					}

					if ( typeof tawcvs !== 'undefined' ) {
						if ( tawcvs.tooltip === 'yes' ) {
							$variation.find( '.swatch' ).tooltip( {
								classes     : { 'ui-tooltip': 'teckzone-tooltip' },
								tooltipClass: 'teckzone-tooltip qv-tool-tip',
								position    : { my: 'center bottom', at: 'center top-13' },
								create      : function () {
									$( '.ui-helper-hidden-accessible' ).remove();
								}
							} );
						}
					}

					teckzone.buyNow();
					teckzone.addToCartAjax();

					$( document.body ).trigger( 'teckzone_product_quickview_loaded', [response, id, teckzone] );
				}
			} );
		} );

		$modal.on( 'click', '.close-modal, .tz-modal-overlay', function ( e ) {
			e.preventDefault();
			teckzone.closeModal( $modal );
		} )

	};

	// Toggle Tab content on mobile
    teckzone.wooTabToggle = function () {

        if (typeof teckzoneData.product_collapse_tab === 'undefined') {
            return;
		}

        var $tab = $('.tz-woo-tabs .tz-Tabs-panel');

        $tab.each(function () {
            var $this = $(this),
                id = $this.attr('id'),
                $target = $('#' + id).find('.tab-title');

            if (teckzoneData.product_collapse_tab.status == 'close') {
                $target.siblings('.tab-content-wrapper').addClass('closed');
            } else {
                $target.addClass('active');
            }

            $target.on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.tab-content-wrapper').slideToggle().toggleClass('opened');
                $(this).toggleClass('active');
            })
        });
    };

	/**
	 * Support lazy load events
	 */
	teckzone.lazyLoadImages = function() {
		var lazyLoadImagesEvent;

		$( document.body ).on( 'post-load teckzone_posts_loaded teckzone_ajax_filter_request_success teckzone_product_quickview_loaded teckzone_products_loaded', function() {
			try {
				lazyLoadImagesEvent = new Event( 'jetpack-lazy-images-load', {
					bubbles: true,
					cancelable: true
				} );
			} catch ( e ) {
				lazyLoadImagesEvent = document.createEvent( 'Event' )
				lazyLoadImagesEvent.initEvent( 'jetpack-lazy-images-load', true, true );
			}

			document.body.dispatchEvent( lazyLoadImagesEvent );
		} );
	};

	/**
	 * Mobile
	 */
	teckzone.mobileNavBottomSpacing = function() {
		if ( ! $( document.body ).hasClass( 'mobile-version' ) ) {
			return;
		}

		if ( ! $( document.body ).hasClass( 'mobile-navigation-enable' ) ) {
			return;
		}

		if ( $( document.body ).hasClass( 'tz-add-to-cart-fixed' ) ) {
			return;
		}

		var $mobileNav = $( document.body ).find( '.tz-navigation-mobile' ),
			height = $mobileNav.height();

		$( document.body ).css({
			'margin-bottom' : height
		});
	}

	teckzone.filterSidebar = function () {
        var $mobileNavMenu = $('.tz-navigation-mobile');

        teckzone.$body.on('click', '#tz-vendor-infor-mobile', function (e) {
            e.preventDefault();
            var $this = $(this),
                height = $mobileNavMenu.height();
                
            teckzone.$body.find('.dokan-store-sidebar').toggleClass('tz-filter-active');
            teckzone.$body.find('#wcfmmp-store .sidebar').toggleClass('tz-filter-active');

            $(document.body).toggleClass('tz-overflow-y');
            teckzone.$body.find('.dokan-store-sidebar').css('height', $(window).height() - height);
            teckzone.$body.find('.dokan-widget-area').css('padding-bottom', height + 'px');

            teckzone.$body.find('#wcfmmp-store .sidebar').css('height', $(window).height() - height);
            teckzone.$body.find('.wcfm-widget-area').css('padding-bottom', height + 'px');
        });

        teckzone.$body.on('click', '.close-sidebar', function (e) {
            e.preventDefault();
            teckzone.$body.find('.dokan-store-sidebar').removeClass('tz-filter-active');
            teckzone.$body.find('#wcfmmp-store .sidebar').removeClass('tz-filter-active');
            $(document.body).removeClass('tz-overflow-y');
        });

    };

	/**
	 * Document ready
	 */
	$( function () {
		teckzone.init();
	} );

})( jQuery );