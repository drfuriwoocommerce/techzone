jQuery( document ).ready( function ( $ ) {
	"use strict";

	/*---------------------
	 Banners Carousel
	 ----------------------*/
	var file_frame,
		$banner_ids = $( '#tz_cat_banners_id' ),
		$banner_link = $( '#tz_cat_banners_link' ),
		$cat_banner = $( '#tz_cat_banners' ),
		$cat_images = $cat_banner.find( '.tz-cat-images' );

	$cat_banner.on( 'click', '.upload_images_button', function ( event ) {
		var $el = $( this );

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			file_frame.open();
			return;
		}

		// Create the media frame.
		file_frame = wp.media.frames.downloadable_file = wp.media( {
			multiple: true
		} );

		// When an image is selected, run a callback.
		file_frame.on( 'select', function () {
			var selection = file_frame.state().get( 'selection' ),
				attachment_ids = $banner_ids.val();

			selection.map( function ( attachment ) {
				attachment = attachment.toJSON();

				if ( attachment.id ) {
					attachment_ids = attachment_ids ? attachment_ids + ',' + attachment.id : attachment.id;
					var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;

					$cat_images.append( '<li class="image" data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" width="100px" height="100px" /><ul class="actions"><li><a href="#" class="delete" title="' + $el.data( 'delete' ) + '">' + $el.data( 'text' ) + '</a></li></ul></li>' );
				}

			} );
			$banner_ids.val( attachment_ids );
		} );


		// Finally, open the modal.
		file_frame.open();
	} );

	// Image ordering.
	$cat_images.sortable( {
		items               : 'li.image',
		cursor              : 'move',
		scrollSensitivity   : 40,
		forcePlaceholderSize: true,
		forceHelperSize     : false,
		helper              : 'clone',
		opacity             : 0.65,
		placeholder         : 'wc-metabox-sortable-placeholder',
		start               : function ( event, ui ) {
			ui.item.css( 'background-color', '#f6f6f6' );
		},
		stop                : function ( event, ui ) {
			ui.item.removeAttr( 'style' );
		},
		update              : function () {
			var attachment_ids = '';

			$cat_images.find( 'li.image' ).css( 'cursor', 'default' ).each( function () {
				var attachment_id = $( this ).attr( 'data-attachment_id' );
				attachment_ids = attachment_ids + attachment_id + ',';
			} );

			$banner_ids.val( attachment_ids );
		}
	} );

	// Remove images.
	$cat_banner.on( 'click', 'a.delete', function () {
		$( this ).closest( 'li.image' ).remove();

		var attachment_ids = '';

		$cat_images.find( 'li.image' ).css( 'cursor', 'default' ).each( function () {
			var attachment_id = $( this ).attr( 'data-attachment_id' );
			attachment_ids = attachment_ids + attachment_id + ',';
		} );

		$banner_ids.val( attachment_ids );

		return false;
	} );

	/*-----------------
	 Banners Grid
	 ------------------*/
	var file_frame_2,
		$banner_2_ids = $( '#tz_cat_banners_2_id' ),
		$banner_2_link = $( '#tz_cat_banners_2_link' ),
		$cat_banner_2 = $( '#tz_cat_banners_2' ),
		$cat_images_2 = $cat_banner_2.find( '.tz-cat-images' );

	$cat_banner_2.on( 'click', '.upload_images_button', function ( event ) {
		var $el = $( this );

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( file_frame_2 ) {
			file_frame_2.open();
			return;
		}

		// Create the media frame.
		file_frame_2 = wp.media.frames.downloadable_file = wp.media( {
			multiple: true
		} );

		// When an image is selected, run a callback.
		file_frame_2.on( 'select', function () {
			var selection = file_frame_2.state().get( 'selection' ),
				attachment_ids = $banner_2_ids.val();

			selection.map( function ( attachment ) {
				attachment = attachment.toJSON();

				if ( attachment.id ) {
					attachment_ids = attachment_ids ? attachment_ids + ',' + attachment.id : attachment.id;
					var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;

					$cat_images_2.append( '<li class="image" data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" width="100px" height="100px" /><ul class="actions"><li><a href="#" class="delete" title="' + $el.data( 'delete' ) + '">' + $el.data( 'text' ) + '</a></li></ul></li>' );
				}

			} );
			$banner_2_ids.val( attachment_ids );
		} );


		// Finally, open the modal.
		file_frame_2.open();
	} );

	// Image ordering.
	$cat_images_2.sortable( {
		items               : 'li.image',
		cursor              : 'move',
		scrollSensitivity   : 40,
		forcePlaceholderSize: true,
		forceHelperSize     : false,
		helper              : 'clone',
		opacity             : 0.65,
		placeholder         : 'wc-metabox-sortable-placeholder',
		start               : function ( event, ui ) {
			ui.item.css( 'background-color', '#f6f6f6' );
		},
		stop                : function ( event, ui ) {
			ui.item.removeAttr( 'style' );
		},
		update              : function () {
			var attachment_ids = '';

			$cat_images_2.find( 'li.image' ).css( 'cursor', 'default' ).each( function () {
				var attachment_id = $( this ).attr( 'data-attachment_id' );
				attachment_ids = attachment_ids + attachment_id + ',';
			} );

			$banner_2_ids.val( attachment_ids );
		}
	} );

	// Remove images.
	$cat_banner_2.on( 'click', 'a.delete', function () {
		$( this ).closest( 'li.image' ).remove();

		var attachment_ids = '';

		$cat_images_2.find( 'li.image' ).css( 'cursor', 'default' ).each( function () {
			var attachment_id = $( this ).attr( 'data-attachment_id' );
			attachment_ids = attachment_ids + attachment_id + ',';
		} );

		$banner_2_ids.val( attachment_ids );

		return false;
	} );

	/*---------------------
	 Ajax Complete
	 ----------------------*/

	$( document ).ajaxComplete( function ( event, request, options ) {
		if ( request && 4 === request.readyState && 200 === request.status
			&& options.data && 0 <= options.data.indexOf( 'action=add-tag' ) ) {

			var res = wpAjax.parseAjaxResponse( request.responseXML, 'ajax-response' );
			if ( !res || res.errors ) {
				return;
			}
			// Clear Thumbnail fields on submit
			$cat_banner.find( 'li.image' ).remove();
			$banner_ids.val( '' );
			$banner_link.val( '' );
			$cat_banner_2.find( 'li.image' ).remove();
			$banner_2_ids.val( '' );
			$banner_2_link.val( '' );
			return;
		}
	} );

	var $banners_group = $( '.tz-cat-banners-group' ),
		$banners_2_group = $( '.tz-cat-banners-2-group' );

	$( '#parent' ).on( 'change', function () {
		var parent_id = $( this ).val();
		if ( parent_id != '-1' ) {

			$banners_group.hide();
			$banners_2_group.hide();

		} else {
			$banners_group.show();
			$banners_2_group.show();
		}
	} ).trigger( 'change' );
} );
