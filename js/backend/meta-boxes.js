jQuery(document).ready(function ($) {
    "use strict";

    // Show/hide settings for post format when choose post format
    var $format = $('#post-formats-select').find('input.post-format'),
        $formatBox = $('#post-format-settings');

    $format.on('change', function () {
        var type = $(this).filter(':checked').val();
        postFormatSettings(type);
    });
    $format.filter(':checked').trigger('change');

    $(document.body).on('change', '.editor-post-format .components-select-control__input', function () {
        var type = $(this).val();
        postFormatSettings(type);
    });

    $(window).load(function () {
        var $el = $(document.body).find('.editor-post-format .components-select-control__input'),
            type = $el.val();
        postFormatSettings(type);
    });

    function postFormatSettings(type) {
        $formatBox.hide();
        if ($formatBox.find('.rwmb-field').hasClass(type)) {
            $formatBox.show();
        }

        $formatBox.find('.rwmb-field').slideUp();
        $formatBox.find('.' + type).slideDown();
    }

    // Show/hide settings for custom page header spacing
    $( '#page_header_custom_spacing' ).on( 'change', function () {
        if ( $( this ).val() == 'custom' ) {
            $( '.rwmb-field.page-header-padding-top, .rwmb-field.page-header-padding-bottom' ).slideDown();
        }
        else {
            $( '.rwmb-field.page-header-padding-top, .rwmb-field.page-header-padding-bottom' ).slideUp();
        }
    } ).trigger( 'change' );

    // Show/hide settings for custom content spacing
    $( '#page_content_top_spacing' ).on( 'change', function () {
        if ( $( this ).val() == 'custom' ) {
            $( '.rwmb-field.page-content-top-padding' ).slideDown();
        }
        else {
            $( '.rwmb-field.page-content-top-padding' ).slideUp();
        }
    } ).trigger( 'change' );

    $( '#page_content_bottom_spacing' ).on( 'change', function () {
        if ( $( this ).val() == 'custom' ) {
            $( '.rwmb-field.page-content-bottom-padding' ).slideDown();
        }
        else {
            $( '.rwmb-field.page-content-bottom-padding' ).slideUp();
        }
    } ).trigger( 'change' );

    // Show/hide settings for template settings
    $( '#page_template' ).on( 'change', function () {

        pageHeaderSettings($(this));

    } ).trigger( 'change' );

    $(document.body).on('change', '.editor-page-attributes__template .components-select-control__input', function () {
        pageHeaderSettings($(this));
    });

    $(window).load(function () {
        var $el = $(document.body).find('.editor-page-attributes__template .components-select-control__input');
        pageHeaderSettings($el);
    });

    function pageHeaderSettings($el) {
        if (
            $el.val() == 'template-homepage.php' ||
            $el.val() == 'template-homepage-full-width.php' ||
            $el.val() == 'template-homepage-wide.php'
        ) {
            $( '#page-header-settings, #page-content-top-spacing-settings, #page-content-container-width-settings' ).hide();
        } else {
            $( '#page-header-settings, #page-content-top-spacing-settings, #page-content-container-width-settings' ).show();
        }
    }
});
