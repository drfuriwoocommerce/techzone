<?php
/**
 * Functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Teckzone
 */

if ( ! function_exists( 'teckzone_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function teckzone_setup() {
		// Make theme available for translation.
		load_theme_textdomain( 'teckzone', get_template_directory() . '/lang' );

		// Theme supports
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'post-formats', array( 'image', 'gallery', 'video', 'audio', 'link', 'quote' ) );
		add_theme_support(
			'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);
		add_theme_support( 'customize-selective-refresh-widgets' );

		add_editor_style( 'css/editor-style.css' );

		// Load regular editor styles into the new block-based editor.
		add_theme_support( 'editor-styles' );

		// Load default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );

		add_theme_support( 'align-wide' );

		add_theme_support( 'align-full' );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );

		add_image_size( 'teckzone-blog-default', 1170, 658, true );
		add_image_size( 'teckzone-blog-grid', 570, 425, true );
		add_image_size( 'teckzone-blog-listing', 1170, 684, true );
		add_image_size( 'teckzone-blog-small-thumb', 700, 700, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary-menu'     => esc_html__( 'Primary Menu', 'teckzone' ),
			)
		);

		global $teckzone_mobile;
		$teckzone_mobile = new Teckzone_Mobile;
	}
endif;
add_action( 'after_setup_theme', 'teckzone_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function teckzone_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'teckzone_content_width', 640 );
}

add_action( 'after_setup_theme', 'teckzone_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function teckzone_widgets_init() {
	// Register primary sidebar
	$sidebars = array(
		'blog-sidebar'           => esc_html__( 'Blog Sidebar', 'teckzone' ),
		'post-sidebar'           => esc_html__( 'Single Post Sidebar', 'teckzone' ),
		'page-sidebar'           => esc_html__( 'Page Sidebar', 'teckzone' ),
		'catalog-sidebar'        => esc_html__( 'Catalog Sidebar', 'teckzone' ),
		'catalog-mobile-sidebar' => esc_html__( 'Catalog Mobile Sidebar', 'teckzone' ),
		'catalog-group-widget'   => esc_html__( 'Catalog Group Widget', 'teckzone' ),
		'product-sidebar'        => esc_html__( 'Single Product Sidebar', 'teckzone' ),
	);

	// Register sidebars
	foreach ( $sidebars as $id => $name ) {
		register_sidebar(
			array(
				'name'          => $name,
				'id'            => $id,
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			)
		);
	}
}

add_action( 'widgets_init', 'teckzone_widgets_init' );


/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce/woocommerce.php';
}

// Vendor
require get_template_directory() . '/inc/vendors/vendors.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/mobile/theme-options.php';
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Custom functions for the theme.
 */
require get_template_directory() . '/inc/functions/header.php';
require get_template_directory() . '/inc/functions/layout.php';
require get_template_directory() . '/inc/functions/entry.php';
require get_template_directory() . '/inc/functions/comments.php';
require get_template_directory() . '/inc/functions/breadcrumbs.php';
require get_template_directory() . '/inc/functions/nav.php';
require get_template_directory() . '/inc/functions/page-header.php';
require get_template_directory() . '/inc/functions/shop.php';
require get_template_directory() . '/inc/functions/style.php';

/**
 * Custom functions for the theme by hooking
 */
require get_template_directory() . '/inc/frontend/header.php';
require get_template_directory() . '/inc/frontend/entry.php';
require get_template_directory() . '/inc/frontend/layout.php';
require get_template_directory() . '/inc/frontend/comments.php';
require get_template_directory() . '/inc/frontend/post.php';
require get_template_directory() . '/inc/frontend/maintenance.php';
require get_template_directory() . '/inc/frontend/footer.php';
require get_template_directory() . '/inc/mega-menu/class-mega-menu-walker.php';
require get_template_directory() . '/inc/mega-menu/class-mobile-menu-walker.php';

// Mobile
require get_template_directory() . '/inc/libs/mobile_detect.php';
require get_template_directory() . '/inc/mobile/layout.php';

if ( is_admin() ) {
	require get_template_directory() . '/inc/libs/class-tgm-plugin-activation.php';
	require get_template_directory() . '/inc/backend/plugins.php';
	require get_template_directory() . '/inc/backend/meta-boxes.php';
	require get_template_directory() . '/inc/backend/product-cat.php';
	require get_template_directory() . '/inc/backend/editor.php';
	require get_template_directory() . '/inc/mega-menu/class-mega-menu.php';
} 