<?php
/**
 * The sidebar containing the main widget area
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Teckzone
 */

if ( teckzone_get_layout() == 'full-content' ) {
	return;
}

$classes = [
	'widget-area primary-sidebar col-flex-md-3 col-flex-sm-12 col-flex-xs-12'
];

$sidebar = 'blog-sidebar';
if ( is_page() ) {
	$sidebar = 'page-sidebar';
} elseif ( is_singular( 'post' ) ) {
	$sidebar = 'post-sidebar';
	if ( ! is_active_sidebar( $sidebar ) ) {
		$sidebar = 'blog-sidebar';
	}
} elseif ( is_singular( 'product' ) ) {
	$sidebar = 'product-sidebar';
} elseif ( teckzone_is_catalog() ) {
	$sidebar = 'catalog-sidebar';

	if ( teckzone_is_mobile() ) {
		$classes[] = 'catalog-sidebar';
		$sidebar = 'catalog-mobile-sidebar';
	}
}

$classes[] = $sidebar;

?>

<aside id="primary-sidebar" class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
	<?php if ( teckzone_is_catalog() ) :?>
		<div class="catalog-filter-mobile-wrapper">
			<div class="catalog-filter-mobile-header">
				<span class="header-text">
					<?php echo apply_filters( 'teckzone_catalog_filter_header_text', esc_html__( 'Filter Products', 'teckzone' ) ) ?>
				</span>
				<a href="#" class="close-catalog-filter"><i class="icon-arrow-right"></i></a>
			</div>
			<div class="catalog-filter-mobile-content">
	<?php endif ?>
	
	<?php
	if ( is_active_sidebar( $sidebar ) ) {
		dynamic_sidebar( $sidebar );
	}
	?>

	<?php if ( teckzone_is_catalog() ) :?>
			</div><!-- .catalog-filter-mobile-content -->
		</div><!-- .catalog-filter-mobile-wrapper -->
	<?php endif; ?>
	
</aside><!-- #secondary -->
