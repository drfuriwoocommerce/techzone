<?php
/**
 * Template Name: HomePage Full Width
 *
 * The template file for displaying home page.
 *
 * @package Teckzone
 */

get_header(); ?>
<div class="teckzone-full-width">
<?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		the_content();
	endwhile;

endif;
?>
</div>
<?php get_footer(); ?>
