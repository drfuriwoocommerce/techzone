<?php
/**
 * The template for displaying product after click add to cart
 *
 */

defined( 'ABSPATH' ) || exit;

global $product;

$classes = ['tz-product-popup-atc'];

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( $classes, $product ); ?>>
	<div class="tz-product-popup-atc__inner clearfix">

		<?php
		/**
		 * Hook: teckzone_before_product_popup_atc_notice.
		 *
		 * @hooked get_box_notice - 5
		 *
		 */
		do_action( 'teckzone_before_product_popup_atc_notice' );
		?>
		<div class="tz-product-popup-atc__recommendation">
			<?php
			/**
			 * Hook: teckzone_before_product_popup_atc_recommendation.
			 *
		  	 * @hooked woocommerce_upsell_display - 5
			 */
			do_action( 'teckzone_before_product_popup_atc_recommendation' );
			?>
		</div>
	</div>
</div>