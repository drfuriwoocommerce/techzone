<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.0.0
 */

global $product;

do_action( 'teckzone_before_single_product_deal_2' );

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$classes = [];

if ( $product->get_type() ) {
	$classes[] = 'product-type-' . $product->get_type();
}

if ( intval( teckzone_get_option( 'product_buy_now' ) ) && $product->get_type() != 'external' ) {
	$classes[] = 'enable-buy-now';
}

?>
<li class="product-deal">
	<div class="product <?php echo esc_attr( implode( ' ', $classes ) ) ?>">

		<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'teckzone_before_single_product_deal_2_summary' );
		?>

		<div class="summary entry-summary">
			<div class="entry-summary-content">
				<?php
				/**
				 * woocommerce_single_product_summary hook.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 15
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				do_action( 'teckzone_single_product_deal_2_content' );
				?>
			</div>
			<div class="entry-summary-sidebar">
				<?php
				/**
				 * woocommerce_single_product_summary hook.
				 *
				 * @hooked single_product_deals_countdown - 10
				 * @hooked woocommerce_template_single_add_to_cart - 20
				 */
				do_action( 'teckzone_single_product_deal_2_sidebar' );
				?>
			</div>
		</div>
		<!-- .summary -->
	</div>
</li>
