<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

?>
<li <?php wc_product_class( '', $product ); ?>>
	<?php do_action( 'teckzone_before_product_thumbnail_2' ); ?>
	<div class="product-thumbnail">
		<?php
		/**
		 * @hooked Teckzone_WooCommerce_Template_Catalog::product_link_open -10
		 * @hooked woocommerce_template_loop_product_thumbnail -20
		 * @hooked Teckzone_WooCommerce_Template::product_ribbons -50
		 * @hooked Teckzone_WooCommerce_Template_Catalog::product_link_close -100
		 */
		do_action( 'teckzone_product_thumbnail_2' );
		?>
	</div>
	<?php
	/**
	 * @hooked Teckzone_WooCommerce_Template_Catalog::open_product_inner -5
	 */
	do_action( 'teckzone_after_product_thumbnail_2' );
	?>
	<div class="product-details">
		<?php
		/**
		 * @hooked Teckzone_WooCommerce_Template_Catalog::tz_product_category -5
		 * @hooked Teckzone_WooCommerce_Template_Catalog::template_loop_product_title -10
		 * @hooked Teckzone_Dokan::template_loop_sold_by -20 & Teckzone_WCFMVendors::template_loop_sold_by -20
		 * @hooked woocommerce_template_loop_rating -30
		 * @hooked woocommerce_template_loop_price -40
		 */
		do_action( 'teckzone_product_details_2' );
		?>
	</div>
	<?php do_action( 'teckzone_after_product_details_2' ); ?>
</li>
