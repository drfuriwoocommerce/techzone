<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$title   = teckzone_get_option( 'related_products_title' );
$columns = intval( teckzone_get_option( 'related_products_columns' ) );

$carousel_settings = array(
	'slidesToShow'   => $columns,
	'slidesToScroll' => $columns,
);

$responsive_settings = [];

$responsive_settings['1200'] = [
	'breakpoint' => 1200,
	'settings'   => array(
		'dots'   => true,
		'arrows' => false,
		'slidesToShow'   => 4,
		'slidesToScroll' => 4,
	)
];

$responsive_settings['992'] = [
	'breakpoint' => 992,
	'settings'   => array(
		'dots'   => true,
		'arrows' => false,
		'slidesToShow'   => 3,
		'slidesToScroll' => 3,
	)
];

$responsive_settings['768'] = [
	'breakpoint' => 768,
	'settings'   => array(
		'dots'   => true,
		'arrows' => false,
		'slidesToShow'   => 2,
		'slidesToScroll' => 2,
	)
];

if( teckzone_get_option( 'product_container_width' ) == 'full-width' || teckzone_get_option( 'product_container_width' ) == 'wide') { 
	$responsive_settings['1500'] = [
		'breakpoint' => 1500,
		'settings'   => array(
			'slidesToShow'   => $columns > 5 ? 5 : $columns,
			'slidesToScroll' => $columns > 5 ? 5 : $columns,
		)
	];
}

krsort($responsive_settings);

foreach ($responsive_settings as $responsive_setting) {
	$carousel_settings['responsive'][] = $responsive_setting;
}

$container = 'container';
if( teckzone_get_option( 'product_container_width' ) == 'full-width') {
	$container = 'teckzone-container';
} elseif( teckzone_get_option( 'product_container_width' ) == 'wide') {
	$container = 'teckzone-wide-container';
} else {
	$container = $container;
}

if ( $related_products ) : ?>

	<section class="teckzone-related-products teckzone-wc-products-carousel related products" data-settings="<?php echo esc_attr( wp_json_encode( $carousel_settings ) ) ?>">
		<div class="<?php echo esc_attr( $container ); ?>">
			<h2 class="section-title"><?php echo esc_html( $title ) ?></h2>

			<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

				<?php
				$post_object = get_post( $related_product->get_id() );

				setup_postdata( $GLOBALS['post'] =& $post_object );

				wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

			<?php woocommerce_product_loop_end(); ?>
		</div>
	</section>

<?php endif;

wp_reset_postdata();
