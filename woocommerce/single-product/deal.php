<?php
/**
 * Single Product Deal
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/deal.php.
 *
 * @author  Drfuri
 * @package WooCommerce Deals/Templates
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$now         = strtotime( current_time( 'Y-m-d H:i:s' ) );
$expire_date = strtotime( '00:00 +1 day' );
$expire      = $expire_date - $now;
$percentage  = $sold / $limit * 100;
?>

<div class="tawc-deal deal">
	<?php if ( $expire ) : ?>
		<div class="deal-expire-date">
			<div class="deal-expire-text"><?php echo wp_kses_post( $expire_text ) ?></div>
			<div class="deal-expire-countdown" data-expire="<?php echo esc_attr( $expire ) ?>"></div>
		</div>
	<?php endif; ?>

	<div class="deal-sold">
		<div class="deal-sold-text"><?php echo wp_kses_post( $sold_text ) ?></div>
		<div class="deal-progress">
			<div class="progress-bar">
				<div class="progress-value" style="width: <?php echo esc_attr( $percentage ) ?>%"></div>
			</div>
		</div>
		<div class="deal-text">
			<span class="sold">
				<span class="text"><?php esc_html_e( 'Sold: ', 'teckzone' ) ?></span>
				<span class="value"><?php echo intval($sold) ?>/<?php echo ! empty( $limit ) ? $limit : '' ?></span>
			</span>
		</div>
	</div>
</div>
