<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Teckzone
 */

get_header();
$image = get_template_directory_uri() . '/images/error.png';
?>

	<div id="primary" class="content-area <?php teckzone_content_columns(); ?>">
		<main id="main" class="site-main">

			<section class="error-404 not-found row-flex">
				<div class="not-found_image page-image col-flex-lg-4 col-flex-md-5 col-flex-xs-12 col-lg-offset-2">
					<img src="<?php echo esc_attr( $image ); ?>"
					     alt="<?php esc_attr_e( 'Oops! Page not found.', 'teckzone' ); ?>"/>
				</div>

				<div class="not-found_content col-flex-lg-6 col-flex-md-7 col-flex-xs-12">
					<h1 class="page-title"><?php esc_html_e( 'Oops! Page not found.', 'teckzone' ); ?></h1>

					<div class="page-content">
						<p>
							<?php
							echo esc_html__( 'We can\'t find the page you\'re looking for. You can either ', 'teckzone' ) .
							     '<a href="javascript:history.go(-1)">' . esc_html__( 'return to the previous page', 'teckzone' ) . '</a>, ' .
							     '<a href="' . esc_url( home_url( '/' ) ) . '">' . esc_html__( 'visit our home page','teckzone' ) . '</a>' .
							     esc_html__( ' or search for something else.', 'teckzone' );
							?>
						</p>

						<?php get_search_form(); ?>
					</div>
				</div>
			</section>
			<!-- .error-404 -->

		</main>
		<!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
